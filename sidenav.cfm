<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
    <div class="sidenav-header d-flex align-items-center justify-content-center">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="menu.cfm">
        <img src="/new-sistem/assets/img/logos/logo-erez.png" class="navbar-brand-img w-100" alt="main_logo">
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto h-auto" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <!--- Dashboard --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#dashboardExamples" class="nav-link active" aria-controls="dashboardExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 45 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>shop </title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1716.000000, -439.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g transform="translate(0.000000, 148.000000)">
                        <path class="color-background" d="M46.7199583,10.7414583 L40.8449583,0.949791667 C40.4909749,0.360605034 39.8540131,0 39.1666667,0 L7.83333333,0 C7.1459869,0 6.50902508,0.360605034 6.15504167,0.949791667 L0.280041667,10.7414583 C0.0969176761,11.0460037 -1.23209662e-05,11.3946378 -1.23209662e-05,11.75 C-0.00758042603,16.0663731 3.48367543,19.5725301 7.80004167,19.5833333 L7.81570833,19.5833333 C9.75003686,19.5882688 11.6168794,18.8726691 13.0522917,17.5760417 C16.0171492,20.2556967 20.5292675,20.2556967 23.494125,17.5760417 C26.4604562,20.2616016 30.9794188,20.2616016 33.94575,17.5760417 C36.2421905,19.6477597 39.5441143,20.1708521 42.3684437,18.9103691 C45.1927731,17.649886 47.0084685,14.8428276 47.0000295,11.75 C47.0000295,11.3946378 46.9030823,11.0460037 46.7199583,10.7414583 Z" opacity="0.598981585"></path>
                        <path class="color-background" d="M39.198,22.4912623 C37.3776246,22.4928106 35.5817531,22.0149171 33.951625,21.0951667 L33.92225,21.1107282 C31.1430221,22.6838032 27.9255001,22.9318916 24.9844167,21.7998837 C24.4750389,21.605469 23.9777983,21.3722567 23.4960833,21.1018359 L23.4745417,21.1129513 C20.6961809,22.6871153 17.4786145,22.9344611 14.5386667,21.7998837 C14.029926,21.6054643 13.533337,21.3722507 13.0522917,21.1018359 C11.4250962,22.0190609 9.63246555,22.4947009 7.81570833,22.4912623 C7.16510551,22.4842162 6.51607673,22.4173045 5.875,22.2911849 L5.875,44.7220845 C5.875,45.9498589 6.7517757,46.9451667 7.83333333,46.9451667 L19.5833333,46.9451667 L19.5833333,33.6066734 L27.4166667,33.6066734 L27.4166667,46.9451667 L39.1666667,46.9451667 C40.2482243,46.9451667 41.125,45.9498589 41.125,44.7220845 L41.125,22.2822926 C40.4887822,22.4116582 39.8442868,22.4815492 39.198,22.4912623 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
          <div class="collapse  show " id="dashboardExamples">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item active">
                <a class="nav-link active" href="../../pages/dashboards/default.html">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal"> Avisos </span>
                </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link active" href="../../pages/dashboards/default.html">
                  <span class="sidenav-mini-icon"> TPR </span>
                  <span class="sidenav-normal"> Tareas pendientes /</br> Recordatorios </span>
                </a>
              </li>              
            </ul>
          </div>
        </li>
        <li class="nav-item mt-3">
          <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">PAGES</h6>
        </li>
        
        <!---Las siguientes 3 secciones son temporales, se usaran de guia para verificar que este todo y ordenar el resto de la pagina, ademas de acceso rapido
        Una vez se verifique que esta todo y todo sea ordenado se eliminaran--->
        <!--- TEMPORAL NOMINA --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#tempNomina" class="nav-link " aria-controls="tempNomina" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">NOMINA</span>
          </a>
          <div class="collapse " id="tempNomina">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/subir_empleados.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Subir Empleados (paso 2) </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/dias_laborados_editb.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Dias Laborados (paso 3) </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/dias_laborados_aut.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Revision dias laborados (paso 4) </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/nomina_semana.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Revision Nomina (paso 5) </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/nomina_publicada.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Nomina Publicada (paso 6) </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Generar_autoriza.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Generar Autorizaciones (paso 7) </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/altasemanas.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Alta semanas </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/semana_activa.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Semana Actual </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Elimina_autoriza.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Rechazar Autorizaciones </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- TEMPORAL HERRAMIENTAS --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#tempHerramientas" class="nav-link " aria-controls="tempHerramientas" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">HERRAMIENTAS</span>
          </a>
          <div class="collapse " id="tempHerramientas">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/bitacoras.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Bitacoras </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/prestamos.cfm">
                  <span class="sidenav-mini-icon"> CI </span>
                  <span class="sidenav-normal"> Prestamos </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/vacaciones.cfm">
                  <span class="sidenav-mini-icon"> HC </span>
                  <span class="sidenav-normal"> Vacaciones </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Usuarios.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Usuarios Tienda </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/subir_deduciones.cfm">
                  <span class="sidenav-mini-icon"> CI </span>
                  <span class="sidenav-normal"> Subir cancelacion prestamos </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/panel_control.cfm">
                  <span class="sidenav-mini-icon"> HC </span>
                  <span class="sidenav-normal"> Panel de control - Asistencias </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/dias_vacaciones.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Dias restantes vacaciones </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/empleado_bus.cfm">
                  <span class="sidenav-mini-icon"> CI </span>
                  <span class="sidenav-normal"> Busqueda Personal </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- TEMPORAL REPORTES --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#tempReportes" class="nav-link " aria-controls="tempReportes" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">REPORTES</span>
          </a>
          <div class="collapse " id="tempReportes">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/nomina_publicada_anterior.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Ultimas Semanas Publicadas </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Prestamos_pend.cfm">
                  <span class="sidenav-mini-icon"> CI </span>
                  <span class="sidenav-normal"> Reporte Prestamos </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/rep_uniformes.cfm">
                  <span class="sidenav-mini-icon"> HC </span>
                  <span class="sidenav-normal"> Reporte Uniformes y Precios </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Infonavit.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Infonavit </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Rep_movimientos.cfm">
                  <span class="sidenav-mini-icon"> CI </span>
                  <span class="sidenav-normal"> Altas, Bajas, Cambios </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Comisiones.cfm">
                  <span class="sidenav-mini-icon"> HC </span>
                  <span class="sidenav-normal"> Tabla de Comisiones </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/rep_PagosEmpleado.cfm">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Historial Sueldos </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Rep_movimientos_hist.cfm">
                  <span class="sidenav-mini-icon"> CI </span>
                  <span class="sidenav-normal"> Historial de Rotacion </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="/new-sistem/coldfusion/Rep_Vacaciones.cfm">
                  <span class="sidenav-mini-icon"> HC </span>
                  <span class="sidenav-normal"> Historial de deducciones/persepciones </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- FIN DE LAS SECCIONES TEMPORALES --->


        <!--- Autorizar Pagos --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#autoriPExamples" class="nav-link " aria-controls="autoriPExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Autorizar Pagos</span>
          </a>
          <div class="collapse " id="autoriPExamples">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> CC </span>
                  <span class="sidenav-normal"> Cancelar cheques </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> CI </span>
                  <span class="sidenav-normal"> Cheques por imprimir </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> HC </span>
                  <span class="sidenav-normal"> Historial de cheques </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Bancos --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#banksExamples" class="nav-link " aria-controls="banksExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Bancos</span>
          </a>
          <div class="collapse " id="banksExamples">
            <ul class="nav ms-4 ps-3">
               <!--- Acreedor --->
               <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#acreedorBanksExample">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal"> Acreedor <b class="caret"></b></span>
                </a>
                <div class="collapse " id="acreedorBanksExample">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> A </span>
                        <span class="sidenav-normal"> Alta </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!--- Cuentas contables --->
              <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#countsExample">
                  <span class="sidenav-mini-icon"> C </span>
                  <span class="sidenav-normal"> Cuentas contables <b class="caret"></b></span>
                </a>
                <div class="collapse " id="countsExample">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/projects/general.html">
                        <span class="sidenav-mini-icon text-xs"> B </span>
                        <span class="sidenav-normal"> Bancos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/projects/timeline.html">
                        <span class="sidenav-mini-icon text-xs"> CC </span>
                        <span class="sidenav-normal"> Cuentas contables </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/projects/new-project.html">
                        <span class="sidenav-mini-icon text-xs"> S </span>
                        <span class="sidenav-normal"> Subcuenta </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!--- Cuentas internet --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> CI </span>
                  <span class="sidenav-normal"> Cuentas internet </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Cat�logos --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#catalogsExamples" class="nav-link " aria-controls="catalogsExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>document</title>
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g transform="translate(-1870.000000, -591.000000)" fill="#FFFFFF" fill-rule="nonzero">
                      <g transform="translate(1716.000000, 291.000000)">
                        <g transform="translate(154.000000, 300.000000)">
                          <path class="color-background" d="M40,40 L36.3636364,40 L36.3636364,3.63636364 L5.45454545,3.63636364 L5.45454545,0 L38.1818182,0 C39.1854545,0 40,0.814545455 40,1.81818182 L40,40 Z" opacity="0.603585379"></path>
                          <path class="color-background" d="M30.9090909,7.27272727 L1.81818182,7.27272727 C0.814545455,7.27272727 0,8.08727273 0,9.09090909 L0,41.8181818 C0,42.8218182 0.814545455,43.6363636 1.81818182,43.6363636 L30.9090909,43.6363636 C31.9127273,43.6363636 32.7272727,42.8218182 32.7272727,41.8181818 L32.7272727,9.09090909 C32.7272727,8.08727273 31.9127273,7.27272727 30.9090909,7.27272727 Z M18.1818182,34.5454545 L7.27272727,34.5454545 L7.27272727,30.9090909 L18.1818182,30.9090909 L18.1818182,34.5454545 Z M25.4545455,27.2727273 L7.27272727,27.2727273 L7.27272727,23.6363636 L25.4545455,23.6363636 L25.4545455,27.2727273 Z M25.4545455,20 L7.27272727,20 L7.27272727,16.3636364 L25.4545455,16.3636364 L25.4545455,20 Z"></path>
                        </g>
                      </g>
                    </g>
                  </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Cat�logos</span>
          </a>
          <div class="collapse " id="catalogsExamples">
            <ul class="nav ms-4 ps-3">
              <!--- Empleados --->
              <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#employeesExample">
                  <span class="sidenav-mini-icon"> E </span>
                  <span class="sidenav-normal"> Empleados <b class="caret"></b></span>
                </a>
                <div class="collapse " id="employeesExample">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/projects/general.html">
                        <span class="sidenav-mini-icon text-xs"> A </span>
                        <span class="sidenav-normal"> Altas </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/projects/new-project.html">
                        <span class="sidenav-mini-icon text-xs"> B </span>
                        <span class="sidenav-normal"> Bajas </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/projects/new-project.html">
                        <span class="sidenav-mini-icon text-xs"> C </span>
                        <span class="sidenav-normal"> Cambios </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/projects/timeline.html">
                        <span class="sidenav-mini-icon text-xs"> ER </span>
                        <span class="sidenav-normal"> Empleados repetidos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/projects/new-project.html">
                        <span class="sidenav-mini-icon text-xs"> E </span>
                        <span class="sidenav-normal"> Entrevistas </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!--- Proveedores --->
              <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#providersExample">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal"> Proveedores <b class="caret"></b></span>
                </a>
                <div class="collapse " id="providersExample">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/reports.html">
                        <span class="sidenav-mini-icon text-xs"> A </span>
                        <span class="sidenav-normal"> Alta </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/new-user.html">
                        <span class="sidenav-mini-icon text-xs"> A </span>
                        <span class="sidenav-normal"> Activos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/new-user.html">
                        <span class="sidenav-mini-icon text-xs"> CP </span>
                        <span class="sidenav-normal"> Cuentas de proveedor </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/new-user.html">
                        <span class="sidenav-mini-icon text-xs"> I </span>
                        <span class="sidenav-normal"> Inactivos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/new-user.html">
                        <span class="sidenav-mini-icon text-xs"> M </span>
                        <span class="sidenav-normal"> Mensaje proveedor </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/new-user.html">
                        <span class="sidenav-mini-icon text-xs"> PR </span>
                        <span class="sidenav-normal"> Por revisar </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/new-user.html">
                        <span class="sidenav-mini-icon text-xs"> T </span>
                        <span class="sidenav-normal"> Todos </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!--- Tienda --->
              <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#storeExample">
                  <span class="sidenav-mini-icon"> T </span>
                  <span class="sidenav-normal"> Tienda <b class="caret"></b></span>
                </a>
                <div class="collapse " id="storeExample">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/profile/overview.html">
                        <span class="sidenav-mini-icon text-xs"> A </span>
                        <span class="sidenav-normal"> Alta </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
        <!--- Cuentas por pagar --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#cppExamples" class="nav-link " aria-controls="cppExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Cuentas por pagar</span>
          </a>
          <div class="collapse " id="cppExamples">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/rtl-page.html">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal"> Acreedores </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/pricing-page.html">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal"> Proveedores </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/messages.html">
                  <span class="sidenav-mini-icon"> PR </span>
                  <span class="sidenav-normal"> Proveedor remisiones </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Herramientas --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#settingsExamples" class="nav-link " aria-controls="settingsExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>settings</title>
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g transform="translate(-2020.000000, -442.000000)" fill="#FFFFFF" fill-rule="nonzero">
                      <g transform="translate(1716.000000, 291.000000)">
                        <g transform="translate(304.000000, 151.000000)">
                          <polygon class="color-background" opacity="0.596981957" points="18.0883333 15.7316667 11.1783333 8.82166667 13.3333333 6.66666667 6.66666667 0 0 6.66666667 6.66666667 13.3333333 8.82166667 11.1783333 15.315 17.6716667"></polygon>
                          <path class="color-background" d="M31.5666667,23.2333333 C31.0516667,23.2933333 30.53,23.3333333 30,23.3333333 C29.4916667,23.3333333 28.9866667,23.3033333 28.48,23.245 L22.4116667,30.7433333 L29.9416667,38.2733333 C32.2433333,40.575 35.9733333,40.575 38.275,38.2733333 L38.275,38.2733333 C40.5766667,35.9716667 40.5766667,32.2416667 38.275,29.94 L31.5666667,23.2333333 Z" opacity="0.596981957"></path>
                          <path class="color-background" d="M33.785,11.285 L28.715,6.215 L34.0616667,0.868333333 C32.82,0.315 31.4483333,0 30,0 C24.4766667,0 20,4.47666667 20,10 C20,10.99 20.1483333,11.9433333 20.4166667,12.8466667 L2.435,27.3966667 C0.95,28.7083333 0.0633333333,30.595 0.00333333333,32.5733333 C-0.0583333333,34.5533333 0.71,36.4916667 2.11,37.89 C3.47,39.2516667 5.27833333,40 7.20166667,40 C9.26666667,40 11.2366667,39.1133333 12.6033333,37.565 L27.1533333,19.5833333 C28.0566667,19.8516667 29.01,20 30,20 C35.5233333,20 40,15.5233333 40,10 C40,8.55166667 39.685,7.18 39.1316667,5.93666667 L33.785,11.285 Z"></path>
                        </g>
                      </g>
                    </g>
                  </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Herramientas</span>
          </a>
          <div class="collapse " id="settingsExamples">
            <ul class="nav ms-4 ps-3">
              <!--- Alta semanas y semana actual --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> AS </span>
                  <span class="sidenav-normal"> Alta semanas /</br> semana actual </span>
                </a>
              </li>
              <!--- Bitacoras --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/sweet-alerts.html">
                  <span class="sidenav-mini-icon"> B </span>
                  <span class="sidenav-normal"> Bitacoras </span>
                </a>
              </li>
              <!--- Busqueda de personal --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/rtl-page.html">
                  <span class="sidenav-mini-icon"> BP </span>
                  <span class="sidenav-normal"> Busqueda Personal </span>
                </a>
              </li>
              <!--- Conceptos --->
              <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#conceptsExample">
                  <span class="sidenav-mini-icon"> C </span>
                  <span class="sidenav-normal"> Conceptos <b class="caret"></b></span>
                </a>
                <div class="collapse " id="conceptsExample">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/reports.html">
                        <span class="sidenav-mini-icon text-xs"> A </span>
                        <span class="sidenav-normal"> Alta </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/users/new-user.html">
                        <span class="sidenav-mini-icon text-xs"> LC </span>
                        <span class="sidenav-normal"> Lista conceptos </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!--- Usuarios --->
              <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#usersExample">
                  <span class="sidenav-mini-icon"> U </span>
                  <span class="sidenav-normal"> Usuarios <b class="caret"></b></span>
                </a>
                <div class="collapse " id="usersExample">
                  <ul class="nav nav-sm flex-column">
                    <!--- Agregar --->
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/profile/teams.html">
                        <span class="sidenav-mini-icon text-xs"> A </span>
                        <span class="sidenav-normal"> Agregar </span>
                      </a>
                    </li>
                    <!--- Usuarios Eliminados --->
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/profile/projects.html">
                        <span class="sidenav-mini-icon text-xs"> UE </span>
                        <span class="sidenav-normal"> Eliminados </span>
                      </a>
                    </li>
                     <!--- Lista usuarios--->
                     <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/profile/overview.html">
                        <span class="sidenav-mini-icon text-xs"> L </span>
                        <span class="sidenav-normal"> Lista Usuarios </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!--- Subir Archivos --->
              <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#filesExample">
                  <span class="sidenav-mini-icon"> SA </span>
                  <span class="sidenav-normal"> Subir archivos <b class="caret"></b></span>
                </a>
                <div class="collapse " id="filesExample">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/billing.html">
                        <span class="sidenav-mini-icon text-xs"> AC </span>
                        <span class="sidenav-normal"> Archivo de cortes </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> AR </span>
                        <span class="sidenav-normal"> Archivo de remisiones </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/invoice.html">
                        <span class="sidenav-mini-icon text-xs"> CP </span>
                        <span class="sidenav-normal"> Cancelaci�n de pr�stamos </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
        <!--- Historial --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#historiExamples" class="nav-link " aria-controls="historiExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1"> Historial </span>
          </a>
          <div class="collapse " id="historiExamples">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> HDP </span>
                  <span class="sidenav-normal"> Historial de deducciones /</br> percepciones </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> HR </span>
                  <span class="sidenav-normal"> Historial de rotaci�n </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> HS </span>
                  <span class="sidenav-normal"> Historial de sueldos </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Notas de cargo --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#notesCargExamples" class="nav-link " aria-controls="notesCargExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Notas de cargo </span>
          </a>
          <div class="collapse " id="notesCargExamples">
            <ul class="nav ms-4 ps-3">
              <!--- Alta --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal"> Alta </span>
                </a>
              </li>
              <!--- B�squeda de notas de cargo --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> BNC </span>
                  <span class="sidenav-normal"> B�squeda de notas</br> de cargo </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Notas de cr�dito --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#notesCreditExamples" class="nav-link " aria-controls="notesCreditExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Notas de cr�dito</span>
          </a>
          <div class="collapse " id="notesCreditExamples">
            <ul class="nav ms-4 ps-3">
              <!--- Alta --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal"> Alta </span>
                </a>
              </li>
              <!--- B�squeda de notas de cargo --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> BNC </span>
                  <span class="sidenav-normal">B�squeda de notas</br> de cr�dito</span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Pagos --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#payExamples" class="nav-link " aria-controls="payExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Pagos</span>
          </a>
          <div class="collapse " id="payExamples">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> C </span>
                  <span class="sidenav-normal"> Cheques </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> E </span>
                  <span class="sidenav-normal"> Efectivo </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> PA </span>
                  <span class="sidenav-normal"> Pagos a acreedor </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> PP </span>
                  <span class="sidenav-normal"> Pagos a proveedor </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Panel de control --->
        <li class="nav-item">
          <a class="nav-link" href="https://github.com/creativetimofficial/ct-soft-ui-dashboard-pro/blob/main/CHANGELOG.md" target="_blank">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center  me-2 d-flex align-items-center justify-content-center">
              <svg width="12px" height="12px" viewBox="0 0 43 36" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>credit-card</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-2169.000000, -745.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g transform="translate(453.000000, 454.000000)">
                        <path class="color-background" d="M43,10.7482083 L43,3.58333333 C43,1.60354167 41.3964583,0 39.4166667,0 L3.58333333,0 C1.60354167,0 0,1.60354167 0,3.58333333 L0,10.7482083 L43,10.7482083 Z" opacity="0.593633743"></path>
                        <path class="color-background" d="M0,16.125 L0,32.25 C0,34.2297917 1.60354167,35.8333333 3.58333333,35.8333333 L39.4166667,35.8333333 C41.3964583,35.8333333 43,34.2297917 43,32.25 L43,16.125 L0,16.125 Z M19.7083333,26.875 L7.16666667,26.875 L7.16666667,23.2916667 L19.7083333,23.2916667 L19.7083333,26.875 Z M35.8333333,26.875 L28.6666667,26.875 L28.6666667,23.2916667 L35.8333333,23.2916667 L35.8333333,26.875 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Panel de control</span>
          </a>
        </li>
        <!--- Pedidos --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#ordersExamples" class="nav-link " aria-controls="ordersExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Pedidos</span>
          </a>
          <div class="collapse " id="ordersExamples">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> PT </span>
                  <span class="sidenav-normal"> Pedidos en tienda </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> PL </span>
                  <span class="sidenav-normal"> Pedidos de limpieza </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> PT </span>
                  <span class="sidenav-normal"> Pedidos de tienda </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Pr�stamos --->
        <li class="nav-item">
          <a class="nav-link" href="https://github.com/creativetimofficial/ct-soft-ui-dashboard-pro/blob/main/CHANGELOG.md" target="_blank">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center  me-2 d-flex align-items-center justify-content-center">
              <svg width="12px" height="12px" viewBox="0 0 43 36" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>credit-card</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-2169.000000, -745.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g transform="translate(453.000000, 454.000000)">
                        <path class="color-background" d="M43,10.7482083 L43,3.58333333 C43,1.60354167 41.3964583,0 39.4166667,0 L3.58333333,0 C1.60354167,0 0,1.60354167 0,3.58333333 L0,10.7482083 L43,10.7482083 Z" opacity="0.593633743"></path>
                        <path class="color-background" d="M0,16.125 L0,32.25 C0,34.2297917 1.60354167,35.8333333 3.58333333,35.8333333 L39.4166667,35.8333333 C41.3964583,35.8333333 43,34.2297917 43,32.25 L43,16.125 L0,16.125 Z M19.7083333,26.875 L7.16666667,26.875 L7.16666667,23.2916667 L19.7083333,23.2916667 L19.7083333,26.875 Z M35.8333333,26.875 L28.6666667,26.875 L28.6666667,23.2916667 L35.8333333,23.2916667 L35.8333333,26.875 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Pr�stamos</span>
          </a>
        </li>
         <!--- Polizas --->
         <li class="nav-item">
          <a data-bs-toggle="collapse" href="#policyExamples" class="nav-link " aria-controls="policyExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">P�lizas</span>
          </a>
          <div class="collapse " id="policyExamples">
            <ul class="nav ms-4 ps-3">
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> PD </span>
                  <span class="sidenav-normal"> P�liza diario </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> PI </span>
                  <span class="sidenav-normal"> P�liza de ingresos </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!--- Reportes --->
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#reportsExamples" class="nav-link " aria-controls="reportsExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center d-flex align-items-center justify-content-center  me-2">
              <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>office</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g id="office" transform="translate(153.000000, 2.000000)">
                        <path class="color-background" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z" opacity="0.6"></path>
                        <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Reportes </span>
          </a>
          <div class="collapse " id="reportsExamples">
            <ul class="nav ms-4 ps-3">
              <!--- Ultimas semanas publicadas --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> USP </span>
                  <span class="sidenav-normal"> Ultimas </br>semanas publicadas </span>
                </a>
              </li>
              <!--- Reporte pr�stamos --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> RP </span>
                  <span class="sidenav-normal"> Reporte pr�stamos </span>
                </a>
              </li>
              <!--- Reporte uniformes y portaprecios --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> RUP </span>
                  <span class="sidenav-normal"> Reporte uniformes </br>y portaprecios </span>
                </a>
              </li>
              <!--- Infonavit --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> I </span>
                  <span class="sidenav-normal"> Infonavit </span>
                </a>
              </li>
              <!--- Altas, bajas, cambios --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> ABC </span>
                  <span class="sidenav-normal"> Altas, bajas, cambios </span>
                </a>
              </li>
              <!--- Tabla de comisiones --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> TC </span>
                  <span class="sidenav-normal"> Tabla de comisiones </span>
                </a>
              </li>
              <!--- Impresi�n de Facturas --->
              <li class="nav-item ">
                <a class="nav-link " href="../../pages/pages/notifications.html">
                  <span class="sidenav-mini-icon"> IF </span>
                  <span class="sidenav-normal"> Impresi�n de Facturas </span>
                </a>
              </li>
               <!--- Proveedores --->
              <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#proveedoresReportesExample">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal"> Proveedores <b class="caret"></b></span>
                </a>
                <div class="collapse " id="proveedoresReportesExample">
                  <ul class="nav nav-sm flex-column">
                    <!--- Listado de cuenta bancarias de proveedor --->
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> LCBP </span>
                        <span class="sidenav-normal"> Listado de cuentas </br>bancarias de proveedor </span>
                      </a>
                    </li>
                    <!--- Historial de proveedor --->
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> HP </span>
                        <span class="sidenav-normal"> Historial de proveedor </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
               <!--- Acreedores --->
               <li class="nav-item ">
                <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#acreedorReportesExample">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal"> Acreedores <b class="caret"></b></span>
                </a>
                <div class="collapse " id="acreedorReportesExample">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> AB </span>
                        <span class="sidenav-normal"> Acreedor baja </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> A </span>
                        <span class="sidenav-normal"> Activos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> BP </span>
                        <span class="sidenav-normal"> Baja de proveedor </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> DA </span>
                        <span class="sidenav-normal"> Devoluciones aplicadas </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> DPA </span>
                        <span class="sidenav-normal"> Devoluciones por aplicar </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> FA </span>
                        <span class="sidenav-normal"> Fondos aplicadas </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> HA </span>
                        <span class="sidenav-normal"> Historial de acreedor </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> HR </span>
                        <span class="sidenav-normal"> Historial de recepciones </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> I </span>
                        <span class="sidenav-normal"> Inactivos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> LCBA </span>
                        <span class="sidenav-normal"> Listado de cuentas </br>bancarias de acreedor </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> LESAIMSS </span>
                        <span class="sidenav-normal"> Listado de Empleados </br>sin alta IMSS </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> NCA </span>
                        <span class="sidenav-normal"> Notas de cr�dito aplicadas </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> PR </span>
                        <span class="sidenav-normal"> Por revisar </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RB </span>
                        <span class="sidenav-normal"> Remisiones Acreedor </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RG </span>
                        <span class="sidenav-normal"> Reporte de gastos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RN </span>
                        <span class="sidenav-normal"> Reporte n�mina </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RP </span>
                        <span class="sidenav-normal"> Reporte de pagos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RPF </span>
                        <span class="sidenav-normal"> Reporte de pago de fondos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RNC </span>
                        <span class="sidenav-normal"> Reporte de notas de cargo </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RPD </span>
                        <span class="sidenav-normal"> Reporte p�liza diario </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RPI </span>
                        <span class="sidenav-normal"> Reporte p�liza ingresos </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RPIC </span>
                        <span class="sidenav-normal"> Reporte p�liza </br>ingresos concentrado </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> RSN </span>
                        <span class="sidenav-normal"> Reporte sobre n�mina </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="../../pages/pages/account/settings.html">
                        <span class="sidenav-mini-icon text-xs"> T </span>
                        <span class="sidenav-normal"> Todos </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
        <!--- Vacaciones --->
        <li class="nav-item">
          <a class="nav-link" href="https://github.com/creativetimofficial/ct-soft-ui-dashboard-pro/blob/main/CHANGELOG.md" target="_blank">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center  me-2 d-flex align-items-center justify-content-center">
              <svg width="12px" height="12px" viewBox="0 0 43 36" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>credit-card</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-2169.000000, -745.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g transform="translate(1716.000000, 291.000000)">
                      <g transform="translate(453.000000, 454.000000)">
                        <path class="color-background" d="M43,10.7482083 L43,3.58333333 C43,1.60354167 41.3964583,0 39.4166667,0 L3.58333333,0 C1.60354167,0 0,1.60354167 0,3.58333333 L0,10.7482083 L43,10.7482083 Z" opacity="0.593633743"></path>
                        <path class="color-background" d="M0,16.125 L0,32.25 C0,34.2297917 1.60354167,35.8333333 3.58333333,35.8333333 L39.4166667,35.8333333 C41.3964583,35.8333333 43,34.2297917 43,32.25 L43,16.125 L0,16.125 Z M19.7083333,26.875 L7.16666667,26.875 L7.16666667,23.2916667 L19.7083333,23.2916667 L19.7083333,26.875 Z M35.8333333,26.875 L28.6666667,26.875 L28.6666667,23.2916667 L35.8333333,23.2916667 L35.8333333,26.875 Z"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <span class="nav-link-text ms-1">Vacaciones</span>
          </a>
        </li>
      </ul>
    </div>
  </aside>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Alta deduccion</title>
</head>
<cfif inicial neq 0>
<cfset restos=inicial-monto>
<cfquery name="qryDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    UPDATE Deduccion_recurrente
    SET Resto=#restos#,
    	Semana_cambio_id=#semanaid#
    WHERE Deduccion_recurrente_id=#recurrenteid#
</cfquery>
</cfif>
<cfquery name="qryDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT * FROM Deduccion_recurrente INNER JOIN Tipo_deduccion ON Deduccion_recurrente.Tipo_deduccion_id=Tipo_deduccion.Tipo_deduccion_id
    WHERE not resto=0 and empleado_id=#empleadoid#
</cfquery>
<body>
<cfoutput>
<cfif qryDeduccionesrec.recordcount neq 0>
   <cfset cargotot =0>
             <table border="0" align="center" width="500">
              <tr>
              	<td align="left">Deducciones recurrentes Pendientes</td>
              </tr>
              <tr>
              	<td><table border="0" align="center">
                <tr><td>Concepto</td><td>Monto inicial</td><td>Resto</td>
                <td>Descuento por semana</td></tr>
                	<cfloop query="qryDeduccionesrec">
                    <tr><td>#Tipo_deduccion#</td>
                    	<td>#LSCurrencyFormat(Monto_inicial)#</td>
                        <td>#LSCurrencyFormat(resto)#</td>
						<td>#LSCurrencyFormat(deduccion_semana)#</td>
                    </tr>
					<tr>
						<td colspan="4">
							<table width="80%" align="center">
								<tr>
									<td>Descuento por semana</td><td><input type="text" name="deducmes#Deduccion_recurrente_id#" size="10" value="#LSNumberFormat(deduccion_semana, '__.__')#" /></td><td><INPUT TYPE="BUTTON" VALUE="Modificar" onclick="dynamic_Select4('deduccionmod.cfm','#Deduccion_recurrente_id#',document.forma.deducmes#Deduccion_recurrente_id#.value);void(0);"></td>
								</tr>
								<tr>
									<td>Aplicar deduccion</td><td><input type="text" name="deduc#Deduccion_recurrente_id#" size="10" /></td><td><INPUT TYPE="BUTTON" VALUE="Alta" onclick="dynamic_Select2('deduccionalta.cfm',document.forma.deduc#Deduccion_recurrente_id#.value,'#Tipo_deduccion_id#','#Deduccion_recurrente_id#','#resto#');document.forma.deduc#Deduccion_recurrente_id#.value='';void(0);"></td>
								</tr>
							</table>
						</td>
					</tr>
                    </cfloop>
                </table></td>
              </tr>
              </table>
</cfif>
</cfoutput>
</body>
</html>
﻿<cfsilent>
<cftransaction>
<cfif bitacoraid eq 0>
	<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resBitacora">
		INSERT INTO Bitacora (Semana_ano_id, Tienda_id, Empleado_id, Comentarios, Genera_historia_bit, Observaciones, Usuario_id, Usuario_revision_id, Revisado_bit) 
		VALUES
		(#semanaid#, #tiendaid#, #empleadoid#, '#Comentarios#', <cfif isdefined("generahistoria") and generahistoria neq "">#generahistoria#<cfelse>0</cfif>, <cfif isdefined("observaciones") >'#observaciones#'<cfelse>NULL</cfif>, #session.usuarioid#, <cfif isdefined("observaciones") >#session.usuarioid#<cfelse>NULL</cfif>, #revisado#)
</cfquery>
	<cfset bitacoraid=resBitacora.IDENTITYCOL>
	<cfset mensaje="EL alta se dio exitosamente">
<cfelse>
	<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resEntrevistado">
		UPDATE Bitacora
        SET Genera_historia_bit=<cfif isdefined("generahistoria") and generahistoria neq "">#generahistoria#<cfelse>0</cfif>,
			Observaciones=<cfif isdefined("observaciones") >'#observaciones#'<cfelse>NULL</cfif>,
			Usuario_revision_id=#session.usuarioid#,
			Revisado_bit=#revisado#
		WHERE Bitacora_id=#bitacoraid#
</cfquery>
	<cfset mensaje="EL cambio se dio exitosamente">
</cfif>
</cftransaction>

</cfsilent>
<cfoutput>
	<div align="center">
    #mensaje#
    </div>
    <cfif #Session.tienda# eq 0>
		<cfif revisado eq 0>
			<table width="100%" border="0" cellpadding="3" cellspacing="1">
				<tr>
				  <td align="left" valign="middle" bgcolor="##FFFFFF"><div align="center">
						<a href="javascript:void(0);" onclick="valida(0);" class="btn-contacto">Guardar</a>
				  </div></td>
				  <td align="left" valign="middle" bgcolor="##FFFFFF"><div align="center">
						<a href="javascript:void(0);" onclick="valida(1);" class="btn-contacto">Terminar</a>
				  </div></td>
				  </tr>
			</table>
		</cfif>
	<cfelse>
		<table width="100%" border="0" cellpadding="3" cellspacing="1">
			<tr>
			  <td align="left" valign="middle" bgcolor="##FFFFFF"><div align="center">
					<a href="javascript:void(0);" onclick="valida(0);" class="btn-contacto">Guardar</a>
			  </div></td>
			  </tr>
		</table>
	</cfif>
	<script>
		$("##bitacoraid").val("#bitacoraid#");
   </script>
</cfoutput>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Men�</title>
    <link rel="stylesheet" type="text/css" href="default.css" media="screen"/>
    <link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
    <SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
    <!--     Fonts and icons     -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
		<!-- Nucleo Icons -->
		<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
		<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
		<!-- Font Awesome Icons -->
		<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
		<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
		<!-- CSS Files -->
		<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
		<!-- Nepcha Analytics (nepcha.com) -->
		<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->

		<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
		
    <cfif isdefined("valor") and valor eq 1>
      <cftransaction>
        <cfquery name="alasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          INSERT INTO semana_ano (semana, ano)
          VALUES (#semana#, #anosel#)
        </cfquery>
        <cfquery name="qyrsemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          SELECT Top 1 semana_ano_id FROM semana_ano
          ORDER BY semana_ano_id DESC
        </cfquery>
        <cfset fecha1=Fecha>
        <cfset fecha2=dateadd("d",1,fechafin)>
        <cfset cont=0>
        <cfloop condition="fecha1 neq fecha2">
          <cfquery name="qyrfecha" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT * FROM dia_semana_ano
            WHERE dia=#CreateODBCDate(DateFormat(fecha1,"mm/dd/yyyy"))#
          </cfquery>
          <cfif qyrfecha.recordcount eq 0>
            <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              INSERT INTO dia_semana_ano (dia, semana_ano_id)
              VALUES (#CreateODBCDate(DateFormat(fecha1,"mm/dd/yyyy"))#, #qyrsemana.semana_ano_id#)
            </cfquery>
            <cfelse>
              <script language="JavaScript">
                <cfoutput>alert("El d��a #DateFormat(fecha1,"dd/mmm/yyyy")# ya est� capturado");</cfoutput>
                window.location="altasemanas.cfm";
              </script>
              <cfabort>
          </cfif>
          <cfif cont eq 10>
            <script language="JavaScript">
              <cfoutput>alert("Los dias no pueden ser mas de 10 #qyrfecha.dia# , #CreateODBCDate(DateFormat(fecha1,"mm/dd/yyyy"))#");</cfoutput>
              window.location="altasemanas.cfm";
            </script>
            <cfabort>
          </cfif>
          <cfset fecha1=dateadd("d",1,fecha1)>
          <cfset cont=cont+1>
        </cfloop>
      </cftransaction>
      <script language="JavaScript">
        alert("La semana se dio de alta con �xito");
        window.location="altasemanas.cfm";
      </script>
    </cfif>
    <cfif not isdefined("anosel")>
      <cfset anosel=year(now())>
    </cfif>
    <script language="JavaScript">
      function valida(val){
        if (val==1){
          if (document.forma.fecha.value==""){
            alert("Favor de ingresar la Fecha de inicio");
            return false;
          }
          if (document.forma.fechafin.value==""){
            alert("Favor de ingresar la Fecha de fin");
            return false;
          }
          document.forma.valor.value=1;
          document.forma.submit();
        }
      }
    </script>
  </head>

  <body class="g-sidenav-show  bg-gray-100">
		<!---sidenav.cfm es la barra del lateral izquierdo--->
		<cfinclude template="../sidenav.cfm">
		<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
		  <!-- Navbar -->
		  <cfinclude template="../navbar.cfm">
		  <!-- End Navbar -->

    <div align="center">
      <table width="772" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">
              <form method="post" name="forma" action="altasemanas.cfm">
                <input name="valor" type="hidden" value="0" />
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td>
                      <div align="center" class="style11">
                        <p>D�as  por semana</p>
                      </div>
                    </td>
                  </tr>
                  <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT * FROM Semana_ano
                    WHERE ano=#anosel#
                  </cfquery>
                  <cfset semanaslist=valuelist(qrySemanas.Semana)>
                  <tr>
                    <td>
                      <div align="right" class="style11">
                        <table border="0" width="100%">
                          <tr>
                            <td>Semana</td>
                            <td>
                              <select name="semana" onchange="submit(this);">
                                <cfoutput>
                                  <option>Seleccionar</option>
                                  <cfloop from="1" to="53" index="i">
                                    <cfif not ListFindNoCase(semanaslist, i)>
                                      <option value="#i#" <cfif (isdefined("semana") and i eq semana) > selected="selected"</cfif>>#i#</option>
                                    </cfif>
                                  </cfloop>
                                </cfoutput>
                              </select>
                            </td>
                            <td>A�o</td>
                            <td>
                              <select name="anosel" onchange="submit(this);">
                                <cfoutput>
                                  <cfset ano=lsdateformat(now(),"yyyy")>
                                  <cfset ano2=ano+3>
                                  <cfset ano=2011>
                                  <cfloop from="#ano#" to="#ano2#" index="i">
                                    <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                                  </cfloop>
                                </cfoutput>
                              </select>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table width="756" border="0" cellpadding="3" cellspacing="1">
                        <tr>
                          <td width="850" valign="top" align="center">
                            <table width="800" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                              <cfif isdefined("semana") and semana neq "">
                                <cfoutput>
                                  <cfset diasrestar=0-DayOfWeek("01/01/"&anosel)+1>
                                  <cfset diasemana=dateadd("d",diasrestar,"01/01/"&anosel)>
                                  <cfset Martes=(semana-1)*7+1>
                                  <cfset Lunes=(semana-1)*7+7>
                                  <cfset diasemanaM=dateadd("d",Martes,diasemana)>
                                  <cfset diasemanaL=dateadd("d",Lunes,diasemana)>
                                  <tr>
                                    <td>inicio</td>
                                    <td align="center">
                                      <cfoutput>
                                        <input size="8" type="text" name="fecha" Value="#lsdateformat(diasemanaM,"mm/dd/yyyy")#" />
                                        <a href="##" onClick="displayCalendar(document.forma.fecha,'mm/dd/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aquí para dar la fecha"></a>&nbsp;
                                      </cfoutput>
                                    </td>
                                    <td>Fin</td>
                                    <td align="center">
                                      <cfoutput>
                                        <input size="8" type="text" name="fechafin"Value="#lsdateformat(diasemanaL,"mm/dd/yyyy")#" />
                                        <a href="##" onClick="displayCalendar(document.forma.fechafin,'mm/dd/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aquí para dar la fecha"></a>&nbsp;
                                      </cfoutput>
                                    </td>
                                  </tr>
                                </cfoutput>
                              </cfif>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table width="100%" border="0" cellpadding="3" cellspacing="1">
                        <tr>
                          <td>
                            <div align="center"><input type="button" onclick="valida(1);void(0);" value="Agregar" /></div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </form>
            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>

  </main>
  <!--   Core JS Files   -->
<script src="../../../new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="../../new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="../../../new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="../../../new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="../../../new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../../new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
  </body>
</html>
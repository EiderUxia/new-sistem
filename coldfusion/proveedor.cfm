<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>


<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
	<CFABORT>
</CFIF>


<cfif isdefined("nombre")>
	<cftransaction>
		<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			INSERT INTO Proveedor (Proveedor,Razon_social,Nombre_corto,RFC,Calle,Colonia,CP,ciudad_id, Nombre_contacto1c,Celular1c,Nextel1c,Telefono1c,Correo1c, Nombre_contacto2c,Celular2c,Nextel2c,Telefono2c,Correo2c,Nombre_contacto3c,Correo3c,VC) VALUES
			('#nombre#','#razonsocial#', '#nombrecorto#', '#RFC#','#direccion#', '#colonia#', '#cp#', #ciudadid#,'#nombre1c#','#celular1c#', '#nextel1c#','#telefono1c#','#correo1c#','#nombre2c#','#celular2c#', '#nextel2c#','#telefono2c#','#correo2c#','#nombre3c#','#correo3c#','#vc#')			
		</cfquery>
		<cfquery name="Selvacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT Top 1 Proveedor_id FROM Proveedor
			ORDER BY proveedor_id Desc
		</cfquery>
		<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="idCtaNueva">
			INSERT INTO Cuenta_banco_prov (Proveedor_id,Cuenta_banco,banco_id,tipo_cuenta_id,Beneficiario, usuario_id, Clabe, sucursal) VALUES
			(#Selvacuna.proveedor_id#,'#cuentabanco#', #bancoid#,#tipocuentaid#, '#beneficiario#',#Session.usuarioid#, '#clabe#', '#sucursal#')			
		</cfquery>

		<cfset CuentaBancoProvId = idCtaNueva.IdentityCol>
		<cfset fechaMovimiento = createodbcdatetime(now())>

		<!--- Guarda el historial de la cuenta a dar de alta --->
		<cfquery datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="ControlId">
			INSERT INTO Cuenta_banco_prov_cambio (
				Cuenta_banco_prov_id_alta,
				Cuenta_banco_prov_id_baja,
				Usuario_propuesta,
				Fecha_propuesta,
				Tipo_Movimiento) 
			VALUES (
				#CuentaBancoProvId#,
				NULL,
				#Session.usuarioid#, 
				#fechaMovimiento#,
				'Alta')			
		</cfquery>

		<cfquery datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			UPDATE Cuenta_banco_prov_cambio
			SET Control_id = #ControlId.IdentityCol# 
			WHERE Cuenta_banco_prov_cambio_id = #ControlId.IdentityCol#
		</cfquery>


	</cftransaction>
	<script language="JavaScript" type="text/javascript">
		alert("El proveedor fue agregado con �xito");
		window.location="proveedor.cfm";
	</script>
</cfif>


<script language="JavaScript">
function valida(){
	var l5 = document.forma.direccion.value;
	var l6 = document.forma.colonia.value;
	var l7 = document.forma.cp.value;
	var l11 = document.forma.nombre2c.value;
	var l12 = document.forma.nombre3c.value;	
	if (document.forma.nombre.value==""){
		alert("Favor de ingresar el nombre del proveedor");
		return false;
	}
	if (document.forma.nombrecorto.value==""){
		alert("Favor de ingresar el nombre corto del proveedor");
		return false;
	}
	if (l5.length == 0){
		alert("Ingrese la direcci�n del proveedor");
		return false;
	}
	if (l6.length == 0){
		alert("Ingrese la colonia del proveedor");
		return false;
	}
	if (l7.length == 0){
		alert("Ingrese el c�digo postal del proveedor");
		return false;
	}
	if (document.forma.paisid.selectedIndex==0){
			alert("El pa�s del proveedor no ha sido seleccionado");
		return false;
	}
	if (document.forma.estadoid.selectedIndex==0){
			alert("El estado de residencia del proveedor no ha sido seleccionado");
		return false;
	}
	if (document.forma.ciudadid.selectedIndex==0){
			alert("La ciudad de residencia del proveedor no ha sido seleccionada");
		return false;
	}
	if (document.forma.nombre1c.value==""){
		alert("Favor de ingresar el nombre del contacto directo");
		return false;
	}
	if (document.forma.telefono1c.value==""){
		alert("Favor de ingresar el telefono del contacto directo");
		return false;
	}
	if (document.forma.cuentabanco.value==""){
		alert("Favor de ingresar la cuenta bancaria del proveedor");
		return false;
	}
	if (document.forma.beneficiario.value==""){
		alert("Favor de ingresar el nombre del titular de la cuenta");
		return false;
	}
	if (document.forma.bancoid.selectedIndex==0){
			alert("El banco del proveedor no ha sido seleccionado");
		return false;
	}
	if (document.forma.tipocuentaid.selectedIndex==0){
			alert("El tipo de cuenta no ha sido seleccionado");
		return false;
	}
	return true;
}
</script>
<!--- Inicio de codigo para el llenado de pais, estados, ciudades --->
<cfquery name="Paissel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT * FROM PAIS	
	</cfquery>

<!-- Script by hscripts.com -->
<!-- copyright hioxindia.com -->
<script language=javascript>
var aa = new Array("Seleccionar"<cfoutput query="paissel">,"#Pais#"</cfoutput>);
var aaid = new Array("sele"<cfoutput query="paissel">,"#Pais_id#"</cfoutput>);
paissele = new Array("Seleccionar Pa�s");
paisidsele = new Array("sele");
estadoidsele = new Array("sele");
estadosele = new Array("Seleccionar Estado");
<cfoutput query="paissel">
<cfquery name="Estadosel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT * FROM Estado
		WHERE pais_id=#pais_id#
		Order BY Estado
	</cfquery>
pais#pais_id# = new Array("Seleccionar"<cfloop query="estadosel">,"#Estado#"</cfloop>);
paisid#pais_id# = new Array("sele"<cfloop query="estadosel">,"#Estado_id#"</cfloop>);
<cfloop query="Estadosel">
<cfquery name="Ciudadsel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT * FROM Ciudad
		WHERE estado_id=#estado_id# and activo=1
		Order BY Ciudad
	</cfquery>
estado#estado_id# = new Array("Seleccionar"<cfloop query="ciudadsel">,"#ciudad#"</cfloop>);
estadoid#estado_id# = new Array("sele"<cfloop query="ciudadsel">,"#ciudad_id#"</cfloop>);
</cfloop>
</cfoutput>


function changeval()
{
 var val1 = "pais"+document.forma.paisid.value;
 var val2 = "paisid"+document.forma.paisid.value;
 var optionArray = eval(val1);
 var optionArray2 = eval(val2);
 for(var df=0; df<optionArray.length; df++)
 {
	var ss = document.forma.estadoid;
	ss.options.length = 0;
	for(var ff=0; ff<optionArray.length; ff++)
	{
	 var val = optionArray[ff];
	 var val3 = optionArray2[ff];
	 ss.options[ff] = new Option(val,val3);
	}
 }
 changeval2()
}

function changeval2()
{
 var val1 = "estado"+document.forma.estadoid.value;
 var val2 = "estadoid"+document.forma.estadoid.value;
 var optionArray = eval(val1);
 var optionArray2 = eval(val2);
 for(var df=0; df<optionArray.length; df++)
 {
	var ss = document.forma.ciudadid;
	ss.options.length = 0;
	for(var ff=0; ff<optionArray.length; ff++)
	{
	 var val = optionArray[ff];
	 var val3 = optionArray2[ff];
	 ss.options[ff] = new Option(val,val3);
	}
 }
}
</script>

</head>

<body>
<div align="center">
	<table width="772" border="0" cellpadding="4" cellspacing="4">
		
		<tr>
			<td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
		</tr>
		<tr>
			<td bgcolor="#FFFFFF"><div align="center" class="style7">
			<form method="post" name="forma" action="proveedor.cfm" onSubmit="return valida(this)">
			<table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
				<tr>
					<td><div align="center" class="style11">Alta de Proveedor </div></td>
				</tr>
				<tr>
					<td>
					<table width="756" border="0" cellpadding="3" cellspacing="1">
						<tr>
							<td width="371" valign="top"><table width="378" border="0" cellpadding="3" cellspacing="1">
								<tr>
									<td align="left" valign="top">*Marca</td>
									<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="nombrecorto" tabindex="1" id="textfield12" /></td>
								</tr>
								<tr>
										<td width="127" align="left" valign="middle"><div align="left">*Calle y n�mero: </div></td>
										<td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><div align="left">
												<input type="text" name="direccion" tabindex="3" />
										</div></td>
								</tr>
									<tr>
										<td width="127" align="left" valign="middle"><div align="left">*Pa&iacute;s: </div></td>
										<td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><div align="left">
					<cfoutput>
										<select name="paisid" onchange="changeval()" tabindex="5">
																	<option value="sele" >Seleccionar</option>
								<CFloop query="paissel">
																	<option value="#pais_id#" >#pais#</option>
																</CFloop>
															</select>
															</cfoutput>
										</div></td>
									</tr>
									<tr>
										<td width="127" align="left" valign="middle"><div align="left">*Ciudad: </div></td>
										<td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><div align="left">
					<select name="ciudadid" tabindex="7">
																	<option value="sele" >Seleccionar</option>
							</select>
										</div></td>
									</tr>
								<tr>
									<td align="left" valign="top">RFC&#13;</td>
									<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="rfc" id="textfield10" tabindex="9" /></td>
								</tr>
								<tr>
									<td align="left" valign="top">C�digo de proveedor (retail)</td>
									<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="vc" id="textfield10" tabindex="9" /></td>
								</tr>
							</table></td>
							<td width="385" valign="top"><table width="378" border="0" cellpadding="3" cellspacing="1">
								<tr>
									<td width="137" align="left" valign="middle">*Nombre&#13;de la Fabrica</td>
									<td width="216" align="left" valign="middle" bgcolor="#FFFFFF"><label>
										<input type="text" name="nombre" id="textfield" tabindex="2" />
									</label></td>
								</tr>
								<tr>
										<td width="137" align="left" valign="middle"><div align="left">*Colonia: </div></td>
										<td width="216" align="left" valign="middle" bgcolor="#FFFFFF"><div align="left">
												<input type="text" name="colonia" tabindex="4"  />
										</div></td>
									</tr>
									<tr>
										<td width="137" align="left" valign="middle"><div align="left">*Estado: </div></td>
										<td width="216" align="left" valign="middle" bgcolor="#FFFFFF"><div align="left">
					<select name="estadoid" onchange="changeval2()" tabindex="6">
																	<option value="sele" >Seleccionar</option>
							</select>
										</div></td>
									</tr>
									<tr>
										<td width="137" align="left" valign="middle"><div align="left">*C.P.:</div></td>
										<td width="216" align="left" valign="middle" bgcolor="#FFFFFF"><div align="left">
												<input name="cp" type="text" size="5" maxlength="5" tabindex="8" />
										</div></td>
									</tr>
								<tr>
									<td width="137" align="left" valign="middle">Raz&oacute;n Social&#13;</td>
									<td width="216" align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="razonsocial" id="textfield4" tabindex="10" /></td>
								</tr>
							</table></td>
						</tr>
					</table></td>
				</tr>
				<tr>
					<td><table width="100%" border="0" cellpadding="3" cellspacing="1">
						<tr>
							<td valign="middle"><table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
									<tr><td colspan="4" align="left"><hr color="#0066FF" /></td></tr>
									<tr><td colspan="4" align="left">Contacto Directo (Propietario) </td></tr>
									<tr>
										<td width="80" align="left" valign="middle"><p>*Nombre</p></td>
										<td width="267" colspan="3" align="left" valign="middle" bgcolor="#FFFFFF"><label>
											<input type="text" name="nombre1c" id="textfield3" size="70" />
										</label></td>
									</tr>
									<tr>
										<td align="left" valign="middle">*Tel&eacute;fono</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="telefono1c" id="textfield17" /></td>
										<td align="left" valign="middle">Nextel</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="nextel1c" id="textfield24" /></td>
									</tr>
									<tr>
										<td align="left" valign="middle">Celular</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="celular1c" id="textfield16" /></td>
										<td align="left" valign="middle">Mail</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="correo1c" id="textfield25" /></td>
									</tr>
									<tr><td colspan="4" align="left"><hr color="#0066FF" /></td></tr>
									<tr><td colspan="4" align="left">Contacto Ventas</td></tr>
									<tr>
										<td width="80" align="left" valign="middle"><p>Nombre</p></td>
										<td align="left" colspan="3" valign="middle" bgcolor="#FFFFFF"><input type="text" name="nombre2c" id="textfield18" size="70" /></td>
									</tr>
									<tr>
										<td align="left" valign="middle">Tel&eacute;fono</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="telefono2c" id="textfield20" /></td>
										<td align="left" valign="middle">Nextel</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="nextel2c" id="textfield27" /></td>
									</tr>
									<tr>
										<td align="left" valign="middle">Celular</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="celular2c" id="textfield19" /></td>
										<td align="left" valign="middle">Mail</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="correo2c" id="textfield28" /></td>
									</tr>
									<tr><td colspan="4" align="left"><hr color="#0066FF" /></td></tr>
									<tr><td colspan="4" align="left">Contacto Pagos</td></tr>
									<tr>
										<td width="80" align="left" valign="middle"><p>Nombre</p></td>
										<td align="left" valign="middle" bgcolor="#FFFFFF" colspan="3"><input type="text" name="nombre3c" id="textfield21" size="70" /></td>
									</tr>
					<cfquery name="Bancos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Banco
						ORDER BY Banco
					</cfquery>
									<cfquery name="Tipocuentas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Tipo_cuenta
						ORDER BY Tipo_cuenta
					</cfquery>
								<tr>
									<td width="127" align="left" valign="middle"><p>*Banco&#13;</p></td>
									<td width="237" align="left" valign="middle" bgcolor="#FFFFFF">
									<cfoutput>
									<select name="bancoid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Bancos">
										<option value="#Banco_id#" >#Banco#</option>
									</CFLOOP>
												</select>
												</cfoutput>
												</td>
									<td align="left" valign="middle"><p>*No. cuenta</p></td>
									<td align="left" valign="middle" bgcolor="#FFFFFF"><label>
									<input type="text" name="cuentabanco" id="textfield2" />
									</label></td>
								</tr>
								<tr>
									<td align="left" valign="middle"><p>No. de Sucursal (banco):</p></td>
									<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="sucursal" id="textfield3" /></td>
									<td align="left" valign="middle"><p>*Titular de la cuenta</p></td>
									<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="beneficiario" id="textfield3" /></td>
								</tr>
								<tr>
									<td align="left" valign="middle"><p>No. Clabe</p></td>
									<td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="clabe" id="textfield3" /></td>
									<td align="left" valign="middle"><p>*Tipo de cuenta</p></td>
									<td align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput>
									<select name="tipocuentaid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Tipocuentas">
										<option value="#Tipo_cuenta_id#" >#Tipo_cuenta#</option>
									</CFLOOP>
												</select>
												</cfoutput></td>
								</tr>
									<tr>
										<td align="left" valign="middle">Correo</td>
										<td align="left" valign="middle" bgcolor="#FFFFFF" colspan="3"><input type="text" name="correo3c" id="textfield31" /></td>
									</tr>
							</table></td>
							</tr>
							<tr><td>
							<div align="center"><input type="submit" value="Agregar" /></div>
							</td></tr>
					</table></td>
				</tr>
			</table></form>
					<table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
						<tr>
							<td><div align="left">
								<table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
									<tr>
										<td width="58%"><table border="0" cellpadding="0" cellspacing="0">
						<tr>
												<td width="186" height="26">
							<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
													<tr>
														<td><a href="menu_catalogos.cfm" class="style9">Regresar</a></td>
													</tr>
												</table></td>
												<td width="186" height="26">
							<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
													<tr>
														<td><a href="menu.cfm" class="style9">Menu</a></td>
													</tr>
												</table></td>
												<td width="186" height="26">
							<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
													<tr>
														<td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
													</tr>
												</table></td>
											</tr>
										</table>                      </td>
										<td width="42%" bgcolor="#FFFFFF">Usuario: 
											<cfoutput>
											<div>#Session.nombre#</div></cfoutput></td>
									</tr>
								</table>
							</div></td>
						</tr>
					</table>
			</div>
			<div align="right"></div></td>
		</tr>
	</table>
</div>
</body>
</html>

CREATE TABLE erezdesarrollo.dbo.Acreedor (
	Acreedor_id int IDENTITY(1,1) NOT NULL,
	Acreedor varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Razon_social varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nombre_corto varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	RFC varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Calle varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Colonia varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CP int NULL,
	ciudad_id int NULL,
	Telefono1 varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Telefono2 varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Correo_empresa varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Fax varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Activo bit DEFAULT 1 NOT NULL,
	Revisado bit DEFAULT 0 NOT NULL,
	CONSTRAINT PK_Acreedor PRIMARY KEY (Acreedor_id)
);

CREATE TABLE erezdesarrollo.dbo.Acreedor_baja (
	Acreedor_id int NOT NULL,
	Acreedor varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Razon_social varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nombre_corto varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	RFC varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Calle varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Colonia varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CP int NULL,
	ciudad_id int NULL,
	Telefono1 varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Telefono2 varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Correo_empresa varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Fax varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Activo bit DEFAULT 1 NOT NULL,
	Revisado bit DEFAULT 0 NOT NULL,
	Fecha_baja smalldatetime NOT NULL,
	Usuario_baja_id int NOT NULL,
	CONSTRAINT PK_Acreedor_baja PRIMARY KEY (Acreedor_id)
);

CREATE TABLE erezdesarrollo.dbo.Aguinaldo (
	Aguinaldo_id int IDENTITY(1,1) NOT NULL,
	Ano int NOT NULL,
	Semana_ano_id int NULL,
	terminado_bit bit DEFAULT 0 NOT NULL,
	Pagado_bit bit DEFAULT 0 NOT NULL,
	CONSTRAINT PK_Aguinaldo PRIMARY KEY (Aguinaldo_id)
);

CREATE TABLE erezdesarrollo.dbo.Aguinaldo_Empleado (
	Aguinaldo_Empleado_id int IDENTITY(1,1) NOT NULL,
	Aguinaldo_id int NOT NULL,
	Empleado_id int NOT NULL,
	Propuesta_mario money NULL,
	Propuesta_rh money NULL,
	Aguinaldo money NULL,
	Sobre_estatus_id int DEFAULT 1 NULL,
	CONSTRAINT PK_Aguinaldo_Empleado PRIMARY KEY (Aguinaldo_Empleado_id)
);

CREATE TABLE erezdesarrollo.dbo.Cheque_2 (
	Cheque_id int IDENTITY(1,1) NOT NULL,
	Cheque_num int NULL,
	Tienda_id int NOT NULL,
	Cuenta_banco_id int NOT NULL,
	Entidad_id int NOT NULL,
	Monto money NOT NULL,
	Fecha datetime NOT NULL,
	Cuenta_pagar bit NOT NULL,
	Tipo_CP_id int NULL,
	Proveedor_id int NULL,
	Acreedor_id int NULL,
	Comentarios varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Usuario_id int NOT NULL,
	Autorizado bit NOT NULL,
	Impreso bit NOT NULL,
	Monto_text varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Abono bit NOT NULL,
	autorizado_vero bit NOT NULL,
	Anticipo bit NOT NULL,
	reviso bit NOT NULL,
	fecha_reviso smalldatetime NULL,
	Pagado bit NOT NULL,
	Fecha_pagado smalldatetime NULL,
	Tipo_pagado_id int NULL,
	Num_trans varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Otro_cheque int NOT NULL,
	Cancelado_bit bit NOT NULL,
	amano_bit bit NOT NULL,
	Tipo_pago_id int NOT NULL
);

CREATE TABLE erezdesarrollo.dbo.Concepto_baja (
	Concepto_baja_id int NOT NULL,
	Concepto_baja varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Concepto_baja PRIMARY KEY (Concepto_baja_id)
);

CREATE TABLE erezdesarrollo.dbo.Cuenta_banco_prov_cambio (
	Cuenta_banco_prov_cambio_id int IDENTITY(1,1) NOT NULL,
	Cuenta_banco_prov_id_alta int NULL,
	Cuenta_banco_prov_id_baja int NULL,
	Usuario_propuesta int NOT NULL,
	Fecha_propuesta smalldatetime NOT NULL,
	Tipo_Movimiento varchar(55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Usuario_autoriza int NULL,
	Fecha_autoriza smalldatetime NULL,
	Autorizado_bit bit DEFAULT 0 NOT NULL,
	Control_id int NULL,
	Comentarios_cambio varchar(511) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK_Cuenta_banco_prov_cambio PRIMARY KEY (Cuenta_banco_prov_cambio_id)
);

CREATE TABLE erezdesarrollo.dbo.Deduccion_recurrente_BAK (
	Deduccion_recurrente_id int NOT NULL,
	tipo_deduccion_id int NOT NULL,
	Empleado_id int NOT NULL,
	Semana_ano_id int NOT NULL,
	Fecha datetime NOT NULL,
	Monto_inicial money NOT NULL,
	resto money NOT NULL,
	deduccion_semana money NOT NULL,
	inicio_semana_ano_id int NOT NULL,
	Comentarios varchar(1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Automatica_bit bit NOT NULL,
	Semana_cambio_id int NULL
);

CREATE TABLE erezdesarrollo.dbo.Empleado_baja (
	Empleado_baja_id int IDENTITY(1,1) NOT NULL,
	Empleado_hist_id int NOT NULL,
	Concepto varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Concepto_baja_id int DEFAULT 0 NOT NULL,
	Monto money NOT NULL,
	Tipo_Documento int NOT NULL,
	Percepcion_bit bit DEFAULT 1 NOT NULL,
	Orden int NULL,
	CONSTRAINT PK_Empleado_baja PRIMARY KEY (Empleado_baja_id)
);

CREATE TABLE erezdesarrollo.dbo.Empleado_baja_Eliminado (
	Empleado_baja_id int NOT NULL,
	Empleado_hist_id int NOT NULL,
	Concepto varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Monto money NOT NULL,
	Tipo_Documento int NOT NULL,
	Percepcion_bit bit NOT NULL,
	Orden int NULL
);

CREATE TABLE erezdesarrollo.dbo.Empleado_bak (
	Empleado_id int IDENTITY(1,1) NOT NULL,
	Nombre varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Apellido_pat varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Apellido_mat varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Sexo bit NOT NULL,
	IMSS varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nacimiento_fecha datetime NOT NULL,
	Telefono varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Estado_Civil_id int NOT NULL,
	Hijos int NOT NULL,
	Escolaridad_id int NULL,
	Ingreso_fecha datetime NULL,
	Capacitacion_fecha datetime NULL,
	Baja_fecha datetime NULL,
	Fecha_reingreso datetime NULL,
	Tienda_id int NOT NULL,
	Tienda_cerca_id int NULL,
	Puesto_id int NOT NULL,
	Sueldo_base money NULL,
	Sueldo_comentario varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Como_entro varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Observaciones varchar(1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Imagen varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Grupo_Sangineo_id int NULL,
	IMSS_fecha datetime NULL,
	RFC varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Afore varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CURP varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Pasaporte varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Contacto_emergencia varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IFE varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Telefono_emergencia varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Camisa varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Overol varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Chamarra varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Cinturon varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Pantalon varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Zapatos varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Casco varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Otro varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Peso varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Talla varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Activo bit NOT NULL,
	Paga_infonavit bit NOT NULL,
	Infonavit money NULL,
	Finger_id int NULL
);

CREATE TABLE erezdesarrollo.dbo.Empleado_dia_semana_bak (
	Empleado_id int NOT NULL,
	Dia_semana_id int NOT NULL
);

CREATE TABLE erezdesarrollo.dbo.Proveedor_bak (
	Proveedor_id int IDENTITY(1,1) NOT NULL,
	Proveedor varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Razon_social varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nombre_corto varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	RFC varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Calle varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Colonia varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CP int NULL,
	ciudad_id int NULL,
	Telefono1 varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Telefono2 varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Telefono3 varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Correo_empresa varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Fax varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Descuento_pesos money NULL,
	Descuento_porciento float NULL,
	Nombre_contacto1c varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Puesto1c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Celular1c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nextel1c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Telefono1c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Correo1c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nombre_contacto2c varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Puesto2c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Celular2c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nextel2c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Telefono2c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Correo2c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nombre_contacto3c varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Puesto3c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Celular3c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Nextel3c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Telefono3c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Correo3c varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Activo bit NOT NULL,
	CONSTRAINT PK__Proveedor_bak__7A3223E8 PRIMARY KEY (Proveedor_id)
);

CREATE TABLE erezdesarrollo.dbo.Proveedor_linea (
	Proveedor_id int NOT NULL,
	Linea_id int NOT NULL,
	Contacto varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK_Proveedor_linea PRIMARY KEY (Proveedor_id,Linea_id)
);

CREATE TABLE erezdesarrollo.dbo.Sobre_estatus (
	Sobre_estatus_id int IDENTITY(1,1) NOT NULL,
	Sobre_estatus varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Activo_bit bit DEFAULT 1 NOT NULL,
	CONSTRAINT PK_Sobre_estatus PRIMARY KEY (Sobre_estatus_id)
);

CREATE TABLE erezdesarrollo.dbo.Tipo_renuncia (
	Tipo_renuncia_id int IDENTITY(1,1) NOT NULL,
	Tipo_renuncia varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Activo_bit bit NOT NULL,
	CONSTRAINT PK_Tipo_renuncia PRIMARY KEY (Tipo_renuncia_id)
);


CREATE VIEW Ultimo_Empleado_hist AS 
SELECT 
	E.Empleado_id,
	MAX(EH.Empleado_hist_id) AS Empleado_hist_id
FROM erezdesarrollo.dbo.Empleado  E
	INNER JOIN erezdesarrollo.dbo.Empleado_hist EH ON E.Empleado_id = EH.Empleado_id
GROUP BY 
	E.Empleado_id;




























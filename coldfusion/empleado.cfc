﻿<cfcomponent output="false">
<cfset varDb = "erezdesarrollo"><cfset varDbU = "sa"><cfset varDbP = "root">
	<cffunction name="getempleado" access="remote" output="false" returntype="query">
		<cfargument name="tiendaid" type="string" required="true" hint="" />   
        <cfif Arguments.tiendaid eq "">
        	<cfset abuscar=0>
        <cfelse>
        	<cfset abuscar=Arguments.tiendaid>
        </cfif>
        <cfquery name="rs" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT '' as Empleado_id, '' as nombre
			UNION
            SELECT 0 as Empleado_id, ' Selecciona un Empleado' as nombre
			UNION
            SELECT convert(varchar(10),Empleado_id) as Empleado_id, Nombre +' '+Apellido_pat as nombre FROM Empleado
            where Tienda_id=<cfqueryparam cfsqltype="cf_sql_integer" value="#abuscar#" /> and Activo=1
            order by Nombre
        </cfquery>

		<cfif rs.RecordCount EQ 2>
			<cfset rs = QueryNew("Empleado_id, nombre") />
			<cfset QueryAddRow(rs) />
			<cfset QuerySetCell(rs, "Empleado_id", "") />
			<cfset QuerySetCell(rs, "nombre", "") />
			<cfset QueryAddRow(rs) />
			<cfset QuerySetCell(rs, "Empleado_id", "") />
			<cfset QuerySetCell(rs, "nombre", "Esta tienda no tiene empleados") />
		</cfif>
		<cfreturn rs />
	</cffunction>
    
    <cffunction name="getsueldo" access="remote" output="false" returntype="string">
		<cfargument name="empleadoid" type="string" required="true" hint="" />   
        <cfif Arguments.empleadoid eq "">
        	<cfset abuscar=0>
        <cfelse>
        	<cfset abuscar=Arguments.empleadoid>
        </cfif>

		<cfquery name="rs2" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT *
            FROM Empleado
            where Empleado_id=<cfqueryparam cfsqltype="cf_sql_integer" value="#abuscar#" />
            order by Nombre
        </cfquery>
        <cfif rs2.recordcount neq 0>
			<cfif rs2.puesto_id eq 1 or rs2.puesto_id eq 9>
                <cfset rs=320>
            <cfelse>
                <cfset rs=LSNumberFormat((rs2.sueldo_base/7)*(rs2.dias_vacaciones-6),"___.__")>
            </cfif>
        <cfelse>
        	<cfset rs=0>
        </cfif>
		<cfreturn rs />
	</cffunction>
    
    <cffunction name="getprima" access="remote" output="false" returntype="string">
		<cfargument name="empleadoid" type="string" required="true" hint="" />   
        <cfif Arguments.empleadoid eq "">
        	<cfset abuscar=0>
        <cfelse>
        	<cfset abuscar=Arguments.empleadoid>
        </cfif>

		<cfquery name="rs2" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT *
            FROM Empleado
            where Empleado_id=<cfqueryparam cfsqltype="cf_sql_integer" value="#abuscar#" />
            order by Nombre
        </cfquery>
        <cfif rs2.recordcount neq 0>
			<cfif rs2.puesto_id eq 1 or rs2.puesto_id eq 9>
                <cfset rs=80>
            <cfelse>
                <cfset rs=LSNumberFormat((rs2.sueldo_base/7)*(rs2.dias_vacaciones*.25),"___.__")>
            </cfif>
        <cfelse>
        	<cfset rs=0>
        </cfif>
		<cfreturn rs />
	</cffunction>

</cfcomponent>
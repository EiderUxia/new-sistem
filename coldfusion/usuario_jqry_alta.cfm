<cfsilent>
    <cftransaction>
        <cfif Usuarioid eq 0>
	        <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resUsuario">
                INSERT INTO Usuario (Nombre, Apellido_Pat, Apellido_Mat, Usuario, Contrasena, Correo, Tipo_Usuario_id, Tienda_id, Activo_bit, Alta_fecha) 
                VALUES('#nombre#', '#apellidopat#', '#apellidomat#', '#usuario#', '#contrasena#', '#Correo#', 8, #tiendaid#, #activo#, #CreateODBCDate(LSDateFormat(FechaAlta,"mm/dd/yyyy"))#)
            </cfquery>
	        <cfset Usuarioid=resUsuario.IDENTITYCOL>
	        <cfset mensaje="EL Usuario se creo con exito">
            <cfelse>
                <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resEntrevistado">
                    UPDATE Usuario
                    SET Nombre='#nombre#',
                        Apellido_Pat='#apellidopat#',
                        Apellido_Mat='#apellidomat#',
                        Usuario='#usuario#',
                        Contrasena='#contrasena#',
                        Correo='#Correo#',
                        Tienda_id=#tiendaid#,
                        Activo_bit=#activo#,
                        Alta_fecha=#CreateODBCDate(LSDateFormat(FechaAlta,"mm/dd/yyyy"))#                     
                    WHERE Usuario_id=#Usuarioid#
                </cfquery>
	            <cfset mensaje="EL cambio se dio exitosamente">
        </cfif>
    </cftransaction>
</cfsilent>
<cfoutput>
	<div align="center">
        #mensaje#<br />
        <br /><br />
        <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr>
			    <td align="left" valign="middle" bgcolor="##FFFFFF">
                    <div align="center">
                        <a href="Carta_usuario.cfm?uid=#Usuarioid#" class="btn-contacto">Carta Responsiva</a>                       
                    </div>
                </td>
            </tr>
        </table>
    </div>
	<script>
		$("##Usuarioid").val("#Usuarioid#");
   </script>
</cfoutput>
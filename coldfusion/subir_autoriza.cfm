<cfparam name="form.InputExcelFile" default="">
<cfsetting requestTimeOut="3600">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Men&uacute;</title>
		<CFIF not isdefined("session.usuarioid")>
			<script language="JavaScript" type="text/javascript">
				alert("Usted no est� dentro del sistema");
				window.location="index.cfm";
			</script>
			<CFABORT>
		</CFIF>
		<style type="text/css">
			<!--
			.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
			.style8 {
				font-size: 24px;
				color: #000000;
			}
			.style9 {color: #FFFFFF}
			.style11 {font-size: 18px}
			-->
		</style>
	</head>

	<body>
		<div align="center">
			<table width="772" border="0" cellpadding="4" cellspacing="4">
				<tr>
					<td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">
						<div align="center" class="style7">
							<table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
								<tbody>
									<tr>
										<td align="center" height="37" valign="middle"><div align="center"><span class="style11">Subir Archivo de Asistencias </span></div></td>
									</tr>
									<tr>
										<td align="center" height="37" valign="middle" width="756">
											<cfif form.InputExcelFile eq "">
												<table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
													<cfform action="subir_autoriza.cfm" method="POST" enctype="multipart/form-data">
														<tr>
															<td nowrap valign="top">Archivo de Excel:</td>
															<td width="100%" valign="top">
																<input type="File" name="InputExcelFile" size="40" class="textfield">
															</td>
														</tr>
														<tr>
															<td colspan="2">
																<input type="Submit" value="Procesar" class="button"><br>
																<br>			
															</td>
														</tr>
													</cfform>
												</table>
												<cfelse>
												<!--- read operation --->
												<hr size="1">
												<!--- define temp excel --->
												<cfset strDir=GetDirectoryFromPath(ExpandPath("*.*")) & "/temp">
												<cfset strInExcel=strDir>	
												<!--- upload image --->
												<cffile action="Upload"
												filefield="InputExcelFile"
												destination="#strInExcel#"
												nameconflict="MAKEUNIQUE" 
												mode="757">
												<cfset prodThumbDir=file.ServerDirectory>
												<cfset prodThumbFile=file.ServerFile>
												<cfset prodThumbExt=file.serverfileext>
												<cfif (prodThumbExt neq "xls")>
													Favor de utilizar archivos de Excel solamente
													<cfoutput>#strInExcel#</cfoutput>
													<cfelse>  
														<cfspreadsheet action="read" columnnames="Tienda_semana_ano_id, Tienda_id, Semana_ano_id, Comentarios, Usuario_autorizo_id, Terminada, Autorizo_cont, Autorizo_cont_id, Comentario_autorizo, Pagada, Usuario_pagado_id, publicar, autorizoencargada, Usuario_autorizoencargada_id, Pares_descontados, Control_id, origen_id" query="Tienda" src="#prodThumbDir#/#prodThumbFile#" sheet="1">
													<cfoutput>
														<cfloop query="Tienda">
															<cfquery  name="qryTienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																SELECT Tienda_semana_ano_id
																FROM Tienda_semana_ano
																WHERE Control_id=#Tienda_semana_ano_id# and origen_id=2
															</cfquery>
															<CFIF qryTienda.recordcount eq 0>
																<cfquery  name="qryInsTienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																	INSERT INTO Tienda_semana_ano(Tienda_id, Semana_ano_id, Comentarios, Usuario_autorizo_id, Terminada, Autorizo_cont, Autorizo_cont_id, Comentario_autorizo, Pagada, Usuario_pagado_id, publicar, autorizoencargada, Usuario_autorizoencargada_id, Pares_descontados, Control_id, origen_id) 
																	values(#Tienda_id#,'#Semana_ano_id#','#Comentarios#','#Usuario_autorizo_id#','#Terminada#','#Autorizo_cont#','#Autorizo_cont_id#','#Comentario_autorizo#','#Pagada#','#Usuario_pagado_id#','#publicar#','#autorizoencargada#','#Usuario_autorizoencargada_id#','#Pares_descontados#', #Control_id#, #origen_id#)
																</cfquery>
																<cfelse>
																	<cfquery  name="qryUpdTienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																		UPDATE Tienda_semana_ano
																		SET Comentarios='#Comentarios#',
																			Usuario_autorizo_id='#Usuario_autorizo_id#',
																			Terminada='#Terminada#',
																			Autorizo_cont='#Autorizo_cont#',
																			Autorizo_cont_id='#Autorizo_cont_id#',
																			Comentario_autorizo='#Comentario_autorizo#',
																			Pagada='#Pagada#',
																			Usuario_pagado_id='#Usuario_pagado_id#',
																			publicar='#publicar#',
																			autorizoencargada='#autorizoencargada#',
																			Usuario_autorizoencargada_id='#Usuario_autorizoencargada_id#',
																			Pares_descontados='#Pares_descontados#'
																		WHERE Tienda_semana_ano_id=#qryTienda.Tienda_semana_ano_id# and origen_id=2
																	</cfquery>
															</CFIF>
														</cfloop>
													</cfoutput>
													<cfspreadsheet action="read" columnnames="Empleado_dia_semana_ano_id, Empleado_id, Dia_Semana_ano_id, Concepto_dia_trabajado_id, Venta_pares, Venta_playa, Pares_descontados, Tienda_semana_ano_id, Control_id, origen_id" query="empleados" src="#prodThumbDir#/#prodThumbFile#" sheet="2" excludeHeaderRow = "true" headerrow="1">
													<cfoutput>
														<cfloop query="empleados">
															<cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																SELECT Empleado_dia_semana_ano_id
																FROM Empleado_dia_semana_ano
																WHERE Control_id=#Empleado_dia_semana_ano_id# and origen_id=2
															</cfquery>
															<cfif qryEmpleados.recordcount eq 0>
																<cfquery  name="qryInsEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																	INSERT INTO Empleado_dia_semana_ano(Empleado_id, Dia_Semana_ano_id, Concepto_dia_trabajado_id, Venta_pares, Pares_descontados, Tienda_semana_ano_id, Control_id, origen_id, Venta_playa) 
																	values(#Empleado_id#,#Dia_Semana_ano_id#, #Concepto_dia_trabajado_id#, #Venta_pares#, #Pares_descontados#, #Tienda_semana_ano_id#, #Control_id#, #origen_id#, #Venta_playa#)
																</cfquery>
																<cfelse>
																	<cfquery  name="qryInsEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																		UPDATE Empleado_dia_semana_ano
																		SET Concepto_dia_trabajado_id=#Concepto_dia_trabajado_id#,
																			Venta_pares=#Venta_pares#,
																			Pares_descontados=#Pares_descontados#
																		WHERE Empleado_dia_semana_ano_id=#qryEmpleados.Empleado_dia_semana_ano_id# and origen_id=2
																	</cfquery>
															</cfif>
														</cfloop>
													</cfoutput>
													<cfspreadsheet action="read" columnnames="Deduccion_id, Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Deduccion_recurrente_id, Comentaios, Tienda_semana_ano_id, Control_id, origen_id" query="deducciones" src="#prodThumbDir#/#prodThumbFile#" sheet="3" excludeHeaderRow = "true" headerrow="1">
													<cfoutput>
														<cfset tiendasemanalist=valuelist(deducciones.Tienda_semana_ano_id)>
														<cfquery  name="qrydeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
															DELETE Deduccion
															WHERE Tienda_semana_ano_id in (#tiendasemanalist#)
														</cfquery>
														<cfloop query="deducciones">
															<cfquery  name="qryInsdeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																INSERT INTO Deduccion(Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Deduccion_recurrente_id, Comentaios, Tienda_semana_ano_id, Control_id, origen_id) 
																values(#Tipo_deduccion_id#,#Empleado_id#, '#Fecha#', #Semana_ano_id#, #Monto#, '#Fecha_movimiento#' , '#Deduccion_recurrente_id#', '#Comentaios#', #Tienda_semana_ano_id#, #Control_id#, #origen_id#)
															</cfquery>
														</cfloop>
													</cfoutput>
													<cfspreadsheet action="read" columnnames="Empleado_semana_ano_id, Tienda_semana_ano_id, Empleado_id, Sueldo_base, Pares_vendidos, Venta_playa, Pares_descontados, Comision_x_par, Comision_x_par_playa, Comision_x_pagar, Otras_deducciones, Otras_percepciones, Otras_comisiones, Sueldo_pagar, Control_id, origen_id, Pago_Efectivo_bit" query="empleadossem" src="#prodThumbDir#/#prodThumbFile#" sheet="4" excludeHeaderRow = "true" headerrow="1">
													<cfoutput>
														<cfset tiendasemanalist=valuelist(empleadossem.Tienda_semana_ano_id)>
														<cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
															DELETE Empleado_semana_ano
															WHERE Tienda_semana_ano_id in (#tiendasemanalist#)
														</cfquery>
														<cfloop query="empleadossem">
															<cfquery  name="qryInsEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																INSERT INTO Empleado_semana_ano(Tienda_semana_ano_id, Empleado_id, Sueldo_base, Pares_vendidos, Pares_descontados, Comision_x_par, Comision_x_par_playa, Comision_x_pagar, Otras_deducciones, Otras_percepciones, Otras_comisiones, Sueldo_pagar, Control_id, origen_id, Pago_Efectivo_bit, Pares_playa) 
																values(#Tienda_semana_ano_id#,#Empleado_id#,#Sueldo_base#, #Pares_vendidos#, #Pares_descontados#, #Comision_x_par#, #Comision_x_par_playa#, #Comision_x_pagar#, #Otras_deducciones#, #Otras_percepciones#, #Otras_comisiones#, #Sueldo_pagar#, #Control_id#, #origen_id#, '#Pago_Efectivo_bit#', #Venta_playa#)
															</cfquery>
														</cfloop>
													</cfoutput>
													<cfspreadsheet action="read" columnnames="Percepcion_id, Tipo_Percepcion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Deduccion_recurrente_id, Comentaios, Tienda_semana_ano_id, Control_id, origen_id" query="Percepcion" src="#prodThumbDir#/#prodThumbFile#" sheet="5" excludeHeaderRow = "true" headerrow="1">
													<cfoutput>
														<cfif Percepcion.recordcount neq 0>
															<cfset tiendasemanalist=valuelist(Percepcion.Tienda_semana_ano_id)>
															<cfquery  name="qrydeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																DELETE Percepcion
																WHERE Tienda_semana_ano_id in (#tiendasemanalist#)
															</cfquery>
															<cfloop query="Percepcion">
																<cfquery  name="qryInsdeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																	INSERT INTO Percepcion(Tipo_Percepcion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Deduccion_recurrente_id, Comentaios, Tienda_semana_ano_id, Control_id, origen_id) 
																	values(#Tipo_Percepcion_id#,#Empleado_id#, '#Fecha#', #Semana_ano_id#, #Monto#, '#Fecha_movimiento#' , '#Deduccion_recurrente_id#', '#Comentaios#', #Tienda_semana_ano_id#, #Control_id#, #origen_id#)
																</cfquery>
															</cfloop>
														</cfif>
													</cfoutput>
													<cfspreadsheet action="read" columnnames="Deduccion_recurrente_id, tipo_deduccion_id, Empleado_id, Semana_ano_id, Fecha, Monto_inicial, resto, deduccion_semana, inicio_semana_ano_id, Comentarios, Automatica_bit, Semana_cambio_id" query="deduccionrec" src="#prodThumbDir#/#prodThumbFile#" sheet="6" excludeHeaderRow = "true" headerrow="1">
													<cfoutput>
														<cfloop query="deduccionrec">
															<cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																SELECT Deduccion_recurrente_id
																FROM Deduccion_recurrente
																WHERE Deduccion_recurrente_id=#Deduccion_recurrente_id#
															</cfquery>
															<cfif qryEmpleados.recordcount eq 0>
																<cfquery  name="qryInsEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																	INSERT INTO Deduccion_recurrente(Deduccion_recurrente_id, tipo_deduccion_id, Empleado_id, Semana_ano_id, Fecha, Monto_inicial, resto, deduccion_semana, inicio_semana_ano_id, Comentarios, Automatica_bit, Semana_cambio_id) 
																	values(#Deduccion_recurrente_id#, #tipo_deduccion_id#, #Empleado_id#, #Semana_ano_id#, '#Fecha#', #Monto_inicial#, #resto#, #deduccion_semana#, #inicio_semana_ano_id#, '#Comentarios#', '#Automatica_bit#', #Semana_cambio_id#)
																</cfquery>
																<cfelse>
																	<cfquery  name="qryInsEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																		UPDATE Deduccion_recurrente
																		SET resto=#resto#,
																			Semana_cambio_id=#Semana_cambio_id#
																		WHERE Deduccion_recurrente_id=#Deduccion_recurrente_id#
																	</cfquery>
															</cfif>
														</cfloop>
													</cfoutput>
													El archivo se subió con exito
												</cfif>
											</CFIF>
										</td>
									</tr>
								</tbody>
							</table>
							<table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
								<tr>
									<td>
										<div align="left">
											<table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
												<tr>
													<td width="58%">
														<table border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
																		<tr>
																			<td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
																		</tr>
																	</table>
																</td>
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
																		<tr>
																			<td><a href="menu.cfm" class="style9">Menu</a></td>
																		</tr>
																	</table>
																</td>
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
																		<tr>
																			<td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>                      
													</td>
													<td width="42%" bgcolor="#FFFFFF">Usuario: 
														<cfoutput>
															<div>#Session.nombre#</div>
														</cfoutput>
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>
						</div>
						<div align="right"></div>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>

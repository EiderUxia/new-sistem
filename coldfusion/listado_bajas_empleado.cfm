<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Bajas Empleados por tienda</title>
    <link rel="stylesheet" type="text/css" href="default.css" media="screen"/>
    <link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
    <SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
    <cfif not isdefined("fecha")>
      <cfset fecha="1/"&LSDateFormat(now(),"mm/yyyy")>
      <cfset fechafin=dateadd("m",1,now())>
      <cfset fechafin=LSDateFormat(fechafin,"mm")&"/1/"&LSDateFormat(fechafin,"yyyy")>
      <cfset fechafin=LSDateFormat(dateadd("d",-1,fechafin),"dd/mm/yyyy")>
    </CFIF>
    <cfif isdefined("empleadoid")>
      <cftransaction>
        <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          UPDATE Empleado 
          SET Activo=0,
          Baja_fecha=#createodbcdate(now())#
          WHERE Empleado_id in (#empleadoid#)
        </cfquery>
        <cfloop list="#empleadoid#" index="i">
          <cfquery name="qryEmpleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT Empleado_hist_id FROM Empleado_hist
            WHERE Empleado_id=#i# and Fecha_baja is null
          </cfquery>
          <cfif qryempleado.recordcount neq 0>
            <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              UPDATE Empleado_hist 
              SET Fecha_baja=#createodbcdate(now())#
              WHERE Empleado_hist_id=#qryEmpleado.Empleado_hist_id#
            </cfquery>
            <cfelse>
              <cfquery name="qryEmpleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                SELECT Ingreso_fecha FROM Empleado
                WHERE Empleado_id=#i#
              </cfquery>
              <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                INSERT INTO Empleado_hist (Fecha_alta, Fecha_baja, Empleado_id)
                VALUES (#LSdateformat(qryEmpleado.Ingreso_fecha,"mm/dd/yyyy")#, #createodbcdate(now())#, #i#)
              </cfquery>
          </cfif>
        </cfloop>
      </cftransaction>
    </cfif>
   
  </head>

  <body>
    <div align="center">
      <table width="772" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">
              <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
                <tbody>
                  <tr>
                    <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Bajas de empleado</span></div></td>
                  </tr>
                  <tr>
                    <td align="left">
                      <cfquery name="qrytiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                        SELECT Tienda_id, tienda
                        FROM Tienda
                        WHERE Activo=1
                      </cfquery>
                      <cfquery name="qryPuesto" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                        SELECT *
                        FROM Puesto
                        WHERE activo=1
                        ORDER BY Puesto
                      </cfquery>
                      <form method="post" name="forma" action="listado_bajas_empleado.cfm" onSubmit="return valida2(this)">
                        <input type="hidden" name="listo" value="0" />
                        <table width="726" height="25" border="0" cellpadding="5" cellspacing="0">
                          <tr>
                            <td width="45" valign="top">Tienda</td>
                            <td width="123" valign="top">
                              <select name="tiendaid" id="tiendaid">
                                <option value="0">Todas</option>
                                <cfoutput query="qrytiendas">
                                  <option value="#Tienda_id#" <cfif isdefined("tiendaid") and tiendaid eq Tienda_id> selected="selected"</cfif>>#tienda#</option>
                                </cfoutput>
                              </select>
                            </td>
                            <td width="42" valign="top">Puesto</td>
                            <td width="117" valign="top">
                             
                              <select name="puestoid" id="puestoid" multiple="multiple" >
                                <option value="0">Todas</option>
                                <cfoutput query="qryPuesto">
                                  <option value="#puesto_id#" <CFIF isdefined ("puestoid") and ListFindNoCase(puestoid, puesto_id)> selected="selected"</cfif>>#puesto#</option>
                                </cfoutput>
                              </select>
                            </td>
                            <td width="40" valign="middle"></td>
                            <td width="199" colspan="2">&nbsp;<input type="submit" value="buscar" /></td>
                          </tr>
                        </table>
                      </form>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" height="37" valign="middle" width="756">
                      <form method="post" name="forma1" action="listado_bajas_empleado.cfm" onSubmit="return valida(this)">
                        <input type="hidden" value="0" name="listo" />
                        <table border="1" align="center">
                          <cfquery name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT Empleado.Empleado_id, Empleado.nombre, empleado.Apellido_pat, empleado.Apellido_mat, Puesto.puesto, Tienda.Tienda, empleado.Nombre +' '+ empleado.Apellido_Pat + ' '+ empleado.Apellido_Mat as busnombre FROM Empleado
                            INNER JOIN Puesto ON Puesto.puesto_id=Empleado.puesto_id
                            INNER JOIN Tienda ON Tienda.Tienda_id=empleado.tienda_id
                            WHERE Empleado.Activo=1
                            <cfif isdefined("tiendaid") and tiendaid neq 0> and Tienda.tienda_id=#tiendaid#</cfif>
                            <cfif isdefined("puestoid") and puestoid neq 0> and Puesto.Puesto_id in (#Puestoid#)</cfif>
                            ORDER BY Tienda.Tienda, empleado.Apellido_pat, empleado.Apellido_mat, Empleado.nombre
                          </cfquery>
                          <tr>
                            <td class="style7b">ID</td>
                            <td class="style7b">Nombre</td>
                            <td class="style7b">Tienda</td>
                            <td class="style7b">Puesto</td>
                            <td class="style7b">Baja</td>
                          </tr>
                          <cfoutput query="qryEmpleados">
                            <tr>
                              <td>#Empleado_id#</td>
                              <td><a href="editempleadostienda.cfm?buscarempleado=#busnombre#<cfif isdefined("tiendaid") and tiendaid neq 0>&tiendabakid=#tiendaid#</cfif>">#Apellido_pat# #Apellido_mat# #NOmbre#</a></td>
                              <td>#tienda#</td>
                              <td>#puesto#</td>
                              <td><input type="checkbox" name="empleadoid" value="#empleado_id#" /></td>
                            </tr>
                          </cfoutput>
                          <cfif (session.usuarioid eq 5 or session.usuarioid eq 4 or session.usuarioid eq 1 or session.usuarioid eq 9 or session.usuarioid eq 10) and qryEmpleados.recordcount neq 0>
                            <tr>
                              <td colspan="5" align="center"><input type="submit" value="Baja" /></td>
                            </tr>
                          </cfif>
                        </table>
                      </form>
                    </td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
                <tr>
                  <td>
                    <div align="left">
                      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                        <tr>
                          <td width="58%">
                            <table border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="menu_catalogos.cfm" class="style9">Regresar</a></td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="menu.cfm" class="style9">Menu</a></td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>                      
                          </td>
                          <td width="42%" bgcolor="#FFFFFF">Usuario: 
                            <cfoutput>
                              <div>#Session.nombre#</div>
                            </cfoutput>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

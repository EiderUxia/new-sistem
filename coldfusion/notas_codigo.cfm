<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panel de control</title>

    <!--- Link para leer boostrap--->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    
    <!--- Tipografia Lato--->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    
    <!--- verificar si tiene sesion iniciada --->
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>

    <!--- En que a�o estamos --->
    <cfif not isdefined("anosel")>
      <cfset anosel=year(now())>
    </cfif>

    <style>
      body{
        font-family: 'Lato', sans-serif;
      }
      .content{
        width: 80vw;
        height: max-content;
        margin:auto;
      }
      .contajus{
        width: 80%;
        margin:auto;
      }
    </style>

    <script language="JavaScript">
      function submit2(){
        document.forma.semana.value=0;
        document.forma.submit();
      }
    </script>
  </head>

  <body>
    <!--- Cabecera, logo Erez--->
    <div class="cabecera"><cfinclude template="top.cfm"></div>

    <!--- Todo el contenido del Body menos la cabecera--->
    <div class="content">


      <!--- Renglon con 2 columnas--->
      <div class="row contajus pb-5 pt-4 fs-7">
          <!--- Primera columna -> Botones Regresar y salir del sistema --->
          <div class="col d-flex align-items-center">
            <a href="menu_nomina.cfm" class="btn btn-primary me-3">Regresar</a>
            <a href="logout.cfm" class="btn btn-warning">Salir del sistema</a>
          </div>
          <!--- Segunda columna nombre del usuario--->
          <div class="col">
            <cfoutput>
              <div class="text-center fw-semibold ">Usuario: #Session.nombre#</div>
            </cfoutput>
          </div>
      </div>


      <!--- Mostrar paso en el que vamos --->
      <div align="center" class="pb-5 fw-bold fs-5">
        <p>Panel de control <cfif session.tienda eq 0>(Asistencia)</cfif></p>
      </div>


      <form method="post" name="forma" action="panel_control.cfm">
        <cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT * FROM Tienda
            <cfif Session.tienda neq 0>WHERE Tienda_id=#Session.tienda#</cfif>
            ORDER BY Tienda
        </cfquery>
        <cfif isdefined("tiendaid")>
            <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                SELECT Semana_ano.semana_ano_id, semana as semanaid
                FROM Semana_ano INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.semana_ano_id=Semana_ano.Semana_ano_id
                WHERE ano=#anosel# and Tienda_id=#tiendaid#
                Order BY Semana_ano.semana_ano_id desc
            </cfquery>
              <cfelse>
              <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  SELECT semana_ano_id, semana as semanaid FROM Semana_ano 
                  WHERE ano=#anosel# 
                  Order BY Semana_ano.semana_ano_id desc
              </cfquery>
        </cfif>

        <!--- Filtros para seleccionar tienda, a�o y semana--->
        <div class="row pb-3 fs-6">
          <div class="col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">Tienda:</p>
            <select class="form-select" name="tiendaid" onchange="submit2(this);">
              <option value="0" >Seleccionar</option>
              <CFOUTPUT query="Tiendas">
                <option value="#Tienda_id#" <cfif isdefined("tiendaid") and tienda_id eq tiendaid> selected="selected"</cfif>>#Tienda#</option>
              </CFOUTPUT>
            </select>
          </div>
          <div class="col col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">A�o:</p>
            <select class="form-select" name="anosel" onchange="submit(this);">
              <cfoutput>
                <cfset ano=lsdateformat(now(),"yyyy")>
                <cfset ano2=ano+3>
                <cfset ano=2011>
                <cfloop from="#ano#" to="#ano2#" index="i">
                  <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                </cfloop>
              </cfoutput>
            </select>
          </div>
          <div class="col col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">Semana:</p>
            <select class="form-select" name="semana" onchange="submit(this);">
              <cfoutput>
                <option value="0" >Seleccionar</option>
                <cfloop query="qrySemanas">
                  <option value="#Semana_ano_id#" <cfif (isdefined("semana") and Semana_ano_id eq semana) > selected="selected"</cfif>>#semanaid#</option>
                </cfloop>
              </cfoutput>
            </select>
          </div>
        </div>

        <table class="table table-hover">
          <cfif isdefined("tiendaid") and tiendaid neq 0 and qrySemanas.recordcount neq 0 and (semana neq 0 and semana neq "")>
            <cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT
              Tienda_semana_ano_id,
              Tienda_id,
              Semana_ano_id,
              Comentarios,
              Usuario_autorizo_id,
              Terminada,
              Autorizo_cont,
              Autorizo_cont_id,
              Comentario_autorizo,
              Pagada,
              Usuario_pagado_id,
              publicar,
              autorizoencargada,
              Usuario_autorizoencargada_id,
              Pares_descontados,
              Pares_agregados,
              Control_id,
              origen_id
              FROM Tienda_semana_ano 
              WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
            </cfquery>
            <cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT Distinct Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo ,  Puesto, Efectivo_bit, sueldo_base
              FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
              INNER JOIN Empleado_dia_semana_ano ON Empleado_dia_semana_ano.Empleado_id=Empleado.Empleado_id
              WHERE Tienda_semana_ano_id=<cfif qrytiendasemana.recordcount neq 0 >#qrytiendasemana.Tienda_semana_ano_id#<cfelse>0</cfif>
              ORDER BY Nombre
            </cfquery>

            <cfquery name="qrySemanaDias" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT * FROM Dia_Semana_ano
              WHERE Semana_ano_id=#semana#
            </cfquery>
            <tr>
              <td align="center" valign="middle"><p>ID</p></td>
              <td align="center" valign="middle"><p>Nombre</p></td>
              <cfset listfecha="">
              <cfoutput query="qrySemanaDias">
                <td align="center" valign="middle">#DayofWeekAsString(DayOfWeek(dia))#<br /> #LSDateFormat(dia,"dd-mmm-yyyy")#</td>
                <cfset listfecha="#listfecha#,#LSDateFormat(dia,"yyyy-mm-dd")#">
              </cfoutput>
              <cfset listfecha=Mid(listfecha, 2, 100)>
            </tr>
            <cfset diaC="">
            <cfoutput query="qryempleados">
              <cfquery name="checkIn" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                SELECT TOP 1 Empoloyee_id, Checador_id, Checador.TIME
                FROM Checador
                WHERE Empoloyee_id='#Empleado_id#' and Date = '#diaC#'
                ORDER BY Checador_id ASC;
                </cfquery>
                <cfquery name="checkOut" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  SELECT TOP 1 Empoloyee_id, Checador_id, Checador.TIME
                  FROM Checador
                  WHERE Empoloyee_id='#Empleado_id#' and Date = '#diaC#'
                  ORDER BY Checador_id DESC;
                </cfquery>
              <tr>
                  <td align="left" valign="middle">No. #Empleado_id#</td>
                  <td align="left" valign="middle">#Nombre#<br />#Puesto#</td>
                  
                  <cfloop from="1" to="#listlen(listfecha)#" index="X">
                    <cfset diaC="#ListGetAt(listfecha,X)#">
                    <td align="left" valign="middle">#diaC# </br> #checkIn.TIME# /</br> #checkOut.TIME#</td>
                  </cfloop>
                  
              </tr>
            </cfoutput>
          </cfif>          
        </table>

      </form>
  </body>
</html>










<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Men&uacute;</title>
		<CFIF not isdefined("session.usuarioid")>
			<script language="JavaScript" type="text/javascript">
				alert("Usted no est� dentro del sistema");
				window.location="index.cfm";
			</script>
			<CFABORT>
		</CFIF>
		<cfif isdefined("valor") and (valor eq 1 or valor eq 2)>
			<cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
				SELECT Terminada FROM Tienda_semana_ano  
				WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
			</cfquery>
			<cfif qrytiendasemana.Terminada eq 0 or qrytiendasemana.recordcount eq 0>
				<cfquery name="qryempleadoida" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
					SELECT Empleado_dia_semana_ano_id, Tienda
					FROM Empleado_dia_semana_ano INNER JOIN Empleado ON Empleado.Empleado_id=Empleado_dia_semana_ano.Empleado_id
					INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
					INNER JOIN TIenda ON Tienda.tienda_id=Empleado.tienda_id
					INNER JOIN Tienda_semana_ano ON (Tienda_semana_ano.Tienda_id=Tienda.Tienda_id and Tienda_semana_ano.Semana_ano_id=Dia_semana_ano.Semana_ano_id)
					WHERE Tienda_semana_ano.Tienda_id=#tiendaid# and Tienda_semana_ano.Semana_ano_id=#semana#  and Tienda_semana_ano.Tienda_semana_ano_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
				</cfquery>
				<cfquery name="qryempleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
					SELECT DISTINCT Empleado.Empleado_id, Tienda_semana_ano.Semana_ano_id, max(dia) as fecha, Tienda_semana_ano.Tienda_semana_ano_id
					FROM Empleado_dia_semana_ano INNER JOIN Empleado ON Empleado.Empleado_id=Empleado_dia_semana_ano.Empleado_id
					INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
					INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
					WHERE Tienda_semana_ano.Tienda_id=#tiendaid# and Tienda_semana_ano.Semana_ano_id=#semana#
					GROUP BY Empleado.Empleado_id, Tienda_semana_ano.Semana_ano_id, Tienda_semana_ano.Tienda_semana_ano_id
				</cfquery>
				<cftransaction>
					<cfif qryempleadoida.recordcount eq 0>
						<cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo
							FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
							WHERE tienda_id=#tiendaid# and Empleado.activo=1
							ORDER BY Nombre
						</cfquery>
						<cfquery name="qrySemanaDias" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT * FROM Dia_Semana_ano
							WHERE Semana_ano_id=#semana#
						</cfquery>
						<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="ressemana">
							INSERT INTO Tienda_semana_ano (Tienda_id, Semana_ano_id, Comentarios, origen_id )
							VALUES (#tiendaid#, #semana#, '#comentario#', 2)
						</cfquery>
						<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" >
							Update Tienda_semana_ano
							SET Control_id=#ressemana.IDENTITYCOL#
							<cfif valor eq 2>, Terminada=1, Usuario_autorizo_id=#Session.usuarioid#</cfif>
							WHERE Tienda_semana_ano_id=#ressemana.IDENTITYCOL#
						</cfquery>
						<cfoutput query="qryempleados">
							<cfset empleadoid=Empleado_id>
							<cfloop query="qrySemanaDias">
								<cfset diaasistencia= evaluate("dia#Dia_Semana_ano_id#0#Empleadoid#")>
								<cfif diaasistencia neq "" and diaasistencia neq 0>
									<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="ressemanaemp">
										INSERT INTO Empleado_dia_semana_ano (Empleado_id, Dia_Semana_ano_id, Concepto_dia_trabajado_id, origen_id, Tienda_semana_ano_id)
										VALUES (#Empleadoid#, #Dia_Semana_ano_id#, #diaasistencia#, 2, #ressemana.IDENTITYCOL#)
									</cfquery>
									<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
										Update Empleado_dia_semana_ano
										SET Control_id=#ressemanaemp.IDENTITYCOL#
										WHERE Empleado_dia_semana_ano_id=#ressemanaemp.IDENTITYCOL#
									</cfquery>
								</cfif>
							</cfloop>
						</cfoutput>
						<cfquery name="qryempleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT DISTINCT Empleado.Empleado_id, Tienda_semana_ano.Semana_ano_id, max(dia) as fecha, Tienda_semana_ano.Tienda_semana_ano_id
							FROM Empleado_dia_semana_ano INNER JOIN Empleado ON Empleado.Empleado_id=Empleado_dia_semana_ano.Empleado_id
							INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
							INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
							WHERE Tienda_semana_ano.Tienda_id=#tiendaid# and Tienda_semana_ano.Semana_ano_id=#semana#
							GROUP BY Empleado.Empleado_id, Tienda_semana_ano.Semana_ano_id, Tienda_semana_ano.Tienda_semana_ano_id
						</cfquery>
						<cfquery name="qryempleadoida" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT Distinct Empleado_dia_semana_ano_id, Tienda
							FROM Empleado_dia_semana_ano INNER JOIN Empleado ON Empleado.Empleado_id=Empleado_dia_semana_ano.Empleado_id
							INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
							INNER JOIN TIenda ON Tienda.tienda_id=Empleado.tienda_id
							INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
							WHERE Tienda_semana_ano.Tienda_id=#tiendaid# and Tienda_semana_ano.Semana_ano_id=#semana#  and Tienda_semana_ano.Tienda_semana_ano_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
						</cfquery>
						<cfelse>
							<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
								UPDATE Tienda_semana_ano 
								SET Comentarios='#comentario#'
								<cfif valor eq 2>
									, Terminada=1,
									Usuario_autorizo_id=#Session.usuarioid#
								</cfif>
								WHERE Tienda_semana_ano_id=#tiendasemamaid#
							</cfquery>
							<cfoutput query="qryempleadoida">
								<cfset diaasistencia= evaluate("dia#Empleado_dia_semana_ano_id#")>
								<cfif diaasistencia neq "" and diaasistencia neq 0>
									<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
										UPDATE Empleado_dia_semana_ano
										SET Concepto_dia_trabajado_id=#diaasistencia#
										WHERE Empleado_dia_semana_ano_id=#Empleado_dia_semana_ano_id#
									</cfquery>
								</cfif>
							</cfoutput>
					</cfif>
					<cfoutput query="qryempleado">
						<cfset Ventaparesfin= evaluate("pares#Empleado_id#")>
						<cfif Ventaparesfin eq "">
							<cfset Ventaparesfin=0>
						</cfif>
						<cfset Ventaotrosparesfin= evaluate("otrospares#Empleado_id#")>
						<cfif Ventaotrosparesfin eq "">
							<cfset Ventaotrosparesfin=0>
						</cfif>
						<cfset Ventabolsafin= evaluate("bolsas#Empleado_id#")>
						<cfif Ventabolsafin eq "">
							<cfset Ventabolsafin=0>
						</cfif>
						<cfset Ventaplayafin= evaluate("playas#Empleado_id#")>
						<cfif Ventaplayafin eq "">
							<cfset Ventaplayafin=0>
						</cfif>
						<cfquery name="qryempleadoida2" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT Empleado_dia_semana_ano_id, Tienda
							FROM Empleado_dia_semana_ano INNER JOIN Empleado ON Empleado.Empleado_id=Empleado_dia_semana_ano.Empleado_id
							INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
							INNER JOIN TIenda ON Tienda.tienda_id=Empleado.tienda_id
							INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
							WHERE Tienda_semana_ano.Tienda_id=#tiendaid# and Tienda_semana_ano.Semana_ano_id=#semana# and Empleado.Empleado_id=#Empleado_id#
							Order by Empleado_dia_semana_ano_id
						</cfquery>
						<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							UPDATE Empleado_dia_semana_ano
							SET Venta_pares=#Ventaparesfin#,
							Venta_pares_otra_tienda=#Ventaotrosparesfin#,
							Venta_bolsa=#Ventabolsafin#,
							Venta_playa=#Ventaplayafin#
							WHERE Empleado_dia_semana_ano_id=#qryempleadoida2.Empleado_dia_semana_ano_id#
						</cfquery>
					</cfoutput>
					<cfoutput query="qryempleado">
						<cfset portapreciofin= evaluate("portaprecio#Empleado_id#")>
						<cfif portapreciofin eq "">
							<cfset portapreciofin=0>
						</cfif>
						<cfset uniformefin= evaluate("uniforme#Empleado_id#")>
						<cfif uniformefin eq "">
							<cfset uniformefin=0>
						</cfif>
						<cfset calzaletafin= evaluate("calzaleta#Empleado_id#")>
						<cfif calzaletafin eq "">
							<cfset calzaletafin=0>
						</cfif>
						<cfset bonoadfin= evaluate("bonoad#Empleado_id#")>
						<cfif bonoadfin eq "">
							<cfset bonoadfin=0>
						</cfif>
						<cfset cambiadofin= evaluate("cambiado#Empleado_id#")>
						<cfif cambiadofin eq "">
							<cfset cambiadofin=0>
						</cfif>
						<cfquery name="qryselDeduc" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
							SELECT * FROM Deducionestmp
							WHERE semana_ano_id=#Semana_ano_id# and Empleado_id=#Empleado_id# and tipo_id=4
						</cfquery>
						<cfif qryselDeduc.recordcount eq 0>
							<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
								INSERT INTO Deducionestmp (Empleado_id, monto, semana_ano_id, tipo_id)
								Values(#Empleado_id#, #portapreciofin#, #Semana_ano_id#, 4)
							</cfquery>
							<cfelse>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deducionestmp
									SET monto=#portapreciofin#
									WHERE Empleado_id=#Empleado_id# and semana_ano_id=#semana_ano_id# and tipo_id=4
								</cfquery>
						</cfif>
						<cfquery name="qryselDeduc" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
							SELECT * FROM Deducionestmp
							WHERE semana_ano_id=#Semana_ano_id# and Empleado_id=#Empleado_id# and tipo_id=2
						</cfquery>
						<cfif qryselDeduc.recordcount eq 0>
							<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
								INSERT INTO Deducionestmp (Empleado_id, monto, semana_ano_id, tipo_id)
								Values(#Empleado_id#, #uniformefin#, #Semana_ano_id#, 2)
							</cfquery>
							<cfelse>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deducionestmp
									SET monto=#uniformefin#
									WHERE Empleado_id=#Empleado_id# and semana_ano_id=#semana_ano_id# and tipo_id=2
								</cfquery>
						</cfif>
						<cfquery name="qryselDeduc" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
							SELECT * FROM Deducionestmp
							WHERE semana_ano_id=#Semana_ano_id# and Empleado_id=#Empleado_id# and tipo_id=1
						</cfquery>
						<cfif qryselDeduc.recordcount eq 0>
							<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
								INSERT INTO Deducionestmp (Empleado_id, monto, semana_ano_id, tipo_id)
								Values(#Empleado_id#, #calzaletafin#, #Semana_ano_id#, 1)
							</cfquery>
							<cfelse>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deducionestmp
									SET monto=#calzaletafin#
									WHERE Empleado_id=#Empleado_id# and semana_ano_id=#semana_ano_id# and tipo_id=1
								</cfquery>
						</cfif>
						<cfquery name="qryselDeduc" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
							SELECT * FROM Deducionestmp
							WHERE semana_ano_id=#Semana_ano_id# and Empleado_id=#Empleado_id# and tipo_id=3
						</cfquery>
						<cfif qryselDeduc.recordcount eq 0>
							<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
								INSERT INTO Deducionestmp (Empleado_id, monto, semana_ano_id, tipo_id)
								Values(#Empleado_id#, #bonoadfin#, #Semana_ano_id#, 3)
							</cfquery>
							<cfelse>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deducionestmp
									SET monto=#bonoadfin#
									WHERE Empleado_id=#Empleado_id# and semana_ano_id=#semana_ano_id# and tipo_id=3
								</cfquery>
						</cfif>
						<cfquery name="qryselDeduc" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
							SELECT * FROM Deducionestmp
							WHERE semana_ano_id=#Semana_ano_id# and Empleado_id=#Empleado_id# and tipo_id=11
						</cfquery>
						<cfif qryselDeduc.recordcount eq 0>
							<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
								INSERT INTO Deducionestmp (Empleado_id, monto, semana_ano_id, tipo_id)
								Values(#Empleado_id#, #cambiadofin#, #Semana_ano_id#, 11)
							</cfquery>
							<cfelse>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deducionestmp
									SET monto=#cambiadofin#
									WHERE Empleado_id=#Empleado_id# and semana_ano_id=#semana_ano_id# and tipo_id=11
								</cfquery>
						</cfif>
					</cfoutput>
					<cfif valor eq 2>
						<cfoutput query="qryempleado">
							<cfset portapreciofin= evaluate("portaprecio#Empleado_id#")>
							<cfif portapreciofin eq "">
								<cfset portapreciofin=0>
							</cfif>
							<cfset uniformefin= evaluate("uniforme#Empleado_id#")>
							<cfif uniformefin eq "">
								<cfset uniformefin=0>
							</cfif>
							<cfif isdefined("tiendaid") and (tiendaid eq 13 or tiendaid eq 19 or tiendaid eq 23 or tiendaid eq 28)><!--- Solo en la tienda 10 remate las calzaletas se usan para los pares de remate por eso la comision es de 5 pesos en lugar de 2 pesos--->
								<cfset calzaletafin= 3*evaluate("calzaleta#Empleado_id#")>
								<cfelse>
									<cfset calzaletafin= 2*evaluate("calzaleta#Empleado_id#")>
							</cfif>
							<cfif calzaletafin eq "">
								<cfset calzaletafin=0>
							</cfif>
							<cfset bonoadfin= evaluate("bonoad#Empleado_id#")>
							<cfif bonoadfin eq "">
								<cfset bonoadfin=0>
							</cfif>
							<cfset cambiadofin= evaluate("cambiado#Empleado_id#")>
							<cfif cambiadofin eq "">
								<cfset cambiadofin=0>
							</cfif>
							<cfset otrofin= evaluate("otro#Empleado_id#")>
							<cfif otrofin eq "">
								<cfset otrofin=0>
							</cfif>
							<cfif portapreciofin neq 0>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
									INSERT INTO Deduccion (Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Comentaios, origen_id, Tienda_semana_ano_id)
									Values(5, #Empleado_id#, #createodbcdatetime(now())#, #Semana_ano_id#, #portapreciofin#, #createodbcdatetime(fecha)#, 'Alta por encargada', 2, #Tienda_semana_ano_id#)
								</cfquery>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deduccion
									SET Control_id=#resdeduccion.IDENTITYCOL#
									WHERE Deduccion_id=#resdeduccion.IDENTITYCOL#
								</cfquery>
							</cfif>
							<cfif uniformefin neq 0>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion2">
									INSERT INTO Deduccion (Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Comentaios, origen_id, Tienda_semana_ano_id)
									Values(3, #Empleado_id#, #createodbcdatetime(now())#, #Semana_ano_id#, #uniformefin#, #createodbcdatetime(fecha)#, 'Alta por encargada', 2, #Tienda_semana_ano_id#)
								</cfquery>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deduccion
									SET Control_id=#resdeduccion2.IDENTITYCOL#
									WHERE Deduccion_id=#resdeduccion2.IDENTITYCOL#
								</cfquery>
							</cfif>
							<cfif calzaletafin neq 0>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resPercepcion2">
									INSERT INTO Percepcion (Tipo_percepcion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Comentaios, origen_id, Tienda_semana_ano_id)
									Values(9, #Empleado_id#, #createodbcdatetime(now())#, #Semana_ano_id#, #calzaletafin#, #createodbcdatetime(fecha)#, 'Alta por encargada', 2, #Tienda_semana_ano_id#)
								</cfquery>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Percepcion
									SET Control_id=#resPercepcion2.IDENTITYCOL#
									WHERE Percepcion_id=#resPercepcion2.IDENTITYCOL#
								</cfquery>
							</cfif>
							<cfif bonoadfin neq 0>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resPercepcion2">
									INSERT INTO Percepcion (Tipo_percepcion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Comentaios, origen_id, Tienda_semana_ano_id)
									Values(5, #Empleado_id#, #createodbcdatetime(now())#, #Semana_ano_id#, #bonoadfin#, #createodbcdatetime(fecha)#, 'Alta por encargada', 2, #Tienda_semana_ano_id#)
								</cfquery>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Percepcion
									SET Control_id=#resPercepcion2.IDENTITYCOL#
									WHERE Percepcion_id=#resPercepcion2.IDENTITYCOL#
								</cfquery>
							</cfif>
							<cfif cambiadofin neq 0>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
									INSERT INTO Deduccion (Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Comentaios, origen_id, Tienda_semana_ano_id)
									Values(11, #Empleado_id#, #createodbcdatetime(now())#, #Semana_ano_id#, #cambiadofin#, #createodbcdatetime(fecha)#, 'Alta por encargada', 2, #Tienda_semana_ano_id#)
								</cfquery>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deduccion
									SET Control_id=#resdeduccion.IDENTITYCOL#
									WHERE Deduccion_id=#resdeduccion.IDENTITYCOL#
								</cfquery>
							</cfif>
							<cfif otrofin neq 0>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resdeduccion">
									INSERT INTO Deduccion (Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Comentaios, origen_id, Tienda_semana_ano_id)
									Values(12, #Empleado_id#, #createodbcdatetime(now())#, #Semana_ano_id#, #otrofin#, #createodbcdatetime(fecha)#, 'Alta por encargada', 2, #Tienda_semana_ano_id#)
								</cfquery>
								<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
									Update Deduccion
									SET Control_id=#resdeduccion.IDENTITYCOL#
									WHERE Deduccion_id=#resdeduccion.IDENTITYCOL#
								</cfquery>
							</cfif>
						</cfoutput>
					</cfif>
				</cftransaction>
				<cfif valor eq 2>
					<cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM  Semana_ano
						WHERE Semana_ano_id=#semana#
					</cfquery>
					<cfif semana neq 0>
						<cfquery name="qryusarioautorizo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT *
							FROM Usuario
							WHERE Usuario_id=#Session.usuarioid#
						</cfquery>
						<!---
						<CFMAIL type="html" to="cesarr@erez.com.mx" from="Sistema Erez <erez@control-web.com.mx>" subject="Captura de informacion tienda #qryempleadoida.tienda#, semana #qrysemanaActual.semana# (paso 3)" username="erez@control-web.com.mx" password="erez_soporte">
							<br />
							<strong>Captura de informacion tienda #qryempleadoida.tienda#</strong> <br />
							<br />
							La tienda #qryempleadoida.tienda# ha terminado de capturar la acistencia de la semana #qrysemanaActual.semana#.<br />
							Capturada por: #qryusarioautorizo.Nombre# #qryusarioautorizo.Apellido_pat# <br />
							Es necesario entrar a la tienda http://ereztienda.control-web.com.mx para la revisi�n de d�as Laborados (paso 4)
						</CFMAIL>
						--->
					</cfif>
				</cfif>
			</cfif>
			<script language="JavaScript">
				<cfif valor eq 2>
					alert("La semana se envi� con �xito");
					<cfelse>
						alert("La semana se capturo con �xito");
				</cfif>
				//window.location="dias_laborados_editb.cfm";
			</script>
		</cfif>

		<cfif not isdefined("anosel")>
			<cfset anosel=year(now())>
		</cfif>
		<cfif Session.tienda neq 0 and not isdefined("tiendaid")><cfset tiendaid=Session.tienda><cfset semana=0></cfif>
		<!---cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT *, DATEadd(d,-13,GETDATE())
		FROM Semana_ano INNER JOIN Dia_semana_ano ON Semana_ano.Semana_ano_id=Dia_semana_ano.Semana_ano_id
		WHERE YEAR(Dia)=YEAR(DATEadd(d,-6,GETDATE())) AND MONTH(Dia)=MONTH(DATEadd(d,-6,GETDATE())) AND DAY(Dia)=DAY(DATEadd(d,-6,GETDATE()))
		</cfquery--->
		<cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT *
			FROM Semana_curso
		</cfquery>
		<cfif not isdefined("semana") or semana eq 0><cfset semana=qrysemanaActual.Semana_ano_id></cfif>
		<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
		<script language="JavaScript">
			function valida(val){
				if (val==2){
					for (i = 1; i <= document.forma.contador.value; i++) {
						descanso=0;
						$.each($(".diatrab"+i), function(index, value){
							if (value.value==6 || value.value==12 || value.value==14) {
								descanso=1;
							}
						});
						incapacidad=1;
						$.each($(".diatrab"+i), function(index, value){
							if (value.value!=9 && value.value!=10) {
								incapacidad=0;
							}
						});
						if (descanso==0 && incapacidad==0){
							alert("El empleado No. "+i+" no tiene un descanso o descanso trabajado");
							return false;
						}
					}
				}
				if (val==2){
					answer = confirm("�Esta seguro que desea enviar la asistencia?","si","no");
					if (!answer){
						alert("La asistencia no se envi�");
						return false;
					}
					$('#botonenviar').hide();
				}
				if (document.forma.comentario.value==""){
					alert("Favor de ingresar el comentario");
					return false;
				}
				document.forma.valor.value=val;
				document.forma.submit();
			}
			function submit2(){
				document.forma.semana.value=0;
				document.forma.submit();
			}
			function sihaypares(pares){
				if (pares==0){
					alert("Recuerda incluir los pares vendidos de la tienda (estos son de otra tienda)");
				}
			}
		</script>
		<style type="text/css">
			<!--
			.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
			.style9 {color: #FFFFFF}
			.style11 {font-size: 18px}
			-->
		</style>
	</head>

	<body>
		<div align="center">
			<table width="1072" border="0" cellpadding="4" cellspacing="4">
				<!--- Encabezado --->
				<tr>
					<td width="860" align="center" height="91" valign="middle">
						<table border="0">
							<tr>
								<td>
									<table border="0" width="456" height="147" >
										<tr>
											<td width="221" height="100"><img src="img/topc.jpg" /></td>
											<td width="225"><font face="Arial, Helvetica, sans-serif" size="+3">Tienda</font></td>
										</tr>
										<tr>
											<td colspan="2">
												<div align="center" class="style11">
													<p>D&iacute;as  laborados por tienda <cfif session.tienda eq 0>(paso3)</cfif></p>
												</div>
											</td>
										</tr>
									</table>
								</td>
								<td>
									<table border="0" width="400"> 
										<tr><td colspan="4" align="center"><b>Claves</b></td></tr>
										<tr><td><b>A</b></td><td>Asistencia</td><td><b>FJ</b></td><td>Falta Justificada</td></tr>
										<!---
										<tr><td><b>B</b></td><td>Baja</td><td><b>I</b></td><td>Cambio de tienda</td></tr>--->
										<tr><td><b>I</b></td><td>Incapacidad Pagada</td><td><b>P</b></td><td>Permiso Pagado</td></tr>
										<tr><td><b>D</b></td><td>Descanso</td><td><b>PNP</b></td><td>Permiso No Pagado</td></tr>
										<tr><td><b>DT</b></td><td>Descanso Trabajado</td><td><b>V</b></td><td>Vacaciones</td></tr>
										<tr><td><b>F</b></td><td>Falta Injustificada</td><td><b>NO</b></td><td>No Activo</td></tr>
										<tr><td><b>Fes</b></td><td>Festivo</td><td colspan="2"></td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<!--- Contenido --->
				<tr>
					<td bgcolor="#FFFFFF">
						<div align="center" class="style7">
							<!--- Botones y nombre de usuario --->
							<table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
								<tr>
									<td>
										<div align="left">
											<table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
												<tr>
													<td width="58%">
														<table border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
																		<tr>
																			<td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
																		</tr>
																	</table>
																</td>
																<!---
																<td width="186" height="26">
																<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
																<tr>
																	<td><a href="menu.cfm" class="style9">Menu</a></td>
																</tr>
																</table></td>--->
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
																		<tr>
																			<td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>                      
													</td>
													<td width="42%" bgcolor="#FFFFFF">Usuario: 
														<cfoutput>
															<div>#Session.nombre#</div>
														</cfoutput>
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>


							<!--- Contenido --->
							<form method="post" name="forma" action="dias_laborados_editb.cfm">
								<input name="valor" type="hidden" value="0" />
								<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
									<cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
										SELECT * FROM Tienda
										WHERE Activo=1
										<cfif Session.tienda neq 0>and Tienda_id=#Session.tienda#</cfif>
										ORDER BY Tienda
									</cfquery>
									<cfif isdefined("tiendaid")>
										<cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
											SELECT Semana_ano.semana_ano_id, semana as semanaid, Terminada
											FROM Semana_ano INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.semana_ano_id=Semana_ano.Semana_ano_id
											WHERE ano=#anosel# and Tienda_id=#tiendaid#  <cfif Session.tienda neq 0>and Semana_ano.semana_ano_id=#semana#</cfif>
											UNION ALL
											SELECT semana_ano_id, semana as semanaid, 0 as terminada FROM Semana_ano
											WHERE ano=#anosel# and semana_ano_id not in (select semana_ano_id FROM Tienda_semana_ano WHERE Tienda_id=#tiendaid#)  <cfif Session.tienda neq 0>and Semana_ano.semana_ano_id=#semana#</cfif>
											Order by semanaid
										</cfquery>
										<cfelse>
											<cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
												SELECT semana_ano_id, semana as semanaid FROM Semana_ano 
												WHERE ano=#anosel# <cfif Session.tienda neq 0>and semana_ano_id=#semana#</cfif>
											</cfquery>
									</cfif>
									<!--- filtros --->
									<tr>
										<td>
											<div align="right" class="style11">
												<table border="0" width="100%">
													<tr>
														<td>Tienda</td>
														<td>
															<select name="tiendaid" onchange="submit2(this);">
																<option value="0" >Seleccionar</option>
																<CFOUTPUT query="Tiendas">
																	<option value="#Tienda_id#" <cfif isdefined("tiendaid") and tienda_id eq tiendaid> selected="selected"</cfif>>#Tienda#</option>
																</CFOUTPUT>
															</select>
														</td>
														<td>A�o</td>
														<td>
															<select name="anosel" onchange="submit(this);">
																<cfoutput>
																	<cfset ano=lsdateformat(now(),"yyyy")>
																	<cfset ano2=ano+3>
																	<cfset ano=2011>
																	<cfloop from="#ano#" to="#ano2#" index="i">
																		<option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
																	</cfloop>
																</cfoutput>
															</select>
														</td>
														<td>Semana</td>
														<td>
															<select name="semana" onchange="submit(this);" readonly="yes">
																<cfoutput>
																	<option value="0" >Seleccionar</option>
																	<cfloop query="qrySemanas">
																		<option value="#Semana_ano_id#" <cfif (isdefined("semana") and Semana_ano_id eq semana) > selected="selected"</cfif>>#semanaid#</option>
																	</cfloop>
																</cfoutput>
															</select>
														</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
									<!--- Contenido --->
									<tr>
										<td>
											<table width="756" border="0" cellpadding="3" cellspacing="1">
												<cfif isdefined("tiendaid") and tiendaid neq 0 and qrySemanas.recordcount neq 0 and semana neq 0>
													<cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
														SELECT
														Tienda_semana_ano_id,
														Tienda_id,
														Semana_ano_id,
														Comentarios,
														Usuario_autorizo_id,
														Terminada,
														Autorizo_cont,
														Autorizo_cont_id,
														Comentario_autorizo,
														Pagada,
														Usuario_pagado_id,
														publicar,
														autorizoencargada,
														Usuario_autorizoencargada_id,
														Pares_descontados,
														Pares_agregados,
														Control_id,
														origen_id
														FROM Tienda_semana_ano 
														WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
													</cfquery>
													<cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
														SELECT Distinct Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo ,  Puesto, Efectivo_bit, sueldo_base
														FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
														INNER JOIN Empleado_dia_semana_ano ON Empleado_dia_semana_ano.Empleado_id=Empleado.Empleado_id
														WHERE Tienda_semana_ano_id=<cfif qrytiendasemana.recordcount neq 0 >#qrytiendasemana.Tienda_semana_ano_id#<cfelse>0</cfif>
														ORDER BY Nombre
													</cfquery>
													<cfif qryempleados.recordcount eq 0>
														<cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
															SELECT Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo,  Puesto, Efectivo_bit, sueldo_base
															FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
															WHERE tienda_id=#tiendaid# and Empleado.activo=1
															ORDER BY Nombre
														</cfquery>
													</cfif>
													<cfquery name="qrySemanaDias" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
														SELECT * FROM Dia_Semana_ano
														WHERE Semana_ano_id=#semana#
														ORDER BY Dia_semana_ano_id
													</cfquery>
													<!--- Contenido --->
													<tr>
														<td width="1072" valign="top" align="center">
															<table width="1072" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
																<!--- Titulos tabla --->
																<tr>
																	<td width="149" align="center" valign="middle"><p>Nombre</p></td>
																	<cfoutput query="qrySemanaDias">
																		<td width="50" align="center">#DayofWeekAsString(DayOfWeek(dia))#<br /> #LSDateFormat(dia,"dd-mmm-yyyy")#</td>
																	</cfoutput>
																	<td align="center">Pares Vendidos</td>
																	<td align="center">Pares Vendidos En Otra tienda</td>
																	<td align="center">Calzaletas <input name="calza" size="4" type="text" /></td>
																	<td align="center">Bolsas</td>
																	<td align="center">Playa</td>
																	<td align="center">Uniformes ($) <input name="unif" size="4" type="text" /><a href="javascript:void(0);" onclick="uniftodos(document.forma.unif.value);">Todos</a></td>
																	<td align="center">Bonos Adicionales ($)</td>
																	<td align="center">Portaprecio ($) <input name="porta" size="5" type="text" /><a href="javascript:void(0);" onclick="portatodos(document.forma.porta.value);">Todos</a></td>
																	<td align="center">Par cambiado ($)</td>
																	<td align="center">Otros ($)</td>
																</tr>
																<cfquery name="qryconceptosfijo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																	SELECT * FROM Concepto_dia_trabajado
																	WHERE Sueldo_fijo=1 and Activo=1
																	Order BY Concepto_dia_trabajado
																</cfquery>
																<cfquery name="qryconceptosvariable" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																	SELECT * FROM Concepto_dia_trabajado
																	WHERE sueldo_variable=1 and Activo=1 
																	Order BY Concepto_dia_trabajado
																</cfquery>
																<cfset colorfondo="##C4C4C4">
																<cfset j=0>
																<!--- Llenado info empleados --->
																<cfoutput query="qryempleados">
																	<cfif colorfondo eq "##C4C4C4">
																		<cfset colorfondo="##FFFFFF">
																		<cfelse>
																			<cfset colorfondo="##C4C4C4">
																	</cfif>
																	<cfset j=j+1>
																	<tr bgcolor="#colorfondo#">
																		<td align="left" valign="middle">No. #j# #Nombre#<br />#Puesto#<br /><cfif Sueldo_fijo eq 1>#lscurrencyformat(sueldo_base)#<cfelse>NA</cfif><br />Pago: <cfif Efectivo_bit eq 1>Efectivo<cfelse>Tarjeta</cfif></td>
																		<cfset tiposueldo=Sueldo_fijo>
																		<cfset empleadoid=Empleado_id>
																		<cfset dianum=2>
																		<cfloop query="qrySemanaDias">
																			<cfquery name="qrytrabajo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																				SELECT Empleado_dia_semana_ano.Concepto_dia_trabajado_id, Empleado_dia_semana_ano_id, Concepto_dia_trabajado, Venta_pares
																				FROM Empleado_dia_semana_ano INNER JOIN Concepto_dia_trabajado ON Concepto_dia_trabajado.Concepto_dia_trabajado_id=Empleado_dia_semana_ano.Concepto_dia_trabajado_id
																				INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
																				WHERE empleado_id=#Empleadoid# and Dia_Semana_ano_id=#Dia_Semana_ano_id# and Tienda_semana_ano.tienda_id=#tiendaid#
																				ORDER BY Empleado_dia_semana_ano_id
																			</cfquery>
																			<cfif qrytrabajo.recordcount eq 0>
																				<cfquery name="qrytrabajadescansa" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																					SELECT Empleado_id, Dia_semana_id
																					FROM  Empleado_dia_semana
																					WHERE empleado_id=#Empleadoid#
																				</cfquery>
																				<cfset listadiastrabaja=valuelist(qrytrabajadescansa.Dia_semana_id)>
																			</cfif>
																			<td>
																				<cfset diasemanaanoid=Dia_Semana_ano_id>
																				<cfif qrytiendasemana.Terminada eq 1>
																					<input type="hidden" name="dia#qrytrabajo.Empleado_dia_semana_ano_id#" value="#qrytrabajo.Concepto_dia_trabajado_id#" class="diatrab#j#" />
																					#qrytrabajo.Concepto_dia_trabajado#
																					<cfelse>
																							<cfquery name="checkIn" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																								SELECT TOP 1 Empoloyee_id, Checador_id, Checador.TIME
																								FROM Checador
																								WHERE Empoloyee_id='#Empleadoid#' and Date = '#LSDateFormat(qrySemanaDias.dia,"yyyy-mm-dd")#'
																								ORDER BY Checador_id ASC;
																							</cfquery>
																						<cfif checkIn.recordcount neq 0>
																							<input type="hidden" name="dia#qrytrabajo.Empleado_dia_semana_ano_id#" value="1" class="diatrab#j#" />
																							CA
																						<CFELSE>
																						<select name="<cfif qrytrabajo.recordcount neq 0>dia#qrytrabajo.Empleado_dia_semana_ano_id#<cfelse>dia#diasemanaanoid#0#Empleadoid#</cfif>" style="width:50px" class="diatrab#j#">
																							<cfif tiposueldo eq 1>
																									<cfloop query="qryconceptosfijo">
																										
																										<option value="#Concepto_dia_trabajado_id#" 
																											<cfif qrytrabajo.recordcount neq 0>
																												<cfif qrytrabajo.Concepto_dia_trabajado_id eq Concepto_dia_trabajado_id>
																													selected="selected"
																												</cfif>
																												<cfelseif qrytrabajadescansa.recordcount neq 0>
																													<CFIF (ListFindNoCase(listadiastrabaja, dianum) and Concepto_dia_trabajado_id eq 1) or (not ListFindNoCase(listadiastrabaja, dianum) and Concepto_dia_trabajado_id eq 6) >
																														selected="selected"
																													</CFIF>
																													<cfelse>
																														<CFIF Concepto_dia_trabajado_id eq 1>
																															selected="selected"
																														</CFIF>
																											</cfif>>
																											#Concepto_dia_trabajado#
																										</option>
																									</cfloop>
																							<cfelse>
																								<cfloop query="qryconceptosvariable">
																									<option value="#Concepto_dia_trabajado_id#" <cfif qrytrabajo.recordcount neq 0><cfif qrytrabajo.Concepto_dia_trabajado_id eq Concepto_dia_trabajado_id>selected="selected"</cfif><cfelseif qrytrabajadescansa.recordcount neq 0><CFIF (ListFindNoCase(listadiastrabaja, dianum) and Concepto_dia_trabajado_id eq 1) or (not ListFindNoCase(listadiastrabaja, dianum) and Concepto_dia_trabajado_id eq 6) >selected="selected"</CFIF><cfelse><CFIF Concepto_dia_trabajado_id eq 1>selected="selected"</CFIF></cfif>>#Concepto_dia_trabajado#</option>
																								</cfloop>
																							</cfif>
																						</select>
																						</CFIF>
																				</cfif>
																			</td>
																			<cfset dianum=dianum+1>
																			<cfif dianum eq 8>
																				<cfset dianum=1>
																			</cfif>
																		</cfloop>
																		<cfquery name="qrytrabajo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																			SELECT Empleado_dia_semana_ano.Concepto_dia_trabajado_id, Empleado_dia_semana_ano_id, Concepto_dia_trabajado, Venta_pares, Venta_pares_otra_tienda, Venta_bolsa, Venta_playa
																			FROM Empleado_dia_semana_ano INNER JOIN Concepto_dia_trabajado ON Concepto_dia_trabajado.Concepto_dia_trabajado_id=Empleado_dia_semana_ano.Concepto_dia_trabajado_id
																			WHERE empleado_id=#Empleadoid# and Dia_Semana_ano_id=#qrySemanaDias.Dia_Semana_ano_id#
																			ORDER BY Empleado_dia_semana_ano_id
																		</cfquery>
																		<!--- Ventas --->
																		<td>
																			<cfif qrytiendasemana.Terminada eq 1>
																				<input type="hidden" name="pares#Empleadoid#" size="5" value="<cfif qrytrabajo.recordcount neq 0>#qrytrabajo.Venta_pares#<cfelse>0</cfif>" />
																				#qrytrabajo.Venta_pares#
																				<cfelse>
																					<input type="text" name="pares#Empleadoid#" size="3" value="<cfif qrytrabajo.recordcount neq 0>#qrytrabajo.Venta_pares#<cfelse>0</cfif>" />
																			</cfif>
																		</td>
																		<td>
																			<cfif qrytiendasemana.Terminada eq 1>
																				<input type="hidden" name="otrospares#Empleadoid#" size="5" value="<cfif qrytrabajo.recordcount neq 0>#qrytrabajo.Venta_pares_otra_tienda#<cfelse>0</cfif>" />
																				#qrytrabajo.Venta_pares_otra_tienda#
																				<cfelse>
																					<input type="text" name="otrospares#Empleadoid#" size="3" onclick="sihaypares(document.forma.pares#Empleadoid#.value)" value="<cfif qrytrabajo.recordcount neq 0>#qrytrabajo.Venta_pares_otra_tienda#<cfelse>0</cfif>" />
																			</cfif>
																		</td>
																		<td>
																			<cfif qrytiendasemana.Terminada eq 0 or qrytiendasemana.recordcount eq 0>
																				<cfquery name="qrycalzaletatmp" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																					SELECT * FROM Deducionestmp
																					WHERE semana_ano_id=#semana# and Empleado_id=#Empleadoid# and tipo_id=1
																				</cfquery>
																				<input type="text" name="calzaleta#Empleadoid#" size="3" value="<cfif qrycalzaletatmp.recordcount neq 0>#LSNumberFormat(qrycalzaletatmp.monto, "___")#<cfelse>0</cfif>" />
																			</cfif>
																		</td>								
																		<td>
																			<cfif qrytiendasemana.Terminada eq 1>
																				<input type="hidden" name="bolsas#Empleadoid#" size="5" value="<cfif qrytrabajo.recordcount neq 0>#qrytrabajo.Venta_bolsa#<cfelse>0</cfif>" />
																				#qrytrabajo.Venta_bolsa#
																				<cfelse>
																					<input type="text" name="bolsas#Empleadoid#" size="3" value="<cfif qrytrabajo.recordcount neq 0>#qrytrabajo.Venta_bolsa#<cfelse>0</cfif>" />
																			</cfif>
																		</td>								
																		<td>
																			<cfif qrytiendasemana.Terminada eq 1>
																				<input type="hidden" name="playas#Empleadoid#" size="5" value="<cfif qrytrabajo.recordcount neq 0>#qrytrabajo.Venta_playa#<cfelse>0</cfif>" />
																				#qrytrabajo.Venta_playa#
																				<cfelse>
																					<input type="text" name="playas#Empleadoid#" size="3" value="<cfif qrytrabajo.recordcount neq 0>#qrytrabajo.Venta_playa#<cfelse>0</cfif>" />
																			</cfif>
																		</td>
																		<td>
																			<cfif qrytiendasemana.Terminada eq 0 or qrytiendasemana.recordcount eq 0>
																				<cfquery name="qryuniformetmp" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																					SELECT * FROM Deducionestmp
																					WHERE semana_ano_id=#semana# and Empleado_id=#Empleadoid# and tipo_id=2
																				</cfquery>
																				<input type="text" name="uniforme#Empleadoid#" onblur="validapesos(this)" size="3" value="<cfif qryuniformetmp.recordcount neq 0>#LSNumberFormat(qryuniformetmp.monto,"___.__")#<cfelse>0</cfif>" />
																			</cfif>
																		</td>
																		<td>
																			<cfif qrytiendasemana.Terminada eq 0 or qrytiendasemana.recordcount eq 0>
																				<cfquery name="qrybonotmp" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																					SELECT * FROM Deducionestmp
																					WHERE semana_ano_id=#semana# and Empleado_id=#Empleadoid# and tipo_id=3
																				</cfquery>
																				<input type="text" name="bonoad#Empleadoid#" size="3" onblur="validapesos(this)" value="<cfif qrybonotmp.recordcount neq 0>#LSNumberFormat(qrybonotmp.monto,"___.__")#<cfelse>0</cfif>" />
																			</cfif>
																		</td>
																		<td>
																			<cfif qrytiendasemana.Terminada eq 0 or qrytiendasemana.recordcount eq 0>
																				<cfquery name="qryportatmp" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																					SELECT * FROM Deducionestmp
																					WHERE semana_ano_id=#semana# and Empleado_id=#Empleadoid# and tipo_id=4
																				</cfquery>
																				<input type="text" name="portaprecio#Empleadoid#" size="3" onblur="validapesos(this)" value="<cfif qryportatmp.recordcount neq 0>#LSNumberFormat(qryportatmp.monto,"___.__")#<cfelse>0</cfif>" />
																			</cfif>
																		</td>
																		<td>
																			<cfif qrytiendasemana.Terminada eq 0 or qrytiendasemana.recordcount eq 0>
																				<cfquery name="qryportatmp" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																					SELECT * FROM Deducionestmp
																					WHERE semana_ano_id=#semana# and Empleado_id=#Empleadoid# and tipo_id=11
																				</cfquery>
																				<input type="text" name="cambiado#Empleadoid#" size="3" onblur="validapesos(this)" value="<cfif qryportatmp.recordcount neq 0>#LSNumberFormat(qryportatmp.monto,"___.__")#<cfelse>0</cfif>" />
																			</cfif>
																		</td>
																		<td>
																			<cfif qrytiendasemana.Terminada eq 0 or qrytiendasemana.recordcount eq 0>
																				<cfquery name="qryportatmp" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
																					SELECT * FROM Deducionestmp
																					WHERE semana_ano_id=#semana# and Empleado_id=#Empleadoid# and tipo_id=12
																				</cfquery>
																				<input type="text" name="otro#Empleadoid#" size="3" onblur="validapesos(this)" value="<cfif qryportatmp.recordcount neq 0>#LSNumberFormat(qryportatmp.monto,"___.__")#<cfelse>0</cfif>" />
																			</cfif>
																		</td>
																	</tr>
																</cfoutput>
															</table>
														</td>
													</tr>
													<cfoutput>
														<input type="hidden" name="contador" value="#j#" />
													</cfoutput>
													<script language="JavaScript">
														function portatodos(val){
															if (val == parseInt(val)){
																<cfoutput query="qryempleados">
																	document.forma.portaprecio#Empleado_id#.value=val;
																</cfoutput>
															}
															else{
																alert("Favor de colocar solo pesos sin centavos");
																return false;
															}
														}
														function uniftodos(val){
															if (val == parseInt(val)){
																<cfoutput query="qryempleados">
																	document.forma.uniforme#Empleado_id#.value=val;
																</cfoutput>
															}
															else{
																alert("Favor de colocar solo pesos sin centavos");
																return false;
															}
														}
														function calzaletas(val){
															if (val == parseInt(val)){
																<cfoutput query="qryempleados">
																	document.forma.calzaleta#Empleado_id#.value=val;
																</cfoutput>
															}
															else{
																alert("Favor de colocar solo pesos sin centavos");
																return false;
															}
														}
														function validapesos(val){
															if (val.value != parseInt(val.value)){
																alert("Favor de colocar solo pesos sin centavos");
																val.value=0;
																return false;
															}
														}
													</script>
													<cfoutput>
														<input type="hidden" name="tiendasemamaid" value="#qrytiendasemana.Tienda_semana_ano_id#" />
														<tr>
															<td>Comentarios</td>
														</tr>
														<tr>
															<td>
																<div align="center"><textarea name="comentario" cols="70" rows="5">#qrytiendasemana.Comentarios#</textarea></div>
															</td>
														</tr>
														<cfif Session.tienda eq 0 or (Session.tienda neq 0 and (qrytiendasemana.recordcount eq 0 or (qrytiendasemana.recordcount neq 0 and qrytiendasemana.Terminada eq 0)))>
															<tr>
																<td>
																	<cfif qrytiendasemana.Terminada eq 0 or qrytiendasemana.recordcount eq 0>
																		<div align="center"><input type="button" onclick="valida(1);void(0);" value="Guardar" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="valida(2);void(0);" id="botonenviar" value="Enviar" /></div>
																	</cfif>
																</td>
															</tr>
														</cfif>
													</cfoutput>
												</cfif>
											</table>
										</td>
									</tr>
								</table>
							</form>

							<!--- Botones pie de pagina --->
							<table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
								<tr>
									<td>
										<div align="left">
											<table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
												<tr>
													<td width="58%">
														<table border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
																		<tr>
																			<td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
																		</tr>
																	</table>
																</td>
																<!---
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
																		<tr>
																			<td><a href="menu.cfm" class="style9">Menu</a></td>
																		</tr>
																	</table>
																</td>--->
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
																		<tr>
																			<td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>                      
													</td>
													<td width="42%" bgcolor="#FFFFFF">Usuario: 
														<cfoutput>
															<div>#Session.nombre#</div>
														</cfoutput>
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>
						</div>
						<div align="right"></div>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>

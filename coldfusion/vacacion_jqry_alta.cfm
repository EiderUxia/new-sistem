﻿<cfsilent>
<cftransaction>
<cfset error=0>
<cfif sueldo eq "" OR prima eq "" or empleadoid eq 0 or empleadoid eq "" or semanaid eq "">
	<cfset error=1>
</cfif>
<cfif error eq 0>
<cfif Vacacionid eq 0>
	<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resVacacion">
		INSERT INTO Vacacion (Semana_ano_id, Tienda_id, Empleado_id, Comentarios, Sueldo, Prima, Observaciones, Usuario_id, Usuario_revision_id, Revisado_bit, Pago_prima_bit, Pago_sueldo_bit) 
		VALUES
		(#semanaid#, #tiendaid#, #empleadoid#, '#Comentarios#', #sueldo#, #prima#, <cfif isdefined("observaciones") >'#observaciones#'<cfelse>NULL</cfif>, #session.usuarioid#, <cfif isdefined("observaciones") >#session.usuarioid#<cfelse>NULL</cfif>, 0, #pagoprima#, #pagovac#)
</cfquery>
	<cfset Vacacionid=resVacacion.IDENTITYCOL>
	<cfset mensaje="Las Vacaciones se solicitaron con éxito">
<cfelse>
	<cfif autorizado eq 0>
    	<cfset revisadobit=0>
        <cfset autorizadobit=0>
        <cfset rechazadobit=0>
    <cfelseif autorizado eq 1>
    	<cfset revisadobit=1>
        <cfset autorizadobit=1>
        <cfset rechazadobit=0>
    </cfif>
    <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resEntrevistado">
		UPDATE Vacacion
        SET Semana_ano_id=#semanaid#,
        	Sueldo=#sueldo#,
        	Prima=#prima#,
			Observaciones=<cfif isdefined("observaciones") >'#observaciones#'<cfelse>NULL</cfif>,
			Usuario_revision_id=#session.usuarioid#,
			Revisado_bit=#revisadobit#,
            Autorizado_bit=#autorizadobit#,
            Rechazado_bit=#rechazadobit#,
            Pago_prima_bit=#pagoprima#,
            Pago_sueldo_bit=#pagovac#
		WHERE Vacacion_id=#Vacacionid#
</cfquery>
	<cfset mensaje="EL cambio se dio exitosamente">
</cfif>
<cfelse>
	<cfset mensaje="Favor de revisar los datos">
</cfif>
</cftransaction>

</cfsilent>
<cfoutput>
	<div align="center">
    #mensaje#
    </div>
	<script>
		$("##Vacacionid").val("#Vacacionid#");
   </script>
   <cfif error eq 1>
   		<cfif #Session.tienda# eq 0 >
        <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td align="left" valign="middle" bgcolor="##FFFFFF"><div align="center">
                    <a href="javascript:void(0);" onclick="valida(1,0);" class="btn-contacto">Guardar</a>
              </div></td>
              <td align="left" valign="middle" bgcolor="##FFFFFF"><div align="center">
                    <a href="javascript:void(0);" onclick="valida(1,1);" class="btn-contacto">Procesar</a>
              </div></td>
              </tr>
        </table>
        <cfelse>
        	<table width="100%" border="0" cellpadding="3" cellspacing="1">
                <tr>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><div align="center">
                        <a href="javascript:void(0);" onclick="valida(0,0);" class="btn-contacto">Solicitar</a>
                  </div></td>
                  </tr>
            </table>
        </cfif>
   </cfif>
</cfoutput>
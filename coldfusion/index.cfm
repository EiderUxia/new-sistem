<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <!--- --->
    <title>EREZ</title>
    <!--- HTML5--->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--- --->
    <link rel="apple-touch-icon" href="http://erezdecache.pcintegral.com/apple-touch-icon.png" />
    <!--- Link para leer boostrap--->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <!--- Tipograf�a Lato--->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

    <cfset clinicaid=1>
    <!--- Verificar si tiene datos --->
    <CFIF isdefined("form.usuario")>
      <cfquery name="login" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT * FROM Usuario WHERE usuario='#form.usuario#' and contrasena='#form.contrasena#' and activo_bit=1
      </cfquery>
      <!--- Si encontr� un registro--->
      <CFIF login.recordcount EQ 1>
        <!--- Guarda los datos en session --->
        <CFSET Session.usuarioid=#login.usuario_id#>
        <CFSET Session.nombre=#login.nombre#>
        <CFSET Session.tipousuario=#login.Tipo_Usuario_id#>
        <CFSET Session.tienda=#login.Tienda_id#>
        <!--- Si el tipo de usuario es 9 -> compras para mostrarles otro men�--->
        <cfif Session.tipousuario eq 9>
          <script language="JavaScript">
            <CFOUTPUT>alert("Bienvenido(a) #session.nombre#.");</CFOUTPUT>
            window.location="menu_oc.cfm";
          </script>
          <cfelse>
            <script language="JavaScript">
              <CFOUTPUT>alert("Bienvenido(a) #session.nombre#.");</CFOUTPUT>
              window.location="menu_nomina.cfm";
            </script>
        </cfif>
        <CFELSE>
        <script language="JavaScript">
          alert("Datos incorrectos o usuario in-activo");
        </script>
      </CFIF>
    </CFIF>

    <!--- Verifica que no falten datos--->
    <script language="JavaScript">
      <cfif isdefined("clinicaid")>
        function valida(){
          var l1 = document.form1.usuario.value;
          var l2 = document.form1.contrasena.value;
          var cont = 0;
          if (l1.length == 0){
            alert(" Ingrese usuario");
            return false;
          }
          if (l2.length == 0){
              alert("Ingrese contrase?a.");
            return false;
          }
          return true;
        }
      </cfif>
    </script>

    <!--- CSS --->
    <style type="text/css">
      <!--
        .style7 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; }
        .style8 {
          font-size: 24px;
          color: #000000;
        }
      -->
     
    </style>
    <style>
      body{
        min-height: 100vh;
        max-height: content;
        background-image: linear-gradient(to top, #e6e9f0 0%, #eef1f5 100%);
        background-repeat: no-repeat;
        font-family: 'Lato', sans-serif;
        font-size: 1.2rem;
      }
      #formulario{
        width: 100vw;
        margin-top: 10vh;
        display: flex;
        align-items: center;
      }
      .content{
        width: 60vw;
        height: max-content;
        margin:auto;
      }
      .cabecera{
        /*position: absolute;*/
        width: 100vw;
        z-index: 5;
      }
      .btn, .btn:hover{
        background-color: black;
        color:white;
      }
    </style>

  </head>

  <body>

    <div class="cabecera"><cfinclude template="top.cfm"></div>

    <form id="formulario" name="form1" method="post" action="index.cfm" onSubmit="return valida(this)">
      <div class="content">
      <cfoutput><input type="hidden" name="clinicaid" value="#clinicaid#" /></cfoutput>

      <p class="d-block">Usuario:</p>
      <input class="d-block form-control" type="text" name="usuario" />
      <br>
      <p class="d-block">Contrase&ntilde;a:</p>
      <input class="d-block form-control" type="password" name="contrasena" />
      <br>
      <input class="btn w-100 py-2 mb-4" type="submit" name="Submit" value="Aceptar" />
      </div>
    </form>

  </body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<link rel="stylesheet" type="text/css" href="default.css" media="screen"/>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif not isdefined("anosel")>
	<cfset anosel=year(now())>
</cfif>
<script language="JavaScript">
function valida(val){
	if (val==1){
	document.forma.valor.value=1;
	document.forma.submit();
	}
}
</script>

<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>

</head>

<body class="g-sidenav-show  bg-gray-100"> 
    
  <!---sidenav.cfm es la barra del lateral izquierdo--->
  <cfinclude template="../sidenav.cfm">
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
  <!-- Navbar -->
  <cfinclude template="../navbar.cfm">
  <!-- End Navbar -->
   

<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="rep_uniformes.cfm">
      <input name="valor" type="hidden" value="0" />
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p>Portaprecio y Uniformes</p>
            </div></td>
        </tr>
                  <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Semana_ano
                        WHERE ano=#anosel#
				  </cfquery>
<cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
SELECT *
FROM Semana_curso
</cfquery>
                  <cfset semanaslist=valuelist(qrySemanas.Semana)>
        <tr>
          <td><div align="right" class="style11"><table border="0" width="100%"><tr>
          <td>A�o</td><td>
          <select name="anosel" onchange="submit(this);">
          <cfoutput>
          <cfset ano=lsdateformat(now(),"yyyy")>
          <cfset ano2=ano+3>
          <cfset ano=2011>
						<cfloop from="#ano#" to="#ano2#" index="i">
                    <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                  </cfloop></cfoutput>
                        </select>
          </td>
          <td>Semana</td><td>
          <select name="semana">
          <cfoutput>
          	<option>Seleccionar</option>
                        <cfloop query="qrySemanas">
                    <option value="#Semana_ano_id#" <cfif qrysemanaActual.Semana_ano_id eq Semana_ano_id > selected="selected"</cfif>>#Semana#</option>
                  </cfloop></cfoutput>
                        </select>
          </td>
		</tr></table>
            </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="3" cellspacing="1">
              <tr><td>
              <div align="center"><input type="button" onclick="valida(1);void(0);" value="Buscar" /></div>
              </td></tr>
          </table></td>
        </tr>
      </table></form>
      <cfif isdefined("semana")>
<cfquery name="qryDatos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
SELECT Semana, Tienda, Tienda_semana_ano_id, Tipo_deduccion_id, SUM(monto) as monto, sum(cantidad) as cantidad from (
SELECT 'a�o '+ convert(varchar(4),ano)+'<br /> semana '+convert(varchar(2),Semana) as semana, Tienda, Tienda_semana_ano.Tienda_semana_ano_id, Tipo_deduccion_id, SUM(monto) as monto, COUNT(Monto) as cantidad
FROM Deduccion INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Deduccion.Tienda_semana_ano_id
	INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
	INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Deduccion.Semana_ano_id
WHERE Tipo_deduccion_id in (5,3) and Deduccion.Semana_ano_id in (SELECT top 5 Semana_ano_id FROM Semana_ano
WHERE Semana_ano_id>=#semana#)
GROUP BY Tienda, ano, Semana, Tienda_semana_ano.Tienda_semana_ano_id, Tipo_deduccion_id
--order by Tienda, Semana
union all
SELECT distinct  'a�o '+ convert(varchar(4),ano)+'<br /> semana '+convert(varchar(2),Semana) as semana, Tienda, Tienda_semana_ano.Tienda_semana_ano_id, 3, SUM(0) as monto, sum(0) as cantidad
FROM Tienda_semana_ano INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
	INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Tienda_semana_ano.Semana_ano_id
WHERE Tienda_semana_ano.Semana_ano_id in (SELECT top 5 Semana_ano_id FROM Semana_ano
WHERE Semana_ano_id>=#semana#)
and convert(varchar(10),semana)+tienda+convert(varchar(10),5) not in (SELECT convert(varchar(10),semana)+tienda+convert(varchar(10),Tipo_deduccion_id)
FROM Deduccion INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Deduccion.Tienda_semana_ano_id
	INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
	INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Deduccion.Semana_ano_id
WHERE Tipo_deduccion_id in (3) and Deduccion.Semana_ano_id in (SELECT top 5 Semana_ano_id FROM Semana_ano
WHERE Semana_ano_id>=#semana#))
GROUP BY Tienda, ano, Semana, Tienda_semana_ano.Tienda_semana_ano_id
--order by Tienda, Semana
union all
SELECT distinct  'a�o '+ convert(varchar(4),ano)+'<br /> semana '+convert(varchar(2),Semana) as semana, Tienda, Tienda_semana_ano.Tienda_semana_ano_id, 5, SUM(0) as monto, sum(0) as cantidad
FROM Tienda_semana_ano INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
	INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Tienda_semana_ano.Semana_ano_id
WHERE Tienda_semana_ano.Semana_ano_id in (SELECT top 5 Semana_ano_id FROM Semana_ano
WHERE Semana_ano_id>=#semana#)
and convert(varchar(10),semana)+tienda+convert(varchar(10),5) not in (SELECT convert(varchar(10),semana)+tienda+convert(varchar(10),Tipo_deduccion_id)
FROM Deduccion INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Deduccion.Tienda_semana_ano_id
	INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
	INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Deduccion.Semana_ano_id
WHERE Tipo_deduccion_id in (5) and Deduccion.Semana_ano_id in (SELECT top 5 Semana_ano_id FROM Semana_ano
WHERE Semana_ano_id>=#semana#))
GROUP BY Tienda, ano, Semana, Tienda_semana_ano.Tienda_semana_ano_id
--order by Tienda, Semana
UNION ALL
SELECT 'a�o '+ convert(varchar(4),ano)+'<br /> semana '+convert(varchar(2),Semana) as semana, 'xTotales' as Tienda, 1, Tipo_deduccion_id, SUM(monto) as monto, COUNT(Monto) as cantidad
FROM Deduccion INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Deduccion.Tienda_semana_ano_id
	INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
	INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Deduccion.Semana_ano_id
WHERE Tipo_deduccion_id in (5,3) and Deduccion.Semana_ano_id in (SELECT top 5 Semana_ano_id FROM Semana_ano
WHERE Semana_ano_id>=#semana#)
GROUP BY  ano, Semana, Tipo_deduccion_id
) as U
GROUP BY Tienda, Semana, Tienda_semana_ano_id, Tipo_deduccion_id
order by Tienda, Semana

</cfquery> 
<cfquery name="qrysemanas" dbtype="query">
	SELECT Distinct Semana
    FROM qryDatos
</cfquery>     
      <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
      	<tr>
        	<td rowspan="2">Tienda</td><cfset semana1=qryDatos.Semana>
        	<cfoutput query="qrysemanas">
            	<td colspan="2">#Semana#</td>
			</cfoutput>
        </tr>
        <tr>
        	<td>Uniformes</td>
            <td>Portaprecio</td>
        	<td>Uniformes</td>
            <td>Portaprecio</td>
        	<td>Uniformes</td>
            <td>Portaprecio</td>
        	<td>Uniformes</td>
            <td>Portaprecio</td>
        	<td>Uniformes</td>
            <td>Portaprecio</td>
        </tr>
        <cfset color="##CCCCCC">
        <cfoutput query="qryDatos" group="Tienda">
        
        <tr bgcolor="#color#">
        	<td>#Tienda#</td>
			<cfoutput group="semana">
                <cfoutput>
                	<td><cfif cantidad eq 0>0<cfelse><a href="javascript:window.open('deducciones_hist.cfm?tiendasemanaid=#Tienda_semana_ano_id#&tipoid=#Tipo_deduccion_id#','Deducciones','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);"><cfif Tipo_deduccion_id eq 3>#cantidad#<cfelse>#lscurrencyformat(monto)#</cfif></a></cfif></td>
                </cfoutput>
            </cfoutput>
        </tr>
        <cfif color eq "##CCCCCC">
       		<cfset color="##FFFFFF">
        <cfelse>
        	<cfset color="##CCCCCC">
        </cfif>
        </cfoutput>
      </table>
      </cfif>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>

<!--   Core JS Files   -->
<script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>

</body>
</html>

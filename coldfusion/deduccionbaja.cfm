<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Alta deduccion</title>
</head>
<cftransaction>
<cfquery name="qryDeduccion" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT * FROM Deduccion
    WHERE Deduccion_id=#deduccionid#
</cfquery>
<cfif qryDeduccion.Deduccion_recurrente_id neq "">
<cfquery name="qryDeduccionrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT *
    FROM Deduccion_recurrente
    WHERE Deduccion_recurrente_id=#qryDeduccion.Deduccion_recurrente_id#
</cfquery>
<cfset resto=qryDeduccionrec.resto+qryDeduccion.monto>
<cfquery name="qryAltaDeduccion" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    UPDATE Deduccion_recurrente
    SET Resto=#resto#,
    	Semana_cambio_id=#semanaid#
    WHERE Deduccion_recurrente_id=#qryDeduccion.Deduccion_recurrente_id#
</cfquery>
</cfif>
<cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    DELETE Deduccion
    WHERE Deduccion_id=#deduccionid#
</cfquery>

</cftransaction>
<cfquery name="qryDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT * FROM Deduccion INNER JOIN Tipo_deduccion ON Deduccion.Tipo_deduccion_id=Tipo_deduccion.Tipo_deduccion_id
    WHERE Semana_ano_id=#semanaid# and empleado_id=#empleadoid#
</cfquery>
<body>
<cfoutput>
   <cfset cargotot =0>
              <table border="0" align="center" width="500">
              <tr>
              	<td align="center">Historial de deducciones<br />
                	La deducci&oacute;n se elimino con &eacute;xito
                </td>
              </tr>
              <tr>
              	<td><table border="0" align="center"><tr><td>Concepto</td><td>Monto</td><td>&nbsp;</td></tr>
                	<cfloop query="qryDeducciones">
                    <tr><td>#Tipo_deduccion#</td><td>#LSCurrencyFormat(Monto)#</td><td><a href="javascript:deduccionbaja('deduccionbaja.cfm','#Deduccion_id#');void(0);"><img src="img/icono_tacha.png" width="20" height="20" alt="borrar" border="0" /></a></td></tr>
                    <cfset cargotot =cargotot+monto>
                    </cfloop>
                    <tr><td>Total</td><td>#LSCurrencyFormat(cargotot)#</td><td>&nbsp;</td></tr>
                </table></td>
              </tr>
              </table>
</cfoutput>
</body>
</html>
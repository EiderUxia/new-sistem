<cfparam name="form.InputExcelFile" default="">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style8 {
	font-size: 24px;
	color: #000000;
}
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>

<cfif isdefined("llego")>
    <cftransaction>
    <cfloop list="#llego#" index="i">
        <cfquery  name="qryOrden" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            UPDATE OC_Orden_Compra_Tienda 
            SET Fecha_llegada_real=#createodbcdatetime(llegatienda)#
            WHERE Orden_Compra_tienda_id=#i#
        </cfquery>
    </cfloop>
    <cfquery  name="qryOrden" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        UPDATE OC_Orden_Compra
        SET paso_id=3
        WHERE Orden_Compra_id in (select Orden_Compra_id FROM OC_Orden_Compra_Tienda WHERE Orden_Compra_tienda_id in (#llego#))
    </cfquery>
    </cftransaction>
    <script language="JavaScript" type="text/javascript">
		alert("Los pedidos se capturaron con �xito");
	</script>
</cfif>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.5/css/bootstrap-select.min.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
    
    <tbody>
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Pedidos por llegar</span></div></td>
	  </tr>
	<tr>
      <td align="center" height="37" valign="middle" width="756">
		<cfform action="oc_entienda.cfm" method="POST" onSubmit="return valida(this)" name="frmOC">
        <table border="0" width="500" align="center">
        	<tr>
            	<td>Fecha de llegada a tienda</td>
                <td><input size="15" type="text" name="llegatienda" id="llegatienda"  value="" />
                       <img src="cal.gif" onclick="displayCalendar(document.frmOC.llegatienda,'dd/mm/yyyy',this)" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></td>
            </tr>
        </table>
      <cfquery  name="qryOrden" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT *
			FROM OC_Orden_Compra INNER JOIN OC_Orden_Compra_Tienda ON OC_Orden_Compra_Tienda.Orden_Compra_id=OC_Orden_Compra.Orden_Compra_id
			WHERE paso_id=2 and tienda_id=#session.tienda#
		</cfquery>
      <table width="900" cellpadding="2" cellspacing="2" border="1" class="tablestandard">
            <tr>
				<td>Pedido</td>
				<td>Linea</td>
				<td>OC</td>
				<td>Proveedor</td>
				<td>Marca</td>
				<td>Estilo</td>
				<td>Color</td>
				<td>Cantidad</td>
				<td>Fecha Pedido</td>
				<td>Fecha llegada Aprox</td>
                <td>Seleccionar</td>
			</tr>
            <cfoutput query="qryOrden">
			<tr>
				<td>#Pedido#</td>
				<td>#Linea#</td>
				<td>#OC#</td>
				<td>#Proveedor_id#</td>
				<td>#Marca#</td>
				<td>#Estilo#</td>
				<td>#Color#</td>
				<td>#Cantidad#</td>
				<td>#LSDateFormat(Fecha_pedido,'dd-mmm-yyyy')#</td>
				<td>#LSDateFormat(Fecha_llegada,'dd-mmm-yyyy')#</td>
                <td><input type="checkbox" name="llego" value="#Orden_Compra_tienda_id#" />
                </td>
			</tr>
            </cfoutput>
	</table>
    		<input type="submit" name="Enviar" value="Enviar" />
		</cfform>
      </td>
    </tr>
  </tbody></table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.5/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
	$('.selectpicker').selectpicker();
	function valida(){
		if ($('#llegatienda').val()==""){
			alert("favor de llenar la fecha de llegada a la tienda");
			return false;	
		}
		return true;
	}
</script>
</body>
</html>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Men�</title>

      <!--- Link para leer boostrap--->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
      <!--- Tipografia Lato--->
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

      <!--- verificar si tiene sesion iniciada --->
      <CFIF not isdefined("session.usuarioid")>
        <script language="JavaScript" type="text/javascript">
          alert("Usted no est� dentro del sistema");
          window.location="index.cfm";
        </script>
        <CFABORT>
      </CFIF>
<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>

      <!--- En que año estamos --->
      <cfif not isdefined("anosel")>
        <cfset anosel=year(now())>
      </cfif>

      <!--- Guardar registro de la semana --->
      <cfif isdefined("valor") and valor eq 2>
        <cftransaction>
          <!--- Se autoriza, se pasa al paso 6 y en el 5 se ve "nomina publicada" --->
          <cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            UPDATE Tienda_semana_ano 
            SET Publicar=1, Comentario_autorizo='#comentarioaut#'
            WHERE Tienda_semana_ano_id=#tiendasemamaid#
          </cfquery>
          <!--- Puesto, si es efectivo o por tarjeta--->
          <cfquery name="qryempleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT distinct Empleado_dia_semana_ano.Empleado_id, Tienda_semana_ano.Control_id as tienda_semana_id, Efectivo_bit, Empleado.Puesto_id
            FROM  Empleado_dia_semana_ano INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
            INNER JOIN Empleado ON Empleado.Empleado_id=Empleado_dia_semana_ano.Empleado_id
            WHERE Tienda_semana_ano.Tienda_semana_ano_id=#tiendasemamaid#
          </cfquery>
          <!--- Borra el registro de la semana del a?o y del empleado x --->
          <cfquery name="borrasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resempleado">
            DELETE Empleado_semana_ano
            WHERE Tienda_semana_ano_id=#tiendasemamaid#
          </cfquery>
          <!--- Guarda la informaci?n del empleado en empleado_semana_ano" --->
          <cfoutput query="qryempleado">
            <cfset listado=evaluate("Comisionxpagar#empleado_id#")>
            <cfset Sueldobasefin= ListGetAt(listado,1)>
            <cfset Paresvendidosfin= ListGetAt(listado,2)>
            <cfset Paresdescontadosfin= ListGetAt(listado,3)>
            <cfset Comisionxparfin= ListGetAt(listado,4)>
            <cfset Comisionxpagarfin= ListGetAt(listado,5)>
            <cfset VentasPlaya= ListGetAt(listado,6)>
            <cfset ComisionPlaya= ListGetAt(listado,7)>
            <cfset Otrasdeduccionesfin= evaluate("Otrasdeducciones#empleado_id#")>
            <cfset Otraspercepcionesfin= evaluate("Otraspercepciones#empleado_id#")>
            <cfset Otrascomisionesfin= evaluate("Otrascomisiones#empleado_id#")>
            <cfset Sueldopagarfin= evaluate("Sueldopagar#empleado_id#")>
            <cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resempleado">
              INSERT INTO Empleado_semana_ano (Tienda_semana_ano_id, Empleado_id, Sueldo_base, Pares_vendidos, Pares_descontados, Comision_x_par, Comision_x_pagar, Otras_deducciones, Otras_percepciones, Otras_comisiones, Sueldo_pagar, origen_id, Pago_Efectivo_bit, Puesto_id, Venta_playa, Comision_x_par_playa)
              VALUES (#tienda_semana_id#, #empleado_id#, #Sueldobasefin#, #Paresvendidosfin#, #Paresdescontadosfin#, #Comisionxparfin#, #Comisionxpagarfin#, #Otrasdeduccionesfin#, #Otraspercepcionesfin#, #Otrascomisionesfin#, #Sueldopagarfin#, 2, #Efectivo_bit#, #Puesto_id#, #VentasPlaya#, #comisionplaya#)
            </cfquery>
            <cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              Update Empleado_semana_ano
              SET Control_id=#resempleado.IDENTITYCOL#
              WHERE Empleado_semana_ano_id=#resempleado.IDENTITYCOL#
            </cfquery>
          </cfoutput>
        </cftransaction>

        <script language="JavaScript">
            alert("La semana se capturo con �xito");
            window.location="nomina_semana.cfm";
        </script>

      </cfif>

      <!--- "El paso se elimin� con exito" --->
      <cfif isdefined("valor") and valor eq 3>
        <!--- indica al sistema de administraci�n de bases de datos que trate varias operaciones de bases de datos como una sola transacci�n 
              Si una etiqueta cfquery genera un error dentro de un bloque cftransaction, todas las operaciones cfquery en la transacci�n retroceden --->
        <cftransaction>

          <cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            UPDATE Tienda_semana_ano 
            SET Autorizo_cont=0
            WHERE Tienda_semana_ano_id=#tiendasemamaid#
          </cfquery>
          <!--- ID Empleado de la semana dd-mm-yy de la tienda x --->
          <cfquery name="qryempleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT distinct Empleado_id, Tienda_semana_ano.Control_id as tienda_semana_id
            FROM  Empleado_dia_semana_ano INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
            WHERE Tienda_semana_ano.Tienda_semana_ano_id=#tiendasemamaid#
          </cfquery>
          <cfoutput query="qryempleado">
            <cfquery name="qrydeduccionrecur" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT *
              FROM Deduccion 
              WHERE empleado_id=#empleado_id# and Deduccion_recurrente_id IS NOT NULL and Semana_ano_id=#semana# and Automatica_bit=1
            </cfquery>
            <cfif qrydeduccionrecur.recordcount neq 0>
              <cfloop query="qrydeduccionrecur">
                <cfquery name="qryDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  UPDATE Deduccion_recurrente
                  SET Resto=#Monto#+Resto, Semana_cambio_id=#semana#
                  WHERE Deduccion_recurrente_id=#Deduccion_recurrente_id#
                </cfquery>
              </cfloop>
            </cfif>
            <cfquery name="qrydeducciondel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              DELETE Deduccion 
              WHERE empleado_id=#empleado_id# and Semana_ano_id=#semana# and Automatica_bit=1
            </cfquery>
            <cfquery name="qryprecepciondel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              DELETE Percepcion 
              WHERE empleado_id=#empleado_id# and Semana_ano_id=#semana# and Automatica_bit=1
            </cfquery>
            <cfquery name="qryprecepciondel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              DELETE Deduccion_recurrente 
              WHERE empleado_id=#empleado_id# and Semana_ano_id=#semana# and Automatica_bit=1
            </cfquery>
            <cfquery name="qrySelVaciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT *
              FROM Vacacion
              WHERE autorizado_bit=1 and procesado_bit=0 and empleado_id=#Empleado_id# and Semana_ano_id=#semana#
            </cfquery>
            <cfquery name="qryUpdVaciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              UPDATE Vacacion
              SET procesado_bit=1
              WHERE autorizado_bit=1 and procesado_bit=0 and empleado_id=#Empleado_id# and Semana_ano_id=#semana#
            </cfquery>
            <cfquery name="qryUpdVaciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              UPDATE prestamo
              SET procesado_bit=0
              WHERE autorizado_bit=1 and procesado_bit=1 and empleado_id=#Empleado_id# and Semana_ano_id=#semana#
            </cfquery>
            <cfif qrySelVaciones.recordcount neq 0>
              <cfquery name="qryDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                UPDATE Empleado
                SET dias_vacaciones=6
                WHERE empleado_id=#Empleado_id#
              </cfquery>

            </cfif>

          </cfoutput>

        </cftransaction>

        <script language="JavaScript">
            alert("El paso se elimin� con �xito");
            window.location="nomina_semana.cfm";
        </script>

      </cfif>

      <script language="JavaScript">
        function valida(val){
          <!--- En caso de que olvidaran anotar el comentario en la nomina de la semana --->
          if (document.forma.comentario.value==""){
            alert("Favor de ingresar el comentario");
            return false;
          }
          document.forma.valor.value=val;
          document.forma.submit();
        }
        function submit2(){
          document.forma.semana.value=0;
          document.forma.submit();
        }
      </script>

      <style>
        body{
          font-family: 'Lato', sans-serif;
        }
        .content{
          width: 80vw;
          height: max-content;
          margin:auto;
        }
        .contajus{
          width: 80%;
          margin:auto;
        }
      </style>
      
  </head>


	<body class="g-sidenav-show  bg-gray-100">
		<!---sidenav.cfm es la barra del lateral izquierdo--->
		<cfinclude template="../sidenav.cfm">
		<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
		  <!-- Navbar -->
		  <cfinclude template="../navbar.cfm">
		  <!-- End Navbar -->

    <!--- Todo el contenido del Body menos la cabecera--->
    <div class="content">

      <!--- Renglon con 2 columnas--->
<!---       <div class="row contajus pb-5 pt-4 fs-7"> --->
        <!--- Primera columna -> Botones Regresar y salir del sistema --->
<!---         <div class="col d-flex align-items-center"> --->
<!---           <a href="menu_nomina.cfm" class="btn btn-primary me-3">Regresar</a> --->
<!---           <a href="logout.cfm" class="btn btn-warning">Salir del sistema</a> --->
<!---         </div> --->
        <!--- Segunda columna nombre del usuario--->
<!---         <div class="col"> --->
<!---           <cfoutput> --->
<!---             <div class="text-center fw-semibold ">Usuario: #Session.nombre#</div> --->
<!---           </cfoutput> --->
<!---         </div> --->
<!---       </div> --->

      <!--- Mostrar paso en el que vamos --->
      <div align="center" class="pb-5 fw-bold fs-5">
        <p>Nomina Semana <cfif session.tienda eq 0>(paso5)</cfif></p>
      </div>

      <!--- Form de la accion --->
      <form method="post" name="forma" action="nomina_semana.cfm">
        <!--- Valor que se usa para saber si se envia o se revierte --->
        <input name="valor" type="hidden" value="0" />
        
        <cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          SELECT * FROM Tienda
          ORDER BY Tienda
        </cfquery>
        <cfif isdefined("tiendaid")>
          <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT Semana_ano.semana_ano_id, semana as semanaid, Terminada
            FROM Semana_ano INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.semana_ano_id=Semana_ano.Semana_ano_id
            WHERE ano=#anosel# and Tienda_id=#tiendaid# and Tienda_semana_ano.Terminada=1
            Order BY Semana_ano.semana_ano_id desc
          </cfquery>
          <cfelse>
            <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT semana_ano_id, semana as semanaid FROM Semana_ano 
              WHERE ano=#anosel# 
              Order BY Semana_ano.semana_ano_id desc
            </cfquery>
        </cfif>
        
        <!--- Filtros para seleccionar tienda, a?o y semana--->
        <div class="row pb-3 fs-6">
          <div class="col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">Tienda:</p>
            <select class="form-select" name="tiendaid" onchange="submit2(this);">
              <option value="0" >Seleccionar</option>
              <CFOUTPUT query="Tiendas">
                <option value="#Tienda_id#" <cfif isdefined("tiendaid") and tienda_id eq tiendaid> selected="selected"</cfif>>#Tienda#</option>
              </CFOUTPUT>
            </select>
          </div>

          <div class="col col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">A�o:</p>
            <select class="form-select" name="anosel" onchange="submit(this);">
              <cfoutput>
                <cfset ano=lsdateformat(now(),"yyyy")>
                <cfset ano2=ano+3>
                <cfset ano=2011>
                <cfloop from="#ano#" to="#ano2#" index="i">
                  <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                </cfloop>
              </cfoutput>
            </select>
          </div>

          <div class="col col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">Semana:</p>
            <select class="form-select" name="semana" onchange="submit(this);">
              <cfoutput>
                <option value="0" >Seleccionar</option>
                <cfloop query="qrySemanas">
                  <option value="#Semana_ano_id#" <cfif (isdefined("semana") and Semana_ano_id eq semana) > selected="selected"</cfif>>#semanaid#</option>
                </cfloop>
              </cfoutput>
            </select>
          </div>
        </div>

        <cfif isdefined("tiendaid") and tiendaid neq 0 and qrySemanas.recordcount neq 0 and semana neq 0>
            <cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                SELECT * FROM Tienda_semana_ano 
                WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
            </cfquery>
            <cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT Empleado.Empleado_id,  Apellido_pat + ' '+ Apellido_Mat +' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Puesto.Sueldo_fijo, Empleado.sueldo_base,
              encargado , puesto, Puesto.Puesto_id, capacitacion_fecha, Supervisor, Puesto.Vendedor_Sueldo_fijo
              FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
              WHERE EMPLEADO.empleado_id in (SELECT Distinct Empleado_id FROM Empleado_dia_semana_ano WHERE Tienda_semana_ano_id=#qrytiendasemana.Tienda_semana_ano_id#)
              --and Empleado.activo=1
              ORDER BY Nombre
            </cfquery>
            <cfquery name="qrySemanaDias" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT * FROM Dia_Semana_ano
              WHERE Semana_ano_id=#semana#
            </cfquery>
            <cfquery name="qrytablacomision" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT * FROM Tabla_Comision
              WHERE Tienda_id=#tiendaid#
            </cfquery>

            <cfif qrytiendasemana.publicar eq 0>
              <table class="table table-hover">
                <cfquery name="qryEquipo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  SELECT Distinct Empleado.empleado_id, Cuenta_equipo
                  FROM Empleado_dia_semana_ano INNER JOIN Dia_semana_ano ON Empleado_dia_semana_ano.Dia_semana_ano_id=Dia_semana_ano.Dia_semana_ano_id
                  INNER JOIN Concepto_dia_trabajado ON Concepto_dia_trabajado.Concepto_dia_trabajado_id=Empleado_dia_semana_ano.Concepto_dia_trabajado_id
                  INNER JOIN Empleado ON Empleado_dia_semana_ano.Empleado_id=Empleado.Empleado_id
                  INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                  WHERE semana_ano_id=#semana# and Tienda_id=#tiendaid# and Puesto.sueldo_fijo=0 and Empleado.empleado_id not in (SELECT Distinct Empleado.empleado_id
                  FROM Empleado_dia_semana_ano INNER JOIN Dia_semana_ano ON Empleado_dia_semana_ano.Dia_semana_ano_id=Dia_semana_ano.Dia_semana_ano_id
                  INNER JOIN Concepto_dia_trabajado ON Concepto_dia_trabajado.Concepto_dia_trabajado_id=Empleado_dia_semana_ano.Concepto_dia_trabajado_id
                  INNER JOIN Empleado ON Empleado_dia_semana_ano.Empleado_id=Empleado.Empleado_id
                  INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                  WHERE semana_ano_id=#semana# and Tienda_id=#tiendaid# and Puesto.sueldo_fijo=0 and cuenta_equipo=0)
                </cfquery>
                <!--- Total de pares vendidos en la tienda x en la semana y--->
                <cfquery name="qryventatotal" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  SELECT SUM(Venta_pares) as ventatotal
                  FROM Empleado_dia_semana_ano INNER JOIN Dia_semana_ano ON Empleado_dia_semana_ano.Dia_semana_ano_id=Dia_semana_ano.Dia_semana_ano_id
                  INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Empleado_dia_semana_ano.Tienda_semana_ano_id
                  WHERE Dia_semana_ano.semana_ano_id=#semana# and Tienda_semana_ano.Tienda_id=#tiendaid#
                </cfquery>
                <!--- Cuenta la cantidad de renglones, si no es 0 entra al if--->
                <cfif qryEquipo.recordcount neq 0>
                  <cfset empleadolist=valuelist(qryEquipo.empleado_id)>
                  <cfelse>
                    <cfset empleadolist=0>
                </cfif>
                <cfset equiponum=round((qrytablacomision.porcentaje/100)*qryEquipo.recordcount)>
                <cfquery name="qryempleadoscom" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  SELECT TOP #equiponum# (Sum(venta_pares)) as pares, empleado_id, pares_descontados
                  FROM Empleado_dia_semana_ano INNER JOIN Dia_semana_ano ON Empleado_dia_semana_ano.Dia_semana_ano_id=Dia_semana_ano.Dia_semana_ano_id
                  WHERE semana_ano_id=#semana# and empleado_id in (#empleadolist#)
                  GROUP BY Empleado_id, pares_descontados
                  ORDER BY pares desc
                </cfquery>
                <cfset empleadomejores=valuelist(qryempleadoscom.empleado_id)>
                <cfset empleadomejor=qryempleadoscom.pares>
                <cfquery name="qryultimodiasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  SELECT top 1 dia FROM dia_semana_ano
                  WHERE Semana_ano_id=#semana#
                  Order by Dia desc
                </cfquery>
                <cfset contador=0>

                <!--- Primera fila --->
                <tr>
                  <cfoutput><td align="center" valign="middle"><p>Nombre</p></td></cfoutput>
                  <td align="center" valign="middle"><p>Sueldo base</p></td>
                  <td align="center" valign="middle"><p>D�as Laborados</p></td>
                  <td align="center" valign="middle"><p>Pares Vendidos</p></td>
                  <td align="center" valign="middle"><p>Pares descontados</p></td>
                  <td align="center" valign="middle"><p>Comisi�n por par de zapato</p></td>
                  <td align="center" valign="middle"><p>Comisi�n por pagar</p></td>
                  <td align="center" valign="middle"><p>Otras deducciones</p></td>
                  <td align="center" valign="middle"><p>Otras percepciones</p></td>
                  <td align="center" valign="middle"><p>Otras comisiones</p></td>
                  <td align="center" valign="middle"><p>Sueldo a pagar</p></td>
                </tr>

                <!--- Inicializaci�n de qerys por cada lectura de cada persona x tienda --->
                <cfset totalsueldo=0>
                <cfset totalplaya=0>
                <cfoutput query="qryempleados">
                  <cfset contador=contador+1>
                  <cfquery name="qrydiastrabajados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT Empleado_dia_semana_ano_id,	Dia, Empleado_id,	Empleado_dia_semana_ano.Dia_Semana_ano_id,	Empleado_dia_semana_ano.Concepto_dia_trabajado_id,	ISNULL(Venta_pares,0) + ISNULL(Venta_bolsa,0) as Venta_pares,
                    ISNULL(Venta_pares_otra_tienda,0) as Venta_pares_otra_tienda, Pares_descontados,	Tienda_semana_ano_id,	Control_id,	origen_id,
                    Empleado_dia_semana_ano.Dia_semana_ano_id,		Semana_ano_id,	Concepto_dia_trabajado.Concepto_dia_trabajado_id,
                    Concepto_dia_trabajado,	Pagado,	Sueldo_fijo,	Sueldo_variable,	Pagado_ext,	Cuenta_equipo,	Descuenta_vacacion, ISNULL(Venta_playa,0) as Playa
                    FROM Empleado_dia_semana_ano INNER JOIN Dia_semana_ano ON 
                    Empleado_dia_semana_ano.Dia_semana_ano_id=Dia_semana_ano.Dia_semana_ano_id
                    INNER JOIN Concepto_dia_trabajado ON Concepto_dia_trabajado.Concepto_dia_trabajado_id=Empleado_dia_semana_ano.Concepto_dia_trabajado_id
                    WHERE empleado_id=#empleado_id# and Semana_ano_id=#semana# and Tienda_semana_ano_id=#qrytiendasemana.Tienda_semana_ano_id#
                  </cfquery>
                  <!--- inicializacion de variables --->
                  <cfset diastrab="">
                  <cfset diastrabnum=0>
                  <cfset faltainjustificada=0>
                  <cfset paresvendidos=0>
                  <cfset playavendidas=0>
                  <cfset paresdescontados=qrydiastrabajados.pares_descontados>
                  <cfset marca="">
                  <cfset paresdif=0>
                  <cfquery name="qryDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT Sum(monto) as monto FROM Deduccion
                    WHERE Semana_ano_id=#semana# and empleado_id=#empleado_id#
                  </cfquery>
                  <cfquery name="qryDeduccionesFaltas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT Sum(monto) as monto FROM Deduccion
                    WHERE Semana_ano_id=#semana# and empleado_id=#empleado_id# and Tipo_deduccion_id=8 or Tipo_deduccion_id=14
                  </cfquery>
                  <cfquery name="qryotracomision" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT Sum(monto) as monto FROM Otra_Comision
                    WHERE Semana_ano_id=#semana# and empleado_id=#empleado_id#
                  </cfquery>
                  <cfquery name="qryPercepciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT Sum(monto) as monto FROM percepcion
                    WHERE Semana_ano_id=#semana# and empleado_id=#empleado_id#
                  </cfquery>

                  <tr>
                    <!--- Nombre --->
                    <!--- 
                          **************************
                          **************************
                                      TD
                          **************************
                          **************************
                          ************************** 
                    --->
                    <td align="left" valign="middle" bgcolor="##FFFFFF">No. #Contador#.- <a href="editempleadostienda.cfm?buscarempleado=#busnombre#">#Nombre#</a><br />#puesto#</td>
                    
                    <cfset sueldobase = 0>
                    <!--- Si no es supervisor/a: --->
                    <cfif tiendaid neq 31>
                        <cfloop query="qrydiastrabajados">
                            <!--- Si no tiene falta se hace el conteo de los d�as que asisti�--->
                            <cfif pagado eq 1>
                              <cfset diastrab="#diastrab#, #Left(DayofWeekAsString(DayOfWeek(dia)),2)#">
                              <cfset diastrabnum=diastrabnum+1>
                            </cfif>

                            <!--- Cuenta equipo Descanso no se cuenta y no descuenta, pero si tiene falta injustificada y falt� se activa que tiene faltas--->
                            <cfif Cuenta_equipo eq 0 and faltainjustificada eq 0>
                              <cfset faltainjustificada=1>
                            </cfif>

                            <!--- Sumatoria de los pares vendidos en la semana --->
                            <cfset paresvendidos=paresvendidos+Venta_pares+Venta_pares_otra_tienda>
                            <cfset playavendidas=playavendidas+Playa>
                        </cfloop>
                        <!--- Si pertenece a la tienda 31 --->
                        <cfelse>
                            <!---As� se pagaba antes pero hay un nuevo metodo--->
                            <cfquery name="qryparestotales" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT SUM(Empleado_dia_semana_ano.Venta_pares)  as total
                              FROM Empleado_dia_semana_ano INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
                              INNER JOIN Tienda_semana_ano ON (Tienda_semana_ano.Semana_ano_id=Dia_Semana_ano.Semana_ano_id and Empleado_dia_semana_ano.Tienda_semana_ano_id=Tienda_semana_ano.Tienda_semana_ano_id)
                              WHERE Dia_semana_ano.Semana_ano_id=#semana# and Tienda_id in (<cfif Puesto_id eq 17>38,39,18,41,32<cfelseif Puesto_id eq 29>38,39,18,41<cfelse>1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 20, 21, 22, 24, 25, 26, 29, 30, 31<cfif Puesto_id neq 7>, 13, 19, 23, 28 </cfif></cfif>)<!--- Se comenta tienda 16 (18, ) por instrucciones de mario--->
                            </cfquery>
                            <cfquery name="qryparesresta" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT isNULL(SUM(Pares_descontados),0) as descontados, isNULL(SUM(Tienda_semana_ano.Pares_agregados),0) as agregados FROM Tienda_semana_ano
                              WHERE Semana_ano_id=#semana# and Tienda_id in (<cfif Puesto_id eq 17>38,39,18,41,32<cfelseif Puesto_id eq 29>38,39,18,41<cfelse>1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 20, 21, 22, 24, 25, 26, 29, 30, 31<cfif Puesto_id neq 7>, 13, 19, 23, 28 </cfif></cfif>)<!--- Se comenta tienda 16 (18, ) por instrucciones de mario--->
                            </cfquery>
                            <cfquery name="qryparestotalesGlobal" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT SUM(Empleado_dia_semana_ano.Venta_pares)  as total
                              FROM Empleado_dia_semana_ano INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
                              INNER JOIN Tienda_semana_ano ON (Tienda_semana_ano.Semana_ano_id=Dia_Semana_ano.Semana_ano_id and Empleado_dia_semana_ano.Tienda_semana_ano_id=Tienda_semana_ano.Tienda_semana_ano_id)
                              WHERE Dia_semana_ano.Semana_ano_id=#semana# and Tienda_semana_ano.tienda_id not in (40,41,32)
                            </cfquery>
                            <cfquery name="qryparesrestaGlobal" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT isNULL(SUM(Pares_descontados),0) as descontados, isNULL(SUM(Tienda_semana_ano.Pares_agregados),0) as agregados FROM Tienda_semana_ano
                              WHERE Semana_ano_id=#semana# and Tienda_semana_ano.tienda_id not in (40,41,32)
                            </cfquery>
                            <cfquery name="qryTiendaPuesto" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT Tienda_id FROM Puesto_tienda
                              WHERE Puesto_id=#Puesto_id#
                            </cfquery>
                            <cfset listatiendas=0>
                            <cfif qryTiendaPuesto.recordcount neq 0>
                              <cfset listatiendas=valuelist(qryTiendaPuesto.tienda_id)>
                            </cfif>
                            <cfquery name="qryparestotalesLocales" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT isnull(SUM(Empleado_dia_semana_ano.Venta_pares),0)  as total
                              FROM Empleado_dia_semana_ano INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
                              INNER JOIN Tienda_semana_ano ON (Tienda_semana_ano.Semana_ano_id=Dia_Semana_ano.Semana_ano_id and Empleado_dia_semana_ano.Tienda_semana_ano_id=Tienda_semana_ano.Tienda_semana_ano_id)
                              WHERE Dia_semana_ano.Semana_ano_id=#semana# and Tienda_id in (#listatiendas#)
                            </cfquery>
                            <cfquery name="qryparesrestaLocales" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT isNULL(SUM(Pares_descontados),0) as descontados, isNULL(SUM(Tienda_semana_ano.Pares_agregados),0) as agregados FROM Tienda_semana_ano
                              WHERE Semana_ano_id=#semana# and Tienda_id in (#listatiendas#)
                            </cfquery>
                            
                            <!--- Si es aux de supervisor --->
                            <cfif Puesto_id eq 12>
                              <cfset paresvendidos=0>
                              <!--- Si es supervisor/a: --->
                              <cfelseif ListFindNoCase("7,16,18,19,20,21,22,23,24,25,26,27,28,31,35", Puesto_id)>
                                  <cfset paresvendidosGlobal=qryparestotalesGlobal.total-qryparesrestaGlobal.descontados+qryparesrestaGlobal.agregados>                                
                                  <CFIF qryparesrestaLocales.recordcount neq 0>
                                    <cfset paresvendidosLocal=qryparestotalesLocales.total-qryparesrestaLocales.descontados+qryparesrestaLocales.agregados>
                                    <cfelse>
                                      <cfset paresvendidosLocal=qryparestotalesLocales.total>
                                  </cfif>
                              <!--- Si no es auxiliar ni supervisor/a --->  
                              <cfelse>
                                  <cfset paresvendidos=qryparestotales.total-qryparesresta.descontados+qryparesresta.agregados>
                            </cfif>
                        
                            <cfset PARESDECONTADOS1=0>
                            <cfset PARESAGREGADOS1=0>
                    </cfif>
                    <cfif sueldo_base neq "">
                      <cfset sueldodias=sueldo_base>
                      <cfelse>
                        <cfset sueldodias=0>
                    </cfif>
                    <cfif paresdescontados neq "">
                      <cfset paresdif=paresvendidos-paresdescontados>
                    </cfif>
                    <!--- Suma de pares si es supervisor--->
                    <cfif ListFindNoCase("7,16,18,19,20,21,22,23,24,25,26,27,28,31,35", Puesto_id)>
                        <cfset paresdif=paresvendidosGlobal-paresdescontados>
                        <cfset paresdifLocal=paresvendidosLocal-paresdescontados>
                    </cfif>
                    <cfif paresdif lt 0>
                        <cfset paresdif=0>
                    </cfif>
                    
                    <cfif capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1>
                        <cfset marca="*">
                        <cfif qrytablacomision.pares_capacitacion gt paresdif>
                          <cfset sueldobase = sueldodias>
                          <cfset sueldodias=qrytablacomision.monto_capacitacion>
                          
                          <cfelse>
                            <cfset sueldodias=0>
                        </cfif>
                    </cfif>
                    <cfset diastrabnumYdescanso=diastrabnum+1>   

                    <!--- Sueldo base --->
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <!--- Coloca el simbolo de pesos, si marca est� sola no se muestra nada, si tiene contenido pone * para marcar que se encuentra en capacitaci�n --->
                    <td>#LSCurrencyFormat(sueldodias)# #marca#</td>



                    <!--- D�as Laborados ---> 
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <!--- Al comenzar a usar la variable se llema con , y un espacio, se quita estos datos de la cadena --->
                    <cfset diastrab=Mid(diastrab, 2, 100)>
                    <td>#diastrab# <span class="fw-bold">#diastrabnum#</span></td>



                    <!--- Pares Vendidos --->
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <td>
                      <cfif not ListFindNoCase("7,16,18,19,20,21,22,23,24,25,26,27,28,31,35", Puesto_id)>#paresvendidos#+#playavendidas#<cfelse>#paresvendidosGlobal#/#paresvendidosLocal#</cfif>
                    </td>



                    <!--- Pares descontados --->
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <td>-#paresdescontados#</td>



                    <!--- Comisi�n por par de zapato --->                  
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <td>
                      <!--- Si sueldo fijo es igual a 1--->
                      <cfif sueldo_fijo eq 1>
                        <cfif faltainjustificada eq 1>
                          <cfif Vendedor_Sueldo_fijo eq 1>
                            #LSCurrencyFormat(qrytablacomision.Comision_falta)# fvf
                            <cfset comision=qrytablacomision.Comision_falta>
                            <cfelse>
                              #LSCurrencyFormat(qrytablacomision.Comision_falta_suldofijo)# ba
                            <cfset comision=qrytablacomision.Comision_falta_suldofijo>
                          </cfif>
                          <cfelse>
                            <cfquery name="qryPuestoComision" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT Comision_Global, Comision_Local FROM Puesto
                              WHERE Puesto_id=#Puesto_id#
                            </cfquery>
                          <cfif Puesto_id eq 17 or Puesto_id eq 29>
                            <cfset comision=qryPuestoComision.Comision_Local>
                            #LSCurrencyFormat(qryPuestoComision.Comision_Local)# k
                            <cfelseif ListFindNoCase("7,16,18,19,20,21,22,23,24,25,26,27,28,31,35", Puesto_id)>
                              #LSCurrencyFormat(qryPuestoComision.Comision_Global)#/#LSCurrencyFormat(qryPuestoComision.Comision_Local)# l
                            <cfset comision=qryPuestoComision.Comision_Global>
                            <cfset comisionlocal=qryPuestoComision.Comision_Local>
                            <cfelseif Vendedor_Sueldo_fijo eq 1>
                              #LSCurrencyFormat(qrytablacomision.Comision_vendedora1)# vf
                            <cfset comision=qrytablacomision.Comision_vendedora1>
                            <cfelse>
                              <cfif #Puesto_id# eq 4>
                                <cfif tiendaid eq 18 or tiendaid eq 38 or tiendaid eq 39 or tiendaid eq 41>
                                  #LSCurrencyFormat(qrytablacomision.Comision_vendedora1)# a4
                                  <cfset comision=qrytablacomision.Comision_vendedora1>
                                <cfelse>
                                    #LSCurrencyFormat(qrytablacomision.Comision_sueldofijo)# a23
                                    <cfset comision=qrytablacomision.Comision_sueldofijo>
                                </cfif>
                                <cfelse>
                                    #LSCurrencyFormat(qrytablacomision.Comision_sueldofijo)# a
                                    <cfset comision=qrytablacomision.Comision_sueldofijo>
                              </cfif>
                          </cfif>
                        </cfif>
                        <cfelseif capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1><!----comision de capacitacion --->
                          <cfif faltainjustificada eq 1>
                            #LSCurrencyFormat(qrytablacomision.Comision_falta)# b
                            <cfset comision=qrytablacomision.Comision_falta>
                            <cfelse>
                              #LSCurrencyFormat(qrytablacomision.Comision_vendedora1)# d
                            <cfset comision=qrytablacomision.Comision_vendedora1>
                          </cfif>
                        <cfelseif faltainjustificada eq 1>
                          #LSCurrencyFormat(qrytablacomision.Comision_falta)# bb
                          <cfset comision=qrytablacomision.Comision_falta>
                        <cfelseif Puesto_id eq 9>
                          #LSCurrencyFormat(qrytablacomision.Comision_finsemana)# c
                          <cfset comision=qrytablacomision.Comision_finsemana>
                        <cfelseif qrytablacomision.pares_venta neq "" and qrytablacomision.pares_venta gt paresdif>
                          #LSCurrencyFormat(qrytablacomision.Comision_vendedora2)# e
                          <cfset comision=qrytablacomision.Comision_vendedora2>
                        <cfelseif qrytablacomision.pares_venta neq "" and qrytablacomision.pares_venta lte paresdif>
                          #LSCurrencyFormat(qrytablacomision.Comision_vendedora1)# f
                          <cfset comision=qrytablacomision.Comision_vendedora1>
                        <cfelseif ListFindNoCase(empleadomejores, empleado_id)>
                          #LSCurrencyFormat(qrytablacomision.Comision_vendedora1)# g
                          <cfset comision=qrytablacomision.Comision_vendedora1>
                        <cfelseif qrytablacomision.masvendio eq 1 and (qrytablacomision.porcentajemas/100)*empleadomejor lt paresdif>
                          #LSCurrencyFormat(qrytablacomision.Comision_vendedora1)# h
                          <cfset comision=qrytablacomision.Comision_vendedora1>
                        <cfelseif qrytablacomision.arriba_x_pares eq 1 and qrytablacomision.X_pares lt paresdif>
                          #LSCurrencyFormat(qrytablacomision.Comision_vendedora1)# i
                          <cfset comision=qrytablacomision.Comision_vendedora1>
                        <cfelse>
                          #LSCurrencyFormat(qrytablacomision.Comision_vendedora2)# j
                          <cfset comision=qrytablacomision.Comision_vendedora2>
                      </cfif>
                     <!--- Fin Si sueldo fijo es igual a 1--->
                    </td>



                    <!--- Calculos si no son supervisores --->
                    <cfif tiendaid neq 31>
                      <cfset comisionplaya=qrytablacomision.Comision_Playa>
                      <cfset comisionpagar=(comision*paresdif)+ (playavendidas*comisionplaya)>
                        <cfif capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1>
                          <cfif sueldodias-qryDeduccionesFaltas.monto lt sueldobase-qryDeduccionesFaltas.monto+comisionpagar>
                            <cfset sueldodias=sueldobase>
                            <cfelse>
                              <cfset comisionpagar=0>
                          </cfif>
                        </cfif>
                     
                      <cfelse>
                        <cfset comisionpagar=Fix(comision*paresdif)>
                    </cfif>

                    <!--- Fin calculos si son supervisores --->
                    <cfif ListFindNoCase("7,16,18,19,20,21,22,23,24,25,26,27,28,31,35", Puesto_id)>
                      <cfset comisionpagarGlobal=comisionpagar>
                      <cfset comisionpagarLocal=Fix(comisionlocal*paresdifLocal)>
                      <cfset comisionpagar=comisionpagarGlobal+comisionpagarLocal>
                    </cfif>



                    <!--- Comisi?n por pagar --->
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <td>
                      <cfif not ListFindNoCase("7,16,18,19,20,21,22,23,24,25,26,27,28,31,35", Puesto_id)>#LSCurrencyFormat(comisionpagar)#<cfelse>#LSCurrencyFormat(comisionpagarGlobal)#/#LSCurrencyFormat(comisionpagarLocal)#</cfif>
                    </td>
                    <input type="hidden" name="Comisionxpagar#empleado_id#" value="#sueldodias#, #paresvendidos#,<cfif paresdescontados eq "">0<cfelse>#paresdescontados#</cfif>, #comision#,#comisionpagar#, <cfif tiendaid neq 31>#playavendidas#<cfelse>0</cfif>, <cfif tiendaid neq 31>#comisionplaya#<cfelse>0</cfif>" />
                    

                    
                    <!--- Otras deducciones --->
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <cfif qryDeducciones.monto neq  "">
                      <cfset deducciones=qryDeducciones.monto>
                      <cfelse>
                        <cfset deducciones=0>
                    </cfif>
                    <td><a href="javascript:window.open('deducciones.cfm?empleadoid=#empleado_id#&semanaid=#semana#&anosel=#anosel#&tiendaid=#tiendaid#','Deducciones','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#LSCurrencyFormat(qryDeducciones.monto)#</a></td>
                    <input type="hidden" name="Otrasdeducciones#empleado_id#" value="#deducciones#" />
                    


                    <!--- Otras percepciones --->
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <cfif qrypercepciones.monto neq  "">
                      <cfset percepciones=qrypercepciones.monto>
                      <cfelse>
                        <cfset percepciones=0>
                    </cfif>
                    <td><a href="javascript:window.open('percepciones.cfm?empleadoid=#empleado_id#&semanaid=#semana#&anosel=#anosel#&tiendaid=#tiendaid#','Percepciones','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#LSCurrencyFormat(qrypercepciones.monto)#</a></td>	
                    <input type="hidden" name="Otraspercepciones#empleado_id#" value="#percepciones#" />



                    <cfif encargado eq 1>
                      <cfif qrytiendasemana.pares_descontados eq "">
                        <cfset paresdecontados1=0>
                        <cfelse>
                          <cfset paresdecontados1=qrytiendasemana.pares_descontados>
                      </cfif>
                      <cfif qrytiendasemana.Pares_agregados eq "">
                        <cfset paresagregados1=0>
                        <cfelse>
                          <cfset paresagregados1=qrytiendasemana.Pares_agregados>
                      </cfif>
                      <cfset bonoextra=fix((qryventatotal.ventatotal- paresdecontados1+paresagregados1)*qrytablacomision.comision_encargada)>
                      <cfset paresparaencargada=(qryventatotal.ventatotal- paresdecontados1+paresagregados1)>
                      <cfif qrytablacomision.comision_encargada2 eq 1>
                        <cfif sueldodias gt bonoextra>
                          <cfset bonoextra=0>
                          <cfelse>
                            <cfset sueldodias=0>	
                        </cfif>
                      </cfif>
                      <cfelse>
                        <cfset bonoextra=0>
                    </cfif>
                    <cfset sueldo=comisionpagar-deducciones+percepciones+sueldodias+bonoextra>
                    <cfset totalsueldo=totalsueldo+sueldo>
                    <cfset totalplaya=totalplaya+playavendidas>

                    

                    <!--- Otras comisiones --->
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                    <td><cfif Supervisor eq 1><a href="javascript:window.open('percepciones.cfm?empleadoid=#empleado_id#&semanaid=#semana#&anosel=#anosel#&tiendaid=#tiendaid#','Percepciones','width=600,height=450,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#LSCurrencyFormat(qryotracomision.monto)#</a><cfelseif encargado eq 1>#LSCurrencyFormat(bonoextra)#<cfelse>#LSCurrencyFormat("0")#</cfif> </td>	
                    <input type="hidden" name="Otrascomisiones#empleado_id#" value="#bonoextra#" />
                  
                      

                    <!--- Sueldo a pagar --->
                    <!--- 
                         **************************
                         **************************
                                     TD
                         **************************
                         **************************
                         ************************** 
                    --->
                      <td>
                        <cfif sueldo lt 0><font color="##FF0000">#LSCurrencyFormat(sueldo)#</font><cfelse>#LSCurrencyFormat(sueldo)#</cfif>
                      </td> 
                      <input type="hidden" name="Sueldopagar#empleado_id#" value="#sueldo#" />
                   </tr>
                </cfoutput>   
                <cfoutput>
                  <cfif isdefined("paresdecontados1")>
                    <cfset ventafintaltotal="+#paresagregados1#="&#qryventatotal.ventatotal#+#paresagregados1#>
                    <cfelse>
                      <cfset ventafintaltotal="="&#qryventatotal.ventatotal#>
                  </cfif>
                  <tr>
                    <tr>
                      <td colspan="3" align="right">Total pares tienda: <br>Total pares playa: </td>
                      <td>#qryventatotal.ventatotal##ventafintaltotal#<br>#totalplaya#</td>
                      <td>&nbsp;</td>
                      <td colspan="5" align="right">Total</td>
                      <td>#LSCurrencyFormat(totalsueldo)#</td>
                    </tr>
                  </tr>
                </cfoutput>          
              </table>

              <cfoutput>
                <div>
                  <cfif Session.tienda eq 0 and qrytiendasemana.Publicar eq 0> 
                    <input type="hidden" name="tiendasemamaid" value="#qrytiendasemana.Tienda_semana_ano_id#" />
                    <p>Comentarios:</p>
                    <div align="center"><textarea name="comentario" cols="70" rows="3" readonly="readonly">#qrytiendasemana.Comentarios#</textarea></div>
                    <div align="center"><textarea name="comentarioaut" cols="70" rows="3">#qrytiendasemana.Comentario_autorizo#</textarea></div>

                    <div class="row contajus pb-5 pt-4 fs-7">
                      <!--- Primera columna -> Botones Regresar y salir del sistema --->
                      <div class="col" align="center">
                        <input class="btn btn-danger" type="button" onclick="valida(3);void(0);" value="Regresar a paso 4" />
                      </div>
                      <!--- Segunda columna nombre del usuario--->
                      <div class="col" align="center">
                        <input class="btn btn-success" type="button" onclick="valida(2);void(0);" value="Publicar" />
                      </div>
                    </div>

                    </div>
                    <cfelse>
                      <div align="center">Publicada</div>
                  </cfif>
                </div>
              </cfoutput>
              <!--- Renglon con 2 columnas--->
              <div class="row contajus pb-5 pt-4 fs-7">
                <!--- Primera columna -> Botones Regresar y salir del sistema --->
                <div class="col d-flex align-items-center">
                  <a href="menu_nomina.cfm" class="btn btn-primary me-3">Regresar</a>
                  <a href="logout.cfm" class="btn btn-warning">Salir del sistema</a>
                </div>
                <!--- Segunda columna nombre del usuario--->
                <div class="col">
                  <cfoutput>
                    <div class="text-center fw-semibold ">Usuario: #Session.nombre#</div>
                  </cfoutput>
                </div>
              </div>
              <cfelse>
                <p>Nomina Publicada</p>
            </cfif>
        </cfif>
    </div>
  </main>
  <!--   Core JS Files   -->
<script src="../../../new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="../../new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="../../../new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="../../../new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="../../../new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../../new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
  </body>
</html>

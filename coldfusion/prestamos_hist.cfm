<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.opener.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
function cerrarventana() {
	window.close();
}
</script>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
</head>

<body>
<div align="center">
  <table width="572" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="deducciones.cfm" onSubmit="return valida(this)">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><div align="center" class="style11">Deducciones</div></td>
        </tr>
        <cfquery name="qryempleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT E.Empleado_id,  Apellido_pat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, sueldo_base, Puesto,
             DR.Monto_inicial, DR.resto, DR.deduccion_semana, DR.resto/DR.deduccion_semana as Sem, DR.Comentarios
            FROM Empleado E INNER JOIN Puesto P ON P.Puesto_id=E.Puesto_id INNER JOIN Deduccion_recurrente DR ON DR.Empleado_id=E.Empleado_id
            WHERE E.Empleado_id=#empleadoid# and Deduccion_recurrente_id=#Deduccionrecurrenteid#
            ORDER BY Nombre
    	</cfquery>
        <tr>
          <td><table width="556" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="74" align="left"><p>Empleado</p></td>
                  <td width="169" align="left"><cfoutput query="qryempleado">#busnombre#</cfoutput></td>
                  <td width="118" align="left"><p>Puesto</p></td>
                  <td width="185" align="left"><cfoutput>#qryempleado.Puesto#</cfoutput></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="center"><table width="426" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td valign="top"><table width="100%" border="1" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="133" align="center"><p>Monto Inicial</p></td>
                  <td width="133" align="center"><p>Monto Restante</p></td>
                  <td width="133" align="center">Semanas Restantes<br />(aproximadamente)</td>
                </tr><tr>
                  <td width="133" align="right"><cfoutput>#LSCurrencyFormat(qryempleado.Monto_inicial)#</cfoutput></td>
                  <td width="133" align="right"><cfoutput>#LSCurrencyFormat(qryempleado.resto)#</cfoutput></td>
                  <td width="133" align="center"><cfoutput>#NumberFormat(qryempleado.Sem,"99")#</cfoutput></td>
                </tr>
                <tr>
                  <td width="133" align="left">Comentarios</td>
                  <td width="266" align="left" colspan="2"><cfoutput>#qryempleado.Comentarios#</cfoutput></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><div align="center">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
               <cfquery name="qryDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * 
						FROM Deduccion INNER JOIN Tipo_deduccion ON Deduccion.Tipo_deduccion_id=Tipo_deduccion.Tipo_deduccion_id
						INNER JOIN Semana_ano ON Semana_ano.semana_ano_id=Deduccion.Semana_ano_id
                        WHERE Deduccion_recurrente_id=#Deduccionrecurrenteid# and empleado_id=#empleadoid#
				  </cfquery>
              <tr><td>
              <div id="txtResult">
                  <cfif qryDeducciones.recordcount neq 0>
                  <cfset cargotot =0>
              <table border="0" align="center" width="500">
              <tr>
              	<td align="left">Historial de deducciones </td>
              </tr>
              <tr>
              	<td><table width="225" border="0" align="center">
              	  <tr><td>Semana</td><td>Monto</td></tr>
                	<cfoutput query="qryDeducciones">
                    <tr><td>#Semana#</td><td align="right">#LSCurrencyFormat(Monto)#</td></tr>
                    <cfset cargotot =cargotot+monto>
                    </cfoutput>
                    <cfoutput>
                    <tr><td>Total</td><td>#LSCurrencyFormat(cargotot)#</td></tr>
                    </cfoutput>
                </table></td>
              </tr>
              </table>
              <cfoutput>
              <input type="hidden" name="cargofin" value="#cargotot#" />
              </cfoutput>
              <cfelse>
              <input type="hidden" name="cargofin" value="0" />
              </cfif></div></td></tr>
            </table>
          </div></td>
        </tr>
      </table>
      </form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0" align="center">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td align="center"><a href="javascript:cerrarventana();void(0);" class="style9">Cerrar</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

<cfparam name="form.InputExcelFile" default="">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta charset="iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>

<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>

</head>

<body class="g-sidenav-show  bg-gray-100"> 
    
  <!---sidenav.cfm es la barra del lateral izquierdo--->
   <cfinclude template="../sidenav.cfm">
   <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
     <!-- Navbar -->
     <cfinclude template="../navbar.cfm">
     <!-- End Navbar -->

<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
    
    <tbody>
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Subir Archivo de Cancelacion de prestamos</span></div></td>
	  </tr>
	<tr>
      <td align="center" height="37" valign="middle" width="756">
      <cfif form.InputExcelFile eq "">
      <table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
		<cfform action="subir_deduciones.cfm" method="POST" enctype="multipart/form-data">
            <tr>
				<td nowrap valign="top">Archivo de Excel:</td>
				<td width="100%" valign="top">
					<input type="File" name="InputExcelFile" size="40" class="textfield">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="Submit" value="Procesar" class="button"><br>
					<br>			
				</td>
			</tr>
		</cfform>
	</table>
    <cfelse>
		<!--- read operation --->
		<hr size="1">
		
		<!--- define temp excel --->
		<cfset strDir=GetDirectoryFromPath(ExpandPath("*.*")) & "/temp">
		<cfset strInExcel=strDir>
		
		<!--- upload image --->
		<cffile action="Upload"
  					filefield="InputExcelFile"
  					destination="#strInExcel#"
  					nameconflict="MAKEUNIQUE" 
						mode="757">
		<cfset prodThumbDir=file.ServerDirectory>
		<cfset prodThumbFile=file.ServerFile>
		<cfset prodThumbExt=file.serverfileext>
		<cfif (prodThumbExt neq "xls")>
			Favor de utilizar archivos de Excel solamente
<cfoutput>#strInExcel#</cfoutput>
		<cfelse>  
		<cfspreadsheet action="read" columnnames="Deduccion_id,Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Deduccion_recurrente_id, Comentaios, Tienda_semana_ano_id, Control_id, Origen_id" query="deducciones" src="#prodThumbDir#/#prodThumbFile#" sheet="1">
        <cftransaction>
      <cfoutput>
      <cfloop query="deducciones">
       	<cfquery  name="qryDeduccion" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        	select Deduccion_id
            FROM Deduccion
            WHERE Control_id=#Deduccion_id# and origen_id=1
        </cfquery>
        <cfif qryDeduccion.recordcount eq 0>
        <cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        INSERT INTO Deduccion(Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Deduccion_recurrente_id, Comentaios,Tienda_semana_ano_id, Control_id, origen_id, Automatica_bit) 
        values(#Tipo_deduccion_id#, #Empleado_id#, '#Fecha#', #Semana_ano_id#, #Monto#, '#Fecha_movimiento#', #Deduccion_recurrente_id#, '#Comentaios#', NULL, #Deduccion_id#, #Origen_id#, 0)
       </cfquery>
       </cfif>
      </cfloop>
      </cfoutput>
	   <cfspreadsheet action="read" columnnames="Deduccion_recurrente_id, Empleado_id, Semana_ano_id, resto, Semana_cambio_id" query="DeduccionRec" src="#prodThumbDir#/#prodThumbFile#" sheet="2">
      <cfloop query="DeduccionRec">
      
      <cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        UPDATE Deduccion_recurrente
        SET resto=#resto#,
        	Semana_cambio_id=#Semana_cambio_id#
        WHERE Deduccion_recurrente_id=#Deduccion_recurrente_id#
       </cfquery>
      </cfloop>
        </cftransaction>
		El archivo se subi� con exito
    	</cfif>
    </CFIF>
      </td>
    </tr>
  </tbody></table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>

<!--   Core JS Files   -->
<script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Men&uacute;</title>
    <CFIF not isdefined("session.usuarioid")>
	    <script language="JavaScript" type="text/javascript">
		    alert("Usted no est� dentro del sistema");
		    window.location="index.cfm";
	    </script>
      <CFABORT>
    </CFIF>

    <cftransaction>
      <cfquery name="qryFechaAlta" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT * FROM Usuario WHERE Usuario_id=#Session.usuarioid#
      </cfquery>
      <CFSET FechaAlta=qryFechaAlta.Alta_fecha>
    </cftransaction>

    <cfif not isdefined("anosel")>
      <cfset anosel=year(now())>
    </cfif>
    <cfif Session.tienda neq 0 and not isdefined("tiendaid")><cfset tiendaid=Session.tienda><cfset semana=0></cfif>
    <cfif Session.tienda neq 0>
      <cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Tienda_semana_ano_id
        FROM Semana_curso INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.semana_ano_id=Semana_curso.Semana_ano_id
        WHERE Tienda_id=#Session.tienda#
      </cfquery>
      <cfelse>
        <cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          SELECT Tienda_semana_ano_id
          FROM Semana_curso INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.semana_ano_id=Semana_curso.Semana_ano_id
          WHERE Tienda_id=<cfif isdefined("tiendaid")>#tiendaid#<cfelse>0</cfif>
        </cfquery>
    </cfif>
    <cfif not isdefined("semana") or semana eq 0><cfset semana=qrysemanaActual.Tienda_semana_ano_id></cfif>
    <script language="JavaScript">
      function submit2(){
        document.forma.semana.value=0;
        document.forma.submit();
      }
    </script>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
    <!-- Nepcha Analytics (nepcha.com) -->
    <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
    <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
  </head>

  <body class="g-sidenav-show  bg-gray-100"> 
    
    <!---sidenav.cfm es la barra del lateral izquierdo--->
     <cfinclude template="../sidenav.cfm">
     <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
       <!-- Navbar -->
       <cfinclude template="../navbar.cfm">
       <!-- End Navbar -->

      <div align="center">
      <cfoutput>#FechaAlta# </cfoutput>
      <table width="772" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">
              <form method="post" name="forma" action="nomina_publicada_anterior.cfm">
                <input name="valor" type="hidden" value="0" />
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td>
                      <div align="center" class="style11">
                        <p>Nomina Publicada Semanas Anteriores </p>
                      </div>
                    </td>
                  </tr>
                  <cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT * FROM Tienda
                    WHERE Activo=1
                    <cfif Session.tienda neq 0>and Tienda_id=#Session.tienda#</cfif>
                    ORDER BY Tienda
                  </cfquery>
                  <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT DISTINCT TOP 60 Tienda_semana_ano_id as semana_ano_id, semana as semanaid 
                    FROM Semana_ano INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.semana_ano_id=Semana_ano.Semana_ano_id
						INNER JOIN Dia_semana_ano ON Dia_semana_ano.Semana_ano_id=Semana_ano.Semana_ano_id
                    WHERE ano=#anosel# and (publicar = 1) AND (autorizoencargada = 1)<cfif isdefined("tiendaid")>and  Tienda_id=#tiendaid#</cfif>
					<cfif FechaAlta neq "">and Dia_semana_ano.Dia>#createODBCDATE(FechaAlta)#</CFIF>
                    ORDER BY Tienda_semana_ano_id DESC
                  </cfquery>
                  <tr>
                    <td>
                      <div align="right" class="style11">
                        <table border="0" width="100%">
                          <tr>
                            <td>Tienda<br /> </td>
                            <td>
                              <select name="tiendaid" onchange="submit2();">
                                <option value="0" >Seleccionar</option>
                                <CFOUTPUT query="Tiendas">
                                  <option value="#Tienda_id#" <cfif isdefined("tiendaid") and tienda_id eq tiendaid> selected="selected"</cfif>>#Tienda#</option>
                                </CFOUTPUT>
                              </select>
                            </td>
                            <td>A�o</td>
                            <td>
                              <select name="anosel" onchange="submit(this);">
                                <cfoutput>
                                  <cfset ano=lsdateformat(now(),"yyyy")>
                                  <cfset ano2=ano+3>
                                  <cfset ano=2011>
                                  <cfloop from="#ano#" to="#ano2#" index="i">
                                    <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                                  </cfloop>
                                </cfoutput>
                              </select>
                            </td>
                            <td>Semana</td>
                            <td>
                              <select name="semana" onchange="submit(this);">
                                <cfoutput>
                                  <option value="0" >Seleccionar</option>
                                  <cfloop query="qrySemanas">
                                    <option value="#Semana_ano_id#" <cfif (isdefined("semana") and Semana_ano_id eq semana) > selected="selected"</cfif>>#semanaid#</option>
                                  </cfloop>
                                </cfoutput>
                              </select>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table width="756" border="0" cellpadding="3" cellspacing="1">
                        <cfif isdefined("tiendaid") and tiendaid neq 0 and qrySemanas.recordcount neq 0 and (semana neq 0 and semana neq "")>
                          <cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, Empleado_semana_ano.*, Efectivo_bit, Puesto, Tienda_semana_ano.semana_ano_id, Venta_playa, Empleado_semana_ano.Pago_Efectivo_bit
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Tienda_semana_ano_id=Empleado_semana_ano.Tienda_semana_ano_id
                            WHERE Empleado_semana_ano.Tienda_semana_ano_id=#semana# --and Empleado.activo=1
                            ORDER BY Nombre
                          </cfquery>
                          <cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT * FROM Tienda_semana_ano 
                            WHERE Tienda_semana_ano_id=#semana#
                          </cfquery>
                          <tr>
                            <td width="850" valign="top" align="center"
                              <table width="800" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                                <tr>
                                  <td width="149" align="center" valign="middle"><p>Nombre</p></td>
                                  <td width="49" align="center" valign="middle"><p>Sueldo base</p></td>
                                  <td width="49" align="center" valign="middle"><p>Pares Vendidos</p></td>
                                  <td width="49" align="center" valign="middle"><p>Pares Playa</p></td>
                                  <td width="49" align="center" valign="middle"><p>Pares descontados</p></td>
                                  <td width="49" align="center" valign="middle"><p>Comisi�n por par de zapato</p></td>
                                  <td width="49" align="center" valign="middle"><p>Comisi�n por pagar</p></td>
                                  <td width="49" align="center" valign="middle"><p>Otras deducciones</p></td>
                                  <td width="49" align="center" valign="middle"><p>Otras percepciones</p></td>
                                  <td width="49" align="center" valign="middle"><p>Otras comisiones</p></td>
                                  <td width="49" align="center" valign="middle"><p>Sueldo a pagar</p></td>
                                </tr>
                                <cfoutput query="qryempleados">
                                  <tr>
                                    <td align="left" valign="middle" bgcolor="##FFFFFF">#Nombre#<br />#puesto#<cfif Pago_Efectivo_bit eq 1><br />Efectivo<cfelse>Tarjeta</cfif></td>
                                    <td>#LSNumberFormat(Sueldo_base,"_,___.__")#</td>
                                    <td>#Pares_vendidos#</td>
                                    <td>#Venta_playa#</td>
                                    <td>#Pares_descontados#</td>
                                    <td>#LSNumberFormat(Comision_x_par,"_,___.__")#</td>
                                    <td>#LSNumberFormat(Comision_x_pagar,"_,___.__")#</td>
                                    <td><cfif Otras_deducciones neq 0><a href="javascript:window.open('deducciones_view.cfm?empleadoid=#empleado_id#&semanaid=#semana_ano_id#','Deducciones','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#LSNumberFormat(Otras_deducciones,"_,___.__")#</a><cfelse>#LSNumberFormat(Otras_deducciones,"_,___.__")#</cfif></td>
                                    <td><cfif Otras_percepciones neq 0><a href="javascript:window.open('percepciones_view.cfm?empleadoid=#empleado_id#&semanaid=#semana_ano_id#','Deducciones','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#LSNumberFormat(Otras_percepciones,"_,___.__")#</a><cfelse>#LSNumberFormat(Otras_percepciones,"_,___.__")#</cfif></td>
                                    <td>#LSNumberFormat(Otras_comisiones,"_,___.__")#</td>
                                    <td>#LSNumberFormat(Sueldo_pagar,"_,___.__")#</td>
                                  </tr>
                                </cfoutput>
                              </table>
                            </td>
                          </tr>
                        </cfif>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table width="100%" border="0" cellpadding="3" cellspacing="1">
                        <tr><td>Comentarios</td></tr>
                        <tr>
                          <td>
                            <cfif isdefined("tiendaid") and tiendaid neq 0 and qrySemanas.recordcount neq 0 and (semana neq 0 and semana neq "")><cfoutput><div align="center"><textarea name="comentario" cols="70" rows="3" readonly="readonly">#qrytiendasemana.Comentarios#</textarea></div>
                            <div align="center"><textarea name="comentarioaut" cols="70" rows="3" readonly="readonly">#qrytiendasemana.Comentario_autorizo#</textarea></div></cfoutput></cfif>
                          </td>
                        </tr>
                      </table>
                    </td>
                </tr>
                </table>
              </form>
              <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
                <tr>
                  <td>
                    <div align="left">
                      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                        <tr>
                          <td width="58%">
                            <table border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>                      
                          </td>
                          <td width="42%" bgcolor="#FFFFFF">Usuario: 
                            <cfoutput>
                              <div>#Session.nombre#</div>
                            </cfoutput>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>
    
    <!--   Core JS Files   -->
    <script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
    <script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
    <!-- Kanban scripts -->
    <script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
    <script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
    <script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
    <!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
    <script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
  </body>
</html>

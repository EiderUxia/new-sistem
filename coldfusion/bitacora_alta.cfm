<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Men&uacute;</title>
  <CFIF not isdefined("session.usuarioid")>
    <script language="JavaScript" type="text/javascript">
      alert("Usted no est� dentro del sistema");
      window.location="index.cfm";
    </script>
    <CFABORT>
  </CFIF>

  <style>
  .btn-contacto{
    background-color: #374859;
    padding:10px 15px;
    border: #fff 1px solid;
    color: #fff;
    font-size: 12px;
    font-weight: 600;
    text-decoration:none;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius:3px;
    transition:all .5s ease;
  }
  .btn-contacto:hover{
    background-color: #000;
    padding: 10px 20px;
  }
  </style>

  <script language="JavaScript">
    function edad() {

      var l_dt = new Date();

      var Ldt_due= document.frmEntrevista.nacimientofec.value;

      var Ldt_Year,Ldt_Month,Meses_nacido,Meses_hoy,anos,meses
      Ldt_Year = String(Ldt_due).substring(6,10)
      Ldt_Month = parseFloat(String(Ldt_due).substring(3,5));
      Ldt_day = parseFloat(String(Ldt_due).substring(0,2));
      
      //Set the two dates
      var millennium =new Date(Ldt_Year, Ldt_Month-1, Ldt_day) //Month is 0-11 in JavaScript
      today=new Date()
      //Get 1 day in milliseconds
      var one_day=1000*60*60*24

      //Calculate difference btw the two dates, and convert to days
      fecha2=Math.ceil((today.getTime()-millennium.getTime())/(one_day));
      anos3=fecha2/365;
      dias3=fecha2%365;
      meses3=dias3/30;
      diasf3=dias3%30-2;

      if (meses3==0) {
        if (diasf3==0){
          document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
        }
        else if (anos3>1) {
          document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
        }
        else {
          document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+diasf3+" dias";
        }
      }else {
        if (diasf3==0){
          document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
        }
        else if (anos3 > 1) {
          document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
        }
        else {
          document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+Math.floor(meses3)+" meses "+diasf3+" dias";
        }
      }
    }

    function show(event) {

      if (typeof event == "undefined")
        event = window.event;
      
      var val = event.keyCode;
      if(val == 13)
        document.forma.submit();
    }
  </script>

  <!--- Llenado de datos para las busquedas--->
  <link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
  <!--- Fin del llenado de datos para busqueda --->
  <link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></link>
  <script type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
</head>

<cfquery name="qryTienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
  SELECT Tienda_id, Tienda FROM Tienda
	WHERE Activo=1 <cfif #Session.tienda# neq 0>and tienda_id=#Session.tienda#</cfif>
</cfquery>

<cfif isdefined("bitacoraid")>
  <cfquery name="qryBitacora" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT Bitacora.bitacora_id, Tienda, Bitacora.Comentarios, Bitacora.Observaciones, Nombre +' '+ apellido_pat +' ' +apellido_mat as Nombre, Bitacora.Semana_ano_id, Genera_historia_bit
    FROM Bitacora
    INNER JOIN Tienda ON Tienda.Tienda_id=Bitacora.tienda_id
    INNER JOIN Empleado ON Empleado.Empleado_id=Bitacora.Empleado_id
    WHERE Bitacora_id=#bitacoraid#
  </cfquery>

  <cfquery name="qrySemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    Select Semana, Semana_ano_id
    FROM Semana_ano 
    WHERE Semana_ano_id=#qryBitacora.Semana_ano_id#
  </cfquery>

  <!--- Variables --->
  <cfset semanaid=#qrySemana.semana_ano_id#>
  <cfset semana=#qrySemana.semana#>
  <cfset tiendaid=#qryTienda.tienda_id#>
  <cfset tienda=qryTienda.tienda>
  <cfset empleado=qryBitacora.Nombre>
  <cfset observaciones=qryBitacora.Observaciones>
  <cfset comentarios=qryBitacora.Comentarios>
  <cfset generahistoria=qryBitacora.Genera_historia_bit>
    
  <cfelse>
    <cfquery name="qrySemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
      Select Semana, Semana_curso.Semana_ano_id
      FROM Semana_ano INNER JOIN Semana_curso ON Semana_ano.Semana_ano_id=Semana_curso.Semana_ano_id
    </cfquery>

    <cfif qryTienda.recordcount eq 1>
      <cfquery name="qryEmpleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Empleado_id, Nombre +' '+Apellido_pat as nombre
        FROM Empleado
        WHERE Activo=1 and Tienda_id=#Session.tienda#
      </cfquery>
    </cfif>  

    <!--- Variables --->
    <cfset bitacoraid=0>
    <cfset semanaid=#qrySemana.semana_ano_id#>
    <cfset semana=#qrySemana.semana#>
    <cfset tiendaid=#qryTienda.tienda_id#>
    <cfset tienda=qryTienda.tienda>
    <cfset empleadoid="">
    <cfset observaciones="">
    <cfset comentarios="">
    <cfset generahistoria=0>
</cfif>

<body>
  <div align="center">
    <table width="772" border="0" cellpadding="4" cellspacing="4">
      <tr>
        <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
      </tr>

      <tr>
        <td bgcolor="#FFFFFF">
          <div align="center" class="style7">
            <cfform method="post" name="frmBitacora" id="frmBitacora" action="Entrevista_alta.cfm" onSubmit="return valida(this)">
              <cfoutput>
                <input type="hidden" name="bitacoraid" id="bitacoraid" value="#bitacoraid#" />
              </cfoutput>
              <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td>
                    <div align="center" class="style11">
                      <p><cfif bitacoraid neq 0>Revisi�n
                        <cfelse>Alta
                        </cfif>de Bitacora
                      </p>
                    </div>
                  </td>
                </tr>
              
                <tr>
                  <td>
                    <table width="756" border="0" cellpadding="3" cellspacing="1">
                      <tr>
                        <td width="371" valign="top">
                          <table width="378" border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
                            <tr>
                              <td width="127" align="left" valign="middle"><p>Semana</p></td>
                              <td width="236" align="left" valign="middle" bgcolor="#FFFFFF">
                                <label><cfoutput><input type="hidden" name="semanaid" id="textfield" value="#semanaid#" />#semana#</cfoutput></label>
                              </td>
                            </tr>
                          </table>
                        </td>

                        <td width="385" valign="top">
                          <table width="378" border="0" cellpadding="0" cellspacing="1" bordercolor="#FFFFFF">
                            <tr>
                              <td align="left" valign="middle">*Tienda</td>
                              <td align="left" valign="middle" bgcolor="#FFFFFF">

                                <cfif bitacoraid eq 0>
                                  <cfif qryTienda.recordcount eq 1>
                                    <cfoutput><input type="hidden" name="tiendaid" id="textfield" value="#tiendaid#" />#tienda#</cfoutput>
                                    <cfelse>
                                      <cfselect class="required" name="tiendaid" id="tiendaid" query="qryTienda" display="tienda" value="tienda_id" queryposition="below" selected="#tiendaid#">
                                        <option value=""></option>
                                      </cfselect>
                                  </cfif>
                                  <cfelse>
                                    <cfoutput>#tienda#</cfoutput>
                                </cfif>                               
                              </td>
                            </tr> 
                          </table>
                        </td>
                      </tr>

                      <tr>
                        <td>Empleado</td>
                          <td>
                            <cfif bitacoraid eq 0>
                              <cfif qryTienda.recordcount eq 1>
                                <cfselect class="required" name="empleadoid" id="empleadoid" query="qryEmpleado" display="nombre" value="Empleado_id" queryposition="below" selected="#empleadoid#">
                                  <option value=""></option>
                                </cfselect>

                                <cfelse>
                                  <!--- <cfselect class="required" name="empleadoid" id="empleadoid" bind="cfc:/empleado.getempleado({frmBitacora:tiendaid})" bindonload="yes" display="nombre" value="Empleado_id" queryposition="below" selected="#empleadoid#"> --->
                                  <cfselect class="required" name="empleadoid" id="empleadoid"  display="nombre" value="Empleado_id" queryposition="below" selected="#empleadoid#">
                                  <option value=""></option>
                                  </cfselect>
                              </cfif>
                              
                              <cfelse>
                              <cfoutput>#empleado#</cfoutput>
                            </cfif>
                          </td>
                      </tr>
                    </table>
                  </td>
                </tr>

                <tr>
                  <td>
                    <div align="center">
                      <table width="497" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                        <cfif (#Session.tienda# eq 0 and bitacoraid eq 0) or (#Session.tienda# eq 0 and bitacoraid neq 0 and Comentarios eq " ")>
                          <input type="hidden" value=" " name="comentarios" />
                          <cfelse>
                            <tr>
                              <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">Comentarios</td>
                            </tr>
                            <tr>
                              <td align="left" valign="middle"><textarea name="comentarios" cols="80" rows="5" id="comentarios" required><cfoutput>#comentarios#</cfoutput></textarea></td>
                            </tr>
                        </cfif>

                        <cfif #Session.tienda# eq 0>
                          <tr>
                            <td>Genera Historial:  SI <input type="radio"name="generahistoria" value="1" <cfif generahistoria eq 1>checked="checked"</cfif>  /> No <input type="radio" name="generahistoria" value="0" <cfif generahistoria eq 0>checked="checked"</cfif> /></td>
                          </tr>
                          <tr>
                            <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">Observaciones</td>
                          </tr>
                          <tr>
                            <td align="left" valign="middle"><textarea name="observaciones" cols="80" rows="5" id="observaciones" required><cfoutput>#observaciones#</cfoutput></textarea></td>
                          </tr>
                        </cfif>

                        <tr>
                          <td align="left" valign="middle">
                            <cfdiv id="alta">
                              <cfif #Session.tienda# eq 0>
                                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                  <tr>
                                    <td align="left" valign="middle" bgcolor="#FFFFFF">
                                      <div align="center">
                                        <a href="javascript:void(0);" onclick="valida(0);" class="btn-contacto">Guardar</a>
                                      </div>
                                    </td>
                                    <td align="left" valign="middle" bgcolor="#FFFFFF">
                                      <div align="center">
                                        <a href="javascript:void(0);" onclick="valida(1);" class="btn-contacto">Terminar</a>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                                <cfelse>
                                  <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                    <tr>
                                      <td align="left" valign="middle" bgcolor="#FFFFFF">
                                        <div align="center">
                                          <a href="javascript:void(0);" onclick="valida(0);" class="btn-contacto">Guardar</a>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                              </cfif>
                            </cfdiv>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </cfform>
            <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
              <tr>
                <td>
                  <div align="left">
                    <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                      <tr>
                        <td width="58%">
                          <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="186" height="26">
                                <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                  <tr>
                                    <td><a href="Bitacoras.cfm" class="style9">Regresar</a></td>
                                  </tr>
                                </table>
                              </td>
                              <td width="186" height="26">
                                <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                  <tr>
                                    <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="42%" bgcolor="#FFFFFF">Usuario: 
                          <cfoutput>
                            <div>#Session.nombre#</div>
                          </cfoutput>
                        </td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
          </div>
          <div align="right">
          </div>
        </td>
      </tr>
    </table>
  </div>
  <!-- jQuery 2.0.2 -->  
  <!--- Llenado de datos para las busquedas--->
  <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
  <script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
  <script type="text/javascript">
    function valida(revisado){
      $("#alta").load('bta_jqry_alta.cfm?crearempleado=0&revisado='+revisado+'&'+$("#frmBitacora").serialize(),'alta');
      // ColdFusion.navigate('bta_jqry_alta.cfm?crearempleado=0&revisado='+revisado+'&'+$("#frmBitacora").serialize(),'alta');
        return false;
      }
  </script>
<!--- Fin del llenado de datos para busqueda --->
</body>
</html>
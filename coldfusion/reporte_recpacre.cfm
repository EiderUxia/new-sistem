<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Historial Remisiones</title>
<link rel="stylesheet" type="text/css" href="default.css" media="screen"/>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif not isdefined("fecha")>
	<cfset fecha="1/"&LSDateFormat(now(),"mm/yyyy")>
    <cfset fechafin=dateadd("m",1,now())>
    <cfset fechafin=LSDateFormat(fechafin,"mm")&"/1/"&LSDateFormat(fechafin,"yyyy")>
	<cfset fechafin=LSDateFormat(dateadd("d",-1,fechafin),"dd/mm/yyyy")>
</CFIF>

<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style8 {
	font-size: 24px;
	color: #000000;
}
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
<script language="JavaScript">
<!--
function valida(){

	document.forma1.listo.value=2;
	document.forma1.submit();
}
function valida2(){

}
-->
</script>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
    
    <tbody>
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Historial Remisiones</span></div></td>
	  </tr>
      <tr><td align="left">
	  <form method="post" name="forma" action="reporte_recpacre.cfm" onSubmit="return valida2(this)">
			<input type="hidden" name="listo" value="0" />
						  <table width="526" height="25" border="0" cellpadding="5" cellspacing="0">
                          <tr>
                            <td>inicio</td>
                            <td align="center"><cfoutput>
			<input size="15" type="text" name="fecha" Value="#fecha#" />
                        <a href="##" onClick="displayCalendar(document.forma.fecha,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></a>&nbsp;
			</cfoutput></td>
            			<td>Fin</td>
                            <td align="center"><cfoutput>
			<input size="15" type="text" name="fechafin"Value="#fechafin#" />
                        <a href="##" onClick="displayCalendar(document.forma.fechafin,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></a>&nbsp;
			</cfoutput></td>
            <td><input type="submit" value="buscar" /></td>
                          </tr>
                        </table></form></td></tr>
	<tr>
      <td align="center" height="37" valign="middle" width="756">
      <form method="post" name="forma1" action="reporte_recpacre.cfm" onSubmit="return valida(this)">
      <input type="hidden" value="0" name="listo" />
      <table border="1" align="center">
      <tr><td>Acreedor</td><td>Tienda</td><td>entidad</td><td>Monto</td><td>Remision</td><td>Fecha de remision</td><td>Tipo de Pago</td></tr>
      <cfquery name="gastos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT   Acreedor,Tienda.Tienda, Entidad.Tienda as entidad, Recepcion_acre.Monto, Remision_num, Fecha_recepcion, cheque.cheque_id, Efectivo_acre.Efectivo_acre_id
FROM         Recepcion_acre INNER JOIN Acreedor ON Acreedor.acreedor_id=Recepcion_acre.acreedor_id
	INNER JOIN Tienda ON Tienda.Tienda_id=Recepcion_acre.tienda_id
	INNER JOIN Tienda as Entidad ON Entidad.tienda_id=Recepcion_acre.entidad_id
	LEFT JOIN Pago_acreedor ON Pago_acreedor.recepcion_acre_id=Recepcion_acre.Recepcion_acre_id
	LEFT JOIN Cheque ON Cheque.cheque_id=Pago_acreedor.cheque_id
	LEFT JOIN Efectivo_acre ON Efectivo_acre.Efectivo_acre_id=Pago_acreedor.Efectivo_acre_id
    WHERE Fecha_recepcion>='#LSDateFormat(fecha,"mm/dd/yyyy")#' and Fecha_recepcion<= '#LSDateFormat(fechafin,"mm/dd/yyyy")#'
    
</cfquery>
	<cfoutput query="gastos">
      <tr><td>#Acreedor#</td><td>#Tienda#</td><td>#entidad#</td><td>#LSCurrencyFormat(monto)#</td><td>#Remision_num#</td><td>#LSDateFormat(Fecha_recepcion,"dd-mmm-yyyy")#</td><td><cfif cheque_id neq "">Cheque<cfelseif Efectivo_acre_id neq "">Efectivo<cfelse><font color="##FF0000">Sin pago</font></cfif></td></tr>
      </cfoutput>
      </table></form></td></tr>
  </tbody></table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                      	<td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_reportes.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

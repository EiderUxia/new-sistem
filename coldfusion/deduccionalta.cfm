<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Alta deduccion</title>
</head>
<cftransaction>
	<cfquery name="qryempleadosemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT Tienda_semana_ano_id
			FROM Tienda_semana_ano
			WHERE Semana_ano_id=#semanaid# and Tienda_id =#tiendaid#
		</cfquery>
<cfif recurrente eq 1>
	<cfif semana eq semanaid>
		<cfset resto=monto-montosemana>
	<cfelse>
		<cfset resto=monto>
	</cfif>
	<cfif global eq 1>
		<cfquery name="qryselempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT Distinct Empleado_id, Empleado_dia_semana_ano.Tienda_semana_ano_id
			FROM Empleado_dia_semana_ano INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
			INNER JOIN Tienda_semana_ano ON (Tienda_semana_ano.Semana_ano_id=Dia_Semana_ano.Semana_ano_id and Empleado_dia_semana_ano.Tienda_semana_ano_id=Tienda_semana_ano.Tienda_semana_ano_id)
			WHERE Dia_semana_ano.Semana_ano_id=#semanaid# and Tienda_id =#tiendaid#
		</cfquery>
		<cfloop query="qryselempleados">
			<cfquery name="qryAltaDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resDeduccionRec">
				INSERT Deduccion_recurrente (tipo_deduccion_id,Empleado_id,Semana_ano_id,Fecha,Monto_inicial,resto,deduccion_semana,inicio_semana_ano_id,Comentarios, Semana_cambio_id)
				Values (#conceptoid#,#Empleado_id#,#semanaid#,#CreateODBCDate(now())#,#monto#,#resto#,#montosemana#,#semana#,'#comentarios#', #semanaid#)
			</cfquery>
			<cfif semana eq semanaid>
				<cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
					INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Tienda_semana_ano_id)
					Values (#conceptoid#,#Empleado_id#,#CreateODBCDate(now())#,#semanaid#,#montosemana#,<cfif fecha neq "">#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#<cfelse>NULL</cfif>,#resDeduccionRec.IDENTITYCOL#,'#comentarios#', #qryempleadosemana.Tienda_semana_ano_id#)
				</cfquery>
			</cfif>
		</cfloop>
	<cfelse>
		<cfquery name="qryAltaDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resDeduccionRec">
			INSERT Deduccion_recurrente (tipo_deduccion_id,Empleado_id,Semana_ano_id,Fecha,Monto_inicial,resto,deduccion_semana,inicio_semana_ano_id,Comentarios, Semana_cambio_id)
			Values (#conceptoid#,#empleadoid#,#semanaid#,#CreateODBCDate(now())#,#monto#,#resto#,#montosemana#,#semana#,'#comentarios#', #semanaid#)
		</cfquery>
		<cfif semana eq semanaid>
			<cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
				INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Tienda_semana_ano_id)
				Values (#conceptoid#,#empleadoid#,#CreateODBCDate(now())#,#semanaid#,#montosemana#,<cfif fecha neq "">#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#<cfelse>NULL</cfif>,#resDeduccionRec.IDENTITYCOL#,'#comentarios#', #qryempleadosemana.Tienda_semana_ano_id#)
			</cfquery>
		</cfif>
	</cfif>
<cfelseif recurrente eq 2>
	<cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Tienda_semana_ano_id)
		Values (#conceptoid#,#empleadoid#,#CreateODBCDate(now())#,#semanaid#,#monto#,<cfif fecha neq "">#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#<cfelse>NULL</cfif>,#recurrenteid#,'#comentarios#', #qryempleadosemana.Tienda_semana_ano_id#)
	</cfquery>
<cfelse>
	<cfif global eq 1>
		<cfquery name="qryselempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT Distinct Empleado_id, Empleado_dia_semana_ano.Tienda_semana_ano_id
			FROM Empleado_dia_semana_ano INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
			INNER JOIN Tienda_semana_ano ON (Tienda_semana_ano.Semana_ano_id=Dia_Semana_ano.Semana_ano_id and Empleado_dia_semana_ano.Tienda_semana_ano_id=Tienda_semana_ano.Tienda_semana_ano_id)
			WHERE Dia_semana_ano.Semana_ano_id=#semanaid# and Tienda_id =#tiendaid#
		</cfquery>
		<cfloop query="qryselempleados">
			<cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
				INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Tienda_semana_ano_id)
				Values (#conceptoid#,#Empleado_id#,#CreateODBCDate(now())#,#semanaid#,#monto#,<cfif fecha neq "">#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#<cfelse>NULL</cfif>,NULL,'#comentarios#', #qryempleadosemana.Tienda_semana_ano_id#)
			</cfquery>
		</cfloop>
	<cfelse>
		<cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Tienda_semana_ano_id)
			Values (#conceptoid#,#empleadoid#,#CreateODBCDate(now())#,#semanaid#,#monto#,<cfif fecha neq "">#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#<cfelse>NULL</cfif>,NULL,'#comentarios#', #qryempleadosemana.Tienda_semana_ano_id#)
		</cfquery>
	</cfif>
</cfif>
</cftransaction>
<cfquery name="qryDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT * FROM Deduccion INNER JOIN Tipo_deduccion ON Deduccion.Tipo_deduccion_id=Tipo_deduccion.Tipo_deduccion_id
    WHERE Semana_ano_id=#semanaid# and empleado_id=#empleadoid#
</cfquery>
<body>
<cfoutput>
   <cfset cargotot =0>
              <table border="0" align="center" width="500">
              <tr>
              	<td align="center">Historial de deducciones<br />
                <cfif recurrente eq 1>
                	<cfif semana eq semanaid>
                    	La deducci&oacute;n se gener&oacute; con &eacute;xito
                    <cfelse>
                    	El descuento del prestamo se aplicara en las siguientes semanas
                    </cfif>
                <cfelse>
                	La deducci&oacute;n se gener&oacute; con &eacute;xito
                </cfif>
                </td>
              </tr>
              <tr>
              	<td><table border="0" align="center"><tr><td>Concepto</td><td>Monto</td><td>&nbsp;</td></tr>
                	<cfloop query="qryDeducciones">
                    <tr><td>#Tipo_deduccion#</td><td>#LSCurrencyFormat(Monto)#</td><td><a href="javascript:deduccionbaja('deduccionbaja.cfm','#Deduccion_id#');void(0);"><img src="img/icono_tacha.png" width="20" height="20" alt="borrar" border="0" /></a></td></tr>
                    <cfset cargotot =cargotot+monto>
                    </cfloop>
                    <tr><td>Total</td><td>#LSCurrencyFormat(cargotot)#</td><td>&nbsp;</td></tr>
                </table></td>
              </tr>
              </table>
</cfoutput>
</body>
</html>
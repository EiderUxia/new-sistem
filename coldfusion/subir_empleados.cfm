<cfparam name="form.InputExcelFile" default="">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Men�</title>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
        <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
        <!-- Nepcha Analytics (nepcha.com) -->
        <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
        <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
  </head>

  <body class="g-sidenav-show  bg-gray-100">
    <!---sidenav.cfm es la barra del lateral izquierdo--->
    <cfinclude template="../sidenav.cfm">
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
      <!-- Navbar -->
      <cfinclude template="../navbar.cfm">
      <!-- End Navbar -->

    <div align="center">
      <table width="772" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">
              <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
                <tbody>
                  <tr>
                    <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Subir Archivo de Empleados <cfif session.tienda eq 0>(paso2)</cfif></span></div></td>
                  </tr>
                  <tr>
                    <td align="center" height="37" valign="middle" width="756">
                      <cfif form.InputExcelFile eq "">
                        <table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
                          <cfform action="subir_empleados.cfm" method="POST" enctype="multipart/form-data">
                            <tr>
                              <td nowrap valign="top">Archivo de Excel:</td>
                              <td width="100%" valign="top">
                                <input type="File" name="InputExcelFile" size="40" class="textfield">
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2">
                                <input type="Submit" value="Procesar" class="button"><br>
                                <br>			
                              </td>
                            </tr>
                          </cfform>
                        </table>
                        <cfelse>
                          <!--- read operation --->
                          <hr size="1">
                          <!--- define temp excel --->
                          <cfset strDir=GetDirectoryFromPath(ExpandPath("*.*")) & "/temp">
                          <cfset strInExcel=strDir>
                          <!--- upload image --->
                          <cffile action="Upload"
                                  filefield="InputExcelFile"
                                  destination="#strInExcel#"
                                  nameconflict="MAKEUNIQUE" 
                                  mode="757">
                          <cfset prodThumbDir=file.ServerDirectory>
                          <cfset prodThumbFile=file.ServerFile>
                          <cfset prodThumbExt=file.serverfileext>
                          <cfif (prodThumbExt neq "xls")>
                            Favor de utilizar archivos de Excel solamente
                            <cfoutput>#strInExcel#</cfoutput>
                            <cfelse>  
                              <cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                DELETE Empleado
                                DELETE Empleado_dia_semana
                              </cfquery>
                              <cfspreadsheet action="read" columnnames="Empleado_id, Nombre, Apellido_pat, Apellido_mat, Sexo, Ingreso_fecha, Capacitacion_fecha, Baja_fecha, Fecha_reingreso, Tienda_id, Puesto_id, Sueldo_base, Activo, Paga_infonavit, Efectivo_bit" query="empleados" src="#prodThumbDir#/#prodThumbFile#" sheet="1">
                              <cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                <cfoutput>
                                  <cfloop query="empleados">
                                    INSERT INTO Empleado(Empleado_id, Nombre, Apellido_pat, Apellido_mat, Sexo, Ingreso_fecha, Capacitacion_fecha, Baja_fecha, Fecha_reingreso, Tienda_id, Puesto_id, Sueldo_base, Activo, Paga_infonavit, Efectivo_bit) 
                                    values(#Empleado_id#,'#Nombre#','#Apellido_pat#','#Apellido_mat#','#Sexo#','#Ingreso_fecha#','#Capacitacion_fecha#','#Baja_fecha#','#Fecha_reingreso#',#Tienda_id#,#Puesto_id#,'#Sueldo_base#','#Activo#','#Paga_infonavit#', '#Efectivo_bit#')
                                  </cfloop>
                                </cfoutput>
                              </cfquery>
                              <cfspreadsheet action="read" columnnames="Empleado_id, Dia_semana_id" query="empleados" src="#prodThumbDir#/#prodThumbFile#" sheet="2">
                              <cfloop query="empleados">
                                <cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                  INSERT INTO Empleado_dia_semana(Empleado_id, Dia_semana_id) 
                                  values(#Empleado_id#,#Dia_semana_id#)
                                </cfquery>
                              </cfloop>
                              El archivo se subi� con �xito
                          </cfif>
                      </CFIF>
                    </td>
                  </tr>
                </tbody>
              </table>

            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>
  </main>
    <!--   Core JS Files   -->
    <script src="../../../new-sistem/assets/js/core/bootstrap.min.js"></script>
    <script src="../../../new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="../../../new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script src="../../new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
    <!-- Kanban scripts -->
    <script src="../../../new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
    <script src="../../../new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
    <script src="../../../new-sistem/assets/js/plugins/chartjs.min.js"></script>
    <!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
    <script src="../../../new-sistem/assets/js/plugins/tilt.min.js"></script>

    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="../../../new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
  </body>
</html>

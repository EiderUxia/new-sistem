<cfparam name="form.InputExcelFile" default="">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style8 {
	font-size: 24px;
	color: #000000;
}
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
<script language="JavaScript">
function valida(val){
	if (document.forma.semanaanoid.selectedIndex==0){
		alert("seleccionar una semana");
		return false;
	}
	document.forma.listo.value=val;
	document.forma.submit();
}
</script>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
    
    <tbody>
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Generar archivo para imprimir la Nomina</span></div></td>
	  </tr>
	<tr>
      <td align="center" height="37" valign="middle" width="756">
      <cfif not isdefined("listo")>
      <table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
		<cfquery name="qrysemanaanoid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT top 9 Semana_ano_id, Convert(varchar(2),Semana)+ '-' + Convert(varchar(4),Ano) as semana
			FROM Semana_ano
			order by Semana_ano_id desc
		</cfquery>
		<cfform name="forma" action="Generar_recibos.cfm" method="POST" enctype="multipart/form-data">
			<tr>
				<td width="100%" valign="top">
					Semana:
					  <cfselect name="semanaanoid" query="qrysemanaanoid" display="Semana" value="Semana_ano_id" queryPosition="below"><option value="">seleccionar</option></cfselect><input type="hidden" name="listo" value="0">
				</td>
			</tr>
			<tr>
				<td align="center">
					<input type="button" value="Recibos" onclick="valida(1);void(0);" class="button"> <input type="button" value="Caratulas" onclick="valida(2);void(0);" class="button"><br>
					<br>			
				</td>
			</tr>
		</cfform>
	</table>
    <cfelse>
	<cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Tienda_semana_ano_id, Tienda_id, Semana_ano_id, Comentarios, Usuario_autorizo_id, Terminada, Autorizo_cont, Autorizo_cont_id, Comentario_autorizo, Pagada, Usuario_pagado_id, publicar, autorizoencargada, Usuario_autorizoencargada_id, Pares_descontados, Control_id, origen_id
		FROM Tienda_semana_ano
		WHERE Semana_ano_id = #semanaanoid# and autorizoencargada=1
	</cfquery> 
 	<cfset semalist=valuelist(qrytiendasemana.Tienda_semana_ano_id)>
<cfif isdefined("listo") and listo eq 1>
<cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, tienda.Tienda, 
Semana_ano.Semana, MIN(Dia_semana_ano.Dia) as inicio, MAX(Dia_semana_ano.Dia) as fin, Empleado_semana_ano.Sueldo_pagar
        FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
		INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
		INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id 
		INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
		INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Tienda_semana_ano.Semana_ano_id
		INNER JOIN Dia_semana_ano ON Dia_semana_ano.Semana_ano_id=Semana_ano.Semana_ano_id
        WHERE Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#) and Empleado_semana_ano.Pago_Efectivo_bit=1 and Empleado_semana_ano.Sueldo_pagar<>0
        -- and Empleado.activo=1 <!--- Se comento lo de activo por que deben ser todos los empleados pagados--->
        GROUP BY Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre , Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat , Sueldo_fijo, tienda.Tienda, 
Semana_ano.Semana, Empleado_semana_ano.Sueldo_pagar
        ORDER BY Tienda, Nombre
</cfquery>
<cfheader name="Content-Disposition" value="inline; filename=recibos.xls">
<cfcontent type="application/vnd.ms-excel">
<table border="0">
<cfoutput query="qryempleados">
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="1">&nbsp;</td><td>Tienda:</td><td>#Tienda#</td><td colspan="3">&nbsp;</td></tr>
<tr>
<td colspan="6">
<table border="2">
<tr><td colspan="6">
<table>
<tr><td>No. EMP. </td><td colspan="2">_________________</td><td colspan="3">&nbsp;</td></tr>
<tr><td colspan="1">&nbsp;</td>
<td> Nombre </td><td colspan="3"> <u>#busnombre#</u>___________ </td><td colspan="1">&nbsp;</td>
</tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr>
<td>Semana No.:</td><td>#Semana#</td><td colspan="4"><u>SEMANA DEL #LSDateFormat(inicio,"dd-mmm")# al #LSDateFormat(fin,"dd-mmm")#</u></td>
</tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr>
<td>Sueldo:</td><td colspan="2">#LSCurrencyFormat(Sueldo_pagar)#</td><td colspan="3">&nbsp;</td>
</tr>
<tr><td colspan="3">&nbsp;</td><td colspan="3" align="center">__________________________________</td></tr>
<tr>
<td colspan="3">Recibo provisional de nomina:</td><td colspan="3" align="center"> FIRMA DEL EMPLEADO</td>
</tr>
<tr><td colspan="6">&nbsp;</td></tr>
</table>
</td></tr>
</table>
</td></tr>
</cfoutput>
</table>
</cfcontent>
<cfabort>
</cfif>
<cfif isdefined("listo") and listo eq 2>
<cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, tienda.Tienda, 
Semana_ano.Semana, MIN(Dia_semana_ano.Dia) as inicio, MAX(Dia_semana_ano.Dia) as fin, Empleado_semana_ano.Sueldo_pagar
        FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
		INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
		INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id 
		INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
		INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Tienda_semana_ano.Semana_ano_id
		INNER JOIN Dia_semana_ano ON Dia_semana_ano.Semana_ano_id=Semana_ano.Semana_ano_id
        WHERE Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#) and Empleado_semana_ano.Pago_Efectivo_bit=1 and Empleado_semana_ano.Sueldo_pagar<>0
        -- and Empleado.activo=1 <!--- Se comento lo de activo por que deben ser todos los empleados pagados--->
        GROUP BY tienda.Tienda, Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre , Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat , Sueldo_fijo,  
Semana_ano.Semana, Empleado_semana_ano.Sueldo_pagar
        ORDER BY Tienda.tienda, Nombre
</cfquery>
<cfheader name="Content-Disposition" value="inline; filename=recibosb.xls">
<cfcontent type="application/vnd.ms-excel">
<cfoutput query="qryempleados" group="Tienda">
<table border="0">
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6" align="center">Erez de Cach�</td></tr>
<tr><td colspan="6" align="center">Relaci�n de pago en efectivo</td></tr>
<tr><td colspan="6" align="center">Semana ## #qryempleados.semana# del SEMANA DEL #LSDateFormat(qryempleados.inicio,"dd-mmm")# al #LSDateFormat(qryempleados.fin,"dd-mmm")#</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr>
<td colspan="6">
<table border="1">
<tr><td>Num.</td><td align="center">No. <br />Tienda</td><td align="center">Forma de <br />Pago</td><td align="center">Nombre</td>
<td align="center">Percepci�n <br />Neta</td><td align="center">Total<br />Tienda</td><td align="center">Observaciones</td></tr>
<cfset totaltienda=0>
<cfset cont=1>
<cfoutput>
<cfset totaltienda=totaltienda+Sueldo_pagar>
<tr><td align="center">#cont#</td><td align="center">#tienda#</td><td align="center">Efectivo</td><td>#busnombre#</td>
<td>#LSCurrencyFormat(Sueldo_pagar)#</td><td align="center"></td><td align="center"></td></tr>
<cfset cont=cont+1>
</cfoutput>
<tr><td align="center">&nbsp;</td><td align="center">#qryempleados.tienda#</td><td align="center">&nbsp;</td><td>&nbsp;</td>
<td>&nbsp;</td><td align="center">#LSCurrencyFormat(totaltienda)#</td><td align="center"></td></tr>
</table>
</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>Nombre:</td><td align="center" colspan="2">___________________________________________</td><td colspan="2">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>Puesto:</td><td align="center" colspan="2">___________________________________________</td><td colspan="2">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td>Firma:</td><td align="center" colspan="2">___________________________________________</td><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>
</cfcontent>
<cfabort>
</cfif>
    </cfif>
      </td>
    </tr>
  </tbody></table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

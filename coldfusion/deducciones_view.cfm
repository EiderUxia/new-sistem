<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.opener.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
function cerrarventana() {
	window.close();
}
</script>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
</head>

<body>
<div align="center">
  <table width="572" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="deducciones.cfm" onSubmit="return valida(this)">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><div align="center" class="style11">Deducciones</div></td>
        </tr>
        <cfquery name="qryempleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Empleado_id,  Apellido_pat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, sueldo_base, Puesto 
        FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
        WHERE Empleado_id=#empleadoid#
        ORDER BY Nombre
</cfquery>
        <tr>
          <td><table width="556" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="74" align="left"><p>Empleado</p></td>
                  <td width="169" align="left"><cfoutput query="qryempleado">#busnombre#</cfoutput></td>
                  <td width="118" align="left"><p>Puesto</p></td>
                  <td width="185" align="left"><cfoutput>#qryempleado.Puesto#</cfoutput></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><div align="center">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
               <cfquery name="qryDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT *, Deduccion_recurrente.Fecha as FechaPrestamo FROM Deduccion INNER JOIN Tipo_deduccion ON Deduccion.Tipo_deduccion_id=Tipo_deduccion.Tipo_deduccion_id
                        	LEFT JOIN Deduccion_recurrente ON Deduccion_recurrente.Deduccion_recurrente_id=Deduccion.Deduccion_recurrente_id
                        WHERE Deduccion.Semana_ano_id=#semanaid# and Deduccion.empleado_id=#empleadoid#
				  </cfquery>
              <tr><td>
              <div id="txtResult">
                  <cfif qryDeducciones.recordcount neq 0>
                  <cfset cargotot =0>
              <table border="0" align="center" width="500">
              <tr>
              	<td align="left">Deducciones de la semana </td>
              </tr>
              <tr>
              	<td><table width="325" border="0" align="center">
              	  <tr><td>Concepto</td><td>Monto</td></tr>
                	<cfoutput query="qryDeducciones">
                    <tr><td><cfif Monto_inicial neq "" and Tipo_deduccion_id neq 10 >
                    <a href="javascript:window.open('prestamos_hist.cfm?empleadoid=#empleado_id#&Deduccionrecurrenteid=#Deduccion_recurrente_id#','Prestamos','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Tipo_deduccion#</a> 
                    <br />(Monto Inicial #LSCurrencyFormat(Monto_inicial)#  Fecha de inicio #LSDATEformat(FechaPrestamo, 'dd/mmm/yyyy')#)<cfelse>#Tipo_deduccion#</cfif></td><td>#LSCurrencyFormat(Monto)#</td></tr>
                    <cfset cargotot =cargotot+monto>
                    </cfoutput>
                    <cfoutput>
                    <tr><td>Total</td><td>#LSCurrencyFormat(cargotot)#</td></tr>
                    </cfoutput>
                </table></td>
              </tr>
              </table>
              <cfoutput>
              <input type="hidden" name="cargofin" value="#cargotot#" />
              </cfoutput>
              <cfelse>
              <input type="hidden" name="cargofin" value="0" />
              </cfif></div></td></tr>
            </table>
          </div></td>
        </tr>
      </table>
      </form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0" align="center">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td align="center"><a href="javascript:cerrarventana();void(0);" class="style9">Cerrar</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

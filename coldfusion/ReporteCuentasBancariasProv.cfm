<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Cuentas Bancarias de Proveedor</title>

<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
	<CFABORT>
</CFIF>

<script language="JavaScript">
	function valida(){
		return true;
	}
</script>



<!--- Llenado de datos para las busquedas--->
<script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
	$(document).ready(function(){
		$("#buscarproveedor").autocomplete("data/proveedor.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	});
</script>
<!--- Fin del llenado de datos para busqueda --->

<!--- Calendarios --->
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<!--- /Calendarios --->

</head>


<body>
<div align="center">
	<table width="772" border="0" cellpadding="4" cellspacing="4">
		<tr>
			<td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
		</tr>
		<tr>
			<td bgcolor="#FFFFFF">
				<div align="center" class="style7">

					<cfset UsaRangoDeFechas = FALSE>


					<!--- Contenido --->
					<cfif isDefined('FechaInicio') AND reFind("([0-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}", FechaInicio)>
						<cfset UsaRangoDeFechas = TRUE>

						<cfset ano = Right(FechaInicio, 4)>
						<cfset mes = MID(FechaInicio, 4, 2)>
						<cfset dia = Left(FechaInicio, 2)>
						<cfset FechaInicioQry = '#ano#-#mes#-#dia#'>
					<cfelse>
						<cfset FechaInicio = ''>
						<cfset FechaInicioQry = '2017-01-01'>
					</cfif>

					<cfif isDefined('FechaFin') AND reFind("([0-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}", FechaFin)>
						<cfset UsaRangoDeFechas = TRUE>

						<cfset ano = Right(FechaFin, 4)>
						<cfset mes = MID(FechaFin, 4, 2)>
						<cfset dia = Left(FechaFin, 2)>
						<cfset FechaFinQry = '#ano#-#mes#-#dia#'>
					<cfelse>
						<cfset FechaFin = ''>
						<cfset FechaFinQry = '#DateFormat(DateAdd("d",1,now()), "yyyy-mm-dd")#'> <!--- Default = media noche del d�a de ma�ana --->
					</cfif>

					<cfoutput>
						<cfif NOT isDefined('buscarproveedor')>
							<cfset buscarproveedor = ''>
						</cfif>
						<script>
							console.log("FechaInicioQry:#FechaInicioQry#", "FechaFinQry:#FechaFinQry#", "Proveedor:#buscarproveedor#");
						</script>
					</cfoutput>

					<cfquery name="qryDatosReporte" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						DECLARE @FechaInicioQry AS smalldatetime = '#FechaInicioQry#'
						DECLARE @FechaFinQry AS smalldatetime = '#FechaFinQry#'

						SELECT 
							qryCambios.Control_id,
							CBP.Proveedor_id, 
							P.Nombre_corto,
							P.Nombre_contacto1c AS Dueno,
							P.Telefono1c AS TelDueno,
							P.Nextel1c AS NexDueno,
							P.Celular1c AS CelDueno,
							qryCambios.*,
							CASE 
								WHEN CBaja.Cuenta_banco IS NULL
									THEN BAlta.Banco + '/' + CAlta.Cuenta_banco
									ELSE BBaja.Banco + '/' + CBaja.Cuenta_banco
								END AS CuentaAfectada,
							CASE 
								WHEN CBaja.Cuenta_banco IS NULL 
									THEN CAlta.Beneficiario
									ELSE CBaja.Beneficiario
								END AS BeneficiarioCtaAfectada,
							BAlta.Banco + '/' + CAlta.Cuenta_banco AS CuentaAlta,
							CAlta.Beneficiario AS BeneficiarioAlta,
							BBaja.Banco + '/' + CBaja.Cuenta_banco AS CuentaBaja,
							CBaja.Beneficiario AS BeneficiarioBaja,
							UP.Nombre + ' ' + UP.Apellido_Pat AS UsuarioPropuesta,
							UA.Nombre + ' ' + UA.Apellido_Pat AS UsuarioAutoriza
						FROM (
							SELECT 
								Control_id,
								CASE WHEN Cuenta_banco_prov_id_alta IS NOT NULL THEN Cuenta_banco_prov_id_alta ELSE Cuenta_banco_prov_id_baja END AS CuentaMovimientoId,
								Cuenta_banco_prov_cambio_id,
								Cuenta_banco_prov_id_alta,
								Cuenta_banco_prov_id_baja,
								Usuario_propuesta,
								Fecha_propuesta,
								Tipo_Movimiento,
								Usuario_autoriza,
								Fecha_autoriza,
								Autorizado_bit,
								Comentarios_cambio
							FROM Cuenta_banco_prov_cambio CC
							WHERE Autorizado_bit = 1
						) as qryCambios
							INNER JOIN Cuenta_banco_prov CBP ON CBP.Cuenta_banco_prov_id = qryCambios.CuentaMovimientoId
							INNER JOIN Proveedor P on P.Proveedor_id = CBP.Proveedor_id
							LEFT JOIN Cuenta_banco_prov CAlta ON CAlta.Cuenta_banco_prov_id = qryCambios.Cuenta_banco_prov_id_alta
							LEFT JOIN Banco BAlta ON BAlta.Banco_id = CAlta.Banco_id
							LEFT JOIN Cuenta_banco_prov CBaja ON CBaja.Cuenta_banco_prov_id = qryCambios.Cuenta_banco_prov_id_baja
							LEFT JOIN Banco BBaja ON BBaja.Banco_id = CBaja.Banco_id
							LEFT JOIN Usuario UP ON UP.Usuario_id = qryCambios.Usuario_propuesta
							LEFT JOIN Usuario UA ON UA.Usuario_id = qryCambios.Usuario_autoriza
						WHERE 1=1
							AND  CAST(Fecha_autoriza AS DATE) >= @FechaInicioQry
							AND CAST(Fecha_autoriza AS DATE) <= @FechaFinQry
							<cfif isDefined('buscarproveedor') AND buscarproveedor NEQ ''>
								AND P.Proveedor = '#buscarproveedor#'
							</cfif>
						ORDER BY Proveedor_id, Fecha_autoriza, CuentaMovimientoId, Cuenta_banco_prov_cambio_id

					</cfquery>




					<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
						<tr>
							<form method="post" name="formaControles" action="ReporteCuentasBancariasProv.cfm">
								<cfoutput>
									<td>
										<label for="buscarproveedor">Proveedor</label>
										<input type="text" id="buscarproveedor" name="buscarproveedor" value="#buscarproveedor#">

									</td>
									<td>
										<label for="FechaInicio">Fecha Inicial</label>
										<input type="text" id="FechaInicio" name="FechaInicio" value="#FechaInicio#">
										<a href="##" onClick="displayCalendar(document.formaControles.FechaInicio,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></a>
									</td>
									<td>
										<label for="FechaFin">Fecha Final</label>
										<input type="text" id="FechaFin" name="FechaFin" value="#FechaFin#">
										<a href="##" onClick="displayCalendar(document.formaControles.FechaFin,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></a>
									</td>
									<td>
										<label>&nbsp;</label>
										<input type="submit" value="buscar" />
									</td>
								</cfoutput>
							</form>
						</tr>

						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>

						<tr>
							<td colspan="4"><div align="center" class="style11">Altas y Bajas de cuentas bancarias</div></td>
						</tr>

						<cfif qryDatosReporte.RecordCount NEQ 0>
							<cfoutput query="qryDatosReporte" group="Proveedor_id">
								<tr>
									<td colspan="4">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<div align="center" class="style11">#Nombre_corto#</div>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<!--- Tabla del reporte --->

										<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
											<thead>
												<tr style="background-color:blue;color:##FFFFFF">
													<th>Tipo Movimiento</th>
													<th>Cuenta/Beneficiario</th>
													<th>Propuesta</th>
													<th>Autorizaci�n</th>
													<th>Informaci�n Adicional</th>
													<th>Comentarios</th>
												<tr>
											</thead>
											<tbody>
											<cfset ColorId = 0>
												<cfoutput group="Control_id">
													<cfif ColorId EQ 0>
														<cfset ColorFila = '##EEF3F7'>
														<cfset ColorId = 1>
													<cfelse>
														<cfset ColorFila = '##FFFFFF'>
														<cfset ColorId = 0>
													</cfif>

													<cfoutput>
														<tr style="background-color:#ColorFila#;">
															<td>#Tipo_Movimiento#</td>
															<td>
																#CuentaAfectada#
																<br>
																<samp>(#BeneficiarioCtaAfectada#)</samp>
															</td>
															<td>
																#UsuarioPropuesta#
																<br>
																<samp>(#DateFormat(Fecha_propuesta, "dd/mm/yy")#&nbsp;#TimeFormat(Fecha_propuesta, "hh:mm")#)</samp>
															</td>
															<td>
																#UsuarioAutoriza#
																<br>
																<samp>(#DateFormat(Fecha_autoriza, "dd/mm/yy")#&nbsp;#TimeFormat(Fecha_autoriza, "hh:mm")#)</samp>
															</td>
															<td>
																Due�o:&nbsp;#Dueno#
																<cfif TelDueno NEQ ''>
																	<br>Tel:&nbsp;#TelDueno#
																</cfif>
																<cfif CelDueno NEQ ''>
																	<br>Cel:&nbsp;#CelDueno#
																</cfif>
															</td>
															<td>#Comentarios_cambio#</td>
														<tr>
													</cfoutput>
												</cfoutput>
											</tbody>
										</table>
										<!--- /Tabla del reporte --->
									</td>
								</tr>
							</cfoutput>
						<cfelse>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="4"><div align="center">No hay movimientos que mostrar</div></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
						</cfif>
						
					</table>
					<!--- /Contenido --->

					<!--- Footer --->
					<table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
						<tr>
							<td><div align="left">
							<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
								<tr>
									<td width="58%">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td width="186" height="26">
													<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
														<tr>
															<td><a href="menu_catalogos.cfm" class="style9">Regresar</a></td>
														</tr>
													</table>
												</td>
												<td width="186" height="26">
													<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
														<tr>
															<td><a href="menu.cfm" class="style9">Menu</a></td>
														</tr>
													</table>
												</td>
												<td width="186" height="26">
													<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
														<tr>
															<td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td width="42%" bgcolor="#FFFFFF">Usuario: 
										<cfoutput><div>#Session.nombre#</div></cfoutput>
									</td>
								</tr>
							</table>
							</div></td>
						</tr>
					</table>
					<!--- /Footer --->
				</div>
				<div align="right">
				</div>
			</td>
		</tr>
	</table>
</div>
</body>
<script type="text/javascript">
	function buscarMovimientos() {

	}
</script>
</html>

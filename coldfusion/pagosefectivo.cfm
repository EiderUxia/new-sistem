<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif isdefined("tipocp")>
<cftransaction>
<cfif tipocp eq 1>
<cfquery name="proveedorid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Proveedor_id FROM Proveedor
	WHERE Nombre_corto='#buscarproveedor#'
</cfquery>
<cfif proveedorid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("El proveedor no existe, favor de teclearlo nuevamente");
		window.location="pagosefectivo.cfm";
	</script>
    <cfabort>
</cfif>
<cfelseif tipocp eq 2>
<cfquery name="acreedorid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT acreedor_id FROM acreedor
	WHERE acreedor='#buscaracreedor#'
</cfquery>
<cfif acreedorid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("El acreedor no existe, favor de teclearlo nuevamente");
		window.location="pagosefectivo.cfm";
	</script>
    <cfabort>
</cfif>
</cfif>
<cfquery name="entidadid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Tienda_id as entidad_id FROM Tienda
	WHERE Tienda='#buscarentidad#'
</cfquery>
<cfif entidadid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("La entidad no existe, favor de teclearla nuevamente");
		window.location="altacuentabancariaprov.cfm";
	</script>
    <cfabort>
</cfif>
<cfquery name="tiendaid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Tienda_id FROM Tienda
	WHERE Tienda='#buscartienda#'
</cfquery>
<cfif Tiendaid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("La tienda no existe, favor de teclearla nuevamente");
		window.location="altacuentabancariaprov.cfm";
	</script>
    <cfabort>
</cfif>
<cfif tipocp eq 1>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Efectivo (Proveedor_id,Cuenta_banco_id,Tienda_id,Monto,Entidad_id, usuario_id, fecha) VALUES
		(#proveedorid.proveedor_id#,'#cuentabanco#', #tiendaid.tienda_id#,#monto#, #entidadid.entidad_id#,#Session.usuarioid#,#CreateODBCDate(now())#)			
</cfquery>
<cfquery name="Efectivolast" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT top 1 Efectivo_id FROM Efectivo
	Order By efectivo_id desc
</cfquery>
<cfelseif tipocp eq 2>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Efectivo_acre (acreedor_id,Cuenta_banco_id,Tienda_id,Monto,Entidad_id, usuario_id, fecha) VALUES
		(#acreedorid.acreedor_id#,'#cuentabanco#', #tiendaid.tienda_id#,#monto#, #entidadid.entidad_id#,#Session.usuarioid#,#CreateODBCDate(now())#)			
</cfquery>
<cfquery name="Efectivolast" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT top 1 Efectivo_acre_id FROM Efectivo_acre
	Order By efectivo_acre_id desc
</cfquery>
</cfif>
</cftransaction>
<script language="JavaScript" type="text/javascript">
		alert("El pago fue agregado con �xito");
		<cfif oculto eq 1>
		window.location="pagosefectivo.cfm";
		<cfelseif oculto eq 2 >
		<cfif tipocp eq 1>
			<cfoutput>window.location="aplicarpagosefectivoprov.cfm?efectivo=#efectivolast.efectivo_id#";</cfoutput>
		<cfelseif tipocp eq 2>
			<cfoutput>window.location="aplicarpagosefectivoacre.cfm?efectivo=#efectivolast.efectivo_acre_id#";</cfoutput>
		</cfif>
		</cfif>
	</script>
</cfif>
<script language="JavaScript">
function valida(val){
	if (document.forma.cuentabanco.selectedIndex==0){
	  	alert("La cuenta no ha sido seleccionada");
		return false;
	}
	if (document.forma.buscartienda.value==""){
		alert("Favor de ingresar la tienda");
		return false;
	}
	if (document.forma.monto.value==""){
		alert("Favor de ingresar el monto del cheque");
		return false;
	}
	if (document.forma.buscarentidad.value==""){
		alert("Favor de ingresar la entidad");
		return false;
	}
	if (document.forma.tipocp.selectedIndex==0){
		alert("Favor de seleccionar el tipo de CxP");
		return false;
	}
	else if (document.forma.tipocp.selectedIndex==1){
		if (document.forma.buscarproveedor.value==""){
			alert("Favor de ingresar el proveedor");
			return false;
		}
	}
	else if (document.forma.tipocp.selectedIndex==2){
		if (document.forma.buscaracreedor.value==""){
			alert("Favor de ingresar el acreedor");
			return false;
		}
	}
	document.forma.oculto.value=val;
	document.forma.submit();
}
function cambio1(opcion) {
	if (opcion==1){
		document.getElementById("provdiv").style.display="";
		document.getElementById("acrediv").style.display="none";
	}
	else if (opcion==2) {
		document.getElementById("provdiv").style.display="none";
		document.getElementById("acrediv").style.display="";
	}
	else {
		document.getElementById("provdiv").style.display="none";
		document.getElementById("acrediv").style.display="none";
	}
}
</script>
<!--- Llenado de datos para las busquedas--->
    <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
$(document).ready(function(){
	$("#buscartienda").autocomplete("data/tienda.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	$("#buscarentidad").autocomplete("data/tienda.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	$("#buscarproveedor").autocomplete("data/proveedor.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	$("#buscaracreedor").autocomplete("data/acreedor.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	});
</script>
<!--- Fin del llenado de datos para busqueda --->
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="pagosefectivo.cfm" onSubmit="return valida(this)">
      <input type="hidden" name="oculto" value="0" />
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">Pagos en Efectivo</div></td>
        </tr>
        
        <tr>
          <td><table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="371" valign="top">
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="127" align="left" valign="middle"><p>Tipo CxP</p></td>
                    <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><select name="tipocp" id="select5" onchange="cambio1(this.value)">
                      <option value="">Seleccionar</option>
                      <option value="1">Proveedor</option>
                      <option value="2">Acreedor</option>
                    </select></td>
                  </tr>

              </table>
              <div id="provdiv" style="display:none">
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="127" align="left" valign="middle"><p>Proveedor</p></td>
                    <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">
                    <input type="text" id="buscarproveedor" name="buscarproveedor" size="34" AUTOCOMPLETE="off" /></td>
                  </tr>

              </table>
              </div>
              <div id="acrediv" style="display:none">
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="127" align="left" valign="middle"><p>Acreedor</p></td>
                    <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">
                    <input type="text" id="buscaracreedor" name="buscaracreedor" size="34" AUTOCOMPLETE="off" /></td>
                  </tr>

              </table>
              </div>
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <cfquery name="Cuentas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Cuenta_banco_id, Cuenta_banco FROM Cuenta_banco
	WHERE caja_general=1 and activo=1
</cfquery>
                <tr>
                  <td align="left" valign="middle"><p>Cta banc</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><label>
                    <cfoutput>
                    <select name="cuentabanco" id="select2">
                      <option value="0">Seleccionar</option>
                    <CFLOOP query="cuentas">
                    <option value="#Cuenta_banco_id#" >#Cuenta_banco#</option>
                  </CFLOOP>
                    </select>
                    </cfoutput>
                  </label></td>
                </tr>

                
              </table></td>
              <td width="385" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td width="127" align="left" valign="middle"><p>Tienda</p></td>
                  <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" id="buscartienda" name="buscartienda" size="34" autocomplete="off" /></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Monto&#13;</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="monto" id="textfield5" /></td>
                </tr>
                <tr>
                  <td align="left" valign="middle">Entidad</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" id="buscarentidad" name="buscarentidad" size="34" autocomplete="off" /></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><div align="center">
            <table width="497" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
              <tr>
                <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><p>Comentarios&#13;</p></td>
              </tr>
              <tr>
                <td align="left" valign="middle"><textarea name="textfield" cols="80" rows="5" id="textfield"></textarea></td>
              </tr>
              <tr>
                <td align="left" valign="middle"><table width="100%" border="0" cellpadding="3" cellspacing="1">
                  <tr>
                    <td width="238" align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
						<INPUT TYPE="BUTTON" VALUE="Capturar pago" onclick="valida(1)">
                        </div></td>
                    <td width="238" align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
						<INPUT TYPE="BUTTON" VALUE="Capturar pago y aplicar" onclick="valida(2)">
                    </div></td>
                  </tr>

                </table></td>
              </tr>
            </table>
          </div></td>
        </tr>
      </table>
      </form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_pago.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

<cfajaximport  />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif isdefined("tiendaid")>
<cftransaction>
<cfif isdefined("tipocp") and tipocp eq 1>
<cfquery name="Proveedorid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Proveedor_id FROM Proveedor
	WHERE Nombre_corto='#buscarproveedor#' and activo=1
</cfquery>
<cfif Proveedorid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("El proveedor no existe, favor de teclearno nuevamente");
		window.location="pagoscheque.cfm";
	</script>
    <cfabort>
<cfelse>
	<cfset provacreid=#proveedorid.proveedor_id#>
</cfif>
</cfif>
<cfif isdefined("tipocp") and tipocp eq 2>
<cfquery name="acreedorid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT acreedor_id FROM acreedor
	WHERE acreedor='#buscaracreedor#'
</cfquery>
<cfif acreedorid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("El acreedor no existe, favor de teclearno nuevamente");
		window.location="pagoscheque.cfm";
	</script>
    <cfabort>
<cfelse>
	<cfset provacreid=#acreedorid.acreedor_id#>
</cfif>
</cfif>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Cheque (tienda_id,Cuenta_banco_id,Entidad_id,Monto, Fecha,Comentarios<cfif isdefined("cuentapor") and cuentapor eq 1>,Cuenta_pagar, Tipo_CP_id <cfif tipocp eq 1>,proveedor_id<cfelseif tipocp eq 2>,acreedor_id</cfif></cfif>, usuario_id, abono, Anticipo <cfif isdefined("cuentapor") and cuentapor eq 0>, Otro_cheque</cfif>, amano_bit, Tipo_pago_id) VALUES
		(#tiendaid#,#cuentaid#,#entidadid#, #monto#, #CreateODBCDate(LSDateFormat(form.fecha,"mm/dd/yyyy"))#,'#comentarios#',<cfif isdefined("cuentapor") and cuentapor eq 1>1,#tipocp#,#provacreid#,</cfif>#Session.usuarioid#, <cfif isdefined("abono2")>1<cfelse>0</cfif>, <cfif isdefined("anticipo")>1<cfelse>0</cfif><cfif isdefined("cuentapor") and cuentapor eq 0>, #tipoch#</cfif>, <cfif isdefined("amano")>1<cfelse>0</cfif>, #tipopagoid#)			
</cfquery>
<cfquery name="chequelast" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT top 1 cheque_id FROM Cheque
    Order by cheque_id Desc
</cfquery>
<cfif isdefined("cuentapor") and cuentapor eq 0>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Cheque_entidad (cheque_id,Cuenta_banco_ent_id,Comentarios, usuario_id) VALUES
		(#chequelast.cheque_id#,<cfif tipoch eq 3>#cuenta2bid#<cfelse>#cuenta2id#</cfif>, '#comentarios#',#Session.usuarioid#)			
</cfquery>
<cfquery name="Aplicaciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						UPDATE Aplicacion 
                        SET llave_id=#chequelast.cheque_id#
                        WHERE Usuario_id=#Session.usuarioid# and llave_id is null
				  </cfquery>
                  </cfif>
</cftransaction>
<script language="JavaScript" type="text/javascript">
		alert("El cheque fue agregado con �xito");
		<cfif isdefined("tipocp") and tipocp eq 1>
		<cfoutput>window.location="pagoschequeprov.cfm?cheque=#chequelast.cheque_id#";</cfoutput>
		<cfelseif isdefined("tipocp") and tipocp eq 2>
		<cfoutput>window.location="pagoschequeacre.cfm?cheque=#chequelast.cheque_id#";</cfoutput>
		<cfelse>
		<cfoutput>window.location="pagoscheque.cfm?cheque=#chequelast.cheque_id#";</cfoutput>
		</cfif>
	</script>
<cfelse>
<cfquery name="aplicaciondel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        DELETE Aplicacion 
        WHERE Usuario_id=#Session.usuarioid# and llave_id is null
    </cfquery>
</cfif>
<script language="JavaScript">
function valida(){
	if (document.forma.tiendaid.selectedIndex==0){
	  	alert("La tienda no ha sido seleccionada");
		return false;
	}
	if (document.forma.entidadid.selectedIndex==0){
	  	alert("La entidad no ha sido seleccionada");
		return false;
	}
	if (document.forma.cuentaid.selectedIndex==0){
	  	alert("La cuenta no ha sido seleccionada");
		return false;
	}
	
	if (document.forma.fecha.value==""){
		alert("Favor de ingresar la fecha");
		return false;
	}
	<!---
	if (document.forma.cheque.value==""){
		alert("Favor de ingresar el n�mero del cheque");
		return false;
	}
	--->
	if (document.forma.monto.value==""){
		alert("Favor de ingresar el monto del cheque");
		return false;
	}
	if (document.forma.cuentapor[1].checked==true){
		if (document.forma.tipoch.selectedIndex==0){
			alert("Favor de seleccionar el tipo de pago");
			return false;
		}
		if (document.forma.tipoch.selectedIndex==1 || document.forma.tipoch.selectedIndex==2){
			if (document.forma.cuenta2id.selectedIndex==0){
				alert("Favor de seleccionar la cuenta de la entidad");
				return false;
			}
		}
		else if (document.forma.tipoch.selectedIndex==3){
			if (document.forma.cuenta2bid.selectedIndex==0){
				alert("Favor de seleccionar la cuenta de staff");
				return false;
			}
		}
		sumafin=document.forma.cargofin.value-document.forma.abonofin.value;
		if (document.forma.monto.value!=sumafin.toFixed(2)){
			alert("Las aplicaciones deben ser igual al monto del cheque"+document.forma.monto.value+","+sumafin.toFixed(2));
			return false;
		}
	}
	if (document.forma.cuentapor[0].checked==true){
		if (document.forma.tipocp.selectedIndex==0){
			alert("Favor de seleccionar el tipo de CxP");
			return false;
		}
		else if (document.forma.tipocp.selectedIndex==1){
			if (document.forma.buscarproveedor.value==""){
				alert("Favor de ingresar el proveedor");
				return false;
			}
		}
		else if (document.forma.tipocp.selectedIndex==2){
			if (document.forma.buscaracreedor.value==""){
				alert("Favor de ingresar el acreedor");
				return false;
			}
		}
	}
	return true;
}
function cambio1(opcion) {
	if (opcion==1){
		document.getElementById("provdiv").style.display="";
		document.getElementById("acrediv").style.display="none";
	}
	else if (opcion==2) {
		document.getElementById("provdiv").style.display="none";
		document.getElementById("acrediv").style.display="";
	}
	else {
		document.getElementById("provdiv").style.display="none";
		document.getElementById("acrediv").style.display="none";
	}
}
function cambio2() {
	if (document.forma.cuentapor[0].checked==true){
		document.getElementById("tipocpdiv").style.display="";
		document.getElementById("otroschdiv").style.display="none";
		document.getElementById("cuentasubcuenta").style.display="none";
	}
	else {
		document.getElementById("tipocpdiv").style.display="none";
		document.getElementById("provdiv").style.display="none";
		document.getElementById("acrediv").style.display="none";
		document.getElementById("otroschdiv").style.display="";
		document.getElementById("cuentasubcuenta").style.display="";
	}
}
function cambio3() {
	if (document.forma.tipoch.selectedIndex==1 || document.forma.tipoch.selectedIndex==2){
		document.getElementById("cuentaentidad").style.display="";
		document.getElementById("cuentamario").style.display="none";
	}
	else if (document.forma.tipoch.selectedIndex==3) {
		document.getElementById("cuentaentidad").style.display="none";
		document.getElementById("cuentamario").style.display="";
	}
}
</script>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
<script src="js/jquery.js" type="text/javascript"></script>

   <script>
   
   window.onload = function ()
{
ColdFusion.navigate('cuentas.cfm?ch=25','txtResult');
cambio2();
}
   
   
function dynamic_Select(ajax_page, country)
{
$.ajax({
type: "GET",
url: ajax_page,
data: "ch=" + country,
dataType: "text/html",
success: function(html){       $("#txtResult").html(html);     }

  }); }
  
  
function dynamic_Select2(ajax_page, country)
{
$.ajax({
type: "GET",
url: ajax_page,
data: "ch=" + country,
dataType: "text/html",
success: function(html){       $("#txtResult2").html(html);     }

  }); }  
  
function dynamic_Select3(ajax_page, country)
{
	if (document.forma.cuenta3id.selectedIndex==0){
			alert("La cuenta no ha sido seleccionada");
			return false;
		}
		if (document.forma.subcuentaid.selectedIndex==0){
			alert("La subcuenta no ha sido seleccionada");
			return false;
		}
		if (document.forma.entidad2id.selectedIndex==0){
			alert("La entidad no ha sido seleccionada");
			return false;
		}
		if (document.forma.cargo.value=="" && document.forma.abono.value==""){
			alert("Favor de ingresar el cargo o el abono");
			return false;
		}
		if (document.forma.cargo.value!="" && isNaN(document.forma.cargo.value)){
			alert("El cargo debe ser numerico");
			return false;
		}
		if (document.forma.abono.value!="" && isNaN(document.forma.abono.value)){
			alert("El abono debe ser numerico");
			return false;
		}
		
$.ajax({
type: "GET",
url: ajax_page,
data: "ch=" + country + "&subcuentaid=" + document.getElementById('subcuentaid').options[document.getElementById('subcuentaid').selectedIndex].value + "&cargo=" + document.forma.cargo.value + "&abono=" + document.forma.abono.value + "&referencia1=" + document.forma.referencia1.value + "&concepto1=" + document.forma.concepto1.value + "&entidad2id=" + document.getElementById('entidad2id').options[document.getElementById('entidad2id').selectedIndex].value,
dataType: "text/html",
success: function(html){       $("#txtResult3").html(html);     }

  });
document.forma.cuenta3id.selectedIndex=0;
document.forma.subcuentaid.selectedIndex=0;
document.forma.entidad2id.selectedIndex=0;
document.forma.cargo.value="";
document.forma.abono.value="";
  
   }
   
   function dynamic_Select4(ajax_page, country)
{
$.ajax({
type: "GET",
url: ajax_page,
data: "ch=" + country,
dataType: "text/html",
success: function(html){       $("#txtResult4").html(html);     }

  }); }
  
     function dynamic_Select5(ajax_page, country)
{
$.ajax({
type: "GET",
url: ajax_page,
data: "ch=" + country,
dataType: "text/html",
success: function(html){       $("#txtResult3").html(html);     }

  }); }
  
  function busproveedor(prov){
	ColdFusion.navigate('jqry_provflag.cfm?prov='+prov,'vaciodiv');
  }
</script>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<!--- Llenado de datos para las busquedas--->
    <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
$(document).ready(function() {
	$("#buscarentidad").autocomplete("data/tienda.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	$("#buscarproveedor").autocomplete("data/proveedor.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	$("#buscaracreedor").autocomplete("data/acreedor.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	});
</script>
<!--- Fin del llenado de datos para busqueda --->
</head>

<body>
<div align="center">
  <div id="vaciodiv" style="display:none"></div>
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="pagoscheque.cfm" onSubmit="return valida(this)">
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">Pagos con Cheque</div></td>
        </tr>
        <cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Tienda
						ORDER BY Tienda
				  </cfquery>
        <tr>
          <td><table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="371" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td width="94" align="left" valign="middle"><p>Tienda</p></td>
                  <td width="263" align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput>
                  <select name="tiendaid" onChange="dynamic_Select('cuentas.cfm', this.value)">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Tiendas">
                    <option value="#Tienda_id#" <cfif Tienda_id eq 25> selected="selected"</cfif> >#Tienda#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput>
                   </td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Cuenta bancaria </p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF">
                    <div id="txtResult">
<select name="cuentaid">
<option value="">Seleccionar</option>
</select>
</div>
                  </td>
                </tr>
                <tr>
                  <td align="left" valign="middle">Fecha</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput><input size="15" type="text" name="fecha" value="#lsdateformat(now(),'dd/mm/yyyy')#"/></cfoutput>
                        <a href="#" onClick="displayCalendar(document.forma.fecha,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></a></td>
                </tr>
                <tr>
                  <td align="left" valign="middle">Abono a cuenta</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="checkbox" name="abono2" value="1" checked="checked" /></td>
                </tr>

                
              </table></td>
              <td width="385" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <!---<tr>
                  <td width="127" align="left" valign="middle"><p>Cheque Num.</p></td>
                  <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="cheque" id="textfield2" /></td>
                </tr>
				--->
                <tr>
                  <td align="left" valign="middle"><p>Monto&#13;</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="monto" id="textfield5" /></td>
                </tr>
                <tr>
                  <td align="left" valign="middle">Entidad</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput>
                  <select name="entidadid" onChange="dynamic_Select2('cuentas2.cfm', this.value)">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Tiendas">
                    <option value="#Tienda_id#" <cfif Tienda_id eq 25> selected="selected"</cfif> >#Tienda#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput></td>
                </tr>
                <tr><td colspan="2">
                <div id="cuentaentidad" style="display:none">
                <table border="0">
                <tr>
                  <td align="left" valign="middle"><p>Cuenta bancaria </p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF">
                    <div id="txtResult2">
                        <select name="cuenta2id">
                        <option value="">Seleccionar</option>
                        </select>
                        </div>
                  </td>
                </tr>
                </table>
                </div>
                <cfquery name="cuentas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT Cuenta_banco_id, Cuenta_banco FROM Cuenta_banco
                    WHERE Tienda_id=25 and activo=1
                    ORDER BY Cuenta_banco
                </cfquery>
                 <div id="cuentamario" style="display:none">
                <table border="0">
                <tr>
                  <td align="left" valign="middle"><p>Cuenta bancaria </p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF">
                        <select name="cuenta2bid">
                        <option value="">Seleccionar</option>
						<cfoutput query="cuentas">
                        	<option value="#Cuenta_banco_id#">#cuenta_banco#</option>
                        </cfoutput>
                        </select>
                  </td>
                </tr>
                </table>
                </div>
                </td></tr>
                <tr>
                  <td align="left" valign="middle">Anticipo</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="checkbox" name="anticipo" value="1" /></td>
                </tr>
                <tr>
                  <td align="left" valign="middle">Impreso a Mano</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="checkbox" name="amano" value="1" /></td>
                </tr>
                <tr>
                  <td align="left" valign="middle">Tipo de Pago</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><select name="tipopagoid">
                        	<option value="1" selected>Cheque</option>
							<option value="2" >Transferencia</option>
                        </select></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td valign="top"><table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="371" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="102" align="left" valign="middle"><p>Cuenta por pagar</p></td>
                    <td width="251" align="left" valign="middle" bgcolor="#FFFFFF"><label>
                      <input type="radio" value="1" name="cuentapor" id="checkbox3" onClick="cambio2()" checked="checked"/>
                    </label></td>
                  </tr>
                  <tr>
                    <td width="102" align="left" valign="middle"><p>Otros cheques</p></td>
                    <td width="251" align="left" valign="middle" bgcolor="#FFFFFF"><label>
                      <input type="radio" value="0" name="cuentapor" id="checkbox4" onClick="cambio2()"/>
                    </label></td>
                  </tr>

              </table></td>
              <td width="385" valign="top">
              <div id="tipocpdiv" style="display:none">
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="127" align="left" valign="middle"><p>Tipo CxP</p></td>
                    <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><select name="tipocp" id="select5" onChange="cambio1(this.value)">
                      <option value="">Seleccionar</option>
                      <option value="1">Proveedor</option>
                      <option value="2">Acreedor</option>
                    </select></td>
                  </tr>

              </table>
              </div>
              <div id="provdiv" style="display:none">
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="127" align="left" valign="middle"><p>Proveedor</p></td>
                    <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">
                    <input type="text" id="buscarproveedor" name="buscarproveedor" size="34" AUTOCOMPLETE="off" onBlur="busproveedor(this.value)" /><br /><samp id="provflag" style="color:#F00"></samp></td>
                  </tr>

              </table>
              </div>
              <div id="acrediv" style="display:none">
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="127" align="left" valign="middle"><p>Acreedor</p></td>
                    <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">
                    <input type="text" id="buscaracreedor" name="buscaracreedor" size="34" AUTOCOMPLETE="off" /></td>
                  </tr>

              </table>
              </div>
              <div id="otroschdiv" style="display:none">
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="127" align="left" valign="middle"><p>Tipo de pago</p></td>
                    <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><select name="tipoch" id="select6" onChange="cambio3(this.value)">
                      <option value="">Seleccionar</option>
                      <option value="1">Traspaso</option>
                      <option value="2">Prestamo</option>
                      <option value="3">Nomina</option>
                    </select></td>
                  </tr>

              </table>
              </div>
              </td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><div align="center" id="cuentasubcuenta" style="display:none">
          <cfquery name="Cuentas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Cuenta_contable
						ORDER BY Cuenta
				  </cfquery>
                  <cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Tienda
						ORDER BY Tienda
				  </cfquery>
            <table border="0"><tr><td>Cuenta</td><td>Subcuenta</td><td>Entidad</td><td>Cargo</td><td>Abono</td><td>Referencia</td><td>Concepto</td><td>&nbsp;</td></tr>
                <tr><td><cfoutput>
                  <select name="cuenta3id" onChange="dynamic_Select4('subcuentas.cfm', this.value)">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Cuentas">
                    <option value="#Cuenta#" >#cuenta#|#Descripcion#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput></td>
                  <td><div id="txtResult4">
<select name="subcuentaid" id="subcuentaid">
<option value="">Seleccionar</option>
</select>
</div></td><td><select name="entidad2id" id="entidad2id">
						<option value="" >Seleccionar</option>
						<CFOUTPUT query="Tiendas">
                    <option value="#Tienda_id#" <cfif isdefined("buscaracreedor") and tienda_id eq entidadid> selected="selected"</cfif>>#Tienda#</option>
                  </CFOUTPUT>
                        </select></td><td><input type="text" name="cargo" size="7" /></td><td><input type="text" name="abono" size="7" /></td><td><input type="text" name="referencia1" size="20" /></td><td><input type="text" name="concepto1" size="20" /></td><td><input type="button" value="aplicar" onClick="dynamic_Select3('addaplicacion.cfm', document.forma.cargo.value)" /></td></tr>
                </table>
                <div id="txtResult3">
                
              <input type="hidden" name="cargofin" value="0" />
              <input type="hidden" name="abonofin" value="0" />
                </div>
            <table width="497" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
              <tr>
                <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><p>Comentarios&#13;</p></td>
              </tr>
              <tr>
                <td align="left" valign="middle"><textarea name="comentarios" cols="80" rows="5" id="textfield"></textarea></td>
              </tr>
            </table>
          </div></td>
        </tr>
         <tr>
                <td align="center" valign="middle" ><table width="100%" border="0" cellpadding="3" cellspacing="1">
                  <tr>
                    <td width="238" align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
						<input type="submit" value="Capturar cheque" />
                        </div></td>
                  </tr>

                </table></td>
              </tr>
      </table></form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_pago.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Men&uacute;</title>
		<CFIF not isdefined("session.usuarioid")>
			<script language="JavaScript" type="text/javascript">
				alert("Usted no est� dentro del sistema");
				window.location="index.cfm";
			</script>
			
			<CFABORT>
		</CFIF>
		<CFIF session.tipousuario neq 1>
			<script language="JavaScript" type="text/javascript">
				alert("Lo sentimos, no tiene permiso para entrar a esta pantalla");
				window.location="index.cfm";
			</script>
			<CFABORT>
		</CFIF>
		<style>
			.btn-contacto{
				background-color: #374859;
				padding:10px 15px;
				border: #fff 1px solid;
				color: #fff;
				font-size: 12px;
				font-weight: 600;
				text-decoration:none;
				-webkit-border-radius: 3px;
				-moz-border-radius: 3px;
				border-radius:3px;
				transition:all .5s ease;
			}
			.btn-contacto:hover{
				background-color: #000;
				padding: 10px 20px;
			}
		</style>
		<script language="JavaScript">
			function edad() {
				var l_dt = new Date();
				var Ldt_due= document.frmEntrevista.nacimientofec.value;
				var Ldt_Year,Ldt_Month,Meses_nacido,Meses_hoy,anos,meses
				Ldt_Year = String(Ldt_due).substring(6,10)
				Ldt_Month = parseFloat(String(Ldt_due).substring(3,5));
				Ldt_day = parseFloat(String(Ldt_due).substring(0,2));
				//Set the two dates
				var millennium =new Date(Ldt_Year, Ldt_Month-1, Ldt_day) //Month is 0-11 in JavaScript
				today=new Date()
				//Get 1 day in milliseconds
				var one_day=1000*60*60*24
				//Calculate difference btw the two dates, and convert to days
				fecha2=Math.ceil((today.getTime()-millennium.getTime())/(one_day));
				anos3=fecha2/365;
				dias3=fecha2%365;
				meses3=dias3/30;
				diasf3=dias3%30-2;
				if (meses3==0) {
					if (diasf3==0){
						document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
					}
					else if (anos3>1) {
						document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
					}
					else {
						document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+diasf3+" dias";
					}
				}
				else {
					if (diasf3==0){
						document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
					}
					else if (anos3>1) {
						document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
					}
					else {
						document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+Math.floor(meses3)+" meses "+diasf3+" dias";
					}
				}
			}
			function show(event) {
				if (typeof event == "undefined")
					event = window.event;
				
				var val = event.keyCode;
				if(val == 13)
					document.forma.submit();
				
			}
		</script>
		<!--- Llenado de datos para las busquedas--->
		<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
		<!--- Fin del llenado de datos para busqueda --->
		<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
		<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
	</head>
	
	<cfquery name="qryTienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Distinct Tienda.Tienda_id, Tienda
		FROM Tienda
		WHERE Tienda.Activo=1
		Order by Tienda
	</cfquery>
	<cfif isdefined("Usuarioid")>
		<cfquery name="qryUsuario" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT *
			FROM Usuario
			WHERE Usuario_id=#usuarioid#
		</cfquery>
		<cfset nombre=#qryUsuario.nombre#>
		<cfset apellidopat=#qryUsuario.apellido_pat#>
		<cfset apellidomat=#qryUsuario.apellido_mat#>
		<cfset tiendaid=#qryUsuario.tienda_id#>
		<cfset usuario=qryUsuario.Usuario>
		<cfset contrasena=qryUsuario.Contrasena>
		<cfset activo=qryUsuario.Activo_bit>
		<cfset Correo=qryUsuario.Correo>
		<cfset FechaAlta=qryUsuario.Alta_fecha>
		<cfelse>
			<cfset Usuarioid=0>
			<cfset nombre="">
			<cfset apellidopat="">
			<cfset apellidomat="">
			<cfset tiendaid=0>
			<cfset usuario="">
			<cfset contrasena="">
			<cfset activo=1>
			<cfset Correo="">
			<cfset FechaAlta="">
	</cfif>

	<body>
		<div align="center">
			<table width="772" border="0" cellpadding="4" cellspacing="4">
				<tr>
					<td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">
						<div align="center" class="style7">
							<cfform method="post" name="frmUsuario" id="frmUsuario" action="Usuario_alta.cfm" onSubmit="return valida(this)">
								<cfoutput><input type="hidden" name="Usuarioid" id="Usuarioid" value="#Usuarioid#" /></cfoutput>
								<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
									<tr>
										<td>
											<div align="center" class="style11">
												<p><cfif Usuarioid neq 0>Edici�n<cfelse>Alta</cfif> 
												de Usuario</p>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<table width="756" border="0" cellpadding="3" cellspacing="1">
												<tr>
													<td width="127" align="left" valign="middle"><p>Estatus</p></td>
													<td width="236" align="left" valign="middle" bgcolor="#FFFFFF">
														<label>
															<cfoutput>Si <input type="radio" name="activo" value="1" <cfif activo eq 1>checked="checked"</cfif> /><br />No  <input type="radio" name="activo" value="0" <cfif activo eq 0>checked="checked"</cfif> /></cfoutput>
														</label>
													</td>
													<td align="left" valign="middle">*Tienda</td>
													<td align="left" valign="middle" bgcolor="#FFFFFF">
														<cfselect class="required" name="tiendaid" id="tiendaid" query="qryTienda" display="tienda" value="tienda_id" queryposition="below" selected="#tiendaid#">
															<option value=""></option>
														</cfselect>
													</td>
												</tr>
												<tr>
													<td>Nombre</td>
													<td>
														<cfoutput><input type="text" name="nombre" id="nombre" value="#nombre#" /></cfoutput>
													</td>
													<td>Apellido Paterno</td>
													<td>
														<cfoutput><input type="text" name="apellidopat" id="apellidopat" value="#apellidopat#" /></cfoutput>
													</td>
												</tr>
												<tr>
													<td>Apellido Materno</td>
													<td>
														<cfoutput><input type="text" name="apellidomat" id="apellidomat" value="#apellidomat#" /></cfoutput>
													</td>
													<td>Correo</td>
													<td>
														<cfoutput><input type="text" name="correo" id="correo" value="#Correo#" /></cfoutput>
													</td>
												</tr>
												<tr>
													<td>Usuario</td>
													<td>
														<cfoutput><input type="text" name="usuario" id="usuario" value="#usuario#" /></cfoutput>
													</td>
													<td>Contrase�a</td>
													<td>
														<cfoutput><input type="text" name="contrasena" id="contrasena" value="#contrasena#" /></cfoutput>
													</td>
												</tr>
												<tr>
													<td>Fecha de alta</td>
													
													<td align="left" valign="middle" bgcolor="#FFFFFF">
														<cfoutput><input size="15" type="text" name="fechaalta" value="#LSDateFormat(FechaAlta,"dd/mm/yyyy")#" /></cfoutput>
                                  						<a href="#" onClick=<cfoutput>"displayCalendar(document.frmUsuario.fechaalta,'dd/mm/yyyy',this)"</cfoutput>><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></a>
                                					</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<div align="center">
												<table width="497" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
													<tr>
														<td align="left" valign="middle">
															<cfdiv id="alta">
																<table width="100%" border="0" cellpadding="3" cellspacing="1">
																	<tr>
																		<td align="left" valign="middle" bgcolor="#FFFFFF">
																			<div align="center">
																				<a href="javascript:void(0)" onclick="valida(0,0)" class="btn-contacto">Guardar</a>
																			</div>
																		</td>
																	</tr>
																</table>
															</cfdiv>
														</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</cfform>
							<table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
								<tr>
									<td>
										<div align="left">
											<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
												<tr>
													<td width="58%">
														<table border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
																		<tr>
																			<td><a href="Usuarios.cfm" class="style9">Regresar</a></td>
																		</tr>
																	</table>
																</td>
																<td width="186" height="26">
																	<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
																		<tr>
																			<td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>                      
													</td>
													<td width="42%" bgcolor="#FFFFFF">Usuario: 
														<cfoutput>
															<div>#Session.nombre#</div>
														</cfoutput>
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>
						</div>
						<div align="right"></div>
					</td>
				</tr>
			</table>
		</div>
		<!--- jQuery 2.0.2 --->  	
		<!--- Llenado de datos para las busquedas--->
		 <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
		<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>

		<script type="text/javascript">
				function valida(revisado, autorizado){
					$("#alta").load('usuario_jqry_alta.cfm?'+$("#frmUsuario").serialize(),'alta');
					return false;
				}
		</script>
		<!--- Fin del llenado de datos para busqueda <cfdump var="#form#" label="Form"> --->
	</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Men�</title>
		<CFIF not isdefined("session.usuarioid")>
			<script language="JavaScript" type="text/javascript">
				alert("Usted no est� dentro del sistema");
				window.location="index.cfm";
			</script>
			<CFABORT>
		</CFIF>
    <!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<!-- Nucleo Icons -->
	<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
	<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
	<!-- Font Awesome Icons -->
	<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
	<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
	<!-- CSS Files -->
	<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
	<!-- Nepcha Analytics (nepcha.com) -->
	<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->

		<cfif isdefined("listo") and listo eq 1>
			<cftransaction>
				<cfloop list="#tiendasemanaid#" index="idx">
					<cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						UPDATE Tienda_semana_ano 
						SET publicar=0,
						autorizoencargada=0
						WHERE Control_id=#idx#
					</cfquery>
					<cfquery  name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						DELETE Empleado_semana_ano
						WHERE Tienda_semana_ano_id=#idx# and origen_id=1
					</cfquery>
				</cfloop>
			</cftransaction>
			<script language="JavaScript" type="text/javascript">
				alert("Las tiendas se rechazar�n con �xito");
				window.location="menu_nomina.cfm";
			</script>
		</cfif>
		<script language="JavaScript">
			function submit1(){
				document.forma.listo.value=1;
				document.forma.submit();
			}
			function submit2(){
				document.forma.submit();
			}
		</script>
	</head>

	<body class="g-sidenav-show  bg-gray-100">
		<!---sidenav.cfm es la barra del lateral izquierdo--->
		<cfinclude template="../sidenav.cfm">
		<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
		  <!-- Navbar -->
		  <cfinclude template="../navbar.cfm">
		  <!-- End Navbar -->

		<div align="center">
			<table width="772" border="0" cellpadding="4" cellspacing="4">
				<tr>
					<td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">
						<div align="center" class="style7">
							<table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
								<tbody>
									<tr>
										<td align="center" height="37" valign="middle"><div align="center"><span class="style11">Rechazar tiendas ya autorizadas</span></div></td>
									</tr>
									<tr>
										<td align="center" height="37" valign="middle" width="756">
											<table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
												<cfquery name="qrysemanaanoid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
													SELECT top 9 Semana_ano_id, Convert(varchar(2),Semana)+ '-' + Convert(varchar(4),Ano) as semana
													FROM Semana_ano
													WHERE Generada_bit=0
													order by Semana_ano_id desc
												</cfquery>
												<cfif not isdefined("semanaanoid")>
													<cfset semanaanoid="">
												</cfif>
												<cfform action="Elimina_autoriza.cfm" name="forma" method="POST">
													<tr>
														<td width="100%" valign="top">
															Semana:
															<cfselect name="semanaanoid" query="qrysemanaanoid" display="Semana" value="Semana_ano_id" queryPosition="below"  onchange="submit2(this);"  selected="#semanaanoid#" ><option value="">seleccionar</option></cfselect>
														</td>
													</tr>
													<cfif semanaanoid neq "">
														<input type="hidden" name="listo" value="0" />
														<cfquery name="qrytiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
															SELECT Tienda_semana_ano_id, Tienda.Tienda, autorizoencargada
															FROM Tienda_semana_ano INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
															WHERE Semana_ano_id=#semanaanoid# and autorizoencargada=1
														</cfquery>
														<tr>
															<td width="100%" valign="top">
																<table border="0" width="100%">
																	<tr><td>Tienda</td><td>Rechazar</td></tr>
																	<cfset color="##999999">
																	<cfoutput query="qrytiendas">
																		<tr bgcolor="#color#"><td>#Tienda#</td><td><input type="checkbox" name="tiendasemanaid" value="#Tienda_semana_ano_id#" /></td></tr>
																		<cfif color eq "##999999">
																			<cfset color="##FFFFFF">
																		<cfelse>
																			<cfset color="##999999">
																		</cfif>
																	</cfoutput>
																</table>
															</td>
														</tr>
														<cfif qrytiendas.recordcount neq 0>
															<tr>
																<td align="center">
																	<input type="button" value="Procesar" class="button" onclick="submit1(this);"><br>
																	<br>			
																</td>
															</tr>
														</cfif>
													</cfif>
												</cfform>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
							
						</div>
						<div align="right"></div>
					</td>
				</tr>
			</table>
		</div>

	</main>
	<!--   Core JS Files   -->
	<script src="../../../new-sistem/assets/js/core/bootstrap.min.js"></script>
	<script src="../../../new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
	<script src="../../../new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
	<script src="../../new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
	<!-- Kanban scripts -->
	<script src="../../../new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
	<script src="../../../new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
	<script src="../../../new-sistem/assets/js/plugins/chartjs.min.js"></script>
	<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
	<script src="../../../new-sistem/assets/js/plugins/tilt.min.js"></script>
	
	<!-- Github buttons -->
	<script async defer src="https://buttons.github.io/buttons.js"></script>
	<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
	<script src="../../../new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
	</body>
</html>

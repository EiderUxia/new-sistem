<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif isdefined("chequeid")>
<script language="JavaScript" type="text/javascript">
		<cfoutput>window.open('Cheques_preview_hp_pop.cfm?imprimir=1&chequeid=#chequeid#','Historia_cheque','width=700,height=400,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');
		</cfoutput>
	</script>
</cfif>
<cfquery name="chequesselprov" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT * FROM Cheque INNER JOIN Proveedor ON Proveedor.Proveedor_id=Cheque.Proveedor_id
            INNER JOIN Tienda ON Tienda.Tienda_id=Cheque.Tienda_id
        WHERE Impreso=0 and autorizado=1 and autorizado_vero=1
</cfquery>
<cfquery name="chequesselacre" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT * FROM Cheque INNER JOIN Acreedor ON Acreedor.Acreedor_id=Cheque.Acreedor_id
    	INNER JOIN Tienda ON Tienda.Tienda_id=Cheque.Tienda_id
	WHERE Impreso=0 and autorizado=1
</cfquery>
<cfquery name="chequesselent" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT *, Entidad.tienda as entidad FROM Cheque INNER JOIN Tienda as entidad ON Entidad.Tienda_id=Cheque.entidad_id
    	INNER JOIN Tienda ON Tienda.Tienda_id=Cheque.Tienda_id
	WHERE Impreso=0 and autorizado=1 and otro_cheque<>0
</cfquery>
<script language="JavaScript">
<!--
function valida(){
	var isChecked = false;
	<cfif chequesselprov.recordcount gt 1>
	for (var i = 0; i < document.forma1.chequeid.length; i++) {
	   if (document.forma1.chequeid[i].checked) {
		  isChecked = true;
	   }
	}

	if (isChecked == false){
		alert("Favor de seleccionar al menos uno");
		return false;
	}
	<cfelse>
	if (!document.forma1.chequeid.checked) {
		  alert("Favor de seleccionar el empleado");
		  return false;
	   }
	</cfif>
	document.forma1.submit();
}
-->
</script>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style8 {
	font-size: 24px;
	color: #000000;
}
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma1" action="cheque_imprimir_hp.cfm" onSubmit="return valida(this)">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
    
    <tbody>
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Cheques por imprimir</span></div></td>
	  </tr>
	<tr>
      <td align="center" height="37" valign="middle" width="756">
      <cfif chequesselprov.recordcount neq 0>
      <table border="1" align="center">
      <tr><td>No. Cheque</td><td>Monto del cheque</td><td>Tienda</td><td>Proveedor</td><td>Fecha</td><td>&nbsp;</td><td>&nbsp;</td></tr>
      <cfoutput query="chequesselprov">
      <tr><td><input type="checkbox" name="chequeid" value="#cheque_id#" /><a href="javascript:window.open('Cheque_info_pop.cfm?Chequeid=#cheque_id# ','Historia_cheque','width=600,height=500,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Cheque_num#</a></td><td>#LSCurrencyFormat(monto)#</td><td>#Tienda#</td><td>#Proveedor#</td><td>#LSDateFormat(fecha,"dd-mmm-yyyy")#</td><td><a href="javascript:window.open('Cheque_preview_pop.cfm?Chequeid=#cheque_id# ','Historia_cheque','width=700,height=400,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">Vista preliminar</a></td><td><a href="javascript:window.open('Cheque_preview_hp_pop.cfm?Chequeid=#cheque_id#&imprimir=1 ','Historia_cheque','width=700,height=400,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">Imprimir</a></td></tr>
      </cfoutput>
      </table>
      <cfelse>
      <div align="center">No ex�sten cheques para Proveedores</div>
      </cfif><br /><br />
      <!--- Se comento los cheques a acredores por el momento
      <cfif chequesselacre.recordcount neq 0>
      <table border="1" align="center">
      <tr><td>No. Cheque</td><td>Monto del cheque</td><td>Tienda</td><td>Proveedor</td><td>Fecha</td></tr>
      <cfoutput query="chequesselacre">
      <tr><td><input type="checkbox" name="chequeid" value="#cheque_id#" /> #Cheque_num#</td><td>#Monto#</td><td>#Tienda#</td><td>#Acreedor#</td><td>#LSDateFormat(fecha,"dd-mmm-yyyy")#</td></tr>
      </cfoutput>
      </table>
      <cfelse>
      <div align="center">No ex�sten cheques para Acreedores</div>
      </cfif>--->
      </td>
	</tr>
  </tbody></table>
  <div align="center"><table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="javascript:valida(this);void(0);" class="style9">Imprimir bloque</a></td>
                          </tr>
                        </table> </div>
  </form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                      	<td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_cheques.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

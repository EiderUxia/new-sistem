<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Alta Percepcion</title>
</head>
<cftransaction>
<cfquery name="qryempleadosemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			SELECT Tienda_semana_ano_id
			FROM Tienda_semana_ano
			WHERE Semana_ano_id=#semanaid# and Tienda_id =#tiendaid#
		</cfquery>
<cfif recurrente eq 1>
<cfif semana eq semanaid>
<cfset resto=monto-montosemana>
<cfelse>
<cfset resto=monto>
</cfif>
<cfquery name="qryseltipoDeduccion" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT TOP 1 tipo_deduccion_id
    FROM Tipo_percepcion
    WHERE Tipo_percepcion_id=#conceptoid#
</cfquery>
<cfquery name="qryAltaDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    INSERT Deduccion_recurrente (tipo_deduccion_id,Empleado_id,Semana_ano_id,Fecha,Monto_inicial,resto,deduccion_semana,inicio_semana_ano_id,Comentarios)
    Values (#qryseltipoDeduccion.tipo_deduccion_id#,#empleadoid#,#semanaid#,#CreateODBCDate(now())#,#monto#,#resto#,#montosemana#,#semana#,'#comentarios#')
</cfquery>
<cfquery name="qryAltaDeduccion" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT TOP 1 Deduccion_recurrente_id
    FROM Deduccion_recurrente
    Order BY Deduccion_recurrente_id DESC
</cfquery>
</cfif>
<cfif recurrente eq 1>
<cfquery name="qryaltaPercepciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    INSERT Percepcion (Tipo_Percepcion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Tienda_semana_ano_id)
    Values (#conceptoid#,#empleadoid#,#CreateODBCDate(now())#,#semanaid#,#monto#,<cfif fecha neq "">#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#<cfelse>NULL</cfif>,#qryAltaDeduccion.Deduccion_recurrente_id#,'#comentarios#', #qryempleadosemana.Tienda_semana_ano_id#)
</cfquery>
<cfelse>
<cfquery name="qryaltaPercepciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    INSERT Percepcion (Tipo_Percepcion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Tienda_semana_ano_id)
    Values (#conceptoid#,#empleadoid#,#CreateODBCDate(now())#,#semanaid#,#monto#,<cfif fecha neq "">#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#<cfelse>NULL</cfif>,NULL,'#comentarios#', #qryempleadosemana.Tienda_semana_ano_id#)
</cfquery>
</cfif>
</cftransaction>
<cfquery name="qryPercepciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT * FROM Percepcion INNER JOIN Tipo_Percepcion ON Percepcion.Tipo_Percepcion_id=Tipo_Percepcion.Tipo_Percepcion_id
    WHERE Semana_ano_id=#semanaid# and empleado_id=#empleadoid#
</cfquery>
<body>
<cfoutput>
   <cfset cargotot =0>
              <table border="0" align="center" width="500">
              <tr>
              	<td align="center">Historial de Percepciones<br />
                	La percepci&oacute;n se gener&oacute; con &eacute;xito
                </td>
              </tr>
              <tr>
              	<td><table border="0" align="center"><tr><td>Concepto</td><td>Monto</td><td>&nbsp;</td></tr>
                	<cfloop query="qryPercepciones">
                    <tr><td>#Tipo_Percepcion#</td><td>#LSCurrencyFormat(Monto)#</td><td><a href="javascript:Percepcionbaja('Percepcionbaja.cfm','#Percepcion_id#');void(0);"><img src="img/icono_tacha.png" width="20" height="20" alt="borrar" border="0" /></a></td></tr>
                    <cfset cargotot =cargotot+monto>
                    </cfloop>
                    <tr><td>Total</td><td>#LSCurrencyFormat(cargotot)#</td><td>&nbsp;</td></tr>
                </table></td>
              </tr>
              </table>
</cfoutput>
</body>
</html>
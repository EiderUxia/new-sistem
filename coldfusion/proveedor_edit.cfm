<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif session.usuarioid neq 7 and session.usuarioid neq 19 and session.usuarioid neq 1 and session.usuarioid neq 16>
	<script language="JavaScript" type="text/javascript">
		alert("No tiene permisos para esta pantalla");
		window.location="index.cfm";
	</script>
	<CFABORT>
</cfif>
<cfif isdefined("buscarproveedor")>
	<cfquery name="proveedorsel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Top 1 Proveedor.*, Ciudad.Ciudad, Estado.* FROM Proveedor INNER JOIN Ciudad ON Ciudad.Ciudad_id=Proveedor.Ciudad_id
			INNER JOIN Estado ON Estado.Estado_id=Ciudad.Estado_id
		WHERE Nombre_corto='#buscarproveedor#'
	</cfquery>
	<cfif proveedorsel.recordcount neq 1>
		<script language="JavaScript" type="text/javascript">
			alert("El proveedor no existe, favor de teclearlo nuevamente");
			window.location="proveedor_edit.cfm";
		</script>
		<cfabort>
	</cfif>
</cfif>
<cfif isdefined("nombre")>

<cftransaction>
<!---
--->
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		UPDATE Proveedor 
		SET Proveedor='#nombre#',
			Razon_social='#razonsocial#',
			Nombre_corto='#nombrecorto#',
			RFC='#RFC#',
			Calle='#direccion#',
			Colonia='#colonia#',
			CP='#cp#',
			ciudad_id=#ciudadid#,
			Nombre_contacto1c='#nombre1c#',
			Celular1c='#celular1c#',
			Nextel1c='#nextel1c#',
			Telefono1c='#telefono1c#',
			Correo1c='#correo1c#', 
			Nombre_contacto2c='#nombre2c#',
			Celular2c='#celular2c#',
			Nextel2c='#nextel2c#',
			Telefono2c='#telefono2c#',
			Correo2c='#correo2c#',
			<!---Nombre_contacto3c='#nombre3c#',--->
			Activo=<cfif isdefined("activo")>1<cfelse>0</cfif>,
			<!---Correo3c='#correo3c#',--->
			VC='#vc#'
			WHERE Proveedor_id=#proveedorid#	
	</cfquery>
	<!---
		<cfif isdefined("cuentaprobid")>    
			<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
				UPDATE Cuenta_banco_prov
				SET Cuenta_banco='#cuentabanco#',
					Banco_id=#bancoid#,
					Tipo_cuenta_id=#tipocuentaid#,
					Beneficiario='#beneficiario#',
					Clabe='#clabe#',
					Sucursal='#sucursal#'
					WHERE Cuenta_banco_prov_id=#cuentaprobid#
			</cfquery>
		<cfelse>
			<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
				INSERT INTO Cuenta_banco_prov (Cuenta_banco, Banco_id, Tipo_cuenta_id, Beneficiario, Clabe, Sucursal, Proveedor_id)
				VALUES ('#cuentabanco#', #bancoid#, #tipocuentaid#, '#beneficiario#', '#clabe#', '#sucursal#', #proveedorid#)
			</cfquery>
		</cfif>
	--->
	</cftransaction>
<script language="JavaScript" type="text/javascript">
		alert("El proveedor fue modificado con �xito");
		window.location="proveedor_edit.cfm";
	</script>
</cfif>
<cfif isdefined("buscarproveedor")>
<script language="JavaScript">
function valida(){
	var l5 = document.forma.direccion.value;
	var l6 = document.forma.colonia.value;
	var l7 = document.forma.cp.value;
	var l11 = document.forma.nombre2c.value;
	<!---var l12 = document.forma.nombre3c.value;--->
	if (document.forma.nombre.value==""){
		alert("Favor de ingresar el nombre del proveedor");
		return false;
	}
	if (document.forma.nombrecorto.value==""){
		alert("Favor de ingresar el nombre corto del proveedor");
		return false;
	}
	if (l5.length == 0){
		alert("Ingrese la direcci�n del proveedor");
		return false;
	}
	if (l6.length == 0){
		alert("Ingrese la colonia del proveedor");
		return false;
	}
	if (l7.length == 0){
		alert("Ingrese el c�digo postal del proveedor");
		return false;
	}
	if (document.forma.paisid.selectedIndex==0){
		alert("El pa�s del proveedor no ha sido seleccionado");
		return false;
	}
	if (document.forma.estadoid.selectedIndex==0){
		alert("El estado de residencia del proveedor no ha sido seleccionado");
		return false;
	}
	if (document.forma.ciudadid.selectedIndex==0){
		alert("La ciudad de residencia del proveedor no ha sido seleccionada");
		return false;
	}
	if (document.forma.nombre1c.value==""){
		alert("Favor de ingresar el nombre del contacto directo");
		return false;
	}
	if (document.forma.telefono1c.value==""){
		alert("Favor de ingresar el telefono del contacto directo");
		return false;
	}
	<!---
	if (document.forma.cuentabanco.value==""){
		alert("Favor de ingresar la cuenta bancaria del proveedor");
		return false;
	}

	if (document.forma.beneficiario.value==""){
		alert("Favor de ingresar el nombre del Titular de la cuenta");
		return false;
	}

	if (document.forma.bancoid.selectedIndex==0){
		alert("El banco del proveedor no ha sido seleccionado");
		return false;
	}
	if (document.forma.tipocuentaid.selectedIndex==0){
		alert("El tipo de cuenta no ha sido seleccionado");
		return false;
	}
	--->
	return true;
}
</script>
<!--- Inicio de codigo para el llenado de pais, estados, ciudades --->
<cfquery name="Paissel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT * FROM PAIS	
	</cfquery>

<!-- Script by hscripts.com -->
<!-- copyright hioxindia.com -->
<script language=javascript>
var aa = new Array("Seleccionar"<cfoutput query="paissel">,"#Pais#"</cfoutput>);
var aaid = new Array("sele"<cfoutput query="paissel">,"#Pais_id#"</cfoutput>);
paissele = new Array("Seleccionar Pa�s");
paisidsele = new Array("sele");
estadoidsele = new Array("sele");
estadosele = new Array("Seleccionar Estado");
<cfoutput query="paissel">
<cfquery name="Estadosel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT * FROM Estado
		WHERE pais_id=#pais_id#
		Order BY Estado
	</cfquery>
pais#pais_id# = new Array("Seleccionar"<cfloop query="estadosel">,"#Estado#"</cfloop>);
paisid#pais_id# = new Array("sele"<cfloop query="estadosel">,"#Estado_id#"</cfloop>);
<cfloop query="Estadosel">
<cfquery name="Ciudadsel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT * FROM Ciudad
		WHERE estado_id=#estado_id# and activo=1
		Order BY Ciudad
	</cfquery>
estado#estado_id# = new Array("Seleccionar"<cfloop query="ciudadsel">,"#ciudad#"</cfloop>);
estadoid#estado_id# = new Array("sele"<cfloop query="ciudadsel">,"#ciudad_id#"</cfloop>);
</cfloop>
</cfoutput>


function changeval()
{
 var val1 = "pais"+document.forma.paisid.value;
 var val2 = "paisid"+document.forma.paisid.value;
 var optionArray = eval(val1);
 var optionArray2 = eval(val2);
 for(var df=0; df<optionArray.length; df++)
 {
	var ss = document.forma.estadoid;
	ss.options.length = 0;
	for(var ff=0; ff<optionArray.length; ff++)
	{
	 var val = optionArray[ff];
	 var val3 = optionArray2[ff];
	 ss.options[ff] = new Option(val,val3);
	}
 }
 changeval2()
}

function changeval2()
{
 var val1 = "estado"+document.forma.estadoid.value;
 var val2 = "estadoid"+document.forma.estadoid.value;
 var optionArray = eval(val1);
 var optionArray2 = eval(val2);
 for(var df=0; df<optionArray.length; df++)
 {
	var ss = document.forma.ciudadid;
	ss.options.length = 0;
	for(var ff=0; ff<optionArray.length; ff++)
	{
	 var val = optionArray[ff];
	 var val3 = optionArray2[ff];
	 ss.options[ff] = new Option(val,val3);
	}
 }
}
</script>
	<cfquery name="Estadoselcont" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT * FROM Estado
		WHERE pais_id=#proveedorsel.pais_id#
	</cfquery>
<cfquery name="Ciudadselcont" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT * FROM Ciudad
		WHERE estado_id=#proveedorsel.estado_id# and activo=1
	</cfquery>	
</cfif>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
<!--- Llenado de datos para las busquedas--->
	<script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
$(document).ready(function(){
	$("#buscarproveedor").autocomplete("data/proveedort.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	});
</script>
<!--- Fin del llenado de datos para busqueda --->
</head>

<body>
<div align="center">
	<table width="772" border="0" cellpadding="4" cellspacing="4">
	
	<tr>
		<td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
	</tr>
	<tr>
		<td bgcolor="#FFFFFF"><div align="center" class="style7">
		<cfif not isdefined("buscarproveedor")>
		<form method="post" name="forma2" action="proveedor_edit.cfm" onSubmit="return valida(this)">
		<table width="756" border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td width="371" valign="top">
				<table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
				<tr>
					<td width="156" align="left" valign="middle"><p>Proveedor&#13;</p></td>
					<td width="197" align="left" valign="middle" bgcolor="#FFFFFF"><label>
					<input type="text" id="buscarproveedor" name="buscarproveedor" size="34" AUTOCOMPLETE="off" />
					</label></td>
				</tr>          
				</table></td>
				<td width="385" valign="center"><div align="center"><input type="submit" value="buscar" /></div></td>
			</tr>
			</table>
			</form>
			<cfelse>
			<cfoutput query="proveedorsel">
		<form method="post" name="forma" action="proveedor_edit.cfm" onSubmit="return valida(this)">
		<input type="hidden" name="proveedorid" value="#proveedor_id#" />
		<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
		<tr>
			<td><div align="center" class="style11">Editar Proveedor </div></td>
		</tr>
		<tr>
			<td>
			<table width="756" border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td width="371" valign="top"><table width="378" border="0" cellpadding="3" cellspacing="1">
				<tr>
					<td align="left" valign="top">*Marca</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="nombrecorto" tabindex="1" id="textfield12" value="#Nombre_corto#" /></td>
				</tr>
				<tr>
					<td width="127" align="left" valign="middle"><div align="left">*Calle y n�mero: </div></td>
					<td width="237" align="left" valign="middle" bgcolor="##FFFFFF"><div align="left">
						<input type="text" name="direccion" tabindex="3" value="#Calle#" />
					</div></td>
				</tr>
					<tr>
					<td width="127" align="left" valign="middle"><div align="left">*Pa&iacute;s: </div></td>
					<td width="237" align="left" valign="middle" bgcolor="##FFFFFF"><div align="left">
					<cfoutput>
					<select name="paisid" onchange="changeval()" tabindex="5">
									<option value="sele" >Seleccionar</option>
								<CFloop query="paissel">
									<option value="#pais_id#" <cfif pais_id eq proveedorsel.pais_id>  selected="selected"</cfif>>#pais#</option>
								</CFloop>
								</select>
								</cfoutput>
					</div></td>
					</tr>
					<tr>
					<td width="127" align="left" valign="middle"><div align="left">*Ciudad: </div></td>
					<td width="237" align="left" valign="middle" bgcolor="##FFFFFF"><div align="left">
					<select name="ciudadid" tabindex="7">
									<option value="sele" >Seleccionar</option>
									<CFloop query="ciudadselcont">
									<option value="#ciudad_id#" <cfif proveedorsel.ciudad_id EQ ciudad_id> selected="selected"</cfif>>#ciudad#</option>
								</CFloop>
							</select>
					</div></td>
					</tr>
				<tr>
					<td align="left" valign="top">RFC&##13;</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="rfc" id="textfield10" tabindex="9" value="#rfc#" /></td>
				</tr>
				<tr>
					<td align="left" valign="top">C�digo de proveedor (retail)</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="vc" id="textfield10" tabindex="9" value="#vc#" /></td>
				</tr>
				</table></td>
				<td width="385" valign="top"><table width="378" border="0" cellpadding="3" cellspacing="1">
				<tr>
					<td width="137" align="left" valign="middle">*Nombre&##13;de la Fabrica</td>
					<td width="216" align="left" valign="middle" bgcolor="##FFFFFF"><label>
					<input type="text" name="nombre" id="textfield" tabindex="2" value="#proveedor#"  />
					</label></td>
				</tr>
				<tr>
					<td width="137" align="left" valign="middle"><div align="left">*Colonia: </div></td>
					<td width="216" align="left" valign="middle" bgcolor="##FFFFFF"><div align="left">
						<input type="text" name="colonia" tabindex="4" value="#Colonia#"  />
					</div></td>
					</tr>
					<tr>
					<td width="137" align="left" valign="middle"><div align="left">*Estado: </div></td>
					<td width="216" align="left" valign="middle" bgcolor="##FFFFFF"><div align="left">
					<select name="estadoid" onchange="changeval2()" tabindex="6">
									<option value="sele" >Seleccionar</option>
									<CFloop query="estadoselcont">
									<option value="#estado_id#" <cfif proveedorsel.estado_id EQ estado_id> selected="selected"</cfif>>#estado#</option>
								</CFloop>
							</select>
					</div></td>
					</tr>
					<tr>
					<td width="137" align="left" valign="middle"><div align="left">*C.P.:</div></td>
					<td width="216" align="left" valign="middle" bgcolor="##FFFFFF"><div align="left">
						<input name="cp" type="text" size="5" maxlength="5" tabindex="8" value="#CP#" />
					</div></td>
					</tr>
				<tr>
					<td width="137" align="left" valign="middle">Raz&oacute;n Social&##13;</td>
					<td width="216" align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="razonsocial" id="textfield4" tabindex="10" value="#Razon_social#" /></td>
				</tr>
				</table></td>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<table width="100%" border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td valign="middle"><table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
					<tr><td colspan="4" align="left"><hr color="##0066FF" /></td></tr>
					<tr><td colspan="4" align="left">Contacto Directo (Propietario) </td></tr>
					<tr>
					<td width="80" align="left" valign="middle"><p>*Nombre</p></td>
					<td width="267" colspan="3" align="left" valign="middle" bgcolor="##FFFFFF"><label>
						<input type="text" name="nombre1c" id="textfield3" size="70" value="#Nombre_contacto1c#" />
					</label></td>
					</tr>
					<tr>
					<td align="left" valign="middle">*Tel&eacute;fono</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="telefono1c" id="textfield17" value="#Telefono1c#" /></td>
					<td align="left" valign="middle">Nextel</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="nextel1c" id="textfield24" value="#Nextel1c#" /></td>
					</tr>
					<tr>
					<td align="left" valign="middle">Celular</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="celular1c" id="textfield16" value="#Celular1c#" /></td>
					<td align="left" valign="middle">Mail</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="correo1c" id="textfield25" value="#Correo1c#" /></td>
					</tr>
					<tr><td colspan="4" align="left"><hr color="##0066FF" /></td></tr>
					<tr><td colspan="4" align="left">Contacto Ventas</td></tr>
					<tr>
					<td width="80" align="left" valign="middle"><p>Nombre</p></td>
					<td align="left" colspan="3" valign="middle" bgcolor="##FFFFFF"><input type="text" name="nombre2c" id="textfield18" size="70" value="#Nombre_contacto2c#" /></td>
					</tr>
					<tr>
					<td align="left" valign="middle">Tel&eacute;fono</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="telefono2c" id="textfield20" value="#Telefono2c#" /></td>
					<td align="left" valign="middle">Nextel</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="nextel2c" id="textfield27" value="#Nextel2c#" /></td>
					</tr>
					<tr>
					<td align="left" valign="middle">Celular</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="celular2c" id="textfield19" value="#Celular2c#" /></td>
					<td align="left" valign="middle">Mail</td>
					<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="correo2c" id="textfield28" value="#Correo2c#" /></td>
					</tr>
					<!---	Esta parte se coment� para poder llevar mejor control de las altas/bajas y cambios de cuentas bancarias --->
					<!---
						<tr><td colspan="4" align="left"><hr color="##0066FF" /></td></tr>
						<tr><td colspan="4" align="left">Contacto Pagos</td></tr>
						<tr>
							<td width="80" align="left" valign="middle"><p>Nombre</p></td>
							<td align="left" valign="middle" bgcolor="##FFFFFF" colspan="3"><input type="text" name="nombre3c" id="textfield21" size="70" value="#Nombre_contacto3c#" /></td>
						</tr>
						<cfquery name="Bancos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT * FROM Banco
							ORDER BY Banco
						</cfquery>
						<cfquery name="Tipocuentas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT * FROM Tipo_cuenta
							ORDER BY Tipo_cuenta
						</cfquery>
						<cfquery name="CuentaProv" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
							SELECT * FROM Cuenta_banco_prov
							WHERE Proveedor_id=#proveedor_id# and Activo=1
						</cfquery>
						<input type="hidden" name="cuentaprobid" value="#Cuentaprov.Cuenta_banco_prov_id#" />
						<tr>
							<td width="127" align="left" valign="middle"><p>*Banco&##13;</p></td>
							<td width="237" align="left" valign="middle" bgcolor="##FFFFFF">
							<cfoutput>
							<select name="bancoid">
								<option value="" >Seleccionar</option>
								<CFLOOP query="Bancos">
							<option value="#Banco_id#" <cfif cuentaprov.banco_id eq banco_id>selected="selected"</cfif> >#Banco#</option>
							</CFLOOP>
								</select>
								</cfoutput>
								</td>
							<td align="left" valign="middle"><p>*No. cuenta</p></td>
							<td align="left" valign="middle" bgcolor="##FFFFFF"><label>
							<input type="text" name="cuentabanco" id="textfield2" value="#cuentaprov.Cuenta_banco#" />
							</label></td>
						</tr>
						<tr>
							<td align="left" valign="middle"><p>No. de Sucursal (banco):</p></td>
							<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="sucursal" id="textfield3" value="#cuentaprov.sucursal#" /></td>
							<td align="left" valign="middle"><p>*Titular de la cuenta</p></td>
							<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="beneficiario" id="textfield3" value="#cuentaprov.Beneficiario#" /></td>
						</tr>
						<tr>
							<td align="left" valign="middle"><p>No. Clabe</p></td>
							<td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="clabe" id="textfield3" value="#cuentaprov.clabe#" /></td>
							<td align="left" valign="middle"><p>*Tipo de cuenta</p></td>
							<td align="left" valign="middle" bgcolor="##FFFFFF"><cfoutput>
							<select name="tipocuentaid">
								<option value="" >Seleccionar</option>
								<CFLOOP query="Tipocuentas">
							<option value="#Tipo_cuenta_id#" <cfif cuentaprov.Tipo_cuenta_id eq Tipo_cuenta_id>selected="selected"</cfif>>#Tipo_cuenta#</option>
							</CFLOOP>
								</select>
								</cfoutput></td>
						</tr>
						<tr>
							<td align="left" valign="middle">Correo</td>
							<td align="left" valign="middle" bgcolor="##FFFFFF" colspan="3"><input type="text" name="correo3c" id="textfield31" value="#Correo3c#" /></td>
						</tr>
					--->
				</table></td>
				</tr>
				<tr><td>
				<div align="center">Activo <input type="checkbox" name="activo" <cfif activo eq 1 >checked="checked"</cfif> /></div> 
				</td></tr>
				<tr><td>
				<div align="center"><input type="submit" value="Modificar" /></div>
				</td></tr>
			</table>
			</td>
		</tr>
		</table></form>
		</cfoutput>
		</cfif>
			<table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
			<tr>
				<td><div align="left">
				<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
					<tr>
					<td width="58%"><table border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td width="186" height="26">
							<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
							<tr>
							<td><a href="menu_catalogos.cfm" class="style9">Regresar</a></td>
							</tr>
						</table></td>
						<td width="186" height="26">
							<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
							<tr>
							<td><a href="menu.cfm" class="style9">Menu</a></td>
							</tr>
						</table></td>
						<td width="186" height="26">
							<table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
							<tr>
							<td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
							</tr>
						</table></td>
						</tr>
					</table>                      </td>
					<td width="42%" bgcolor="#FFFFFF">Usuario: 
						<cfoutput>
						<div>#Session.nombre#</div></cfoutput></td>
					</tr>
				</table>
				</div></td>
			</tr>
			</table>
		</div>
		<div align="right"></div></td>
	</tr>
	</table>
</div>
</body>
</html>

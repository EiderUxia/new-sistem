<cfparam name="form.InputExcelFile" default="">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Men&uacute;</title>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
    <style type="text/css">
      <!--
      .style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
      .style8 {
        font-size: 24px;
        color: #000000;
      }
      .style9 {color: #FFFFFF}
      .style11 {font-size: 18px}
      -->
    </style>
  </head>

  <body>
    <div align="center">
      <table width="772" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">
              <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
                <tbody>
                  <tr>
                    <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Generar archivo de empleados </span></div></td>
                  </tr>
                  <tr>
                    <td align="center" height="37" valign="middle" width="756">
                      <cfif not isdefined("listo")>
                        <table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
                          <cfform action="Generar_empleados.cfm" method="POST" enctype="multipart/form-data">
                            <tr>
                              <td width="100%" valign="top">
                                <input type="hidden" name="listo" value="1">
                              </td>
                            </tr>
                            <tr>
                              <td align="center">
                                <input type="Submit" value="Procesar" class="button"><br>
                                <br>			
                              </td>
                            </tr>
                          </cfform>
                        </table>
                        <cfelse>
                          <cfquery name="qryEmpleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT  Empleado_id, Nombre, Apellido_pat, Apellido_mat, Sexo, Ingreso_fecha, Capacitacion_fecha, Baja_fecha, Fecha_reingreso, Tienda_id, Puesto_id, Sueldo_base, Activo, Paga_infonavit, Efectivo_bit
                            FROM Empleado
                          </cfquery> 
                          <cfquery name="qryEmpleadodias" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                            SELECT Empleado_dia_semana.Empleado_id, Empleado_dia_semana.Dia_semana_id
                            FROM Empleado_dia_semana INNER JOIN Empleado ON Empleado_dia_semana.Empleado_id=Empleado.Empleado_id
                            WHERE Activo=1
                          </cfquery> 
                          <cfscript> 
                            //Use an absolute path for the files. ---> 
                            theDir=GetDirectoryFromPath(GetCurrentTemplatePath()); 
                            theFile=theDir & "empleados.xls"; 
                            //Create two empty ColdFusion spreadsheet objects. ---> 
                            theSheet = SpreadsheetNew("Empleados"); 
                            theSecondSheet = SpreadsheetNew("Empleados_dias"); 
                            //Populate each object with a query. ---> 
                            SpreadsheetAddRows(theSheet,qryEmpleados); 
                            SpreadsheetAddRows(theSecondSheet,qryEmpleadodias); 
                          </cfscript> 
                          <!--- Write the two sheets to a single file ---> 
                          <cfspreadsheet action="write" filename="#theFile#" name="theSheet"  sheetname="Empleados" overwrite=true> 
                          <cfspreadsheet action="update" filename="#theFile#" name="theSecondSheet"  sheetname="Empleados_dias"> 
                          <cfheader name="Content-Type" value="unknown">
                          <cfheader name="Content-Disposition" value="attachment; filename=empleados.xls">
                          <cfcontent type="application/Unknown" deletefile="yes" file="C:\inetpub\\erezoficina\empleados.xls">
                          <!---
                            <cfheader name="Content-Disposition" value="inline; filename=Employee_Report.xls">
                            <cfcontent type="application/vnd.ms-excel">
                            
                            <table border="2">
                            <cfoutput query="qryEmpleados">
                            <tr>
                            <td>#Empleado_id#</td><td>#Nombre#</td><td>#Apellido_pat#</td><td>#Apellido_mat#</td><td>#Sexo#</td><td>#Ingreso_fecha#</td><td>#Capacitacion_fecha#</td><td>#Baja_fecha#</td><td>#Fecha_reingreso#</td><td>#Tienda_id#</td>
                            <td>#Puesto_id#</td><td>#Sueldo_base#</td><td>#Activo#</td><td>#Paga_infonavit#</td>
                            </tr>
                            </cfoutput>
                            </table>
                            
                            </cfcontent>
                          --->
                      </cfif>
                    </td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
                <tr>
                  <td>
                    <div align="left">
                      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                        <tr>
                          <td width="58%">
                            <table border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="menu.cfm" class="style9">Menu</a></td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>                      
                          </td>
                          <td width="42%" bgcolor="#FFFFFF">Usuario: 
                            <cfoutput>
                              <div>#Session.nombre#</div>
                            </cfoutput>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

<cfparam name="form.InputExcelFile" default="">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Men&uacute;</title>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
    <style type="text/css">
      <!--
      .style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
      .style8 {
        font-size: 24px;
        color: #000000;
      }
      .style9 {color: #FFFFFF}
      .style11 {font-size: 18px}
      -->
    </style>
    <script language="JavaScript">
      function valida(val){
        if (document.forma.semanaanoid.selectedIndex==0){
          alert("seleccionar una semana");
          return false;
        }
        document.forma.listo.value=val;
        document.forma.submit();
      }
    </script>
  </head>

  <body>
    <div align="center">
      <table width="772" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">
              <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
                <tbody>
                  <tr>
                    <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Generar archivo para imprimir la Nomina</span></div></td>
                  </tr>
                  <tr>
                    <td align="center" height="37" valign="middle" width="756">
                      <cfif not isdefined("listo")>
                        <table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
                          <cfquery name="qrysemanaanoid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT top 9 Semana_ano_id, Convert(varchar(2),Semana)+ '-' + Convert(varchar(4),Ano) as semana
                            FROM Semana_ano
                            order by Semana_ano_id desc
                          </cfquery>
                          <cfform name="forma" action="Generar_reporte.cfm" method="POST" enctype="multipart/form-data">
                            <tr>
                              <td width="100%" valign="top">
                                Semana:
                                <cfselect name="semanaanoid" query="qrysemanaanoid" display="Semana" value="Semana_ano_id" queryPosition="below"><option value="">seleccionar</option></cfselect><input type="hidden" name="listo" value="0">
                              </td>
                            </tr>
                            <tr>
                              <td align="center">
                                <input type="button" value="Procesar Excel" onclick="valida(1);void(0);" class="button"> <input type="button" value="Procesar Txt" onclick="valida(2);void(0);" class="button"><br>
                                <br>			
                              </td>
                            </tr>
                          </cfform>
                        </table>
                        <cfelseif listo eq 1>
                          <cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT Tienda_semana_ano_id, Tienda_id, Semana_ano_id, Comentarios, Usuario_autorizo_id, Terminada, Autorizo_cont, Autorizo_cont_id, Comentario_autorizo, Pagada, Usuario_pagado_id, publicar, autorizoencargada, Usuario_autorizoencargada_id, Pares_descontados, Control_id, origen_id
                            FROM Tienda_semana_ano
                            WHERE Semana_ano_id = #semanaanoid# and autorizoencargada=1
                          </cfquery> 
                          <cfset semalist=valuelist(qrytiendasemana.Tienda_semana_ano_id)>
                          <cfquery name="qryEmpleadosemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT  Tienda.tienda, CASE WHEN Empleado_semana_ano.Pago_Efectivo_bit=1 THEN 'Efectivo' ELSE 'Tarjeta' END as Tipo_Pago, Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, 
                            Empleado_semana_ano.Pares_vendidos, Empleado_semana_ano.Pares_playa,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Comision_x_par as money),-1) as Comision_x_par, 
                            '$' + convert(varchar,cast(Empleado_semana_ano.Comision_x_par_playa as money),-1) as Comision_x_par_playa, 
                            Empleado_semana_ano.Pares_descontados, Empleado_semana_ano.Pares_vendidos-Empleado_semana_ano.Pares_descontados as pares_totales,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Comision_x_pagar as money),-1) as Comision_x_pagar,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Sueldo_base as money),-1) as Sueldo_base, 
                            '$' + convert(varchar,cast(Empleado_semana_ano.Otras_comisiones as money),-1) as Otras_comisiones, 
                            '$' + convert(varchar,cast(Empleado_semana_ano.Otras_percepciones as money),-1) as Otras_percepciones,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Otras_deducciones as money),-1) as Otras_deducciones, 
                            '$' + convert(varchar,cast(Empleado_semana_ano.Sueldo_pagar as money),-1) as Sueldo_pagar, 
                            '$' + convert(varchar,cast(0 as money),-1) as total_tienda
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#))
                            UNION ALL        
                            SELECT  Tienda.tienda, CASE WHEN Empleado_semana_ano.Pago_Efectivo_bit=1 THEN 'Efectivo' ELSE 'Tarjeta' END as Tipo_Pago, 0 as Empleado_id,  '' as NOmbre, 
                            0 as Pares_vendidos, 0 as Pares_playa,
                            '$' + convert(varchar,cast(0 as money),-1) as Comision_x_par, 
                            '$' + convert(varchar,cast(0 as money),-1) as Comision_x_par_playa,
                            0 as Pares_descontados, 0 as pares_totales,
                            '$' + convert(varchar,cast(0 as money),-1) as Comision_x_pagar,
                            '$' + convert(varchar,cast(0 as money),-1) as Sueldo_base, 
                            '$' + convert(varchar,cast(0 as money),-1) as Otras_comisiones, 
                            '$' + convert(varchar,cast(0 as money),-1) as Otras_percepciones,
                            '$' + convert(varchar,cast(0 as money),-1) as Otras_deducciones, 
                            '$' + convert(varchar,cast(0 as money),-1) as sueldo_pagar, 
                            '$' + convert(varchar,cast(SUM(Sueldo_pagar) as money),-1) as total_tienda
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#))
                            GROUP BY  Tienda.tienda, Empleado_semana_ano.Pago_Efectivo_bit
                            UNION ALL
                            SELECT  'Todas' as Tienda, CASE WHEN Empleado_semana_ano.Pago_Efectivo_bit=1 THEN 'Total  Efectivo' ELSE 'Total  Tarjeta' END as Tipo_Pago, 0 as Empleado_id,  '' as NOmbre, 
                            sum(Empleado_semana_ano.Pares_vendidos) as Pares_vendidos, sum(Empleado_semana_ano.Pares_playa) as Pares_playa,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Comision_x_par) as money),-1) as Comision_x_par,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Comision_x_par_playa) as money),-1) as Comision_x_par_playa, 
                            sum(Empleado_semana_ano.Pares_descontados) as Pares_descontados, sum(Empleado_semana_ano.Pares_vendidos-Empleado_semana_ano.Pares_descontados) as pares_totales,
                            '$' + convert(varchar,cast(SUM(Comision_x_pagar) as money),-1) as Comision_x_pagar,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Sueldo_base) as money),-1) as Sueldo_base, 
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Otras_comisiones) as money),-1) as Otras_comisiones, 
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Otras_percepciones) as money),-1) as Otras_percepciones,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Otras_deducciones) as money),-1) as Otras_deducciones, 
                            '$' + convert(varchar,cast(SUM(Sueldo_pagar) as money),-1) as sueldo_pagar, '$' + convert(varchar,cast(SUM(Sueldo_pagar) as money),-1) as total_tienda
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#))
                            GROUP BY  Empleado_semana_ano.Pago_Efectivo_bit
                            UNION ALL
                            SELECT  'Todas' as Tienda, 'Total Final' as Tipo_Pago, 0 as Empleado_id,  '' as NOmbre, 
                            sum(Empleado_semana_ano.Pares_vendidos) as Pares_vendidos, sum(Empleado_semana_ano.Pares_playa) as Pares_playa,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Comision_x_par) as money),-1) as Comision_x_par,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Comision_x_par_playa) as money),-1) as Comision_x_par_playa,
                            sum(Empleado_semana_ano.Pares_descontados) as Pares_descontados, sum(Empleado_semana_ano.Pares_vendidos-Empleado_semana_ano.Pares_descontados) as pares_totales,
                            '$' + convert(varchar,cast(SUM(Comision_x_pagar) as money),-1) as Comision_x_pagar,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Sueldo_base) as money),-1) as Sueldo_base, 
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Otras_comisiones) as money),-1) as Otras_comisiones, 
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Otras_percepciones) as money),-1) as Otras_percepciones,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Otras_deducciones) as money),-1) as Otras_deducciones, 
                            '$' + convert(varchar,cast(SUM(Sueldo_pagar) as money),-1) as sueldo_pagar, 
                            '$' + convert(varchar,cast(SUM(Sueldo_pagar) as money),-1) as total_tienda
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#))
                            ORDER BY CASE WHEN Empleado_semana_ano.Pago_Efectivo_bit=1 THEN 'Efectivo' ELSE 'Tarjeta' END , Tienda.tienda, total_tienda
                          </cfquery>
                          <cfquery name="qryEmpleadogeneral" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT  Tienda.tienda, CASE WHEN Empleado_semana_ano.Pago_Efectivo_bit=1 THEN 'Efectivo' ELSE 'Tarjeta' END as Tipo_Pago, Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, 
                            Empleado_semana_ano.Pares_vendidos, Empleado_semana_ano.Pares_playa,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Comision_x_par as money),-1) as Comision_x_par,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Comision_x_par_playa as money),-1) as Comision_x_par_playa,
                            Empleado_semana_ano.Pares_descontados, Empleado_semana_ano.Pares_vendidos-Empleado_semana_ano.Pares_descontados as pares_totales,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Comision_x_pagar as money),-1) as Comision_x_pagar,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Sueldo_base as money),-1) as Sueldo_base, '$' + convert(varchar,cast(Empleado_semana_ano.Otras_comisiones as money),-1) as Otras_comisiones, '$' + convert(varchar,cast(Empleado_semana_ano.Otras_percepciones as money),-1) as Otras_percepciones,
                            '$' + convert(varchar,cast(Empleado_semana_ano.Otras_deducciones as money),-1) as Otras_deducciones, '$' + convert(varchar,cast(Empleado_semana_ano.Sueldo_pagar as money),-1) as Sueldo_pagar, '$' + convert(varchar,cast(0 as money),-1) as total_tienda
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#))
                            UNION ALL        
                            SELECT  Tienda.tienda, 'Sub-Total' as Tipo_Pago, 0 as Empleado_id,  '' as NOmbre, 
                            sum(Empleado_semana_ano.Pares_vendidos) as Pares_vendidos, sum(Empleado_semana_ano.Pares_playa) as Pares_playa,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Comision_x_par) as money),-1) as Comision_x_par,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Comision_x_par_playa) as money),-1) as Comision_x_par_playa,
                            sum(Empleado_semana_ano.Pares_descontados) as Pares_descontados, sum(Empleado_semana_ano.Pares_vendidos-Empleado_semana_ano.Pares_descontados) as pares_totales,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Comision_x_pagar) as money),-1) as Comision_x_pagar,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Sueldo_base) as money),-1) as Sueldo_base, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_comisiones) as money),-1) as Otras_comisiones, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_percepciones) as money),-1) as Otras_percepciones,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_deducciones) as money),-1) as Otras_deducciones, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Sueldo_pagar) as money),-1) as sueldo_pagar, '$' + convert(varchar,cast(SUM(Sueldo_pagar) as money),-1) as total_tienda
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#))
                            GROUP BY  Tienda.tienda
                            UNION ALL
                            SELECT  'z Todas' as Tienda, 'Total' as Tipo_Pago, 0 as Empleado_id,  '' as NOmbre, 
                            sum(Empleado_semana_ano.Pares_vendidos) as Pares_vendidos, sum(Empleado_semana_ano.Pares_playa) as Pares_playa,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Comision_x_par) as money),-1) as Comision_x_par,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Comision_x_par_playa) as money),-1) as Comision_x_par_playa,
                            sum(Empleado_semana_ano.Pares_descontados) as Pares_descontados, sum(Empleado_semana_ano.Pares_vendidos-Empleado_semana_ano.Pares_descontados) as pares_totales,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Comision_x_pagar) as money),-1) as Comision_x_pagar,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Sueldo_base) as money),-1) as Sueldo_base, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_comisiones) as money),-1) as Otras_comisiones, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_percepciones) as money),-1) as Otras_percepciones,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_deducciones) as money),-1) as Otras_deducciones, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Sueldo_pagar) as money),-1) as sueldo_pagar, '$' + convert(varchar,cast(SUM(Sueldo_pagar) as money),-1) as total_tienda
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#))
                            UNION ALL
                            SELECT  'z Todas' as Tienda, 'Total Final' as Tipo_Pago, 0 as Empleado_id,  '' as NOmbre, 
                            sum(Empleado_semana_ano.Pares_vendidos) as Pares_vendidos, sum(Empleado_semana_ano.Pares_playa) as Pares_playa,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Comision_x_par) as money),-1) as Comision_x_par,
                            '$' + convert(varchar,cast(SUM(Empleado_semana_ano.Comision_x_par_playa) as money),-1) as Comision_x_par_playa,
                            sum(Empleado_semana_ano.Pares_descontados) as Pares_descontados, sum(Empleado_semana_ano.Pares_vendidos-Empleado_semana_ano.Pares_descontados) as pares_totales,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Comision_x_pagar) as money),-1) as Comision_x_pagar,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Sueldo_base) as money),-1) as Sueldo_base, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_comisiones) as money),-1) as Otras_comisiones, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_percepciones) as money),-1) as Otras_percepciones,
                            '$' + convert(varchar,cast(sum(Empleado_semana_ano.Otras_deducciones) as money),-1) as Otras_deducciones, '$' + convert(varchar,cast(sum(Empleado_semana_ano.Sueldo_pagar) as money),-1) as sueldo_pagar, '$' + convert(varchar,cast(SUM(Sueldo_pagar) as money),-1) as total_tienda
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#))
                            ORDER BY Tienda.tienda, total_tienda, nombre
                          </cfquery>
                          <cfquery name="qryDesglosepago" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                            SELECT  Tienda.tienda,  'Efectivo' Tipo_Pago, Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, 
                            '$' + convert(varchar,Empleado_semana_ano.Sueldo_pagar) as Sueldo_pagar, floor(Empleado_semana_ano.Sueldo_pagar/500) as B500,
                            floor((Empleado_semana_ano.Sueldo_pagar%500)/200) as B200,
                            floor(((Empleado_semana_ano.Sueldo_pagar%500)%200)/100) as B100,
                            floor((((Empleado_semana_ano.Sueldo_pagar%500)%200)%100)/50) as B50,
                            floor(((((Empleado_semana_ano.Sueldo_pagar%500)%200)%100)%50)/20) as B20,
                            floor((((((Empleado_semana_ano.Sueldo_pagar%500)%200)%100)%50)%20)/10) as M10,
                            floor(((((((Empleado_semana_ano.Sueldo_pagar%500)%200)%100)%50)%20)%10)/5) as M5,
                            floor((((((((Empleado_semana_ano.Sueldo_pagar%500)%200)%100)%50)%20)%10)%5)/2) as M2,
                            floor(((((((((Empleado_semana_ano.Sueldo_pagar%500)%200)%100)%50)%20)%10)%5)%2)/1) as M1,
                            floor((((((((((Empleado_semana_ano.Sueldo_pagar%500)%200)%100)%50)%20)%10)%5)%2)%1)/.5) as C5
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#)) and Empleado_semana_ano.Pago_Efectivo_bit=1 
                            Order By Tienda.tienda, Apellido_pat+' '+Apellido_mat+' '+Nombre
                          </cfquery>
                          <cfquery name="qryDepositos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                            SELECT  Tienda.tienda,  'Tarjeta' Tipo_Pago, Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, 
                            '1170' as institucion, '011020080011' as cuenta, '$' + convert(varchar,Empleado_semana_ano.Sueldo_pagar) as Sueldo_pagar,Cta_nomina, Num_nomina 
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#)) and Empleado_semana_ano.Pago_Efectivo_bit=0 and Sueldo_pagar>0
                            ORDER BY Num_nomina, Cta_nomina
                          </cfquery>
                          <cfscript> 
                            //Use an absolute path for the files. ---> 
                            theDir=GetDirectoryFromPath(GetCurrentTemplatePath()); 
                            theFile=theDir & "Reporte.xls"; 
                            //Create two empty ColdFusion spreadsheet objects. ---> 
                            theSheet = SpreadsheetNew("EmpleadosGeneral"); 
                            theSecondSheet = SpreadsheetNew("Empleados"); 
                            theThirdSheet = SpreadsheetNew("DesglosePago"); 
                            theFourthSheet = SpreadsheetNew("Depositos"); 
                            //Populate each object with a query. ---> 
                            SpreadsheetAddRow(theSheet,"Tienda, Forma de pago, Empleado_id, Nombre, Pares vendidos, Pares playa, Comision por par, Comision par Playa, Pares Reb, Total pares, Total comision, Sueldo, Comision Adicional, Total Percepciones, Total deducciones, Percepcion Neta, Total Tienda",1,1); 
                            SpreadsheetAddRows(theSheet,qryEmpleadogeneral); 
                            SpreadsheetAddRow(theSecondSheet,"Tienda, Forma de pago, Empleado_id, Nombre, Pares vendidos, Pares playa, Comision por par, Comision par Playa, Pares Reb, Total pares, Total comision, Sueldo, Comision Adicional, Total Percepciones, Total deducciones, Percepcion Neta, Total Tienda",1,1); 
                            SpreadsheetAddRows(theSecondSheet,qryEmpleadosemana); 
                            SpreadsheetAddRow(theThirdSheet,"Tienda, Forma de pago, Empleado_id, Nombre, Percepcion Neta, 500, 200, 100, 50, 20, 10, 5, 2, 1, .5",1,1);
                            SpreadsheetAddRows(theThirdSheet,qryDesglosepago); 
                            SpreadsheetAddRow(theFourthSheet,"Tienda, Forma de pago, Empleado_id, Nombre, Cuenta institucion, Cuenta, Percepcion Neta, Cuenta abono, Numero nomina",1,1);
                            SpreadsheetAddRows(theFourthSheet,qryDepositos); 
                          </cfscript> 
                          <!--- Write the two sheets to a single file ---> 
                          <cfspreadsheet action="write" filename="#theFile#" name="theSheet"  sheetname="EmpleadosGeneral" overwrite=true> 
                          <cfspreadsheet action="update" filename="#theFile#" name="theSecondSheet"  sheetname="Empleados"> 
                          <cfspreadsheet action="update" filename="#theFile#" name="theThirdSheet"  sheetname="DesglosePago"> 
                          <cfspreadsheet action="update" filename="#theFile#" name="theFourthSheet"  sheetname="Depositos"> 
                          <cfheader name="Content-Type" value="unknown">
                          <cfheader name="Content-Disposition" value="attachment; filename=Reporte.xls">
                          <cfcontent type="application/Unknown" deletefile="yes" file="C:\inetpub\erezoficina\Reporte.xls">
                          <cfelse>
                          <cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT Tienda_semana_ano_id, Tienda_id, Semana_ano_id, Comentarios, Usuario_autorizo_id, Terminada, Autorizo_cont, Autorizo_cont_id, Comentario_autorizo, Pagada, Usuario_pagado_id, publicar, autorizoencargada, Usuario_autorizoencargada_id, Pares_descontados, Control_id, origen_id
                            FROM Tienda_semana_ano
                            WHERE Semana_ano_id = #semanaanoid# and autorizoencargada=1
                          </cfquery> 
                          <cfset semalist=valuelist(qrytiendasemana.Tienda_semana_ano_id)>
                          <cfquery name="qryDepositos2" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                            SELECT  'D' as D, row_number() OVER (ORDER BY Num_nomina, Cta_nomina) n, 
                            '1170' as institucion, '011020080011' as cuenta, Empleado_semana_ano.Sueldo_pagar,Cta_nomina, Num_nomina 
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
                            INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
                            INNER JOIN Tienda ON Tienda.tienda_id=Tienda_semana_ano.Tienda_id
                            WHERE (Tienda_semana_ano.Tienda_semana_ano_id in (#semalist#)) and Empleado_semana_ano.Pago_Efectivo_bit=0 and Sueldo_pagar>0
                          </cfquery>
                          <cfset pagototal=0>
                          <cfloop query="qryDepositos2">
                            <cfset pagototal=pagototal+Sueldo_pagar>
                          </cfloop>
                          <cfset Content = "" />
                          <cfset Content &= '"H","01102008","011020080011","00'&qryDepositos2.recordcount&'",'&NumberFormat(pagototal,'__.__')>
                          <cfset Content &= chr(13)&chr(10) />
                          <cfloop query="qryDepositos2">
                              <cfset Content &= '"'&qryDepositos2["D"][currentrow]&'","'&qryDepositos2["n"][currentrow]&'","'&qryDepositos2["institucion"][currentrow]&'","'&qryDepositos2["cuenta"][currentrow]&'",'&NumberFormat(qryDepositos2["Sueldo_pagar"][currentrow],'____.__')&',"'&qryDepositos2["Cta_nomina"][currentrow]&'","'&qryDepositos2["Num_nomina"][currentrow]&'"'>
                              <cfset Content &= chr(13)&chr(10) />
                          </cfloop>
                          <cffile action="write" file="C:\inetpub\erezoficina\nomina.txt" output="#Content#" />
                          <cfheader name="Content-Disposition" value="attachment; filename=nomina.txt">
                          <cfcontent file="C:\inetpub\erezoficina\nomina.txt" type="text/plain">
                      </cfif>
                    </td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
                <tr>
                  <td>
                    <div align="left">
                      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                        <tr>
                          <td width="58%">
                            <table border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="menu.cfm" class="style9">Menu</a></td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>                      
                          </td>
                          <td width="42%" bgcolor="#FFFFFF">Usuario: 
                            <cfoutput>
                              <div>#Session.nombre#</div>
                            </cfoutput>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>
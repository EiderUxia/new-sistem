<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no está dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<CFIF session.tienda neq 0>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no esta autorizado para entrar a esta pantalla");
		window.location="menu_nomina.cfm";
	</script>
<CFABORT>
</CFIF>

<script language="JavaScript">
function valida(){
	document.forma.valor.value=1;
	document.forma.submit();
}


function submit2(){
	document.forma.semana.value=0;
	document.forma.submit();
}
</script>

<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>

</head>

<body class="g-sidenav-show  bg-gray-100"> 
    
  <!---sidenav.cfm es la barra del lateral izquierdo--->
   <cfinclude template="../sidenav.cfm">
   <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
     <!-- Navbar -->
     <cfinclude template="../navbar.cfm">
     <!-- End Navbar -->

<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="Comisiones.cfm">
      <input name="valor" type="hidden" value="0" />
	  <cfsavecontent variable="savesummary">
      <table width="100%" border="1" cellpadding="3" cellspacing="1" bordercolor="#<cfif not isdefined("excel")>FFFFFF<cfelse>000000</cfif>">
        <tr>
          <td><div align="center" class="style11">
            <p>Reporte de Comisiones por tienda</p>
            </div></td>
        </tr>
                  <cfquery name="qryComision" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT T.Tienda, TC.Comision_Encargada, TC.Comision_vendedora1, TC.Comision_sueldofijo, TC.Comision_falta, TC.Comision_Finsemana, TC.Pares_capacitacion, TC.Monto_capacitacion
FROM Tabla_Comision TC
	INNER JOIN Tienda T ON TC.Tienda_id=T.Tienda_id
Order By T.Tienda
				  </cfquery>
        <tr>
          <td><table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="850" valign="top" align="center"><table width="800" border="1" cellpadding="3" cellspacing="1" bordercolor="#<cfif not isdefined("excel")>FFFFFF<cfelse>000000</cfif>">
                <tr>
                  <td width="49" align="center" valign="middle"><p>Tienda</p></td>
                  <td width="49" align="center" valign="middle"><p>Comisión Encargada</p></td>
                  <td width="49" align="center" valign="middle"><p>Comisión Vendedora</p></td>
                  <td width="49" align="center" valign="middle"><p>Comisión Falta</p></td>
                  <td width="49" align="center" valign="middle"><p>Comisión sueldo fijo</p></td>
                  <td width="49" align="center" valign="middle"><p>Comisión Fin de semana</p></td>
                  <td width="49" align="center" valign="middle"><p>Pares capacitación</p></td>
                  <td width="49" align="center" valign="middle"><p>Monto Capacitación</p></td>
                  </tr>
                  <cfset color="##CCCCCC">
                  <cfoutput query="qryComision">
                  <tr bgcolor="#color#">
				  <td>#Tienda#</td>
				  <td align="right">#LSCurrencyFormat(Comision_Encargada)#</td>
				  <td align="right">#LSCurrencyFormat(Comision_vendedora1)#</td>
				  <td align="right">#LSCurrencyFormat(Comision_falta)#</td>
				  <td align="right">#LSCurrencyFormat(Comision_sueldofijo)#</td>
				  <td align="right">#LSCurrencyFormat(Comision_Finsemana)#</td>
				  <td align="right">#Pares_capacitacion#</td>
				  <td align="right">#LSCurrencyFormat(Monto_capacitacion)#</td>
                </tr>
                	<cfif color eq "##CCCCCC">
                    	<cfset color="##FFFFFF">
                    <cfelse>
                    	<cfset color="##CCCCCC">
                    </cfif>
                  </cfoutput>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table>
	  </cfsavecontent>
	  <cfif not isdefined("excel")>
	  <cfoutput>#savesummary#</cfoutput>
	  <cfelse>
	  <cffile action="WRITE" file="C:\inetpub\ereztienda\comisiones.xls" output="#savesummary#" >
<cfheader name="Content-Type" value="unknown">
<cfheader name="Content-Disposition" value="attachment; filename=comisiones.xls">
<cfcontent type="application/Unknown" deletefile="yes" file="C:\inetpub\ereztienda\comisiones.xls">
	  </cfif>
	  <a href="comisiones.cfm?excel=1">En Excel</a>
	  </form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>

<!--   Core JS Files   -->
<script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>

</body>
</html>

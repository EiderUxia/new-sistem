<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<CFIF session.tienda neq 0>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no esta autorizado para entrar a esta pantalla");
		window.location="menu_nomina.cfm";
	</script>
<CFABORT>
</CFIF>
<cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
SELECT Semana, Semana_curso.Semana_ano_id
FROM Semana_curso INNER JOIN Semana_ano ON Semana_ano.semana_ano_id=Semana_curso.Semana_ano_id
</cfquery>

<script language="JavaScript">
function valida(){
	document.forma.valor.value=1;
	document.forma.submit();
}


function submit2(){
	document.forma.semana.value=0;
	document.forma.submit();
}
</script>

<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>

</head>

<body class="g-sidenav-show  bg-gray-100"> 
    
  <!---sidenav.cfm es la barra del lateral izquierdo--->
   <cfinclude template="../sidenav.cfm">
   <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
     <!-- Navbar -->
     <cfinclude template="../navbar.cfm">
     <!-- End Navbar -->

<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="nomina_publicada.cfm">
      <input name="valor" type="hidden" value="0" />
	  <cfsavecontent variable="savesummary">
      <table width="100%" border="1" cellpadding="3" cellspacing="1" bordercolor="#<cfif not isdefined("excel")>FFFFFF<cfelse>000000</cfif>">
        <tr>
          <td><div align="center" class="style11">
            <p>Reporte de Infonavit semana <cfoutput>#qrysemanaActual.semana#</cfoutput> </p>
            </div></td>
        </tr>
                  <cfquery name="qryPrestamos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT  Deduccion_recurrente_id, Deduccion_recurrente.Empleado_id, Monto_inicial, resto, deduccion_semana, Empleado.Nombre, Empleado.Apellido_pat,
	Tienda.Tienda, Tienda.nombre_corto
FROM         Deduccion_recurrente INNER JOIN Empleado ON Empleado.Empleado_id=Deduccion_recurrente.Empleado_id
	INNER JOIN Tienda ON Tienda.Tienda_id=Empleado.Tienda_id
WHERE     (Deduccion_recurrente.tipo_deduccion_id = 10) AND (resto > 0)
	<cfif Session.tienda neq 0>and Empleado.Tienda_id=#Session.tienda#</cfif>
Order By tienda
				  </cfquery>
        <tr>
          <td><table width="756" border="<cfif not isdefined("excel")>0<cfelse>1</cfif>" cellpadding="3" cellspacing="1">
            <tr>
              <td width="850" valign="top" align="center"><table width="800" border="1" cellpadding="3" cellspacing="1" bordercolor="#<cfif not isdefined("excel")>FFFFFF<cfelse>000000</cfif>">
                <tr>
                  <td width="149" align="center" valign="middle"><p>Nombre</p></td>
                  <td width="49" align="center" valign="middle"><p>Tienda</p></td>
                  <td width="49" align="center" valign="middle"><p>Prestamo</p></td>
                  <td width="49" align="center" valign="middle"><p>Al inicio de<br /> semana</p></td>
                  <td width="49" align="center" valign="middle"><p>Descuento<br /> por semana</p></td>
                  <td width="49" align="center" valign="middle"><p>Descontado<br /> esta semana</p></td>
                  <td width="49" align="center" valign="middle"><p>Saldo actual</p></td>
                  <td width="49" align="center" valign="middle"><p>Descuento<br /> acumulado</p></td>
                  <cfif not isdefined("excel")><td width="49" align="center" valign="middle"><p>Comprobaci�n</p></td></cfif>
                  </tr>
				  <cfset sum1=0>
				  <cfset sum2=0>
				  <cfset sum3=0>
				  <cfset sum4=0>
				  <cfset sum5=0>
				  <cfset sum6=0>
				  <cfset sum7=0>
                  <cfoutput query="qryPrestamos">
<cfquery name="qrydescuentosemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT * FROM Deduccion
	WHERE     (tipo_deduccion_id = 10) and empleado_id=#empleado_id# and semana_ano_id=#qrysemanaActual.semana_ano_id# and Deduccion_recurrente_id=#Deduccion_recurrente_id#
</cfquery>
<cfquery name="qryDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT Sum(Monto) as acumulado
						FROM Deduccion 
                        WHERE Deduccion_recurrente_id=#Deduccion_recurrente_id# and empleado_id=#empleado_id#
				  </cfquery>
			<cfif qrydescuentosemana.recordcount eq 0>
				<cfset descestasemana=0>
			<cfelse>
				<cfset descestasemana=qrydescuentosemana.monto>
			</cfif>
				<cfset alinicio=resto+descestasemana>
				<cfset alfinal=resto>
                <cfset comprobacion=-monto_inicial+LSNumberFormat(qryDeducciones.acumulado,"__.__")+alfinal>
                  <tr>
                  <td align="left" valign="middle" bgcolor="##FFFFFF">#Nombre# #Apellido_pat#</td>
				  <td>#Nombre_Corto#</td>
				  <td>#LSCurrencyFormat(Monto_inicial)#</td>
				  <td><cfif alinicio lt 2500><font color="##FF0000">#LSCurrencyFormat(alinicio)#</font><cfelse>#LSCurrencyFormat(alinicio)#</cfif></td>
				  <td>#LSCurrencyFormat(deduccion_semana)#</td>
				  <td><cfif Monto_inicial neq alfinal and  not isdefined("excel")><a href="javascript:window.open('prestamos_hist.cfm?empleadoid=#empleado_id#&Deduccionrecurrenteid=#Deduccion_recurrente_id#','Deducciones','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#LSCurrencyFormat(descestasemana)#</a><cfelse>#LSCurrencyFormat(descestasemana)#</cfif></td>
				  <td>#LSCurrencyFormat(alfinal)#</td>
				  <td>#LSCurrencyFormat(qryDeducciones.acumulado)#</td>
				  <cfif not isdefined("excel")><td><cfif comprobacion neq 0><font color="##FF0000">#LSCurrencyFormat(comprobacion)#</font><cfelse>#LSCurrencyFormat(comprobacion)#</cfif></td></cfif>
                </tr>
				  <cfset sum1=sum1+Monto_inicial>
				  <cfset sum2=sum2+alinicio>
				  <cfset sum3=sum3+deduccion_semana>
				  <cfset sum4=sum4+descestasemana>
				  <cfset sum5=sum5+alfinal>
				  <cfif qryDeducciones.acumulado neq "">
				  <cfset sum6=sum6+qryDeducciones.acumulado>
				  </cfif>
				  <cfset sum7=sum7+comprobacion>
                  </cfoutput>
                <cfoutput>
				<tr>
                  <td align="right" valign="middle" colspan="2">Totales</td>
                  <td width="49" align="center" valign="middle">#LSCurrencyFormat(sum1)#</td>
                  <td width="49" align="center" valign="middle">#LSCurrencyFormat(sum2)#</td>
                  <td width="49" align="center" valign="middle">#LSCurrencyFormat(sum3)#</td>
                  <td width="49" align="center" valign="middle">#LSCurrencyFormat(sum4)#</td>
                  <td width="49" align="center" valign="middle">#LSCurrencyFormat(sum5)#</td>
                  <td width="49" align="center" valign="middle">#LSCurrencyFormat(sum6)#</td>
                  <cfif not isdefined("excel")><td width="49" align="center" valign="middle">#LSCurrencyFormat(sum7)#</td></cfif>
                  </tr>
				  </cfoutput>

              </table></td>
            </tr>
          </table></td>
        </tr>
      </table>
	  </cfsavecontent>
	  <cfif not isdefined("excel")>
	  <cfoutput>#savesummary#</cfoutput>
	  <cfelse>
	  <cffile action="WRITE" file="C:\inetpub\ereztienda\infonavit.xls" output="#savesummary#" >
<cfheader name="Content-Type" value="unknown">
<cfheader name="Content-Disposition" value="attachment; filename=infonavit.xls">
<cfcontent type="application/Unknown" deletefile="yes" file="C:\inetpub\ereztienda\infonavit.xls">
	  </cfif>
	  <a href="infonavit.cfm?excel=1">En Excel</a>
	  </form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
<!--   Core JS Files   -->
<script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif not isdefined("listo")>
	<cfset listo=0>
</cfif>
<cfif isdefined("buscarproveedor") and listo eq 2>
<cftransaction>
<cfquery name="proveedorid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Proveedor_id FROM Proveedor
	WHERE Nombre_corto='#buscarproveedor#'
</cfquery>
<cfif proveedorid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("El proveedor no existe, favor de teclearlo nuevamente");
		window.location="notacargo.cfm";
	</script>
    <cfabort>
</cfif>
<cfquery name="entidadid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Tienda_id as entidad_id FROM Tienda
	WHERE Tienda='#buscarentidad#'
</cfquery>
<cfif entidadid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("La entidad no existe, favor de teclearla nuevamente");
		window.location="notacargo.cfm";
	</script>
    <cfabort>
</cfif>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO notacargo (Proveedor_id,Concepto,tipo_pago_id,Monto,Entidad_id, usuario_id, comentarios,Fecha) VALUES
		(#proveedorid.proveedor_id#,'#concepto#', #tipopagoid#,#monto#, #entidadid.entidad_id#,#Session.usuarioid#, '#comentarios#',#CreateODBCDate(LSDateFormat(form.fecha,"mm/dd/yyyy"))#)			
</cfquery>
<cfquery name="notacargolast" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT top 1 notacargo_id FROM notacargo
	Order By notacargo_id desc
</cfquery>
<cfquery name="Aplicaciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						UPDATE Aplicacion 
                        SET llave_id=#notacargolast.notacargo_id#
                        WHERE Usuario_id=#Session.usuarioid# and llave_id is null
				  </cfquery>
</cftransaction>
<script language="JavaScript" type="text/javascript">
		<cfoutput>alert("La nota No. #notacargolast.notacargo_id# fue creada con �xito ");</cfoutput>
		<cfif oculto eq 1>
		window.location="notacargo.cfm";
		<cfelseif oculto eq 2 >
		<cfoutput>window.location="aplicarnotacargoprov.cfm?notacargo=#notacargolast.notacargo_id#";</cfoutput>
		</cfif>
	</script>
<cfelseif listo eq 1>
    <cfquery name="Altaaplicacion" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            INSERT INTO Aplicacion (Subcuenta_id,<cfif cargo neq "">Cargo,</cfif><cfif abono neq "">Abono,</cfif>Evento_id, usuario_id, Fecha) VALUES
            (#subcuentaid#,<cfif cargo neq "">#Cargo#,</cfif><cfif abono neq "">#Abono#,</cfif> 1,#Session.usuarioid#,#CreateODBCDate(LSDateFormat(now(),"mm/dd/yyyy"))#)			
    </cfquery>
<cfelseif listo eq 3>
	<cfquery name="aplicaciondel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        DELETE Aplicacion 
        WHERE Usuario_id=#Session.usuarioid# and llave_id is null and aplicacion_id=#oculto#
    </cfquery>
<cfelse>
    <cfquery name="aplicaciondel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        DELETE Aplicacion 
        WHERE Usuario_id=#Session.usuarioid# and llave_id is null
    </cfquery>
</cfif>
<script language="JavaScript">
function valida(val,preparado){
	if (document.forma.buscarproveedor.value==""){
		alert("Favor de ingresar el proveedor");
		return false;
	}
	if (document.forma.concepto.value==""){
		alert("Favor de ingresar el concepto");
		return false;
	}
	if (document.forma.fecha.value==""){
		alert("Favor de ingresar la fecha");
		return false;
	}
	if (document.forma.tipopagoid.selectedIndex==0){
	  	alert("El tipo de pago no ha sido seleccionado");
		return false;
	}
	if (document.forma.monto.value==""){
		alert("Favor de ingresar el monto del cheque");
		return false;
	}
	if (document.forma.buscarentidad.value==""){
		alert("Favor de ingresar la entidad");
		return false;
	}
	if (preparado==1){
		if (document.forma.cuentaid.selectedIndex==0){
			alert("La cuenta no ha sido seleccionada");
			return false;
		}
		if (document.forma.subcuentaid.selectedIndex==0){
			alert("La subcuenta no ha sido seleccionada");
			return false;
		}
		if (document.forma.cargo.value=="" && document.forma.abono.value==""){
			alert("Favor de ingresar el cargo o el abono");
			return false;
		}
		if (document.forma.cargo.value!="" && isNaN(document.forma.cargo.value)){
			alert("El cargo debe ser numerico");
			return false;
		}
		if (document.forma.abono.value!="" && isNaN(document.forma.abono.value)){
			alert("El abono debe ser numerico");
			return false;
		}
	}
	if (preparado==2){
	sumafin=document.forma.cargofin.value-document.forma.abonofin.value;
		if (document.forma.monto.value!=sumafin){
			alert("Las aplicaciones deben ser igual al monto de la nota");
			return false;
		}
	}
	document.forma.oculto.value=val;
	document.forma.listo.value=preparado;
	document.forma.submit();
}
</script>
<script src="js/jquery.js" type="text/javascript"></script>

   <script>
function dynamic_Select(ajax_page, country)
{
$.ajax({
type: "GET",
url: ajax_page,
data: "ch=" + country,
dataType: "text/html",
success: function(html){       $("#txtResult").html(html);     }

  }); }
</script>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<!--- Llenado de datos para las busquedas--->
    <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
$(document).ready(function(){
	$("#buscarentidad").autocomplete("data/tienda.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	$("#buscarproveedor").autocomplete("data/proveedor.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	});
</script>
<!--- Fin del llenado de datos para busqueda --->
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="notacargo.cfm" onSubmit="return valida(this)">
      <input type="hidden" name="oculto" value="0" />
      <cfoutput><input type="hidden" name="listo" value="#listo#" /></cfoutput>
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">Nota de cargo</div></td>
        </tr>
        
        <tr>
          <td><table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="371" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td width="94" align="left" valign="middle"><p>Proveedor&#13;</p></td>
                  <td width="263" align="left" valign="middle" bgcolor="#FFFFFF"><label>
                    <cfoutput><input type="text" id="buscarproveedor" name="buscarproveedor" size="34" AUTOCOMPLETE="off" <cfif isdefined("buscarproveedor")>value="#buscarproveedor#"</cfif> /></cfoutput>
                  </label></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Concepto</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><label><cfoutput><input type="text" name="concepto" <cfif isdefined("buscarproveedor")>value="#concepto#"</cfif>/> </cfoutput>       </label></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Fecha </p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput><input size="15" type="text" name="fecha" <cfif isdefined("buscarproveedor")>value="#fecha#"</cfif>/></cfoutput>
                        <a href="#" onClick="displayCalendar(document.forma.fecha,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></a></td>
                </tr>
                
                  <cfquery name="Tipopagos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Tipo_pago
						ORDER BY Tipo_pago
				  </cfquery>
              </table></td>
              <td width="385" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td width="127" align="left" valign="middle"><p>Tipo de pago</p></td>
                  <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput>
                  <select name="tipopagoid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="tipopagos">
                    <option value="#tipo_pago_id#" <cfif isdefined("buscarproveedor") and tipopagoid eq tipo_pago_id>selected="selected"</cfif>>#Tipo_pago#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Monto&#13;</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput><input type="text" name="monto" <cfif isdefined("buscarproveedor")>value="#monto#"</cfif> id="textfield5" /></cfoutput></td>
                </tr>
                <tr>
                  <td align="left" valign="middle">Entidad</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput><input type="text" id="buscarentidad" <cfif isdefined("buscarproveedor")>value="#buscarentidad#"</cfif> name="buscarentidad" size="34" autocomplete="off" /></cfoutput></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><div align="center">
            <table width="497" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
              <tr>
                <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><p>Comentarios&#13;</p></td>
              </tr>
              <tr>
                <td align="left" valign="middle"><textarea name="comentarios" cols="80" rows="3" id="textfield"></textarea></td>
              </tr>
              <cfquery name="Cuentas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Cuenta_contable
						ORDER BY Cuenta
				  </cfquery>
              <tr>
              	<td><table border="0"><tr><td>Cuenta</td><td>Subcuenta</td><td>Cargo</td><td>Abono</td><td>&nbsp;</td></tr>
                <tr><td><cfoutput>
                  <select name="cuentaid" onchange="dynamic_Select('subcuentas.cfm', this.value)">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Cuentas">
                    <option value="#Cuenta#" >#cuenta#|#Descripcion#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput></td>
                  <td><div id="txtResult">
<select name="subcuentaid">
<option value="">Seleccionar</option>
</select>
</div></td><td><input type="text" name="cargo" size="10" /></td><td><input type="text" name="abono" size="10" /></td><td><input type="button" value="aplicar" onclick="valida(0,1)" /></td></tr>
                </table> </td>
              </tr>
               <cfquery name="Aplicaciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Aplicacion INNER JOIN Subcuenta ON Subcuenta.subcuenta_id=Aplicacion.Subcuenta_id
                        WHERE Usuario_id=#Session.usuarioid# and llave_id is null
						ORDER BY Cuenta, subcuenta
				  </cfquery>
                  <cfif aplicaciones.recordcount neq 0>
                  <cfset abonotot =0>
                  <cfset cargotot =0>
              <tr>
              	<td align="left">Historial de aplicaciones</td>
              </tr>
              <tr>
              	<td><table border="0"><tr><td>Cuenta</td><td>Nombre cuenta</td><td>Cargo</td><td>Abono</td><td>&nbsp;</td></tr>
                	<cfoutput query="aplicaciones">
                    <cfif cargo neq "">
                    <cfset cargotot =cargotot+cargo>
                    </cfif>
                    <cfif abono neq "">
                    <cfset abonotot =abonotot+abono>
                    </cfif>
                    <tr><td>#cuenta#-#subcuenta#</td><td>#nombre#</td><td>#LSCurrencyFormat(Cargo)#</td><td>#LSCurrencyFormat(Abono)#</td><td><a href="javascript:valida(#aplicacion_id#,3);void(0);"><img src="images/icono_tacha.png" width="20" height="20" alt="borrar" border="0" /></a></td></tr>
                    </cfoutput>
                </table></td>
              </tr>
              <cfoutput>
              <input type="hidden" name="cargofin" value="#cargotot#" />
              <input type="hidden" name="abonofin" value="#abonotot#" />
              </cfoutput>
              <cfelse>
              <input type="hidden" name="cargofin" value="0" />
              <input type="hidden" name="abonofin" value="0" />
              </cfif>
              <tr>
                <td align="left" valign="middle"><table width="100%" border="0" cellpadding="3" cellspacing="1">
                  <tr>
                    <td width="238" align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
						<INPUT TYPE="BUTTON" VALUE="Generar nota" onclick="valida(1,2)">
                        </div></td>
                    <td width="238" align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
						<INPUT TYPE="BUTTON" VALUE="Generar y aplicar nota" onclick="valida(2,2)">
                    </div></td>
                  </tr>

                </table></td>
              </tr>
            </table>
          </div></td>
        </tr>
      </table>
      </form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_notacargo.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

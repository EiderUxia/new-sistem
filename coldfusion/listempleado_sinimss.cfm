<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif isdefined("empleadoid")>
<cfloop list="#empleadoid#" index="i">
<cfquery name="empleadosel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	UPDATE Notificacion
    	SET	posponer_fecha=DATEADD(day,30,posponer_fecha)
	WHERE Empleado_id=#i#
</cfquery>
</cfloop>
<script language="JavaScript" type="text/javascript">
		alert("Los cambios se hicieron con �xito");
		window.location="listempleado_sinimss.cfm";
	</script>
</cfif>
<cfquery name="empleadosel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT *, nombre+' '+apellido_pat+' '+apellido_mat as empleadonom
    FROM Empleado INNER JOIN Notificacion ON Empleado.Empleado_id=Notificacion.Empleado_id
    	INNER JOIN Tienda ON Empleado.Tienda_id=Tienda.Tienda_id
	WHERE posponer_fecha <= #CreateODBCDate(LSDateFormat(now(),"mm/dd/yyyy"))#
</cfquery>

<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style8 {
	font-size: 24px;
	color: #000000;
}
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
<script language="JavaScript">
<!--
function valida(){
	var isChecked = false;
	<cfif empleadosel.recordcount gt 1>
	for (var i = 0; i < document.forma1.empleadoid.length; i++) {
	   if (document.forma1.empleadoid[i].checked) {
		  isChecked = true;
	   }
	}

	if (isChecked == false){
		alert("Favor de seleccionar al menos uno");
		return false;
	}
	<cfelse>
	if (!document.forma1.empleadoid.checked) {
		  alert("Favor de seleccionar el empleado");
		  return false;
	   }
	</cfif>
	document.forma1.submit();
}
-->
</script>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma1" action="listempleado_sinimss.cfm" onSubmit="return valida(this)">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Listado de empleados sin IMSS</span></div></td>
	  </tr>
	<tr>
      <td align="center" height="37" valign="middle" width="756">
      <cfif empleadosel.recordcount neq 0>
      <table border="1" align="center">
      <tr><td>Empleado</td><td>Fecha de alta</td><td>Tienda</td><td>Editar</td></tr>
      <cfoutput query="empleadosel">
      <tr><td align="left"><input type="checkbox" id="empleadoid" name="empleadoid" value="#empleado_id#" />#empleadonom#</td><td align="right">#LSDateFormat(Ingreso_fecha,"dd/mm/yyyy")#</td><td align="left">#Tienda#</td><td><a href="editempleadostienda.cfm?buscarempleado=#empleadonom#">Editar</a></td></tr>
      </cfoutput>
      </table>
      <cfelse>
      <div align="center">No ex�sten empleados sin IMSS</div>
      </cfif><br /><br />
      </td>
	</tr>
    </table>
    <div align="center"><table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="javascript:valida(this);void(0);" class="style9">Posponer 30 dias</a></td>
                          </tr>
                        </table> </div>
  </form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                      	<td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_reportes.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

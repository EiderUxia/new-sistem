﻿<cfif isdefined("actualizasem")>
<cfsilent>
	<cfquery name="qrySemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        Select Semana, Semana_curso.Semana_ano_id
        FROM Semana_ano INNER JOIN Semana_curso ON Semana_ano.Semana_ano_id=Semana_curso.Semana_ano_id
    </cfquery>
    <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resEntrevistado">
		UPDATE Prestamo
        SET Semana_ano_id=#qrySemana.Semana_ano_id#
		WHERE Prestamo_id=#Prestamoid#
</cfquery>
	<cfset mensaje="La semana se actualizo con éxito">
</cfsilent>
<cfoutput>
	<div align="center">
    #mensaje#
    </div>
	<script>
		$("##semanasamp").html("#qrySemana.Semana#");
		$("##semanaid").val("#qrySemana.Semana_ano_id#");
   </script>
</cfoutput>
<cfelse>
<cfsilent>
<cftransaction>
<cfif Prestamoid eq 0>
	<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resPrestamo">
		INSERT INTO Prestamo (Semana_ano_id, Tienda_id, Empleado_id, Comentarios, Monto, Descuento_semanal, Observaciones, Usuario_id, Usuario_revision_id, Revisado_bit) 
		VALUES
		(#semanaid#, #tiendaid#, #empleadoid#, '#Comentarios#', #monto#, #descuentosemana#, <cfif isdefined("observaciones") >'#observaciones#'<cfelse>NULL</cfif>, #session.usuarioid#, <cfif isdefined("observaciones") >#session.usuarioid#<cfelse>NULL</cfif>, 0)
</cfquery>
	<cfset Prestamoid=resPrestamo.IDENTITYCOL>
	<cfset mensaje="EL prestamo se solicitó con exito">
<cfelse>
    <cfset montoautoriza=montoaut>
	<cfif autorizado eq 0>
    	<cfset revisadobit=1>
        <cfset autorizadobit=0>
        <cfset rechazadobit=0>
        <cfset aceptadobit=0>
    <cfelseif autorizado eq 1>
    	<cfset revisadobit=1>
        <cfset autorizadobit=1>
        <cfset rechazadobit=0>
        <cfset aceptadobit=1>
    <cfelseif autorizado eq 2>
    	<cfset revisadobit=1>
        <cfset autorizadobit=0>
        <cfset rechazadobit=1>
        <cfset aceptadobit=0>
    <cfelseif autorizado eq 3>
    	<cfset revisadobit=1>
        <cfset autorizadobit=1>
        <cfset rechazadobit=0>
        <cfset aceptadobit=1>
    <cfelseif autorizado eq 4>
    	<cfset revisadobit=1>
        <cfset autorizadobit=0>
        <cfset rechazadobit=1>
        <cfset aceptadobit=0>
        <cfset montoautoriza=0>
    </cfif>
    <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resEntrevistado">
		UPDATE Prestamo
        SET Descuento_semanal=#descuentosemana#,
			Observaciones=<cfif isdefined("observaciones") >'#observaciones#'<cfelse>NULL</cfif>,
			Usuario_revision_id=#session.usuarioid#,
			Revisado_bit=#revisadobit#,
            Autorizado_bit=#autorizadobit#,
            Monto_autoriza=#montoautoriza#,
            Rechazado_bit=#rechazadobit#,
            Aceptado_bit=#aceptadobit#
		WHERE Prestamo_id=#Prestamoid#
</cfquery>
	<cfset mensaje="EL cambio se dio exitosamente">
</cfif>
</cftransaction>

</cfsilent>
<cfoutput>
	<div align="center">
    #mensaje#
    </div>
	<script>
		$("##Prestamoid").val("#Prestamoid#");
   </script>
</cfoutput>
</cfif>
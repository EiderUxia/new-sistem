<cfparam name="form.InputExcelFile" default="">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Men�</title>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
    		<!--     Fonts and icons     -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
		<!-- Nucleo Icons -->
		<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
		<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
		<!-- Font Awesome Icons -->
		<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
		<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
		<!-- CSS Files -->
		<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
		<!-- Nepcha Analytics (nepcha.com) -->
		<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
		<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
		
  </head>

  <body class="g-sidenav-show  bg-gray-100">
		<!---sidenav.cfm es la barra del lateral izquierdo--->
		<cfinclude template="../sidenav.cfm">
		<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
		  <!-- Navbar -->
		  <cfinclude template="../navbar.cfm">
		  <!-- End Navbar -->

    <div align="center">
      <table width="772" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">
              <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
                <tbody>
                  <tr>
                    <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Generar archivo para imprimir la Nomina <cfif session.tienda eq 0>(paso7)</cfif></span></div></td>
                  </tr>
                  <tr>
                    <td align="center" height="37" valign="middle" width="756">
                      <cfif not isdefined("listo")>
                        <table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
                          <cfquery name="qrysemanaanoid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT top 9 Semana_ano_id, Convert(varchar(2),Semana)+ '-' + Convert(varchar(4),Ano) as semana
                            FROM Semana_ano
                            order by Semana_ano_id desc
                          </cfquery>
                          <cfform action="Generar_autoriza.cfm" method="POST" enctype="multipart/form-data">
                            <tr>
                              <td width="100%" valign="top">
                                Semana:
                                <cfselect name="semanaanoid" query="qrysemanaanoid" display="Semana" value="Semana_ano_id" queryPosition="below"><option value="">seleccionar</option></cfselect><input type="hidden" name="listo" value="1">
                              </td>
                            </tr>
                            <tr>
                              <td align="center">
                                <input type="Submit" value="Procesar" class="button"><br>
                                <br>			
                              </td>
                            </tr>
                          </cfform>
                        </table>
                        <cfelse>
                          <cfquery name="qryupdsemanaanoid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            UPDATE Semana_ano
                            SET Generada_bit=1
                            WHERE Semana_ano_id=#semanaanoid#
                          </cfquery>
                        <cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                          SELECT Tienda_semana_ano_id, Tienda_id, Semana_ano_id, Comentarios, Usuario_autorizo_id, Terminada, Autorizo_cont, Autorizo_cont_id, Comentario_autorizo, Pagada, Usuario_pagado_id, publicar, autorizoencargada, Usuario_autorizoencargada_id, Pares_descontados, Control_id, origen_id
                          FROM Tienda_semana_ano
                          WHERE Semana_ano_id = #semanaanoid#
                        </cfquery> 
                        <cfset semalist=valuelist(qrytiendasemana.Tienda_semana_ano_id)>
                        <cfquery name="qryEmpleadosemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                          SELECT Empleado_dia_semana_ano_id, Empleado_id, Dia_Semana_ano_id, Concepto_dia_trabajado_id,  ISNULL(Venta_pares, 0) as Venta_pares, ISNULL(Venta_playa, 0) as Venta_playa, Pares_descontados, Tienda_semana_ano_id, Control_id, origen_id
                          FROM Empleado_dia_semana_ano
                          WHERE (Tienda_semana_ano_id in (#semalist#))
                        </cfquery>
                        <cfquery name="qryDeduccionsemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                          SELECT Deduccion_id, Tipo_deduccion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Deduccion_recurrente_id, Comentaios, Tienda_semana_ano_id, Deduccion_id as Control_id, origen_id
                          FROM Deduccion
                          WHERE (Tienda_semana_ano_id in (#semalist#)) 
                        </cfquery>
                        <cfquery name="qryPercepcionsemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                          SELECT  Percepcion_id, Tipo_Percepcion_id, Empleado_id, Fecha, Semana_ano_id, Monto, Fecha_movimiento, Deduccion_recurrente_id, Comentaios, Tienda_semana_ano_id, Percepcion_id as Control_id, origen_id
                          FROM Percepcion
                          WHERE (Tienda_semana_ano_id in (#semalist#)) 
                        </cfquery>
                        <cfquery name="qryEmpleadosemana2" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                          SELECT  Empleado_semana_ano_id, Tienda_semana_ano_id, Empleado_id, Sueldo_base, Pares_vendidos, ISNULL(Venta_playa, 0) as Venta_playa, Pares_descontados, Comision_x_par, Comision_x_par_playa, Comision_x_pagar, Otras_deducciones, Otras_percepciones, Otras_comisiones, Sueldo_pagar, Control_id, origen_id, Pago_Efectivo_bit
                          FROM Empleado_semana_ano
                          WHERE (Tienda_semana_ano_id in (#semalist#))
                        </cfquery>
                        <cfquery name="qryDeduccionRecurrente" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#"> 
                          SELECT Deduccion_recurrente_id, tipo_deduccion_id, Empleado_id, Semana_ano_id, Fecha, Monto_inicial, resto, deduccion_semana, inicio_semana_ano_id, Comentarios, Automatica_bit, Semana_cambio_id
                          FROM Deduccion_recurrente
                          WHERE Semana_cambio_id=#semanaanoid#
                        </cfquery>
                        <cfscript> 
                          //Use an absolute path for the files. ---> 
                          theDir=GetDirectoryFromPath(GetCurrentTemplatePath()); 
                          theFile=theDir & "autorizonom.xls"; 
                          //Create two empty ColdFusion spreadsheet objects. ---> 
                          theSheet = SpreadsheetNew("Tienda"); 
                          theSecondSheet = SpreadsheetNew("Empleados"); 
                          theThirdSheet = SpreadsheetNew("Deducciones"); 
                          theFourthSheet = SpreadsheetNew("EmpleadosSem"); 
                          theFifthSheet = SpreadsheetNew("Percepcion");  
                          theSixSheet = SpreadsheetNew("DeduccionRecu"); 
                          //Populate each object with a query. ---> 
                          SpreadsheetAddRows(theSheet,qrytiendasemana);
                        </cfscript> 
                        <!--- Write the two sheets to a single file ---> 
                        <cfspreadsheet action="write" filename="#theFile#" name="theSheet"  sheetname="Tienda" overwrite=true> 
                        <cfspreadsheet action="update" filename="#theFile#" query="qryEmpleadosemana"  sheetname="Empleados"> 
                        <cfspreadsheet action="update" filename="#theFile#" query="qryDeduccionsemana"  sheetname="Deducciones"> 
                        <cfspreadsheet action="update" filename="#theFile#" query="qryEmpleadosemana2"  sheetname="EmpleadosSem"> 
                        <cfspreadsheet action="update" filename="#theFile#" query="qryPercepcionsemana"  sheetname="Percepcion"> 
                        <cfspreadsheet action="update" filename="#theFile#" query="qryDeduccionRecurrente"  sheetname="DeduccionRecu"> 
                        <cfheader name="Content-Type" value="unknown">
                        <cfheader name="Content-Disposition" value="attachment; filename=autorizonom.xls">
                        <cfcontent type="application/Unknown" deletefile="yes" file="C:\inetpub\EREZtienda\autorizonom.xls">
                      </cfif>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>

  </main>
  <!--   Core JS Files   -->
<script src="../../../new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="../../new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="../../../new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="../../../new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="../../../new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="../../../new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../../new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
  </body>
</html>
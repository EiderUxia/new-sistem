<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Men&uacute;</title>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
    <cfif isdefined("appaterno")>
      <cfquery name="tiendaid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT tienda_id FROM tienda
        WHERE tienda='#buscartienda#'
      </cfquery>
      <cfif tiendaid.recordcount neq 1>
        <script language="JavaScript" type="text/javascript">
          alert("La tienda no existe, favor de teclearno nuevamente");
          window.location="altaempleadostienda.cfm";
        </script>
        <cfabort>
      </cfif>

      <cfif isdefined("archivo") and archivo neq "">
        <cfset request.AcceptImage=        "image/jpg, image/jpeg, image/pjpeg">
        <!---       now try to upload the file       --->    
        <cftry> 
          <cffile action="UPLOAD" filefield="archivo" destination="#gsPath#fotos\Original" nameconflict="MAKEUNIQUE" accept="#request.AcceptImage#">
          <cfset variables.Success="Uploaded."> 
          <cfcatch type="Application"> 
            <!---          something went wrong.  Was it a mime type failure?          --->
            <cfif isdefined("cfcatch.MimeType")>
              <!---              yes it was.  show the friendly error message.              --->
              <cfif not ListContains (request.AcceptImage,cfcatch.MimeType)>    
                <div align="center"><font face="Arial, Helvetica, sans-serif"><span class="style1">Favor de s&oacute;lo subir im�genes en formato jpg</span></font></div>
                <cfelse>       
                  <!---                 Hmmm.  the mimetype is there but the file was on the list.  Better dump out the whole error message.                --->
                  <cfoutput><b>Error</b><br> #cfcatch.Message#  #cfcatch.Detail# </cfoutput>
              </cfif>
              <cfelse> 
                <!---              Hmmm.  No mimetype error in the catch scope.              Better dump out the whole error message.             --->
                <cfoutput>             <b>Error</b><br>             #cfcatch.Message#             #cfcatch.Detail#             </cfoutput> 
            </cfif>
            <cfabort> 
          </cfcatch> 
        </cftry>
        <cfset gsPath2=gsPath&"images\">
        <CFSET Imagen=#File.ServerFile#>
        <cfset imageCFC = createObject("component","image")>
        <cfset imgInfo = imageCFC.getImageInfo("", gsPath2&"Original\"&Imagen)>
        <cfset anchoimg=imginfo.width>
        <cfset altoimg=imginfo.height>
        <cfset anchop=imgInfo.height*0.75188>
        <CFIF imgInfo.width GT anchop>
          <cfset imgInfo2 = imageCFC.scaleWidth("", gsPath2&"original\"&Imagen, gsPath2&Imagen,100)>
          <cfset imgInfo3 = imageCFC.getImageInfo("", gsPath2&Imagen)>
          <cfset altura= (137-imgInfo3.height)/2>
          <cfset results1 = imageCFC.watermark("", "", gsPath2&"fondo2.jpg", gsPath2&Imagen, 1, 0, altura,gsPath2&Imagen)>
          <cfelse>
            <cfset imgInfo2 = imageCFC.scaleHeight("", gsPath2&"original\"&Imagen, gsPath2&Imagen,137)>
            <cfset imgInfo3 = imageCFC.getImageInfo("", gsPath2&Imagen)>
            <cfset ancho= (100-imgInfo3.width)/2>
            <cfset results1 = imageCFC.watermark("", "", gsPath2&"fondo2.jpg", gsPath2&Imagen, 1, ancho, 0,gsPath2&Imagen)>
        </CFIF>
        <cfset subimagen=1>
        <cfelse>
          <cfset subimagen=0>
      </cfif>
      <cfquery name="SelAltavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Nombre, apellido_pat, apellido_mat
        FROM Empleado
        WHERE Nombre='#nombre#' and apellido_pat='#appaterno#' and apellido_mat='#apmaterno#'
      </cfquery>
      <cfif SelAltavacuna.recordcount eq 0>
        <cftransaction>
          <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="EpleadoRes">
            INSERT INTO Empleado (Nombre, apellido_pat, apellido_mat, imss, <cfif isdefined("nacimientofec") and nacimientofec neq "">nacimiento_fecha,</cfif> telefono, estado_civil_id, hijos, <!---escolaridad_id,---> <cfif isdefined("altafec") and altafec neq "">ingreso_fecha,</cfif><cfif capfec neq "">Capacitacion_fecha,</cfif> tienda_id,<!--- tienda_cerca_id,--->puesto_id, sueldo_base, como_entro, <cfif subimagen eq 1>Imagen,</cfif> observaciones, <cfif isdefined("imssfec") and imssfec neq "">IMSS_fecha,</cfif> RFC, Afore, CURP, Pasaporte, Contacto_emergencia, IFE, Telefono_emergencia, Camisa,Paga_infonavit <cfif pagoinfonavit eq 1>, Infonavit</cfif>, Efectivo_bit, Num_nomina, Cta_nomina) VALUES
            ('#nombre#', '#appaterno#', '#apmaterno#', <cfif imss neq "">'#imss#'<cfelse>NULL</cfif>, <cfif isdefined("nacimientofec") and nacimientofec neq "">#CreateODBCDate(LSDateFormat(form.nacimientofec,"mm/dd/yyyy"))#,</cfif>'#telefono#',<cfif isdefined("estadocivilid") and estadocivilid neq "">#estadocivilid#<cfelse>NULL</cfif>,#hijos#,<!---#escolaridadid#,---><cfif isdefined("altafec") and altafec neq "">#CreateODBCDate(LSDateFormat(form.altafec,"mm/dd/yyyy"))#,</cfif><cfif capfec neq "">#CreateODBCDate(LSDateFormat(form.capfec,"mm/dd/yyyy"))#,</cfif> #tiendaid.tienda_id#, <!---#tienda2id.tienda_id#,---> #puestoid#, '#sueldo#', '#comoentero#', <cfif subimagen eq 1>'#Imagen#',</cfif> '#observaciones#', <cfif isdefined("imssfec") and imssfec neq "">#CreateODBCDate(LSDateFormat(imssfec,"mm/dd/yyyy"))#,</cfif> '#rfc#', '#Afore#', '#CURP#', '#Pasaporte#', '#emergencianombre#', '#IFE#', '#emergenciatelefono#', '#Camisa#',#pagoinfonavit# <cfif pagoinfonavit eq 1>,#montoinfonavit#</cfif>, #pagoefectivo#, <cfif numnomina neq "">#numnomina#<cfelse>0</cfif>, '#cuentanum#')
          </cfquery>

          <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            INSERT INTO Empleado_hist (Fecha_alta, Empleado_id)
            VALUES (#CreateODBCDate(LSDateFormat(form.altafec,"mm/dd/yyyy"))#, #EpleadoRes.IDENTITYCOL#)
          </cfquery>
          <cfif FileExists("C:\inetpub\erezdecache\images\foto_new.jpg")>
            <cffile action="rename" source = "C:\inetpub\erezdecache\images\foto_new.jpg" destination = "C:\inetpub\erezdecache\images\foto_#EpleadoRes.IDENTITYCOL#.jpg"> 
            <cfset Imagen="foto_#empleadoid#.jpg">
            <cfset subimagen=1>
            <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              UPDATE Empleado 
              SET Imagen=#Imagen#
              WHERE empleado_id=#EpleadoRes.IDENTITYCOL#
            </cfquery>
          </cfif>
          <cfloop list="#diastrabaja#" index="i">
            <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              INSERT INTO empleado_dia_semana (empleado_id, dia_semana_id) VALUES
              ('#EpleadoRes.IDENTITYCOL#', '#i#')		
            </cfquery>
          </cfloop>
          <cfif anexo1 neq "" or  anexo2 neq "" or  anexo3 neq "" >
            <cfif  anexo1 neq "">
              <cffile action="UPLOAD" filefield="anexo1" destination="#gsPath#anexo" nameconflict="MAKEUNIQUE">
              <cfquery name="altaanexo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                INSERT INTO Anexo (Anexo, Tipo_anexo_id, Empleado_id)
                VALUES ('#File.ServerFile#',#tipoanexo1id#, #EpleadoRes.IDENTITYCOL#)
              </cfquery>
            </cfif>
            <cfif  anexo2 neq "">
              <cffile action="UPLOAD" filefield="anexo2" destination="#gsPath#anexo" nameconflict="MAKEUNIQUE">
              <cfquery name="altaanexo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                INSERT INTO Anexo (Anexo, Tipo_anexo_id, Empleado_id)
                VALUES ('#File.ServerFile#',#tipoanexo2id#, #EpleadoRes.IDENTITYCOL#)
              </cfquery>
            </cfif>
            <cfif  anexo3 neq "">
              <cffile action="UPLOAD" filefield="anexo3" destination="#gsPath#anexo" nameconflict="MAKEUNIQUE">
              <cfquery name="altaanexo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                INSERT INTO Anexo (Anexo, Tipo_anexo_id, Empleado_id)
                VALUES ('#File.ServerFile#',#tipoanexo3id#, #EpleadoRes.IDENTITYCOL#)
              </cfquery>
            </cfif>
          </cfif>
        </cftransaction>
        <script language="JavaScript" type="text/javascript">
          alert("El empleado fue agregado con �xito");
          window.location="altaempleadostienda.cfm";
        </script>
        <cfelse>
          <script language="JavaScript" type="text/javascript">
            <cfoutput>alert("El empleado ya est� registrado");</cfoutput>
            window.location="menu_catalogos.cfm";
          </script>
      </cfif>
    </cfif>
    <script src="swfobject.js" language="javascript"></script>
    <script language="JavaScript">
      function edad() {
        var l_dt = new Date();
        var Ldt_due= document.forma.nacimientofec.value;
        var Ldt_Year,Ldt_Month,Meses_nacido,Meses_hoy,anos,meses
        Ldt_Year = String(Ldt_due).substring(6,10)
        Ldt_Month = parseFloat(String(Ldt_due).substring(3,5));
        Ldt_day = parseFloat(String(Ldt_due).substring(0,2));
        //Set the two dates
        var millennium =new Date(Ldt_Year, Ldt_Month-1, Ldt_day) //Month is 0-11 in JavaScript
        today=new Date()
        //Get 1 day in milliseconds
        var one_day=1000*60*60*24
        //Calculate difference btw the two dates, and convert to days
        fecha2=Math.ceil((today.getTime()-millennium.getTime())/(one_day));
        anos3=fecha2/365;
        dias3=fecha2%365;
        meses3=dias3/30;
        diasf3=dias3%30-2;
        if (meses3==0) {
          if (diasf3==0){
            document.forma.edadanos.value=Math.floor(anos3)+" a�os";
          }
          else if (anos3>1) {
            document.forma.edadanos.value=Math.floor(anos3)+" a�os";
          }
          else {
            document.forma.edadanos.value=Math.floor(anos3)+" a�o "+diasf3+" dias";
          }
        }
        else {
          if (diasf3==0){
            document.forma.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
          }
          else if (anos3>1) {
            document.forma.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
          }
          else {
            document.forma.edadanos.value=Math.floor(anos3)+" a�o "+Math.floor(meses3)+" meses "+diasf3+" dias";
          }
        }
      }
      function valida(){
        if (document.forma.appaterno.value==""){
          alert("Favor de ingresar el apellido paterno");
          return false;
        }
        if (document.forma.apmaterno.value==""){
          alert("Favor de ingresar el apellido materno");
          return false;
        }
        if (document.forma.nombre.value==""){
          alert("Favor de ingresar el nombre");
          return false;
        }

        if (document.forma.altafec.value==""){
          alert("Favor de ingresar la fecha de alta");
          return false;
        }
        if (document.forma.buscartienda.value==""){
          alert("Favor de ingresar la tienda");
          return false;
        }

        if (document.forma.puestoid.selectedIndex==0){
          alert("El puesto no ha sido seleccionado");
          return false;
        }

        if (!document.forma.diastrabaja[0].checked && !document.forma.diastrabaja[1].checked && !document.forma.diastrabaja[2].checked && !document.forma.diastrabaja[3].checked && !document.forma.diastrabaja[4].checked && !document.forma.diastrabaja[5].checked && !document.forma.diastrabaja[6].checked){
          alert("Favor de seleccionar los dias de trabajo");
          return false;
        }
        if (document.forma.pagoinfonavit[1].checked) {
          if (document.forma.montoinfonavit.value==""){
            alert("Favor de ingresar el Monto del pago infonavit");
            return false;
          }
        }

        $("#btnGuardar").attr("disabled", true);
        return true;
      }
      function show(event) {
        if (typeof event == "undefined")
          event = window.event;
        var val = event.keyCode;
        if(val == 13)
          document.forma.submit();
      }
    </script>

    <!--- Llenado de datos para las busquedas--->
    <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
    <link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
    <script type="text/javascript">
      $(document).ready(function() {
        $("#buscartienda").autocomplete("data/tienda.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
        $("#buscartiendacer").autocomplete("data/tienda.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
      });
    </script>
    <!--- Fin del llenado de datos para busqueda --->
    <link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
    <SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
  </head>

  <body>
    <div align="center">
      <table width="772" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">
              <form method="post" name="forma" action="altaempleadostienda.cfm" onSubmit="return valida(this)" enctype="multipart/form-data">
                <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td>
                      <div align="center" class="style11">
                        <p>Alta de Empleados Tienda</p>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div id="foto">
                        <a href="javascript:document.getElementById('flashArea').style.display='';document.getElementById('ocultarfoto').style.display='';document.getElementById('foto').style.display='none';void(0);"><span class="style19">Tomar Foto</span></a>
                      </div>
                      <div id="ocultarfoto" style="display:none">
                        <a href="javascript:document.getElementById('flashArea').style.display='none';document.getElementById('ocultarfoto').style.display='none';document.getElementById('foto').style.display='';void(0);"><span class="style19">Ocultar Foto</span></a>
                      </div>
                      <div id="flashArea" class="flashArea" style="height:100%;display:none"></div>
                      <script type="text/javascript">
                        var mainswf = new SWFObject("take_picture.swf?myURL=new", "main", "700", "400", "9", "#ffffff");
                        mainswf.addParam("scale", "noscale");
                        mainswf.addParam("wmode", "window");
                        mainswf.addParam("allowFullScreen", "true");
                        //mainswf.addVariable("requireLogin", "false");
                        mainswf.write("flashArea");
                      </script>
                      <table width="756" border="0" cellpadding="3" cellspacing="1">
                        <tr>
                          <td width="371" valign="top">
                            <table width="378" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                              <tr>
                                <td width="127" align="left" valign="middle">Fotograf�a</td>
                                <td width="236" align="left" valign="middle" bgcolor="#FFFFFF">
                                  <label>
                                    <input type="file" name="archivo" />
                                  </label>
                                </td>
                              </tr>
                              <tr>
                                <td width="127" align="left" valign="middle">*Apellido paterno</td>
                                <td width="236" align="left" valign="middle" bgcolor="#FFFFFF">
                                  <label>
                                    <input type="text" name="appaterno" id="textfield" />
                                  </label>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">*Apellido materno</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF">
                                  <label>
                                    <input type="text" name="apmaterno" id="textfield2" />
                                  </label>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">*Nombre (s)</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="nombre" id="textfield3" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">No. IMSS</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="imss" id="textfield7" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Fecha alta imss</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF">
                                  <input size="15" type="text" name="imssfec"  />
                                  <a href="#" onClick="displayCalendar(document.forma.imssfec,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Presiona aqu� para dar la fecha"></a>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Fecha de nacimiento</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF">
                                  <input size="15" type="text" name="nacimientofec" onchange="edad()"  />
                                  <a href="#" onClick="displayCalendar(document.forma.nacimientofec,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Presiona aqu� para dar la fecha"></a>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Edad</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input name="edadanos" type="text" disabled="disabled"  /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Telefono</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="telefono" id="textfield9" onKeyPress="show(event);" /></td>
                              </tr>
                              <cfquery name="EstadoCivil" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                SELECT * FROM Estado_Civil
                                ORDER BY Estado_Civil
                              </cfquery>
                              <tr>
                                <td align="left" valign="middle">*Estado civil</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF">
                                  <select name="estadocivilid">
                                    <option value="" >Seleccionar</option>
                                    <cfoutput>
                                      <CFLOOP query="EstadoCivil">
                                        <option value="#Estado_Civil_id#" <cfif estado_civil_id eq 1> selected="selected"</cfif>>#Estado_Civil#</option>
                                      </CFLOOP>
                                    </cfoutput>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">*Hijos</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF">
                                  <select name="hijos" id="select6">
                                    <option value="">Seleccionar</option>
                                    <option value="0" selected="selected">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Pago en efectivo</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="radio" name="pagoefectivo" value="0"  /> No <input type="radio" name="pagoefectivo" value="1" checked="checked" />  Si </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">N�mero de nomina</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input name="numnomina" type="text" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Cuenta Bancaria</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="cuentanum" id="cuentanum" onKeyPress="show(event);" /></td>
                              </tr>
                            </table>
                          </td>
                          <td width="385" valign="top">
                            <table width="378" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                              <cfquery name="Escolaridades" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                SELECT * FROM Escolaridad
                                WHERE activo=1
                                ORDER BY Escolaridad
                              </cfquery>
                              <tr>
                                <td align="left" valign="middle">Escolaridad</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF">
                                  <select name="escolaridadid">
                                    <option value="" >Seleccionar</option>
                                    <cfoutput>
                                      <CFLOOP query="Escolaridades">
                                        <option value="#Escolaridad_id#" >#Escolaridad#</option>
                                      </CFLOOP>
                                    </cfoutput>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">*Fecha de ingreso</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input size="15" type="text" name="altafec"  />
                                  <a href="#" onClick="displayCalendar(document.forma.altafec,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Presiona aqu� para dar la fecha"></a>
                                </td>
                              </tr> 
                              <tr>
                                <td align="left" valign="middle">*Fecha de termino de capacitaci�n</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input size="15" type="text" name="capfec" />
                                  <img src="cal.gif" onclick="displayCalendar(document.forma.capfec,'dd/mm/yyyy',this)" width="16" height="16" border="0" alt="Presiona aqu� para dar la fecha">
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">*Tienda</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" id="buscartienda" name="buscartienda" size="34" AUTOCOMPLETE="off" onKeyPress="show(event);" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Tienda cercana</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" id="buscartiendacer" name="buscartiendacer" size="34" AUTOCOMPLETE="off" onKeyPress="show(event);" /></td>
                              </tr>
                              <cfquery name="Puestos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                SELECT * FROM Puesto
                                WHERE activo=1
                                ORDER BY Puesto
                              </cfquery>
                              <tr>
                                <td align="left" valign="middle">*Puesto</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF">
                                  <select name="puestoid">
                                    <option value="" >Seleccionar</option>
                                    <cfoutput>
                                      <CFLOOP query="Puestos">
                                        <option value="#Puesto_id#" >#Puesto#</option>
                                      </CFLOOP>
                                    </cfoutput>
                                  </select>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">*Sueldo Base</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="sueldo" id="textfield14" onKeyPress="show(event);" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Como se entero de la vacante</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="comoentero" id="textfield15" onKeyPress="show(event);" /></td>
                              </tr>

                              <tr>
                                <td align="left" valign="middle">Contacto en caso de emergencia (nombre)</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="emergencianombre" id="textfield15" onKeyPress="show(event);" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle">Tel�fono de emergencia</td>
                                <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="emergenciatelefono" id="textfield15" onKeyPress="show(event);" /></td>
                              </tr>
                              <tr>
                                <td align="left" valign="middle"  colspan="2">
                                  <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                    <tr>
                                      <td align="left" valign="middle" bgcolor="#FFFFFF">
                                        <div align="center">
                                          <input type="submit" id="btnGuardar" value="Capturar" />
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <table border="0" width="100%">
                              <tr>
                                <td>*Dias que trabaja</td>
                                <td><input type="checkbox" name="diastrabaja" value="1" checked="checked" />Lunes</td>
                                <td><input type="checkbox" name="diastrabaja" value="2" checked="checked" />Martes</td>
                                <td><input type="checkbox" name="diastrabaja" value="3" checked="checked" />Miercoles</td>
                                <td><input type="checkbox" name="diastrabaja" value="4" checked="checked" />Jueves</td>
                                <td><input type="checkbox" name="diastrabaja" value="5" checked="checked" />Viernes</td>
                                <td><input type="checkbox" name="diastrabaja" value="6" checked="checked" />Sabado</td>
                                <td><input type="checkbox" name="diastrabaja" value="7" checked="checked" />Domingo</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <table border="0" width="100%">
                              <tr>
                                <td colspan="4">
                                  <div id="otrosmas">Otros 
                                    <a href="javascript:document.getElementById('otrosdatos').style.display='';document.getElementById('otrosmenos').style.display='';document.getElementById('otrosmas').style.display='none';void(0);"><span class="style19">+</span></a>
                                  </div>
                                  <div id="otrosmenos" style="display:none">Otros 
                                    <a href="javascript:document.getElementById('otrosdatos').style.display='none';document.getElementById('otrosmenos').style.display='none';document.getElementById('otrosmas').style.display='';void(0);"><span class="style19">-</span></a>
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td width="14%">RFC</td>
                                <td width="36%"><input type="text" name="rfc" /></td><td width="24%">CURP</td>
                                <td width="26%"><input type="text" name="curp" /></td>
                              </tr>
                            </table>
                            <div id="otrosdatos" style="display:none">
                              <table border="0" width="100%">
                                <tr><td width="14%">No. Afore</td><td width="36%"><input type="text" name="afore" /></td><td width="24%">Pasaporte</td><td width="26%"><input type="text" name="pasaporte" /></td></tr>
                                <tr><td>Clave Elector</td><td><input type="text" name="ife" /></td><td>Talla Camisa</td><td><input type="text" name="camisa" /></td></tr>
                              </table>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <table border="0" width="100%">
                              <tr>
                                <td>Paga Infonavit</td><td><input type="radio" name="pagoinfonavit" value="0" checked="checked"/> No <input type="radio" name="pagoinfonavit" value="1" />  Si </td><td>Monto pago Infonavit</td><td><input type="text" name="montoinfonavit" /></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div align="center">
                        <cfquery name="tipoanexo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                          SELECT * FROM Tipo_anexo
                          WHERE activo=1
                          ORDER BY Tipo_anexo
                        </cfquery>
                        <table width="550" border="0">
                          <tr><td>&nbsp;</td><td>Tipo anexo</td><td>Archivo</td></tr>
                          <tr>
                            <td>Anexo 1</td>
                            <td>
                              <select name="tipoanexo1id">
                                <option value="" >Seleccionar</option>
                                <cfoutput>
                                  <CFLOOP query="tipoanexo">
                                    <option value="#Tipo_anexo_id#" >#Tipo_anexo#</option>
                                  </CFLOOP>
                                </cfoutput>
                              </select>
                            </td>
                            <td><input type="file" name="anexo1" /></td>
                          </tr>
                          <tr>
                            <td>Anexo 2</td>
                            <td>
                              <select name="tipoanexo2id">
                                <option value="" >Seleccionar</option>
                                <cfoutput>
                                  <CFLOOP query="tipoanexo">
                                    <option value="#Tipo_anexo_id#" >#Tipo_anexo#</option>
                                  </CFLOOP>
                                </cfoutput>
                              </select>
                            </td>
                            <td><input type="file" name="anexo2" /></td>
                          </tr>
                          <tr>
                            <td>Anexo 3</td>
                            <td>
                              <select name="tipoanexo3id">
                                <option value="" >Seleccionar</option>
                                <cfoutput>
                                  <CFLOOP query="tipoanexo">
                                    <option value="#Tipo_anexo_id#" >#Tipo_anexo#</option>
                                  </CFLOOP>
                                </cfoutput>
                              </select>
                            </td>
                            <td><input type="file" name="anexo3" /></td>
                          </tr>
                        </table>
                        <table width="497" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                          <tr>
                            <td width="237" align="left" valign="middle" bgcolor="#FFFFFF"><p>Observaciones</p></td>
                          </tr>
                          <tr>
                            <td align="left" valign="middle"><textarea name="observaciones" cols="80" rows="5" id="textfield6"></textarea></td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                </table>
              </form>
              <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
                <tr>
                  <td>
                    <div align="left">
                      <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                        <tr>
                          <td width="58%">
                            <table border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="menu_catalogos.cfm" class="style9">Regresar</a></td>
                                    </tr>
                                  </table>
                                </td>
                                <!---
                                  <td width="186" height="26">
                                    <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                      <tr>
                                        <td><a href="menu.cfm" class="style9">Menu</a></td>
                                      </tr>
                                    </table>
                                  </td>
                                --->
                                <td width="186" height="26">
                                  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                    <tr>
                                      <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>                      
                          </td>
                          <td width="42%" bgcolor="#FFFFFF">Usuario: 
                            <cfoutput>
                              <div>#Session.nombre#</div>
                            </cfoutput>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

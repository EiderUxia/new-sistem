<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style8 {
	font-size: 24px;
	color: #000000;
}
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
    
    <tbody>
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Nomina</span></div></td>
	  </tr>
	<cfif (Session.tipousuario neq 8 and Session.tipousuario neq 3 and Session.tipousuario neq 10) or (session.usuarioid eq 8  or session.usuarioid eq 23)>
    <tr>
      <td align="center" height="37" valign="middle" width="756"><table border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtnin.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                  <tbody>
                    <tr>
                      <td align="center"><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="generar_empleados.cfm" class="style9">Generar archivo de</a>
                        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="generar_empleados.cfm" class="style9">Empleados</a> (paso 1)</span></td>
                    </tr>
                  </tbody>
                          </table>
            </div></td>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtn.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="185">
                    <tbody>
                      <tr>
                        <td><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="subir_autoriza.cfm" class="style9">Cargar archivo de</a>
                        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="subir_autoriza.cfm" class="style9">Nomina Autorizada</a> (paso 8)</span></td>
                      </tr>
                    </tbody>
              </table>
            </div></td>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtnfn.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                  <tbody>
                    <tr>
                      <td><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="nomina_imprimir.cfm" class="style9">Imprimir nomina</a> (paso 9)</span></td>
                    </tr>
                  </tbody>
                          </table>
            </div></td>
          </tr>
          <tr>
		  	<cfif Session.tipousuario neq 3 or (session.usuarioid eq 8  or session.usuarioid eq 23)>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtnin.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                    <tbody>
                      <tr>
                        <td><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;<a href="Generar_reporte.cfm" class="style9">Generar Reportes</a> (paso 10)</span></td>
                      </tr>
                    </tbody>
              </table>
            </div></td>
			
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtn.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="185">
                  <tbody>
                    <tr>
                      <td><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;<a href="generar_recibos.cfm" class="style9">Generar recibos (paso10b)</a></span></td>
                    </tr>
                  </tbody>
                          </table>
            </div></td>
			</cfif>
			<cfif session.usuarioid eq 1 or session.usuarioid eq 7 or session.usuarioid eq 19 or session.usuarioid eq 9 or session.usuarioid eq 17 or session.usuarioid eq 22>
			<td height="25" width="185"><div align="left">
              <table background="images/fondobtnfn.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                  <tbody>
                    <tr>
                      <td align="center"><span class="style9"><a href="nomina_regresada.cfm" class="style9">Sobres No entregados (paso12)</a></span></td>
                    </tr>
                  </tbody>
                          </table>
            </div></td>
			<td height="25" width="185"><div align="left">
              <table background="images/fondobtn.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="185">
                  <tbody>
                    <tr>
                      <td><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;<a href="Rep_movimientos.cfm" class="style9">Altas, Bajas, Cambios</a>  <font color="#00FF66"></font></span></td>
                    </tr>
                  </tbody>
                          </table>
            </div></td>
			</cfif>
          </tr>
		  <tr>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtnin.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                    <tbody>
                      <tr>
                        <td><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="altasemanas_oficina.cfm" class="style9">Alta de Semanas</a></span></td>
                      </tr>
                    </tbody>
              </table>
            </div></td>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtn.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="185">
                  <tbody>
                    <tr>
                      <td><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;<a href="altaconcepto_dia_trabajado.cfm" class="style9">Alta conceptos</a></span></td>
                    </tr>
                  </tbody>
                          </table>
            </div></td>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtnfn.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                  <tbody>
                    <tr>
                      <td><span class="style9">&nbsp;&nbsp;&nbsp;&nbsp;<a href="listaconcepto_dia_trabajado.cfm" class="style9">Editar conceptos</a></span></td>
                    </tr>
                  </tbody>
                          </table>
            </div></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
	<CFIF SESSION.USUARIOID EQ 1 or Session.tipousuario eq 3 or Session.tipousuario eq 10 or session.usuarioid eq 22>
	<tr>
      <td align="center" height="37" valign="middle" width="756"><table border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtnin.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                    <tbody>
                      <tr>
                        <td align="center"><span class="style9"><a href="nomina_entregada.cfm" class="style9">Sobres entregados (paso11)</a></span></td>
                      </tr>
                    </tbody>
              </table>
            </div></td>
            
          </tr>
        </tbody>
      </table></td>
    </tr>
	</CFIF>
	<cfelseif Session.tipousuario eq 3 or Session.tipousuario eq 10>
	<tr>
      <td align="center" height="37" valign="middle" width="756"><table border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtnin.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                    <tbody>
                      <tr>
                        <td align="center"><span class="style9"><a href="nomina_entregada.cfm" class="style9">Sobres entregados (paso11)</a></span></td>
                      </tr>
                    </tbody>
              </table>
            </div></td>
            
          </tr>
        </tbody>
      </table></td>
    </tr>
    <cfelse>
    <tr>
      <td align="center" height="37" valign="middle" width="756"><table border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtnin.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="190">
                    <tbody>
                      <tr>
                        <td align="center"><span class="style9"><a href="dias_laborados.cfm" class="style9">Dias laborados (fingerprint)</a><br />(paso 1)</span></td>
                      </tr>
                    </tbody>
              </table>
            </div></td>
            <td height="25" width="185"><div align="left">
              <table background="images/fondobtn.jpg" border="0" cellpadding="0" cellspacing="0" height="37" width="185">
                  <tbody>
                    <tr>
                      <td align="center"><span class="style9"><a href="dias_laborados_edit.cfm" class="style9">Dias laborados (Encargada)</a><br />(paso 2)</span></td>
                    </tr>
                  </tbody>
                          </table>
            </div></td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    </cfif>
  </tbody></table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

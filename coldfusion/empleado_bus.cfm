<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--- <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> --->
<meta charset="iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style>
.btn-contacto{
	background-color: #374859;
	padding:10px 15px;
	border: #fff 1px solid;
	color: #fff;
	font-size: 12px;
	font-weight: 600;
	text-decoration:none;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius:3px;
	transition:all .5s ease;
}
.btn-contacto:hover{
	background-color: #000;
	padding: 10px 20px;
}
</style>
<script language="JavaScript">

	function edad() {
	
			var l_dt = new Date();
		 
var Ldt_due= document.frmEntrevista.nacimientofec.value;

		
		var Ldt_Year,Ldt_Month,Meses_nacido,Meses_hoy,anos,meses
		Ldt_Year = String(Ldt_due).substring(6,10)
		Ldt_Month = parseFloat(String(Ldt_due).substring(3,5));
		Ldt_day = parseFloat(String(Ldt_due).substring(0,2));
		//Set the two dates
		var millennium =new Date(Ldt_Year, Ldt_Month-1, Ldt_day) //Month is 0-11 in JavaScript
		today=new Date()
		//Get 1 day in milliseconds
		var one_day=1000*60*60*24

		//Calculate difference btw the two dates, and convert to days
		fecha2=Math.ceil((today.getTime()-millennium.getTime())/(one_day));
		anos3=fecha2/365;
		dias3=fecha2%365;
		meses3=dias3/30;
		diasf3=dias3%30-2;
		if (meses3==0) {
			if (diasf3==0){
		  document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
			}
			else if (anos3>1) {
				document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
			}
			else {
				document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+diasf3+" dias";
			}
		}
		else {
			if (diasf3==0){
		  document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
			}
			else if (anos3>1) {
				document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
			}
			else {
				document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+Math.floor(meses3)+" meses "+diasf3+" dias";
			}
		}
}

function show(event) {
		if (typeof event == "undefined")
			event = window.event;
		
		var val = event.keyCode;
		if(val == 13)
			document.forma.submit();
		
	}
</script>

<!--- Llenado de datos para las busquedas--->
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<!--- Fin del llenado de datos para busqueda --->
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
</head>

<cfquery name="qryTienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT Tienda_id, Tienda FROM Tienda
	WHERE Activo=1 <cfif #Session.tienda# neq 0>and tienda_id=#Session.tienda#</cfif>
</cfquery>
<cfif isdefined("bitacoraid")>
<cfquery name="qryBitacora" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Bitacora.bitacora_id, Tienda, Bitacora.Comentarios, Bitacora.Observaciones, Nombre +' '+ apellido_pat +' ' +apellido_mat as Nombre, Bitacora.Semana_ano_id, Genera_historia_bit
	FROM Bitacora
    	INNER JOIN Tienda ON Tienda.Tienda_id=Bitacora.tienda_id
        INNER JOIN Empleado ON Empleado.Empleado_id=Bitacora.Empleado_id
    WHERE Bitacora_id=#bitacoraid#
</cfquery>
<cfquery name="qrySemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    Select Semana, Semana_ano_id
	FROM Semana_ano 
	WHERE Semana_ano_id=#qryBitacora.Semana_ano_id#
</cfquery>
	<cfset semanaid=#qrySemana.semana_ano_id#>
	<cfset semana=#qrySemana.semana#>
	<cfset tiendaid=#qryTienda.tienda_id#>
	<cfset tienda=qryTienda.tienda>
	<cfset empleado=qryBitacora.Nombre>
	<cfset observaciones=qryBitacora.Observaciones>
    <cfset comentarios=qryBitacora.Comentarios>
	<cfset generahistoria=qryBitacora.Genera_historia_bit>
    
<cfelse>
<cfquery name="qrySemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    Select Semana, Semana_curso.Semana_ano_id
	FROM Semana_ano INNER JOIN Semana_curso ON Semana_ano.Semana_ano_id=Semana_curso.Semana_ano_id
</cfquery>
<cfif qryTienda.recordcount eq 1>
<cfquery name="qryEmpleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT Empleado_id, Nombre +' '+Apellido_pat as nombre
    FROM Empleado
    WHERE Activo=1 and Tienda_id=#Session.tienda#
</cfquery>
</cfif>  
	<cfset bitacoraid=0>
	<cfset semanaid=#qrySemana.semana_ano_id#>
	<cfset semana=#qrySemana.semana#>
	<cfset tiendaid=#qryTienda.tienda_id#>
	<cfset tienda=qryTienda.tienda>
	<cfset empleadoid="">
	<cfset observaciones="">
    <cfset comentarios="">
	<cfset generahistoria=0>
</cfif>

<body class="g-sidenav-show  bg-gray-100"> 
    
	<!---sidenav.cfm es la barra del lateral izquierdo--->
	 <cfinclude template="../sidenav.cfm">
	 <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
	   <!-- Navbar -->
	   <cfinclude template="../navbar.cfm">
	   <!-- End Navbar -->
  
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p>Busqueda Personal</p>
            </div></td>
        </tr>
      
        <tr>
          <td>
			
          <!--- <cfform role="form" name="frmContrato" id="frmContrato"> --->
          <cfform name="frmContrato" id="frmContrato">
                    <div class="row">
                      <div class="col-md-12">
                        	<label for="contratobus">Nombre o apellidos</label>
										<cfoutput><input type="text" class="form-control" id="contratobus" name="contratobus" placeholder="Nombre o apellido" ></cfoutput>
                        </div>
                    </div>   <!-- /.row -->
                    <div class="row">
                    	 <div class="col-md-12">
                         &nbsp;
                         </div>
                    </div>
                    <div class="row">
                    	 <!--- <cfdiv id="searchFormResults" bind="url:empleado_busqueda.cfm?contratobus={frmContrato:contratobus@keyup}" bindonload="false" class="col-md-12"> --->
                    	 <cfdiv id="searchFormResults" bind="url:empleado_busqueda.cfm?contratobus={frmContrato:contratobus@keyup}" bindonload="false" >
                         
                         </cfdiv>
                    </div>
                    </cfform>
            </td>
        </tr>
      </table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
 <!-- jQuery 2.0.2 -->  
    
    <!--<!--- Llenado de datos para las busquedas--->
    <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
<script type="text/javascript">
		 function valida(revisado){
			ColdFusion.navigate('bta_jqry_alta.cfm?crearempleado=0&revisado='+revisado+'&'+$("#frmBitacora").serialize(),'alta');
			 return false;
		 }
		 
</script>
bitacoras_alta.cfm esta una version alterna a lo de arriba por si se ocupa esta si es compatible con lucee
<!--- Fin del llenado de datos para busqueda --->

<!--   Core JS Files   -->
<script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
</body>
</html>

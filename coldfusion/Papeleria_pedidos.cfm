<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Pedidos de Papeler�a</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style>
.btn-contacto{
	background-color: #374859;
	padding:10px 15px;
	border: #fff 1px solid;
	color: #fff;
	font-size: 12px;
	font-weight: 600;
	text-decoration:none;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius:3px;
	transition:all .5s ease;
}
.btn-contacto:hover{
	background-color: #000;
	padding: 10px 20px;
}
</style>
<script language="JavaScript">

function show(event) {
		if (typeof event == "undefined")
			event = window.event;
		
		var val = event.keyCode;
		if(val == 13)
			document.forma.submit();
		
	}
</script>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
<!--- Llenado de datos para las busquedas--->
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<!--- Fin del llenado de datos para busqueda --->
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
</head>
<cfquery name="qrybitacora" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT P.Papeleria_pedido_id, Tienda, P.Comentarios, Nombre +' '+ apellido_pat +' ' +apellido_mat as Nombre, P.Fecha, Tienda.Tienda_id
	FROM Papeleria_Pedido P
    	INNER JOIN Tienda ON Tienda.Tienda_id=P.tienda_id
        INNER JOIN Usuario U ON U.usuario_id=P.Usuario_id
    WHERE Enviado_bit=0 and Surtido_bit=0 and Cancelado_bit=0 <cfif session.tienda neq 0>and Tienda.tienda_id=#session.tienda#</cfif>
</cfquery>

   
<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="frmEntrevista" id="frmEntrevista" action="Entrevista_alta.cfm" onSubmit="return valida(this)">
      <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p> Pedidos de Papeler�a en proceso</p>
            </div></td>
        </tr>
        
        <tr>
          <td>
          <table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="89" valign="top">Tienda</td>
              <td width="178" valign="top">Nombre</td>
			  <td width="178" valign="top">Fecha</td>
              <td width="292" valign="top">Comentarios</td>
            </tr>
            <cfset fondo="##CCCCCC">
            <cfoutput query="qrybitacora">
            <tr bgcolor="#fondo#">
              <td valign="top">#Tienda#</td>
              <td valign="top"><a href="Papeleria_alta.cfm?TiendaidPed=#Tienda_id#">#Nombre#</a></td>
			  <td valign="top">#LSDateFormat(Fecha, 'dd/MMM/yyyy')#</td>
              <td valign="top">#left(Comentarios,100)#</td>
            </tr>
            <cfif fondo eq "##CCCCCC">
            	<cfset fondo="##FFFFFF">
            <cfelse>
            	<cfset fondo="##CCCCCC">
            </cfif>
            </cfoutput>
          </table></td>
        </tr>
                <tr>
        	<td>&nbsp;
            	
            </td>
        </tr>
        <tr>
        	<td align="left" valign="middle" bgcolor="#FFFFFF">
                <div align="center"><a href="Papeleria_alta.cfm" class="btn-contacto">Nuevo Pedido</a></div>
            </td>
        </tr>
      </table></form><br />
      <br />
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
						  </table>
					   </td>
					   <cfif isdefined("session.tipousuario") AND  session.tipousuario eq 1>
						   <td width="186" height="26">
							  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
								  <tr>
									<td><a href="Papeleria_pedidos_Solicitados.cfm" class="style9">Pedidos Solicitados</a></td>
								  </tr>
								</table>
							</td>
						</cfif>
                        <td width="186" height="26">
                          <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
                              <tr>
                                <td><a href="Papeleria_pedidos_pendientes.cfm" class="style9">Pedidos en Proceso</a></td>
                              </tr>
                            </table>
                        </td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
							  <tr>
								<td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
							  </tr>
							</table>
						</td>
                      </tr>
                      <cfif isdefined("session.tipousuario") AND  session.tipousuario eq 1>
                      <tr>
						   <td width="186" height="26">
							  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
								  <tr>
									<td><a href="Pedidos_pape_rep.cfm" class="style9">REPORTE DE REQUISICIONES</a></td>
								  </tr>
								</table>
							</td>
                      </tr>
					  </cfif>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
 <!-- jQuery 2.0.2 -->  
   
</body>
</html>

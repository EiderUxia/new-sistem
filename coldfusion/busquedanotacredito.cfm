<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<script language="JavaScript">
function valida(){
	return true;
}
</script>
<!--- Llenado de datos para las busquedas--->
    <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
$(document).ready(function(){
	$("#buscarproveedor").autocomplete("data/proveedor.cfm",{minChars:3,delay:200,autoFill:false,matchSubset:false,matchContains:1,cacheLength:10,selectOnly:1});
	});
</script>
<!--- Fin del llenado de datos para busqueda --->
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">B&uacute;squeda de Notas de cr&eacute;dito</div></td>
        </tr>
        
        <tr>
          <td>
      <form method="post" name="forma" action="busquedanotacredito.cfm" onSubmit="return valida(this)">
      <table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="371" valign="top">
              <table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td width="156" align="left" valign="middle"><p>Proveedor&#13;</p></td>
                  <td width="197" align="left" valign="middle" bgcolor="#FFFFFF"><label>
                    <input type="text" id="buscarproveedor" name="buscarproveedor" size="34" AUTOCOMPLETE="off" />
                  </label></td>
                </tr>          
              </table></td>
              <td width="385" valign="center"><div align="center"><input type="submit" value="buscar" /></div></td>
            </tr>
          </table>
          </form>
          </td>
        </tr>
        

        <tr>
          <td valign="top"><div align="center">
          <cfif isdefined("buscarproveedor")>
          <cfquery name="proveedorid" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Proveedor_id FROM Proveedor
	WHERE Nombre_corto='#buscarproveedor#'
</cfquery>
<cfif proveedorid.recordcount neq 1>
<script language="JavaScript" type="text/javascript">
		alert("El proveedor no existe, favor de teclearlo nuevamente");
		window.location="busquedanotacredito.cfm";
	</script>
    <cfabort>
</cfif>
          <cfquery name="datos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
SELECT entidad.tienda as entidad, notacredito.notacredito_id, notacredito.entidad_id, notacredito.monto, tipo_pago 
FROM Proveedor INNER JOIN notacredito ON notacredito.proveedor_id=proveedor.proveedor_id
	LEFT JOIN Tienda as Entidad ON Entidad.Tienda_id=notacredito.entidad_id
    INNER JOIN Tipo_pago ON Tipo_pago.tipo_pago_id=notacredito.tipo_pago_id
WHERE proveedor.proveedor_id=#proveedorid.proveedor_id#
GROUP BY entidad.tienda, notacredito.notacredito_id, notacredito.entidad_id, notacredito.monto, tipo_pago
        </cfquery>
            <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
              <tr>
                <td width="24%" bgcolor="#FFFFFF">Num. Nota</td>
                <td width="17%" bgcolor="#FFFFFF">Entidad</td>
                <td width="11%" bgcolor="#FFFFFF">Monto</td>
              </tr>
              <cfoutput query="datos">
              <tr>
                <td><a href="aplicarnotacreditoprov.cfm?notacredito=#notacredito_id#">#notacredito_id#</a></td>
                <td>#entidad#</td>
                <td>#LSCurrencyFormat(monto)#</td>
              </tr>
              </cfoutput>
            </table>
            </cfif>
          </div></td>
        </tr>
      </table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_notacredito.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

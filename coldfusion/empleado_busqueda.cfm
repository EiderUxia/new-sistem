<cfsilent>
	<!--- This file checks to see if a username is available. Dan.Short is always taken... --->
	<cfparam name="URL.contratobus" default="" />
<cfquery name="qrycontratos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT TOP 50 E.nombre, E.Apellido_pat, E.Apellido_mat, T.Tienda, T2.Tienda TiendaDest, E.Activo, T.Tienda_id, E.Empleado_id
    FROM Empleado E INNER JOIN PUesto ON Puesto.PUesto_id=E.Puesto_id
    INNER JOIN Tienda T ON T.Tienda_id=E.Tienda_id
    LEFT JOIN Cambio_Tienda C ON (C.Empleado_id=E.Empleado_id and C.Realizado_bit=0)
    LEFT JOIN Tienda T2 ON C.Tienda_id=T2.Tienda_id
    WHERE nombre+' '+Apellido_pat+' '+ Apellido_mat like '%#contratobus#%' 
</cfquery>
</cfsilent>
<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Resultados de la busqueda</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
    <cfif qrycontratos.recordcount neq 0>
<table class="table table-bordered table-responsive">
    <tr>
        <th>Nombre</th>
        <th>Tienda</th>
        <th>Activo</th>
        <th>Comentarios</th>
    </tr>
    <cfoutput query="qrycontratos">
    <tr>
        <td>#nombre# #Apellido_pat# #Apellido_mat#</td>
        <td>#Tienda#</td>
        <td><cfif activo eq 1>Si<cfelse>NO</cfif></td>
        <td><cfif TiendaDest neq "">En proceso de cambio a tienda #TiendaDest#<cfelseif activo eq 1 and isdefined("Alta")><a href="CambioDeTienda.cfm?Tiendaid=#Tienda_id#">Cambiar de tienda</a><cfelseif activo eq 0 and isdefined("Alta")><a href="editempleadostienda.cfm?Empleadoid=#Empleado_id#">Re activar</a></cfif></td>
    </tr>
    </cfoutput>
</table>
	<cfelse>
    	<h3>No se encotraron resultado</h3>
        <cfif isdefined("Alta") and len(contratobus) gt 5>
        	<div align="center"><a href="entrevista_alta.cfm"  class="btn-contacto">Nuevo empleado</a></div>
      <br />
        </cfif>
	</cfif>
	</div>
</div>
<div class="clearfix"></div>
                    <div class="row">
                    	 <cfdiv id="displayresult" bindonload="false" class="col-md-12">
                         
                         </cfdiv>
                    </div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif isdefined("cuenta")>
<cftransaction>
<cfquery name="qrycuenta" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Cuenta FROM Cuenta_contable
        WHERE Cuenta=#Cuenta#
</cfquery>
<cfif qrycuenta.recordcount neq 0>
<script language="JavaScript" type="text/javascript">
		alert("Esta cuenta ya existe");
		window.location="altacuenta.cfm";
	</script>
<CFABORT>
<cfelse>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Cuenta_contable (Cuenta, Descripcion) VALUES
		(#Cuenta#,'#nombre#')			
</cfquery>
</cfif>
</cftransaction>
<script language="JavaScript" type="text/javascript">
		alert("La cuenta fue agregado con �xito");
		window.location="altacuenta.cfm";
	</script>
</cfif>
<script language="JavaScript">
function valida(){
	if (document.forma.cuenta.value==""){
		alert("Favor de ingresar el numero de la cuenta");
		return false;
	}
	if (!(/^\d{3}$/.test(document.forma.cuenta.value))){
		alert("Favor de ingresar 3 digitos para la cuenta");
		return false;
	}
	if (document.forma.nombre.value==""){
		alert("Favor de ingresar el nombre de la cuenta");
		return false;
	}
	return true;
}
</script>

</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="altacuenta.cfm" onSubmit="return valida(this)">
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p>Alta de Cuenta</p>
            </div></td>
        </tr>
        
        <tr>
          <td><table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="371" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td align="left" valign="middle"><p>*Cuenta</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><label>
                  <input type="text" name="cuenta" id="textfield2" />
                  </label></td>
                </tr>
              </table></td>
              <td width="385" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td align="left" valign="middle"><p>*Nombre</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="nombre" id="textfield3" /></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="3" cellspacing="1">
              <tr><td>
              <div align="center"><input type="submit" value="Agregar" /></div>
              </td></tr>
          </table></td>
        </tr>
      </table></form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_catalogos.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

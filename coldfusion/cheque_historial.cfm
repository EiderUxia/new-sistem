<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<link rel="stylesheet" type="text/css" href="default.css" media="screen"/>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif not isdefined("fecha")>
	<cfset fecha=LSDateFormat(now(),"dd/mm/yyyy")>
</CFIF>
<cfquery name="Saldos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Cuenta_banco_id FROM Cuenta_banco_internet
WHERE Fecha <=#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#
GROUP BY Cuenta_banco_id
</cfquery>
<cfif isdefined("listo") and listo eq 2>
<cftransaction>
<cfquery name="chequesselprov" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT * FROM Cheque
        WHERE autorizado=0
</cfquery>
<cfloop query="chequesselprov">
<cfif isdefined("nuevo#cheque_id#")>
<cfset chequenuevo=EVALUATE("nuevo#cheque_id#")>
<cfif chequenuevo neq "">
<cfquery name="chequesdel" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        UPDATE Cheque 
        SET Cheque_num='#chequenuevo#',
        	autorizado=1
        WHERE Cheque_id='#cheque_id#'
</cfquery>
</cfif>
</cfif>
</cfloop>
</cftransaction>
<script language="JavaScript" type="text/javascript">
		alert("El cheque se autoriz� con �xito");
		window.location="cheque_autorizar.cfm";
	</script>
</cfif>
<cfquery name="chequesselprov" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Cheque.cheque_id, cheque_num, proveedor.nombre_corto, Tienda.tienda, monto, fecha_reviso, titular, anticipo, Beneficiario, Cuenta_banco.Cuenta_banco_id, T.Tienda as T, Cheque.amano_bit
		FROM Cheque INNER JOIN Proveedor ON Proveedor.Proveedor_id=Cheque.Proveedor_id
			INNER JOIN Cheque_provedor ON Cheque_provedor.Cheque_id=Cheque.cheque_id
			INNER JOIN Cuenta_banco_prov ON Cheque_provedor.cuenta_banco_prov_id=Cuenta_banco_prov.cuenta_banco_prov_id	
            INNER JOIN Tienda ON Tienda.Tienda_id=Cheque.Tienda_id
			INNER JOIN Cuenta_banco ON Cuenta_banco.Cuenta_banco_id=Cheque.cuenta_banco_id
        	INNER JOIN Tienda as T ON T.Tienda_id=Cuenta_banco.Tienda_id
        WHERE YEAR(Fecha_reviso)=YEAR(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND MONTH(Fecha_reviso)=MONTH(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND DAY(Fecha_reviso)=DAY(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) and reviso=1
		GROUP BY Titular, Cuenta_banco.Cuenta_banco_id, Cheque.cheque_id, cheque_num, proveedor.nombre_corto, Tienda.tienda, monto, fecha_reviso, anticipo, beneficiario, T.Tienda, amano_bit
        ORDER BY Cuenta_banco.Cuenta_banco_id, cheque_num
</cfquery>
<cfquery name="efectivosselprov" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Efectivo.efectivo_id, proveedor.nombre_corto, Tienda.tienda, monto, fecha_reviso, titular, Cuenta_banco.Cuenta_banco_id, T.Tienda as T
		FROM efectivo INNER JOIN Proveedor ON Proveedor.Proveedor_id=efectivo.Proveedor_id
            INNER JOIN Tienda ON Tienda.Tienda_id=efectivo.Tienda_id
			INNER JOIN Cuenta_banco ON Cuenta_banco.Cuenta_banco_id=efectivo.cuenta_banco_id
        	INNER JOIN Tienda as T ON T.Tienda_id=Cuenta_banco.Tienda_id
        WHERE YEAR(Fecha_reviso)=YEAR(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND MONTH(Fecha_reviso)=MONTH(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND DAY(Fecha_reviso)=DAY(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) and reviso=1
		GROUP BY Titular, Cuenta_banco.Cuenta_banco_id, efectivo.efectivo_id, proveedor.nombre_corto, Tienda.tienda, monto, fecha_reviso, T.Tienda
</cfquery>
<cfquery name="chequesselacre" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Cheque.cheque_id, cheque_num, acreedor.nombre_corto, acreedor, Tienda.tienda, monto, fecha_reviso, titular, beneficiario, Cuenta_banco.Cuenta_banco_id, T.Tienda as T, Cheque.amano_bit
	FROM Cheque INNER JOIN Acreedor ON Acreedor.Acreedor_id=Cheque.Acreedor_id
			INNER JOIN Cheque_acreedor ON Cheque_acreedor.Cheque_id=Cheque.cheque_id
			INNER JOIN Cuenta_banco_acre ON Cheque_acreedor.cuenta_banco_acre_id=Cuenta_banco_acre.cuenta_banco_acre_id	
    	INNER JOIN Tienda ON Tienda.Tienda_id=Cheque.Tienda_id
			INNER JOIN Cuenta_banco ON Cuenta_banco.Cuenta_banco_id=Cheque.cuenta_banco_id
        INNER JOIN Tienda as T ON T.Tienda_id=Cuenta_banco.Tienda_id
	WHERE YEAR(Fecha_reviso)=YEAR(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND MONTH(Fecha_reviso)=MONTH(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND DAY(Fecha_reviso)=DAY(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) and reviso=1
	GROUP BY titular, Cuenta_banco.Cuenta_banco_id, Cheque.cheque_id, cheque_num, acreedor.nombre_corto, acreedor, Tienda.tienda, monto, fecha_reviso, beneficiario, T.Tienda, amano_bit
</cfquery>
<cfquery name="efectivosselacre" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT efectivo_acre.efectivo_acre_id, acreedor.nombre_corto, acreedor, Tienda.tienda, monto, fecha_reviso, titular, T.Tienda as T
	FROM efectivo_acre INNER JOIN Acreedor ON Acreedor.Acreedor_id=efectivo_acre.Acreedor_id
    	INNER JOIN Tienda ON Tienda.Tienda_id=efectivo_acre.Tienda_id
		INNER JOIN Cuenta_banco ON Cuenta_banco.Cuenta_banco_id=efectivo_acre.cuenta_banco_id
        INNER JOIN Tienda as T ON T.Tienda_id=Cuenta_banco.Tienda_id
	WHERE YEAR(Fecha_reviso)=YEAR(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND MONTH(Fecha_reviso)=MONTH(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND DAY(Fecha_reviso)=DAY(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) and reviso=1
	GROUP BY titular, Cuenta_banco.Cuenta_banco_id, efectivo_acre.efectivo_acre_id, acreedor.nombre_corto, acreedor, Tienda.tienda, monto, fecha_reviso, T.Tienda
</cfquery>
<cfquery name="chequesselotro" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Cheque.cheque_id, cheque_num, Tienda.tienda, monto, fecha_reviso, titular, Cuenta_banco.Cuenta_banco_id, T.Tienda as T, Tipo_cheque_pago.Tipo_cheque_pago, Cuenta_banco.Cuenta_banco, Cheque.amano_bit
		FROM Cheque INNER JOIN Tienda ON Tienda.Tienda_id=Cheque.Tienda_id
			INNER JOIN Cuenta_banco ON Cuenta_banco.Cuenta_banco_id=Cheque.cuenta_banco_id
        	INNER JOIN Tienda as T ON T.Tienda_id=Cuenta_banco.Tienda_id
        	INNER JOIN Tipo_cheque_pago ON Tipo_cheque_pago_id=Otro_cheque
        WHERE YEAR(Fecha_reviso)=YEAR(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND MONTH(Fecha_reviso)=MONTH(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) AND DAY(Fecha_reviso)=DAY(#CreateODBCDate(LSDateFormat(fecha,"mm/dd/yyyy"))#) and reviso=1
		GROUP BY Titular, Cuenta_banco.Cuenta_banco_id, Cheque.cheque_id, cheque_num, Tipo_cheque_pago.Tipo_cheque_pago, Tienda.tienda, monto, fecha_reviso, T.Tienda, Cuenta_banco.Cuenta_banco, amano_bit
</cfquery>

<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style8 {
	font-size: 24px;
	color: #000000;
}
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
<script language="JavaScript">
<!--
function valida(){

	document.forma1.listo.value=2;
	document.forma1.submit();
}
function valida2(){

}
-->
</script>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
    
    <tbody>
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Historial Pagos</span></div></td>
	  </tr>
      <tr><td align="right">
	  <form method="post" name="forma" action="cheque_historial.cfm" onSubmit="return valida2(this)">
			<input type="hidden" name="listo" value="0" />
						  <table width="226" height="25" border="0" cellpadding="5" cellspacing="0">
                          <tr>
                            <td align="center"><cfoutput>
			<input size="15" type="hidden" name="fecha" onchange="submit(this)" <cfif isdefined("fecha")>Value="#fecha#"<cfelse>Value="#LSDateFormat(now(),"dd/mm/yyyy")#"</CFIF> />
			<a href="cheque_historial.cfm?fecha=#LSDateFormat(DateAdd("d", -1, LSDateFormat(fecha,"mm/dd/yyyy") ),"dd/mm/yyyy")#"><img src="images/back.gif" width="18" height="18" border="0" /></a>#fecha# (#LSDateFormat(fecha,"Dddd")#)<a href="cheque_historial.cfm?fecha=#LSDateFormat(DateAdd("d", 1, LSDateFormat(fecha,"mm/dd/yyyy")),"dd/mm/yyyy")#"><img src="images/next.gif" width="18" height="18" border="0" /></a>
			
                        <a href="##" onClick="displayCalendar(document.forma.fecha,'dd/mm/yyyy',this)"><img src="cal.gif" width="16" height="16" border="0" alt="Preciona aqu� para dar la fecha"></a>&nbsp;
			</cfoutput></td>
                          </tr>
                        </table></form></td></tr>
	<tr>
      <td align="center" height="37" valign="middle" width="756">
      <form method="post" name="forma1" action="cheque_autorizar.cfm" onSubmit="return valida(this)">
      <input type="hidden" value="0" name="listo" />
      <cfif chequesselprov.recordcount neq 0>
      <table border="1" align="center">
      <tr><td>No. Cheque</td><td>Beneficiario</td><td>Marca</td><td>Tienda</td><td>Remisi�n</td><td>Cantidad pares</td><td>Monto del cheque</td><td>Fecha remisi�n</td><td>Fecha recepci�n</td>
      <td>Fecha Revisi&oacute;n </td>
      </tr>
      <cfoutput query="chequesselprov" group="Titular">
	  <tr><td colspan="10">Cuenta #Titular# Tienda #T#</td></tr>
	  <cfset total=0>
	  <cfoutput>
      <cfquery name="pagoprov" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT sum(monto) as total FROM Pago_proveedor
	WHERE cheque_id=#cheque_id#
</cfquery>
<cfset restan=monto>
<cfif pagoprov.total neq "">
	<cfset restan=restan-pagoprov.total>
</cfif>
<cfif restan eq 0 or anticipo eq 1>
<cfif anticipo eq 0>
<cfquery name="pagos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT Fecha_recepcion, Sum(pares) as pares, fecha_remision, remision.remision FROM Pago_proveedor INNER JOIN Remision ON Remision.remision_id=Pago_proveedor.remision_id
            	INNER JOIN Remision_recepcion ON Remision_recepcion.remision_id=remision.remision_id
                INNER JOIN Recepcion ON Remision_recepcion.recepcion_id=recepcion.recepcion_id
            WHERE Cheque_id=#cheque_id#
            GROUP BY Fecha_recepcion, fecha_remision, remision.remision
            Order BY Fecha_remision
        </cfquery>
      <tr><td>#Cheque_num#<cfif amano_bit eq 1> (Impreso a mano)</cfif></td><td>#Beneficiario#</td><td><a href="javascript:window.open('Cheque_info_pop.cfm?Chequeid=#cheque_id# ','Historia_cheque','width=600,height=500,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Nombre_corto#</a></td><td>#tienda#</td><td>#pagos.remision#</td><td>#pagos.pares#</td><td>#LSCurrencyFormat(monto)#</td><td>#LSDateFormat(pagos.fecha_remision,"dd-mmm-yyyy")#</td><td>#LSDateFormat(pagos.fecha_recepcion,"dd-mmm-yyyy")#</td><td>#LSDateFormat(Fecha_reviso,"dd-mmm-yyyy")#</td></tr>
      <cfelse>
      <tr><td>#cheque_num#<cfif amano_bit eq 1> (Impreso a mano)</cfif><cfif anticipo eq 1>&nbsp;Anticipo</cfif></td><td><a href="javascript:window.open('Cheque_info_pop.cfm?Chequeid=#cheque_id# ','Historia_cheque','width=600,height=500,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Nombre_corto#</a></td><td>#tienda#</td><td>&nbsp;</td><td>&nbsp;</td><td>#LSCurrencyFormat(monto)#</td><td>&nbsp;</td><td>&nbsp;</td><td>#LSDateFormat(Fecha_reviso,"dd-mmm-yyyy")#</td></tr>
      </cfif>
      </cfif>
	  <cfset total=total+monto>
      </cfoutput>
      <cfquery name="SaldoUltimo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                    SELECT Top 1 * FROM Cuenta_banco_internet 
                                    INNER JOIN Cuenta_banco ON Cuenta_banco.Cuenta_banco_id=Cuenta_banco_internet.Cuenta_banco_id
                                    INNER JOIN Banco ON Banco.Banco_id=Cuenta_banco.Banco_id
                                    WHERE Cuenta_banco.Cuenta_banco_id=#cuenta_banco_id# 
                                    ORDER BY Fecha Desc
                            </cfquery>
	  <tr><td colspan="10">
      <table border="1" width="100%">
      <cfif SaldoUltimo.recordcount neq 0>
      <tr>
        <td>Total de monto en cheques</td>
        <td>Saldo de internet al d�a #LSDateFormat(SaldoUltimo.Fecha,"dd-mmm-yyyy")#</td><td>Resto</td></tr>
	  <cfset resto=saldoUltimo.saldo-total>
      <tr><td>#LSCurrencyFormat(Total)#</td><td>#LSCurrencyFormat(SaldoUltimo.Saldo)#</td><td>#LSCurrencyFormat(resto)#</td></tr>
      <cfelse>
      <tr>
        <td>Total de monto en cheques</td></tr>
        <tr><td>#LSCurrencyFormat(Total)#</td></tr>
        </cfif>
      </table>
      </td></tr>
	  </cfoutput>
      </table>
      <cfelse>
      <div align="center">No ex�sten cheques para Proveedores</div>
      </cfif><br /><br />
      <cfif efectivosselprov.recordcount neq 0>
      <table border="1" align="center">
      <tr><td>Marca</td><td>Tienda</td><td>Remisi�n</td><td>Cantidad pares</td><td>Monto del efectivo</td><td>Fecha remisi�n</td>
      <td>Fecha recepci�n</td><td>Fecha Revisi&oacute;n </td>
      </tr>
      <cfoutput query="efectivosselprov" group="Titular">
	  <tr><td colspan="8">Cuenta #Titular# Tienda #T#</td></tr>
	  <cfset total=0>
	  <cfoutput>
      <cfquery name="pagoprov" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT sum(monto) as total FROM Pago_proveedor
	WHERE efectivo_id=#efectivo_id#
</cfquery>
<cfset restan=monto>
<cfif pagoprov.total neq "">
	<cfset restan=restan-pagoprov.total>
</cfif>
<cfif restan eq 0>
<cfquery name="pagos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT Fecha_recepcion, Sum(pares) as pares, fecha_remision, remision.remision FROM Pago_proveedor INNER JOIN Remision ON Remision.remision_id=Pago_proveedor.remision_id
            	INNER JOIN Remision_recepcion ON Remision_recepcion.remision_id=remision.remision_id
                INNER JOIN Recepcion ON Remision_recepcion.recepcion_id=recepcion.recepcion_id
            WHERE efectivo_id=#efectivo_id#
            GROUP BY Fecha_recepcion, fecha_remision, remision.remision
            Order BY Fecha_remision
        </cfquery>
      <tr><td><!---<a href="javascript:window.open('Efectivo_info_pop.cfm?efectivoid=#efectivo_id# ','Historia_efectivo','width=600,height=500,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);"></a>--->#Nombre_corto#</td><td>#tienda#</td><td>#pagos.remision#</td><td>#pagos.pares#</td><td>#LSCurrencyFormat(monto)#</td><td>#LSDateFormat(pagos.fecha_remision,"dd-mmm-yyyy")#</td><td>#LSDateFormat(pagos.fecha_recepcion,"dd-mmm-yyyy")#</td><td>#LSDateFormat(Fecha_reviso,"dd-mmm-yyyy")#</td></tr>
	  <cfset total=total+monto>
      </cfif>
      </cfoutput>
	  <tr><td colspan="7" align="right">Total</td><td>#LSCurrencyFormat(Total)#</td></tr>
	  </cfoutput>
      </table>
      <cfelse>
      <div align="center">No ex�sten pagos en efectivo para Proveedores</div>
      </cfif><br /><br />
      <!--- Se comento los cheques a acredores por el momento--->
      <cfif chequesselacre.recordcount neq 0>
      <table border="1" align="center">
      <tr><td>No. Cheque</td><td>Acreedor</td><td>Nombre Corto</td><td>Tienda</td><td>Monto del cheque</td>
      <td>Fecha Revis&oacute; </td>
      </tr>
      <cfoutput query="chequesselacre" group="Titular">
	  <tr><td colspan="6">Cuenta #Titular# Tienda #T#</td></tr>
	  <cfset total=0>
	  <cfoutput>
      <cfquery name="pagoprov" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT sum(monto) as total FROM Pago_acreedor
	WHERE cheque_id=#cheque_id#
</cfquery>
<cfset restan=monto>
<cfif pagoprov.total neq "">
	<cfset restan=restan-pagoprov.total>
</cfif>
<cfif restan eq 0 or (isdefined("anticipo") and anticipo eq 1)>
      <tr><td>#Cheque_num#<cfif amano_bit eq 1> (Impreso a mano)</cfif></td><td>#acreedor#</td><td><a href="javascript:window.open('Cheque_info_acre_pop.cfm?Chequeid=#cheque_id# ','Historia_cheque','width=600,height=500,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Nombre_corto#</a></td><td>#LSCurrencyFormat(monto)#</td><td>#Tienda#</td><td>#LSDateFormat(Fecha_reviso,"dd-mmm-yyyy")#</td></tr>
      </cfif>
	  <cfset total=total+monto>
      </cfoutput>
	  <tr><td colspan="5" align="right">Total</td><td>#LSCurrencyFormat(Total)#</td></tr>
      </cfoutput>
      </table>
      <cfelse>
      <div align="center">No ex�sten cheques para Acreedores</div>
      </cfif>
      <cfif efectivosselacre.recordcount neq 0>
      <table border="1" align="center">
      <tr><td>Acreedor</td><td>Nombre Corto</td><td>Tienda</td><td>Monto del efectivo</td>
      <td>Fecha Revis&oacute; </td>
      </tr>
      <cfoutput query="efectivosselacre" group="Titular">
	  <tr><td colspan="5">Cuenta #Titular# Tienda #T#</td></tr>
	  <cfset total=0>
	  <cfoutput>
      <cfquery name="pagoprov" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT sum(monto) as total FROM Pago_acreedor
	WHERE efectivo_acre_id=#efectivo_acre_id#
</cfquery>
<cfset restan=monto>
<cfif pagoprov.total neq "">
	<cfset restan=restan-pagoprov.total>
</cfif>
<cfif restan eq 0>
      <tr><td>#acreedor#</td><td><a href="javascript:window.open('efectivo_info_acre_pop.cfm?efectivoid=#efectivo_acre_id# ','Historia_efectivo','width=600,height=500,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Nombre_corto#</a></td><td>#LSCurrencyFormat(monto)#</td><td>#Tienda#</td><td>#LSDateFormat(Fecha_reviso,"dd-mmm-yyyy")#</td></tr>
      </cfif>
	  <cfset total=total+monto>
      </cfoutput>
	  <tr><td colspan="4" align="right">Total</td><td>#LSCurrencyFormat(Total)#</td></tr>
      </cfoutput>
      </table>
      <cfelse>
      <div align="center">No ex�sten pagos en efectivo para Acreedores</div>
      </cfif>
      <cfif chequesselotro.recordcount neq 0>
      <table border="1" align="center">
      <tr><td>No. Cheque</td><td>Titular</td><td>Cuena erez</td><td>Tienda</td><td>Tipo de pago</td><td>Monto del cheque</td>
      <td>Fecha Revis&oacute; </td>
      </tr>
	  <cfset total=0>
      <cfoutput query="chequesselotro" group="Titular">
	  <cfoutput>
      <tr><td>#Cheque_num#<cfif amano_bit eq 1> (Impreso a mano)</cfif></td><td>#Titular#</td><td>#Cuenta_banco#</td><td>#Tienda#</td><td>#Tipo_cheque_pago#</td><td>#LSCurrencyFormat(monto)#</td><td>#LSDateFormat(Fecha_reviso,"dd-mmm-yyyy")#</td></tr>
	  <cfset total=total+monto>
      </cfoutput>
      </cfoutput>
      <cfoutput>
	  <tr><td colspan="6" align="right">Total</td><td>#LSCurrencyFormat(Total)#</td></tr>
      </cfoutput>
      </table>
      <cfelse>
      <div align="center">No ex�sten cheques para Transpasos y Prestamos</div>
      </cfif>
      </form><br />
	  <cfoutput><div align="center"><a href="cheque_historial_excel.cfm?fecha=#fecha#">para Imprimir</a></div></cfoutput>
      </td>
	</tr>
  </tbody></table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                      	<td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_cheques.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<link rel="stylesheet" type="text/css" href="default.css" media="screen"/>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif not isdefined("anosel")>
	<cfset anosel=year(now())>
</cfif>
<script language="JavaScript">
function valida(val){
	if (val==1){
	document.forma.valor.value=1;
	document.forma.submit();
	}
}
</script>

<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>

</head>

<body class="g-sidenav-show  bg-gray-100"> 
    
  <!---sidenav.cfm es la barra del lateral izquierdo--->
   <cfinclude template="../sidenav.cfm">
   <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
     <!-- Navbar -->
     <cfinclude template="../navbar.cfm">
     <!-- End Navbar -->

<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="rep_movimientos_hist.cfm">
      <input name="valor" type="hidden" value="0" />
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p>Altas, Bajas y Cambios de tienda</p>
            </div></td>
        </tr>
                  <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT Distinct Ano, Semana, Semana_ano_id
                        FROM Rotacion_hist RH
                        WHERE ano>2014
                        order by ano desc, semana desc
				  </cfquery>
                  <cfif isdefined("semana")>
                  	<cfset semanaslist=semana>
                  </cfif>
        <tr>
          <td><div align="right" class="style11"><table border="0" width="100%"><tr>
          <td>A�o-Semana</td><td>
          <select name="semana" multiple="multiple">
          <cfoutput>
          	<option>Seleccionar</option>
                        <cfloop query="qrySemanas">
                    <option value="#Semana_ano_id#" <cfif isdefined("semanaslist") and ListFindNoCase(semanaslist, Semana_ano_id)>selected="selected"</cfif> >#Ano#-#Semana#</option>
                  </cfloop></cfoutput>
                        </select>
          </td>
		</tr></table>
            </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="3" cellspacing="1">
              <tr><td>
              <div align="center"><input type="button" onclick="valida(1);void(0);" value="Buscar" /></div>
              </td></tr>
          </table></td>
        </tr>
      </table></form>
      <cfif isdefined("semana")>
<cfquery name="qryDatos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
SELECT Tienda_id, Semana, Ano, Tienda_semana_ano_id, tienda, Semana_ano_id, Plantilla_Final as totales, Altas, Cambios, Bajas, Tiendasemanaid, Plantilla_inicial
FROM         Rotacion_hist
WHERE Semana_ano_id in (#semana#)
GROUP BY Tienda_id, Semana, Ano, Tienda_semana_ano_id, tienda, Semana_ano_id, Plantilla_Final, Altas, Cambios, Bajas, Tiendasemanaid, Plantilla_inicial
ORDER BY tienda, Semana_ano_id
</cfquery> 
   
   <cfquery name="qrysemanas" dbtype="query">
   		SELECT Distinct Semana, Ano
        FROM qryDatos
   </cfquery>
      <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
      	<tr>
        	<td rowspan="2">Tienda</td>
            <cfoutput query="qrysemanas">
            	<td colspan="5" align="center">Semana #qrysemanas.Semana# A�o #qrysemanas.ano#</td>
            </cfoutput>
        </tr>
        <tr>
        	<cfoutput query="qrysemanas">
                <td>Total de Empleados</td>
                <td>Altas</td>
                <td>Bajas</td>
                <td>Cambios</td>
                <td>IRP</td>
            </cfoutput>
                <td>IRP Total</td>
        </tr>
        <cfset color="##CCCCCC">
        <cfset sumtotal=0>
        <cfset sumcambios=0>
        <cfset sumInicial=0>
        <cfoutput query="qryDatos" group="tienda">
        <tr bgcolor="#color#">
        	<td>#Tienda#</td>
            <cfset PlantillaInicial=Totales>
			<cfset sumaltas=0>
            <cfset sumbajas=0>
            <cfoutput>
				<cfif (Totales+Plantilla_inicial) eq 0>
					<cfset IPR=0>
				<cfelse>
					<cfset IPR=(((Altas+Bajas)/2)*100)/((Totales+Plantilla_inicial)/2)>
				</cfif>
                <td><cfif Totales eq 0>0<cfelse><a href="javascript:window.open('movimiento_hist.cfm?tiendasemanaid=#Tienda_semana_ano_id#&tipoid=Todos','Detalle','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Totales#</a></cfif></td>
                <td><cfif Altas eq 0>0<cfelse><a href="javascript:window.open('movimiento_hist.cfm?tiendasemanaid=#Tienda_semana_ano_id#&tipoid=Altas&semanaid=#Semana_ano_id#','Detalle','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Altas#</a></cfif></td>
                <td><cfif Bajas eq 0>0<cfelse><a href="javascript:window.open('movimiento_hist.cfm?tiendasemanaid=#Tiendasemanaid#&tipoid=Bajas&semanaid=#Semana_ano_id#','Detalle','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Bajas#</a></cfif></td>
                <td><cfif Cambios eq 0>0<cfelse><a href="javascript:window.open('movimiento_hist.cfm?tiendasemanaid=#Tienda_semana_ano_id#&tipoid=Cambios&semanaid=#Semana_ano_id#','Detalle','width=900,height=590,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=50,left=50');void(0);">#Cambios#</a></cfif></td>
                <td>#Numberformat(IPR,"99.99")#% </td>
				<cfset sumaltas=sumaltas+Altas>
                <cfset sumbajas=sumbajas+Bajas>
                <cfset PlantillaFinal=Totales>
            </cfoutput>
            <cfif PlantillaInicial+PlantillaFinal eq 0>
            	<cfset IPRtotal=0>
            <cfelse>
            	<cfset IPRtotal=(((sumaltas+sumbajas)/2)*100)/((PlantillaInicial+PlantillaFinal)/2)>
            </cfif>
            <td><cfif IPRtotal gt 100><font color="##FF0000">#Numberformat(IPRtotal,"99.99")#</font><cfelse>#Numberformat(IPRtotal,"99.99")#</cfif>%</td>
        </tr>
        <cfif color eq "##CCCCCC">
       		<cfset color="##FFFFFF">
        <cfelse>
        	<cfset color="##CCCCCC">
        </cfif>
        
        <cfset sumtotal=sumtotal+Totales>
        <cfset sumcambios=sumcambios+Cambios>
        <cfset sumInicial=sumInicial+Plantilla_inicial>
        </cfoutput>
        <cfquery name="qryTotales" dbtype="query">
SELECT SUM(totales) as sumtotal, sum(Altas) as sumaltas, sum(Cambios) as sumcambios, sum(Bajas) as sumbajas, sum(Plantilla_inicial) as sumInicial, Semana_ano_id
FROM         qryDatos
GROUP BY Semana_ano_id
ORDER BY Semana_ano_id
</cfquery> 
        <cfoutput>
        	<cfset totalaltas=0>
            <cfset totalbajas=0>
            <cfset inicial=qryTotales.sumInicial>
        	<tr bgcolor="#color#">
            	<td align="right">Totales</td>
                <cfloop query="qryTotales">
        			<cfif (sumtotal+sumInicial) eq 0>
						<cfset sumIPR=0>
					<cfelse>
						<cfset sumIPR=(((sumaltas+sumbajas)/2)*100)/((sumtotal+sumInicial)/2)>
					</cfif>
                <td>#sumtotal#</td>
                <td>#sumaltas#</td>
                <td>#sumbajas#</td>
                <td>#sumcambios#</td>
                <td>#Numberformat(sumIPR,"99.99")#%</td>
                <cfset totalaltas=totalaltas+sumaltas>
           	 	<cfset totalbajas=totalbajas+sumbajas>
                <cfset final=sumtotal>
                </cfloop>
                <cfset TotalIPR=(((totalaltas+totalbajas)/2)*100)/((final+inicial)/2)>
                <td>#Numberformat(TotalIPR,"99.99")#%</td>
            </tr>
        </cfoutput>
      </table>
      </cfif>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>

<!--   Core JS Files   -->
<script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>

</body>
</html>

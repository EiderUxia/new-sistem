<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<link rel="stylesheet" type="text/css" href="default.css" media="screen"/>
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<CFIF Session.tienda neq 0>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no tiene permiso para entrar a esta pantalla");
		window.location="menu_nomina.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif not isdefined("anosel")>
	<cfset anosel=year(now())>
</cfif>
<script language="JavaScript">
function valida(val){
	if (val==1){
	document.forma.valor.value=1;
	document.forma.submit();
	}
}
</script>

<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>

</head>

<body class="g-sidenav-show  bg-gray-100"> 
    
  <!---sidenav.cfm es la barra del lateral izquierdo--->
   <cfinclude template="../sidenav.cfm">
   <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
     <!-- Navbar -->
     <cfinclude template="../navbar.cfm">
     <!-- End Navbar -->

<div align="center">
  <table width="100%" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="100%" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="rep_Vacaciones.cfm">
      <input name="valor" type="hidden" value="0" />
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p>Historial de pagos percepiones y deducciones</p>
            </div></td>
        </tr>		<cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Tienda
                        WHERE Activo=1
                        <cfif Session.tienda neq 0>and Tienda_id=#Session.tienda#</cfif>
						ORDER BY Tienda
				  </cfquery>
                  <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Semana_ano
                        WHERE ano=#anosel#
				  </cfquery>
                  <cfquery name="qryPuestos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT Puesto_id, Puesto, Sueldo_fijo, Encargado, Supervisor
						FROM Puesto
                        WHERE Activo=1
				  </cfquery>
				  
                  <cfquery name="qryDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT *
						FROM Tipo_deduccion
						WHERE Activo=1
				  </cfquery>
				  
                  <cfquery name="qryPercepciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT *
						FROM Tipo_percepcion
						WHERE Activo=1
				  </cfquery>
<cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
SELECT *
FROM Semana_curso
</cfquery>
                  <cfset semanaslist=valuelist(qrySemanas.Semana)>
        <tr>
          <td><div align="right" class="style11"><table border="0" width="100%"><tr>
          <td>Tienda<br /> </td>
          <td>
            <select name="tiendaid">
						<option value="0" >Todas</option>
						<CFOUTPUT query="Tiendas">
                    <option value="#Tienda_id#" <cfif isdefined("tiendaid") and tienda_id eq tiendaid> selected="selected"</cfif>>#Tienda#</option>
                  </CFOUTPUT>
                        </select></td>
          <td>A�o</td><td>
          <select name="anosel" onchange="submit(this);">
          <cfoutput>
          <cfset ano=lsdateformat(now(),"yyyy")>
          <cfset ano2=ano+1>
          <cfset ano=2011>
						<cfloop from="#ano#" to="#ano2#" index="i">
                    <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                  </cfloop></cfoutput>
                        </select>
          </td>
          <td>Semana</td><td>
          <cfif isdefined("semana")>
          	<cfset semanaactual=semana>
          <cfelse>
          	<cfset semanaactual=qrysemanaActual.Semana_ano_id>
          </cfif>
          <select name="semana" multiple="multiple">
          <cfoutput>
          	<option>Seleccionar</option>
          	<option value="0" <cfif ListFindNoCase(semanaactual, 0) > selected="selected"</cfif>>Todas</option>
                        <cfloop query="qrySemanas">
                    <option value="#Semana_ano_id#" <cfif ListFindNoCase(semanaactual, Semana_ano_id) > selected="selected"</cfif>>#Semana#</option>
                  </cfloop></cfoutput>
                        </select>
          </td>
		</tr>
        <tr>
          <td>Puesto<br /> </td>
          <td>
            <select name="puestoid">
						<option value="0" >Todos</option>
						<CFOUTPUT query="qryPuestos">
                    <option value="#Puesto_id#" <cfif isdefined("puestoid") and Puesto_id eq puestoid> selected="selected"</cfif>>#Puesto#</option>
                  </CFOUTPUT>
                        </select></td>
          <td>Deduccion<br /> </td>
          <td>
            <select name="deduccionid" multiple="multiple">
						<option value="10000" <cfif isdefined("deduccionid") and 10000 eq deduccionid> selected="selected"</cfif>>Ninguna</option>
						<option value="0" <cfif isdefined("deduccionid") and 0 eq deduccionid> selected="selected"</cfif>>Todos</option>
						<CFOUTPUT query="qryDeducciones">
                    <option value="#Tipo_deduccion_id#" <cfif isdefined("deduccionid") and Tipo_deduccion_id eq deduccionid> selected="selected"</cfif>>#Tipo_deduccion#</option>
                  </CFOUTPUT>
                        </select></td>
			<td>Percepcion<br /> </td>
          <td>
            <select name="percepcionid" multiple="multiple">
						<option value="10000" <cfif isdefined("percepcionid") and 10000 eq percepcionid> selected="selected"</cfif>>Ninguna</option>
						<option value="0" <cfif isdefined("percepcionid") and 0 eq percepcionid> selected="selected"</cfif>>Todos</option>
						<CFOUTPUT query="qryPercepciones">
                    <option value="#Tipo_percepcion_id#" <cfif isdefined("percepcionid") and Tipo_percepcion_id eq percepcionid> selected="selected"</cfif>>#Tipo_percepcion#</option>
                  </CFOUTPUT>
                        </select></td>
		</table>
            </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="3" cellspacing="1">
              <tr><td>
              <div align="center"><input type="button" onclick="valida(1);void(0);" value="Buscar" /></div>
              </td></tr>
          </table></td>
        </tr>
      </table></form>
      <cfif isdefined("semana")>
<cfquery name="qryDatos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">

SELECT Semana, Ano, Tienda, Nombre, Apellido_pat, Apellido_mat, SUM(Percepcion) as Percepcion, SUM(Deduccion) as Deduccion
FROM (
SELECT S.Semana, S.Ano, T.Tienda, E.Nombre, E.Apellido_pat, E.Apellido_mat,SUM(P.Monto) as Percepcion, 0 as deduccion
FROM Empleado_semana_ano ES
	INNER JOIN Empleado E ON E.Empleado_id=ES.Empleado_id
	INNER JOIN Tienda_semana_ano TS ON TS.Tienda_semana_ano_id=ES.Tienda_semana_ano_id
	INNER JOIN Tienda T ON T.Tienda_id=TS.Tienda_id
	INNER JOIN Semana_ano S ON TS.Semana_ano_id=S.Semana_ano_id
	INNER JOIN Percepcion P ON P.Empleado_id=E.Empleado_id and P.Semana_ano_id=S.Semana_ano_id
WHERE S.ano=#anosel# <cfif semana neq 0>and S.Semana_ano_id in (#Semana#)</cfif>
    	<cfif isdefined("tiendaid") and tiendaid neq 0>and T.tienda_id=#tiendaid#</cfif>
        <cfif isdefined("puestoid") and puestoid neq 0>and E.puesto_id=#puestoid#</cfif>
        <cfif isdefined("percepcionid") and percepcionid neq 0>and P.Tipo_percepcion_id in (#percepcionid#)</cfif>
GROUP BY	S.Semana, S.Ano, T.Tienda, E.Nombre, E.Apellido_pat, E.Apellido_mat
UNION ALL
SELECT S.Semana, S.Ano, T.Tienda, E.Nombre, E.Apellido_pat, E.Apellido_mat,0 as Percepcion, SUM(D.Monto) as deduccion
FROM Empleado_semana_ano ES
	INNER JOIN Empleado E ON E.Empleado_id=ES.Empleado_id
	INNER JOIN Tienda_semana_ano TS ON TS.Tienda_semana_ano_id=ES.Tienda_semana_ano_id
	INNER JOIN Tienda T ON T.Tienda_id=TS.Tienda_id
	INNER JOIN Semana_ano S ON TS.Semana_ano_id=S.Semana_ano_id
	INNER JOIN Deduccion D ON D.Empleado_id=E.Empleado_id and D.Semana_ano_id=S.Semana_ano_id
WHERE S.ano=#anosel# <cfif semana neq 0>and S.Semana_ano_id in (#Semana#)</cfif>
    	<cfif isdefined("tiendaid") and tiendaid neq 0>and T.tienda_id=#tiendaid#</cfif>
        <cfif isdefined("puestoid") and puestoid neq 0>and E.puesto_id=#puestoid#</cfif>
    	<cfif isdefined("deduccionid") and deduccionid neq 0>and D.Tipo_deduccion_id in (#deduccionid#)</cfif>
GROUP BY	S.Semana, S.Ano, T.Tienda, E.Nombre, E.Apellido_pat, E.Apellido_mat) as Sumados
GROUP BY	Semana, Ano, Tienda, Nombre, Apellido_pat, Apellido_mat
ORDER BY	Semana, Ano, Tienda, Nombre, Apellido_pat, Apellido_mat
</cfquery> 
<cfquery dbtype="query" name="qrysemanaslst">
 SELECT Distinct Semana FROM qryDatos
</cfquery>
   
   <cfoutput>
   	<div align="center" class="style11">
            <p>Semana #valuelist(qrysemanaslst.Semana, ", ")# A�o #qryDatos.ano#</p>
            </div>
   </cfoutput>
   
      <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
      	<tr>
        	<td>Semana</td>
        	<td>A�o</td>
            <td>Tienda</td>
        	<td>Empleado</td>
            <td>Deducciones</td>
        	<td>Percepciones</td>
        </tr>
        <cfset color="##CCCCCC">
        <cfset sumtotal=0>
        <cfset sumaltas=0>
        <cfset sumbajas=0>
        <cfset sumcambios=0>
        <cfoutput query="qryDatos">
        
        <tr bgcolor="#color#">
        	<td>#Semana#</td>
            <td>#Ano#</td>
            <td>#Tienda#</td>
            <td>#Nombre# #Apellido_pat# #Apellido_mat#</td>
            <td align="right">#lscurrencyformat(Deduccion)#</td>
            <td align="right">#lscurrencyformat(Percepcion)#</td>
        </tr>
        <cfif color eq "##CCCCCC">
       		<cfset color="##FFFFFF">
        <cfelse>
        	<cfset color="##CCCCCC">
        </cfif>
        
       </cfoutput>
      </table>
      </cfif>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>

<!--   Core JS Files   -->
<script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>

</body>
</html>

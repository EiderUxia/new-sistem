<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panel de control</title>

    <!--- Link para leer boostrap--->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    
    <!--- Tipografia Lato--->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    
    <!--- verificar si tiene sesion iniciada --->
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>

    <!--- En que a�o estamos --->
    <cfif not isdefined("anosel")>
      <cfset anosel=year(now())>
    </cfif>

    <style>
      body{
        font-family: 'Lato', sans-serif;
      }
      .content{
        width: 80vw;
        height: max-content;
        margin:auto;
      }
      .contajus{
        width: 80%;
        margin:auto;
      }
    </style>

    <script language="JavaScript">
      function submit2(){
        document.forma.semana.value=0;
        document.forma.submit();
      }
    </script>

    <!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
<link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
<!-- CSS Files -->
<link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
<!-- Nepcha Analytics (nepcha.com) -->
<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>

  </head>

  <body class="g-sidenav-show  bg-gray-100"> 
    
    <!---sidenav.cfm es la barra del lateral izquierdo--->
     <cfinclude template="../sidenav.cfm">
     <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
       <!-- Navbar -->
       <cfinclude template="../navbar.cfm">
       <!-- End Navbar -->
  
    <!--- Cabecera, logo Erez--->
    <div class="cabecera"><cfinclude template="top.cfm"></div>

    <!--- Todo el contenido del Body menos la cabecera--->
    <div class="content">


      <!--- Renglon con 2 columnas--->
      <div class="row contajus pb-5 pt-4 fs-7">
          <!--- Primera columna -> Botones Regresar y salir del sistema --->
          <div class="col d-flex align-items-center">
            <a href="menu_nomina.cfm" class="btn btn-primary me-3">Regresar</a>
            <a href="logout.cfm" class="btn btn-warning">Salir del sistema</a>
          </div>
          <!--- Segunda columna nombre del usuario--->
          <div class="col">
            <cfoutput>
              <div class="text-center fw-semibold ">Usuario: #Session.nombre#</div>
            </cfoutput>
          </div>
      </div>


      <!--- Mostrar paso en el que vamos --->
      <div align="center" class="pb-5 fw-bold fs-5">
        <p>Panel de control <cfif session.tienda eq 0>(Asistencia)</cfif></p>
      </div>


      <form method="post" name="forma" action="panel_control.cfm">
        <cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT * FROM Tienda
            <cfif Session.tienda neq 0>WHERE Tienda_id=#Session.tienda#</cfif>
            ORDER BY Tienda
        </cfquery>
        <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT semana_ano_id, semana as semanaid FROM Semana_ano 
            WHERE ano=#anosel# 
            Order BY Semana_ano.semana_ano_id desc
        </cfquery>

        <!--- Filtros para seleccionar tienda, a�o y semana--->
        <div class="row pb-3 fs-6">
          <div class="col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">Tienda:</p>
            <select class="form-select" name="tiendaid" onchange="submit2(this);">
              <option value="0" >Seleccionar</option>
              <CFOUTPUT query="Tiendas">
                <option value="#Tienda_id#" <cfif isdefined("tiendaid") and tienda_id eq tiendaid> selected="selected"</cfif>>#Tienda#</option>
              </CFOUTPUT>
            </select>
          </div>
          <div class="col col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">A�o:</p>
            <select class="form-select" name="anosel" onchange="submit(this);">
              <cfoutput>
                <cfset ano=lsdateformat(now(),"yyyy")>
                <cfset ano2=ano+3>
                <cfset ano=2011>
                <cfloop from="#ano#" to="#ano2#" index="i">
                  <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                </cfloop>
              </cfoutput>
            </select>
          </div>
          <div class="col col d-flex align-items-center justify-content-center">
            <p class="m-0 pe-2">Semana:</p>
            <select class="form-select" name="semana" onchange="submit(this);">
              <cfoutput>
                <option value="0" >Seleccionar</option>
                <cfloop query="qrySemanas">
                  <option value="#Semana_ano_id#" <cfif (isdefined("semana") and Semana_ano_id eq semana) > selected="selected"</cfif>>#semanaid#</option>
                </cfloop>
              </cfoutput>
            </select>
          </div>
        </div>

        <table class="table table-hover">
          <cfif isdefined("tiendaid") and tiendaid neq 0 and qrySemanas.recordcount neq 0 and (semana neq 0 and semana neq "")>
            <cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT
              Tienda_semana_ano_id,
              Tienda_id,
              Semana_ano_id
              FROM Tienda_semana_ano 
              WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
            </cfquery>
            <cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT Distinct Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo ,  Puesto, Efectivo_bit, sueldo_base
              FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
              INNER JOIN Empleado_dia_semana_ano ON Empleado_dia_semana_ano.Empleado_id=Empleado.Empleado_id
              WHERE Tienda_semana_ano_id=<cfif qrytiendasemana.recordcount neq 0 >#qrytiendasemana.Tienda_semana_ano_id#<cfelse>0</cfif>
              ORDER BY Nombre
            </cfquery>
            <cfquery name="qrySemanaDias" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT * FROM Dia_Semana_ano
              WHERE Semana_ano_id=#semana#
            </cfquery>
            <cfquery name="qryempleados2" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT Distinct Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo ,  Puesto, Efectivo_bit, sueldo_base
              FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
              INNER JOIN Empleado_dia_semana_ano ON Empleado_dia_semana_ano.Empleado_id=Empleado.Empleado_id
              WHERE Empleado.Tienda_id = #tiendaid# and Empleado.Activo = 1
              ORDER BY Nombre
            </cfquery>
            
            <cfif qryempleados.recordcount neq 0>
              <tr>
                <td align="center" valign="middle"><p>ID</p></td>
                <td align="center" valign="middle"><p>Nombre</p></td>
                <cfset listfecha="">
                <cfoutput query="qrySemanaDias">
                  <td align="center" valign="middle">#DayofWeekAsString(DayOfWeek(dia))#<br /> #LSDateFormat(dia,"dd-mmm-yyyy")#</td>
                  <cfset listfecha="#listfecha#,#LSDateFormat(dia,"yyyy-mm-dd")#">
                </cfoutput>
                <cfset listfecha=Mid(listfecha, 2, 100)>
              </tr>
              <cfset diaC="">
              <cfoutput query="qryempleados">
                <tr>
                    <td align="left" valign="middle">No. #Empleado_id#</td>
                    <td align="left" valign="middle">#Nombre#<br />#Puesto#</td>
                    
                    <cfloop from="1" to="#listlen(listfecha)#" index="X">  
                      <cfset diaC="#ListGetAt(listfecha,X)#">
                      <cfquery name="checkIn" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                      SELECT TOP 1 Empoloyee_id, Checador_id, Checador.TIME
                      FROM Checador
                      WHERE Empoloyee_id='#Empleado_id#' and Date = '#diaC#'
                      ORDER BY Checador_id ASC;
                      </cfquery>
                      <cfquery name="checkOut" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                        SELECT TOP 1 Empoloyee_id, Checador_id, Checador.TIME
                        FROM Checador
                        WHERE Empoloyee_id='#Empleado_id#' and Date = '#diaC#'
                        ORDER BY Checador_id DESC;
                      </cfquery>
                      <td align="center" valign="middle">#checkIn.TIME# /</br> #checkOut.TIME#</td>
                    </cfloop>
                </tr>
              </cfoutput>


              <cfelse>
                <!---<td align="center" valign="middle"> PRIMERO SE DEBE AUTORIZAR LA NOMINA DE LA SEMANA PASADA</td>--->
                <tr>
                  <td align="center" valign="middle"><p>ID</p></td>
                  <td align="center" valign="middle"><p>Nombre</p></td>
                  <cfset listfecha="">
                  <cfoutput query="qrySemanaDias">
                    <td align="center" valign="middle">#DayofWeekAsString(DayOfWeek(dia))#<br /> #LSDateFormat(dia,"dd-mmm-yyyy")#</td>
                    <cfset listfecha="#listfecha#,#LSDateFormat(dia,"yyyy-mm-dd")#">
                  </cfoutput>
                  <cfset listfecha=Mid(listfecha, 2, 100)>
                </tr>
                <cfset diaC="">
                <cfoutput query="qryempleados2">
                  <tr>
                      <td align="left" valign="middle">No. #Empleado_id#</td>
                      <td align="left" valign="middle">#Nombre#<br />#Puesto#</td>
                      
                      <cfloop from="1" to="#listlen(listfecha)#" index="X">  
                        <cfset diaC="#ListGetAt(listfecha,X)#">
                        <cfquery name="checkIn" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                        SELECT TOP 1 Empoloyee_id, Checador_id, Checador.TIME
                        FROM Checador
                        WHERE Empoloyee_id='#Empleado_id#' and Date = '#diaC#'
                        ORDER BY Checador_id ASC;
                        </cfquery>
                        <cfquery name="checkOut" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                          SELECT TOP 1 Empoloyee_id, Checador_id, Checador.TIME
                          FROM Checador
                          WHERE Empoloyee_id='#Empleado_id#' and Date = '#diaC#'
                          ORDER BY Checador_id DESC;
                        </cfquery>
                        <td align="center" valign="middle">#checkIn.TIME# /</br> #checkOut.TIME#</td>
                      </cfloop>
                  </tr>
                </cfoutput>
                
            </cfif>
          </cfif>          
        </table>

      </form>

      <!--   Core JS Files   -->
<script src="/new-sistem/assets/js/core/bootstrap.min.js"></script>
<script src="/new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="/new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Kanban scripts -->
<script src="/new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
<script src="/new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
<script src="/new-sistem/assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
<script src="/new-sistem/assets/js/plugins/tilt.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>

  </body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif isdefined("valor") and valor eq 1>
<cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, tienda.Tienda, 
Semana_ano.Semana, MIN(Dia_semana_ano.Dia) as inicio, MAX(Dia_semana_ano.Dia) as fin, Empleado_semana_ano.Sueldo_pagar
        FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
		INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
		INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id 
		INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
		INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Tienda_semana_ano.Semana_ano_id
		INNER JOIN Dia_semana_ano ON Dia_semana_ano.Semana_ano_id=Semana_ano.Semana_ano_id
        WHERE Empleado_semana_ano.Tienda_semana_ano_id=#semana# and Empleado.activo=1 and Efectivo_bit=1
        GROUP BY Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre , Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat , Sueldo_fijo, tienda.Tienda, 
Semana_ano.Semana, Empleado_semana_ano.Sueldo_pagar
        ORDER BY Nombre
</cfquery>
<cfheader name="Content-Disposition" value="inline; filename=recibos.xls">
<cfcontent type="application/vnd.ms-excel">
<table border="0">
<cfoutput query="qryempleados">
<tr><td colspan="9">&nbsp;</td></tr>
<tr><td colspan="9">&nbsp;</td></tr>
<tr><td colspan="9">&nbsp;</td></tr>
<tr><td colspan="2">&nbsp;</td><td>Tienda:</td><td>#Tienda#</td><td colspan="5">&nbsp;</td></tr>
<tr><td>&nbsp;</td>
<td colspan="8">
<table border="2">
<tr><td colspan="8">
<table>
<tr><td>No. EMP. </td><td colspan="2">_________________</td><td colspan="5">&nbsp;</td></tr>
<tr><td colspan="2">&nbsp;</td>
<td> Nombre </td><td colspan="3"> <u>#busnombre#</u>___________ </td><td colspan="2">&nbsp;</td>
</tr>
<tr><td colspan="8">&nbsp;</td></tr>
<tr>
<td>Semana No.:</td><td>#Semana#</td><td>&nbsp;</td><td colspan="5"><u>SEMANA DEL #LSDateFormat(inicio,"dd-mmm")# al #LSDateFormat(fin,"dd-mmm")#</u></td>
</tr>
<tr><td colspan="8">&nbsp;</td></tr>
<tr>
<td>Sueldo:</td><td colspan="2">#Sueldo_pagar#</td><td colspan="5">&nbsp;</td>
</tr>
<tr><td colspan="3">&nbsp;</td><td colspan="5" align="center">__________________________________</td></tr>
<tr>
<td colspan="3">Recibo provisional de nomina:</td><td colspan="5" align="center"> FIRMA DEL EMPLEADO</td>
</tr>
<tr><td colspan="8">&nbsp;</td></tr>
</table>
</td></tr>
</table>
</td></tr>
<tr><td colspan="9">&nbsp;</td></tr>
<tr><td colspan="9">&nbsp;</td></tr>
</cfoutput>
</table>
</cfcontent>
<cfabort>
</cfif>
<cfif isdefined("valor") and valor eq 2>
<cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, tienda.Tienda, 
Semana_ano.Semana, MIN(Dia_semana_ano.Dia) as inicio, MAX(Dia_semana_ano.Dia) as fin, Empleado_semana_ano.Sueldo_pagar
        FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
		INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
		INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id 
		INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
		INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id=Tienda_semana_ano.Semana_ano_id
		INNER JOIN Dia_semana_ano ON Dia_semana_ano.Semana_ano_id=Semana_ano.Semana_ano_id
        WHERE Empleado_semana_ano.Tienda_semana_ano_id=#semana# and Empleado.activo=1 and Efectivo_bit=1
        GROUP BY Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre , Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat , Sueldo_fijo, tienda.Tienda, 
Semana_ano.Semana, Empleado_semana_ano.Sueldo_pagar
        ORDER BY Nombre
</cfquery>
<cfheader name="Content-Disposition" value="inline; filename=recibosb.xls">
<cfcontent type="application/vnd.ms-excel">
<table border="0">
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6" align="center">Erez de Cach�</td></tr>
<tr><td colspan="6" align="center">Relaci�n de Tarjetas de N�mina</td></tr>
<cfoutput><tr><td colspan="6" align="center">Semana ## #qryempleados.semana# del SEMANA DEL #LSDateFormat(qryempleados.inicio,"dd-mmm")# al #LSDateFormat(qryempleados.fin,"dd-mmm")#</td></tr></cfoutput>
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr>
<td colspan="6">
<table border="1">
<tr><td align="center">No. <br />Tienda</td><td align="center">Forma de <br />Pago</td><td align="center">Nombre</td>
<td align="center">Percepci�n <br />Neta</td><td align="center">Total<br />Tienda</td><td align="center">Observaciones</td></tr>
<cfset totaltienda=0>
<cfoutput query="qryempleados">
<cfset totaltienda=totaltienda+Sueldo_pagar>
<tr><td align="center">#tienda#</td><td align="center">Efectivo</td><td>#busnombre#</td>
<td>#Sueldo_pagar#</td><td align="center"></td><td align="center"></td></tr>
</cfoutput>
<cfoutput><tr><td align="center">#qryempleados.tienda#</td><td align="center">&nbsp;</td><td>&nbsp;</td>
<td>&nbsp;</td><td align="center">#totaltienda#</td><td align="center"></td></tr></cfoutput>
</table>
</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" colspan="3">________________________________________________</td><td colspan="2">&nbsp;</td></tr>
</table>
</cfcontent>
<cfabort>
</cfif>
<cfif not isdefined("anosel")>
	<cfset anosel=year(now())>
</cfif>
<cfif not isdefined("filtro")>
	<cfset filtro="all">
</cfif>
<cfif Session.tienda neq 0><cfset tiendaid=Session.tienda></cfif>
<script language="JavaScript">
function valida(val){
	document.forma.valor.value=val;
	document.forma.submit();
}

function rechaza(){
	if (document.forma.comentariorechaza.value==""){
		alert("Favor de ingresar el motivo del rechazo");
		return false;
	}
	document.forma.valor.value=2;
	document.forma.submit();
}

function submit2(){
	document.forma.semana.value=0;
	document.forma.submit();
}
</script>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
      
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="rep_mov_total.cfm">
      <input name="valor" type="hidden" value="0" />
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p>Imprimir nomina </p>
            </div></td>
        </tr>
                  <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT semana_ano_id, semana as semanaid 
						FROM Semana_ano
                        WHERE ano=#anosel#
				  </cfquery>
        <tr>
          <td><div align="right" class="style11"><table border="0" width="100%"><tr>
          <td>A�o</td><td>
          <select name="anosel" onchange="submit(this);">
          <cfoutput>
          <cfset ano2=lsdateformat(now(),"yyyy")>
          <cfset ano=2011>
						<cfloop from="#ano#" to="#ano2#" index="i">
                    <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                  </cfloop></cfoutput>
                        </select>
          </td>
          <td>Semana</td><td>
          <select name="semana" onchange="submit(this);">
          <cfoutput>
						<option value="0" >Seleccionar</option>
                        <cfloop query="qrySemanas">
                   <option value="#Semana_ano_id#" <cfif (isdefined("semana") and Semana_ano_id eq semana) > selected="selected"</cfif>>#semanaid#</option>
                  </cfloop></cfoutput>
                        </select>
          </td>
          <td>Filtro</td>
          <td> <select name="filtro" onchange="submit(this);">
						<option value="all" <cfif (isdefined("filtro") and "all" eq filtro) > selected="selected"</cfif>>Todas</option>
                        <option value="0" <cfif (isdefined("filtro") and "0" eq filtro) > selected="selected"</cfif>>Tarjeta</option>
                        <option value="1" <cfif (isdefined("filtro") and "1" eq filtro) > selected="selected"</cfif>>Efectivo</option>
                        </select></td>
          </tr></table>
            </div></td>
        </tr>
        <tr>
          <td><table width="756" border="0" cellpadding="3" cellspacing="1">
            <cfif qrySemanas.recordcount neq 0 and isdefined("semana") and semana neq 0>
                  <cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, Empleado_semana_ano.*, Tienda.tienda, Efectivo_bit
        FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
		INNER JOIN Empleado_semana_ano ON Empleado_semana_ano.empleado_id=Empleado.Empleado_id
        INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.Control_id=Empleado_semana_ano.Tienda_semana_ano_id
        INNER JOIN Tienda ON Tienda_semana_ano.Tienda_id=Tienda.tienda_id
        WHERE Tienda_semana_ano.semana_ano_id=#semana# <cfif filtro neq "all">and Efectivo_bit=#filtro#</cfif>
        ORDER BY  Efectivo_bit, Tienda.tienda, Nombre
</cfquery>
<cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                SELECT * FROM Tienda_semana_ano 
                WHERE Control_id=#semana#
            </cfquery>
            <tr>
              <td width="850" valign="top" align="center"><table width="800" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td width="149" align="center" valign="middle"><p>Tienda</p></td>
                  <td width="149" align="center" valign="middle"><p>Nombre</p></td>
                  <td width="149" align="center" valign="middle"><p>Forma de Pago</p></td>
                  <td width="49" align="center" valign="middle"><p>Sueldo base</p></td>
                  <td width="49" align="center" valign="middle"><p>Pares Vendidos</p></td>
                  <td width="49" align="center" valign="middle"><p>Pares descontados</p></td>
                  <td width="49" align="center" valign="middle"><p>Comisi�n por par de zapato</p></td>
                  <td width="49" align="center" valign="middle"><p>Comisi�n por pagar</p></td>
                  <td width="49" align="center" valign="middle"><p>Otras deducciones</p></td>
                  <td width="49" align="center" valign="middle"><p>Otras percepciones</p></td>
                  <td width="49" align="center" valign="middle"><p>Otras comisiones</p></td>
                  <td width="49" align="center" valign="middle"><p>Sueldo a pagar</p></td>
                  </tr>
                  <cfoutput query="qryempleados">
                  <tr>
                  <td align="left" valign="middle" bgcolor="##FFFFFF">#Tienda#</td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF">#Nombre#</td>
				  <td><cfif Efectivo_bit eq 1>Efectivo<cfelse>Tarjeta</cfif></td>
                  <td>#Sueldo_base#</td>
				  <td>#Pares_vendidos#</td>
				  <td>#Pares_descontados#</td>
				  <td>#Comision_x_par#</td>
				  <td>#Comision_x_pagar#</td>
				  <td>#Otras_deducciones#</td>
				  <td>#Otras_percepciones#</td>
				  <td>#Otras_comisiones#</td>
				  <td>#Sueldo_pagar#</td>
                </tr>
                  </cfoutput>
              </table></td>
            </tr></cfif>
          </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="3" cellspacing="1">
              <tr><td>
              <div align="center"><input type="button" onclick="valida(1);void(0);" value="Generar Recibos" /></div>
			   <div align="center"><input type="button" onclick="valida(2);void(0);" value="Generar Caratula" /></div>
              </td></tr>
          </table></td>
        </tr>
      </table></form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="iso-8859-1" />
    <title>Men&uacute;</title>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
    <CFIF session.tipousuario neq 1>
      <script language="JavaScript" type="text/javascript">
        alert("Lo sentimos, no tiene permiso para entrar a esta pantalla");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
    <style>
      .btn-contacto{
        background-color: #374859;
        padding:10px 15px;
        border: #fff 1px solid;
        color: #fff;
        font-size: 12px;
        font-weight: 600;
        text-decoration:none;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius:3px;
        transition:all .5s ease;
      }
      .btn-contacto:hover{
        background-color: #000;
        padding: 10px 20px;
      }
    </style>
    <script language="JavaScript">
      function edad() {
        var l_dt = new Date();
        var Ldt_due= document.frmEntrevista.nacimientofec.value;
        var Ldt_Year,Ldt_Month,Meses_nacido,Meses_hoy,anos,meses
        Ldt_Year = String(Ldt_due).substring(6,10)
        Ldt_Month = parseFloat(String(Ldt_due).substring(3,5));
        Ldt_day = parseFloat(String(Ldt_due).substring(0,2));
        //Set the two dates
        var millennium =new Date(Ldt_Year, Ldt_Month-1, Ldt_day) //Month is 0-11 in JavaScript
        today=new Date()
        //Get 1 day in milliseconds
        var one_day=1000*60*60*24
        //Calculate difference btw the two dates, and convert to days
        fecha2=Math.ceil((today.getTime()-millennium.getTime())/(one_day));
        anos3=fecha2/365;
        dias3=fecha2%365;
        meses3=dias3/30;
        diasf3=dias3%30-2;
        if (meses3==0) {
          if (diasf3==0){
            document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
          }
          else if (anos3>1) {
            document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
          }
          else {
            document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+diasf3+" dias";
          }
        }
        else {
          if (diasf3==0){
            document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
          }
          else if (anos3>1) {
            document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
          }
          else {
            document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+Math.floor(meses3)+" meses "+diasf3+" dias";
          }
        }
      }
      function show(event) {
        if (typeof event == "undefined")
          event = window.event;
        var val = event.keyCode;
        if(val == 13)
          document.forma.submit();
      }
    </script>
   
    <!--- Llenado de datos para las busquedas--->
    <link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
    <!--- Fin del llenado de datos para busqueda --->
    <link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
    <SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
  </head>

  <cfquery name="qryPrestamo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    SELECT *
    FROM Usuario INNER JOIN Tienda ON Tienda.Tienda_id=Usuario.Tienda_id
    WHERE Tipo_usuario_id=8
    Order BY Activo_bit desc, Tienda, Nombre
  </cfquery>

  <body>
    <div align="center">
      <cfsavecontent variable="savesummary">
        <table width="772" border="0" cellpadding="4" cellspacing="4">
          <cfif not isdefined("excel")>
            <tr>
              <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
            </tr>
          </cfif>
          <tr>
            <td bgcolor="#FFFFFF"> 
              <div align="center" class="style7">
                <form method="post" name="frmEntrevista" id="frmEntrevista" action="Entrevista_alta.cfm" onSubmit="return valida(this)">
                  <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                    <cfoutput query="qryPrestamo" group="Activo_bit">
                      <tr>
                        <td>
                          <div align="center" class="style11">
                            <p> Usuarios <cfif Activo_bit eq '1'>Activos<cfelse>In-Activos</cfif></p>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <cfif isdefined("excel")>
                            <table width="756"  border="1" cellpadding="3" cellspacing="1" bordercolor="##000000">
                            <cfelse>
                              <table width="756"  border="0" cellpadding="3" cellspacing="1">
                          </cfif>
                          <tr <cfif isdefined("excel")>bgcolor="##CCCCCC"</cfif>>
                            <td valign="top">Tienda</td>
                            <td  valign="top">Nombre</td>
                            <td  valign="top">Usuario</td>
                            <cfif isdefined("excel")>
                              <td  valign="top">Contrase�a</td>
                            </cfif>
                          </tr>
                          <!--- Aqui estaba --->
                          <cfif isdefined("excel")>
                            <cfset fondo="##FFFFFF">
                            <cfelse>
                              <cfset fondo="##CCCCCC">
                          </cfif>
                          <cfoutput>
                            <tr bgcolor="#fondo#">
                              <td valign="top">#Tienda#</td>
                              <td valign="top">
                              <cfif isdefined("excel")>
                                #Nombre# #Apellido_pat# #Apellido_mat#
                                <cfelse>
                                  <a href="Usuario_alta.cfm?Usuarioid=#Usuario_id#">#Nombre# #Apellido_pat# #Apellido_mat#</a></td>
                              </cfif>
                              <td valign="top">#Usuario#</td>
                              <cfif isdefined("excel")>
                                <td  valign="top">#Contrasena#</td>
                              </cfif>
                            </tr>
                            <cfif isdefined("excel")>
                              <cfset fondo="##FFFFFF">
                              <cfelse>
                                <cfif fondo eq "##CCCCCC">
                                  <cfset fondo="##FFFFFF">
                                  <cfelse>
                                    <cfset fondo="##CCCCCC">
                                </cfif>
                            </cfif>
                          </cfoutput>
                          <!--- Table  135 o 137--->
                            </table>
                            </form>
                            </div>
                        </td>
                      </tr>
                    </cfoutput>
                  </table><br />
                </cfsavecontent>
                <cfif not isdefined("excel")>
                  <cfoutput>#savesummary#</cfoutput>
                  <cfelse>
                    <cffile action="WRITE" file="C:\inetpub\ereztienda\usuarios.xls" output="#savesummary#" >
                    <cfheader name="Content-Type" value="unknown">
                    <cfheader name="Content-Disposition" value="attachment; filename=usuarios.xls">
                    <cfcontent type="application/Unknown" deletefile="yes" file="C:\inetpub\ereztienda\usuarios.xls">
                </cfif>
                <br />
                <div align="center">
                  <a href="Usuarios.cfm?excel=1">En Excel</a>
                </div>
                <br />  
                <div align="center"><a href="Usuario_alta.cfm"  class="btn-contacto">Nuevo Usuario</a></div>
                <br />
                <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
                  <tr>
                    <td>
                      <div align="left">
                        <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                          <tr>
                            <td width="58%">
                              <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td width="186" height="26">
                                    <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                      <tr>
                                        <td><a href="menu_nomina.cfm" class="style9">Regresar</a></td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td width="186" height="26">
                                    <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                                      <tr>
                                        <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>                      
                            </td>
                            <td width="42%" bgcolor="#FFFFFF">Usuario: 
                              <cfoutput>
                                <div>#Session.nombre#</div>
                              </cfoutput>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div align="right"></div>
            </td>
          </tr>
        </table>
    </div>
    <!-- jQuery 2.0.2 -->  
      
  </body>
</html>

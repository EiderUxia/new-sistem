<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style>
.btn-contacto{
	background-color: #374859;
	padding:10px 15px;
	border: #fff 1px solid;
	color: #fff;
	font-size: 12px;
	font-weight: 600;
	text-decoration:none;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius:3px;
	transition:all .5s ease;
}
.btn-contacto:hover{
	background-color: #000;
	padding: 10px 20px;
}
</style>
<script language="JavaScript">

	function edad() {
	
			var l_dt = new Date();
		 
var Ldt_due= document.frmEntrevista.nacimientofec.value;

		
		var Ldt_Year,Ldt_Month,Meses_nacido,Meses_hoy,anos,meses
		Ldt_Year = String(Ldt_due).substring(6,10)
		Ldt_Month = parseFloat(String(Ldt_due).substring(3,5));
		Ldt_day = parseFloat(String(Ldt_due).substring(0,2));
		//Set the two dates
		var millennium =new Date(Ldt_Year, Ldt_Month-1, Ldt_day) //Month is 0-11 in JavaScript
		today=new Date()
		//Get 1 day in milliseconds
		var one_day=1000*60*60*24

		//Calculate difference btw the two dates, and convert to days
		fecha2=Math.ceil((today.getTime()-millennium.getTime())/(one_day));
		anos3=fecha2/365;
		dias3=fecha2%365;
		meses3=dias3/30;
		diasf3=dias3%30-2;
		if (meses3==0) {
			if (diasf3==0){
		  document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
			}
			else if (anos3>1) {
				document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os";
			}
			else {
				document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+diasf3+" dias";
			}
		}
		else {
			if (diasf3==0){
		  document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
			}
			else if (anos3>1) {
				document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�os "+Math.floor(meses3)+" meses";
			}
			else {
				document.frmEntrevista.edadanos.value=Math.floor(anos3)+" a�o "+Math.floor(meses3)+" meses "+diasf3+" dias";
			}
		}
}

function show(event) {
		if (typeof event == "undefined")
			event = window.event;
		
		var val = event.keyCode;
		if(val == 13)
			document.forma.submit();
		
	}
</script>

<!--- Llenado de datos para las busquedas--->
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<!--- Fin del llenado de datos para busqueda --->
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
</head>
<cfquery name="qryTienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Distinct Tienda.Tienda_id, Tienda
    FROM Tienda
	WHERE Tienda.Activo=1 <cfif #Session.tienda# neq 0>and Tienda.tienda_id=#Session.tienda#</cfif>
    Order by Tienda
</cfquery>
<cfif isdefined("Prestamoid")>
<cfquery name="qryPrestamo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Prestamo.*, Tienda, Nombre +' '+ apellido_pat +' ' +apellido_mat as Nombre, Ingreso_fecha, Fecha_reingreso, year(Fecha_reingreso) as anioingreso
	FROM Prestamo
    	INNER JOIN Tienda ON Tienda.Tienda_id=Prestamo.tienda_id
        INNER JOIN Empleado ON Empleado.Empleado_id=Prestamo.Empleado_id
    WHERE Prestamo_id=#Prestamoid#
</cfquery>
<cfquery name="qrySemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    Select Semana, Semana_ano_id
	FROM Semana_ano 
	WHERE Semana_ano_id=#qryPrestamo.Semana_ano_id#
</cfquery>
	<cfset semanaid=#qrySemana.semana_ano_id#>
	<cfset semana=#qrySemana.semana#>
	<cfset tiendaid=#qryPrestamo.tienda_id#>
	<cfset tienda=qryPrestamo.tienda>
	<cfset empleado=qryPrestamo.Nombre>
	<cfset observaciones=qryPrestamo.Observaciones>
    <cfset comentarios=qryPrestamo.Comentarios>
	<cfset revisado=qryPrestamo.Revisado_bit>
	<cfset autorizado=qryPrestamo.Autorizado_bit>
	<cfset monto=qryPrestamo.Monto>
	<cfset descuentosemana=qryPrestamo.Descuento_semanal>
	<cfset montoaut=qryPrestamo.Monto_autoriza>
    <cfif qryPrestamo.anioingreso eq 1900>
    	<cfset ingreso=LSDateFormat(qryPrestamo.Ingreso_fecha,"dd-mmm-yyyy")>
    <cfelse>
    	<cfset ingreso=LSDateFormat(qryPrestamo.Fecha_reingreso,"dd-mmm-yyyy")>
    </cfif>
<cfelse>
<cfquery name="qrySemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
    Select Semana, Semana_curso.Semana_ano_id
	FROM Semana_ano INNER JOIN Semana_curso ON Semana_ano.Semana_ano_id=Semana_curso.Semana_ano_id
</cfquery>
<cfif qryTienda.recordcount eq 1>
    <cfquery name="qryEmpleado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Empleado_id, Nombre +' '+Apellido_pat as nombre
        FROM Empleado
        WHERE Activo=1 and Tienda_id=#Session.tienda#
    </cfquery>

</cfif>  
	<cfset Prestamoid=0>
	<cfset semanaid=#qrySemana.semana_ano_id#>
	<cfset semana=#qrySemana.semana#>
	<cfset tiendaid=#qryTienda.tienda_id#>
	<cfset tienda=qryTienda.tienda>
	<cfset empleadoid="">
	<cfset observaciones="">
    <cfset comentarios="">
	<cfset revisado=0>
	<cfset autorizado=0>
	<cfset monto="">
	<cfset descuentosemana="">
	<cfset montoaut="">
</cfif>
<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <cfform method="post" name="frmPrestamo" id="frmPrestamo" action="Entrevista_alta.cfm" onSubmit="return valida(this)">
      <cfoutput><input type="hidden" name="Prestamoid" id="Prestamoid" value="#Prestamoid#" /></cfoutput>
      <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p><cfif Prestamoid neq 0>Revisi�n<cfelse>Alta</cfif> 
            de Pr&eacute;stamo</p>
            </div></td>
        </tr>
      
        <tr>
          <td>
          <table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
                  <td width="127" align="left" valign="middle"><p>Semana</p></td>
                  <td width="236" align="left" valign="middle" bgcolor="#FFFFFF"><label>
                  <cfoutput><input type="hidden" name="semanaid" id="textfield" value="#semanaid#" /><samp id="semanasamp">#semana#</samp></cfoutput>
                  </label></td>
                  <td align="left" valign="middle">*Tienda</td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF">
				  <cfif Prestamoid eq 0>
				  <cfif qryTienda.recordcount eq 1>
                  <cfoutput><input type="hidden" name="tiendaid" id="textfield" value="#tiendaid#" />#tienda#</cfoutput>
                  <cfelse>
                          <cfselect class="required" name="tiendaid" id="tiendaid" query="qryTienda" display="tienda" value="tienda_id" queryposition="below" selected="#tiendaid#">
												<option value=""></option>
											</cfselect>
                  </cfif>
				  <cfelse>
				  	<cfoutput>#tienda#</cfoutput>
				  </cfif>
                  </td>
            </tr>
            <tr><td colspan="2">Empleado</td>
            	<td colspan="2">
				<cfif Prestamoid eq 0>
				<cfif qryTienda.recordcount eq 1>
                  <cfselect class="required" name="empleadoid" id="empleadoid" query="qryEmpleado" display="nombre" value="Empleado_id" queryposition="below" selected="#empleadoid#">
												<option value=""></option>
											</cfselect>
                  <cfelse>
                          <!--- <cfselect class="required" name="empleadoid" id="empleadoid" bind="cfc:/empleado.getempleado({frmPrestamo:tiendaid})" bindonload="yes" display="nombre" value="Empleado_id" queryposition="below" selected="#empleadoid#"> --->
                          <cfselect class="required" name="empleadoid" id="empleadoid" display="nombre" value="Empleado_id" queryposition="below" selected="#empleadoid#">
												<option value=""></option>
											</cfselect>
                  </cfif>
				  <cfelse>
				  <cfoutput>#empleado#</cfoutput>
				  </cfif>
				  </td>
            </tr>
            <cfif Prestamoid eq 0>
                <tr>
                      <td width="127" align="left" valign="middle"><p>Monto</p></td>
                      <td width="236" align="left" valign="middle" bgcolor="#FFFFFF">
                      <input type="text" name="monto" id="monto" value="" /></td>
                      <td align="left" valign="middle">Descuento por semana</td>
                      <td align="left" valign="middle" bgcolor="#FFFFFF">
                      <input type="text" name="descuentosemana" id="descuentosemana" value="" />
                      </td>
                </tr>
            <cfelse>
                <tr>
                      <td width="127" align="left" valign="middle">Monto Solicitado</td>
                      <td width="236" align="left" valign="middle" bgcolor="#FFFFFF"><label>
                      <cfoutput>#monto#</cfoutput>
                      </label></td>
                      <td align="left" valign="middle">Fecha Ingreso</td>
                      <td><cfoutput>#ingreso#</cfoutput></td>
                </tr>
                <tr>
                      <td width="127" align="left" valign="middle"><p>Monto autorizado</p></td>
                      <td width="236" align="left" valign="middle" bgcolor="#FFFFFF"><label>
                      <cfoutput>
                      <cfif Revisado eq 0 and #Session.tienda# eq 0>
                      <input type="text" name="montoaut" id="montoaut" value="#monto#" />
					  <cfelse>
                      <input type="hidden" name="montoaut" id="montoaut" value="#montoaut#" />
                      	#montoaut#
                      </cfif>
					  </cfoutput>
                      </label></td>
                      <td align="left" valign="middle">Descuento por semana</td>
                      <td align="left" valign="middle" bgcolor="#FFFFFF">
                      <label>
                      <cfoutput>
                      <cfif Revisado eq 0 and #Session.tienda# eq 0>
                      <input type="text" name="descuentosemana" id="descuentosemana" value="#descuentosemana#" />
                      <cfelse>
                      <input type="hidden" name="descuentosemana" id="descuentosemana" value="#descuentosemana#" />
                      #descuentosemana#
					  </cfif></cfoutput>
                      </label>
                      </td>
                </tr>
            </cfif>
            </table>
            </td>
        </tr>
        <tr>
          <td><div align="center">
            <table width="497" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
			  <cfif (#Session.tienda# eq 0 and Prestamoid eq 0) or (#Session.tienda# eq 0 and Prestamoid neq 0 and Comentarios eq " ")>
			  	<input type="hidden" value=" " name="comentarios" />
			  <cfelse>
			  <tr>
                <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">Comentarios</td>
              </tr>
              <tr>
                <td align="left" valign="middle"><textarea name="comentarios" cols="80" rows="5" id="comentarios" required><cfoutput>#comentarios#</cfoutput></textarea></td>
              </tr>
			  </cfif>
			   <cfif #Session.tienda# eq 0>
			  <tr>
                <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">Observaciones</td>
              </tr>
              <tr>
                <td align="left" valign="middle"><textarea name="observaciones" cols="80" rows="5" id="observaciones" required><cfoutput>#observaciones#</cfoutput></textarea></td>
              </tr>
			  </cfif>
              <tr>
                <td align="left" valign="middle">
                <cfdiv id="alta">
                	<cfif #Session.tienda# eq 0 and revisado eq 0>
                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                    <tr>
                      <td align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
                            <a href="javascript:void(0);" onclick="valida(1,0);" class="btn-contacto">Guardar</a>
                      </div></td>
                      <td align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
                            <a href="javascript:void(0);" onclick="valida(1,1);" class="btn-contacto">Autorizar</a>
                      </div></td>
					  <td align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
                            <a href="javascript:void(0);" onclick="valida(1,2);" class="btn-contacto">rechazar</a>
                      </div></td>
                      </tr>
                </table>
				<cfelseif Prestamoid eq 0>
                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                    <tr>
                      <td align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
                            <a href="javascript:void(0);" onclick="valida(0,0);" class="btn-contacto">Solicitar</a>
                      </div></td>
                      </tr>
                </table>
                <cfelseif revisado eq 1 and #Session.tienda# neq 0 and autorizado eq 0>
                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                    <tr>
                      <td align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
                            <a href="javascript:void(0);" onclick="valida(0,3);" class="btn-contacto">Aceptar</a>
                      </div></td>
                      <td align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
                            <a href="javascript:void(0);" onclick="valida(0,4);" class="btn-contacto">Rechazar</a>
                      </div></td>
                      </tr>
                </table>
                </cfif><br />
                <cfif #Session.tienda# eq 0>
                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                    <tr>
                      <td align="left" valign="middle" bgcolor="#FFFFFF"><div align="center">
                            <a href="javascript:void(0);" onclick="actualiza();" class="btn-contacto">Actualizar Semana</a>
                      </div></td>
                      </tr>
                </table>
                </cfif>
                </cfdiv>
                </td>
              </tr>
            </table>
          </div></td>
        </tr>
      </table></cfform>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="Prestamos.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
 <!-- jQuery 2.0.2 -->  
    
    <!--- Llenado de datos para las busquedas--->
    <script type="text/javascript" src="scripts/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>
<script type="text/javascript">
		 function valida(revisado, autorizado){
			// ColdFusion.navigate('prestamo_jqry_alta.cfm?crearempleado=0&revisado='+revisado+'&autorizado='+autorizado+'&'+$("#frmPrestamo").serialize(),'alta');
			$("#alta").load('prestamo_jqry_alta.cfm?crearempleado=0&revisado='+revisado+'&autorizado='+autorizado+'&'+$("#frmPrestamo").serialize(),'alta'); 
      return false;
		 }
		  function actualiza(){
      $("#alta").load('prestamo_jqry_alta.cfm?actualizasem=1&'+$("#frmPrestamo").serialize(),'alta'); 
			 return false;
		 }
     
</script>
<!--- Fin del llenado de datos para busqueda --->
</body>
</html>

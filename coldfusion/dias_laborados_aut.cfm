<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Men�</title>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
      <CFABORT>
    </CFIF>
     <!--     Fonts and icons     -->
     <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
     <!-- Nucleo Icons -->
     <link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
     <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
     <!-- Font Awesome Icons -->
     <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
     <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
     <!-- CSS Files -->
     <link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
     <!-- Nepcha Analytics (nepcha.com) -->
     <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
     <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
    <cfif isdefined("valor") and valor eq 4>
      <cftransaction>
        <cfquery name="qrysemanaano" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          SELECT Tienda_semana_ano_id
          FROM Tienda_semana_ano
          WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
        </cfquery>
        <cfquery name="qryUpdsemanaano" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          UPDATE Tienda_semana_ano 
          SET Terminada=0
          WHERE Tienda_semana_ano_id=#qrysemanaano.Tienda_semana_ano_id#
        </cfquery>
        <cfquery name="qryDelsemanaano" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          DELETE Percepcion
          WHERE Tienda_semana_ano_id=#qrysemanaano.Tienda_semana_ano_id#
        </cfquery>
        <cfquery name="qryDelsemanaano" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          DELETE Deduccion
          WHERE Tienda_semana_ano_id=#qrysemanaano.Tienda_semana_ano_id#
        </cfquery>
      </cftransaction>
      <script language="JavaScript">
        alert("El paso se elimin� con �xito");
        window.location="dias_laborados_aut.cfm";
      </script>
      <cfabort>
    </cfif>
    <cfif isdefined("valor") and (valor eq 3)>
      <cfquery name="qrysemanaano" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Tienda_semana_ano_id
        FROM Tienda_semana_ano
        WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
      </cfquery>
      <cfquery name="qrysemanaanoupd" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        UPDATE Empleado_dia_semana_ano
        SET Tienda_semana_ano_id=#qrysemanaano.Tienda_semana_ano_id#,
        Venta_pares=Venta_pares_otra_tienda,
        Venta_pares_otra_tienda=Venta_pares
        WHERE Tienda_semana_ano_id=#tiendasemanaanoid# and empleado_id=#empleadoid#
      </cfquery>
    </cfif>
    <cfif isdefined("valor") and (valor eq 1 or valor eq 2)>
      <cfif isdefined("nuevoempleado")>
        <cfset valor=1><!--- si se va a dar de alta uno nuevo no permite que se autorize la semana--->
      </cfif>
      <cfquery name="qrysemanaano" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Tienda_semana_ano_id
        FROM Tienda_semana_ano
        WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
      </cfquery>
      <cfset tiendasemanaanoid=qrysemanaano.Tienda_semana_ano_id>
      <cfquery name="qryempleadoida" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Empleado_dia_semana_ano_id, Empleado.Empleado_id, Sueldo_base, capacitacion_fecha, Puesto.Sueldo_fijo 
        FROM Empleado_dia_semana_ano INNER JOIN Empleado ON Empleado.Empleado_id=Empleado_dia_semana_ano.Empleado_id
        INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
        INNER JOIN Puesto ON Puesto.puesto_id=Empleado.Puesto_id
        WHERE Empleado_dia_semana_ano.Tienda_semana_ano_id=#tiendasemanaanoid#
        Order BY Empleado.Empleado_id, Empleado_dia_semana_ano_id
      </cfquery>
      <cfquery name="qryultimodiasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT top 1 dia FROM dia_semana_ano
        WHERE Semana_ano_id=#semana#
        Order by Dia desc
      </cfquery>
      <cftransaction>
        <cfif tiendapares eq "">
          <cfset tiendapares=0>
        </cfif>
        <cfif otratiendapares eq "">
          <cfset otratiendapares=0>
        </cfif>
        <cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
          UPDATE Tienda_semana_ano 
          SET Comentario_autorizo='#comentarioaut#',
          <cfif valor eq 2>Autorizo_cont=1,</cfif>
          Autorizo_cont_id=#Session.usuarioid#,
          Pares_descontados=#tiendapares#,
          Pares_agregados=#otratiendapares#
          WHERE Tienda_semana_ano_id=#tiendasemamaid#
        </cfquery>
        <cfoutput query="qryempleadoida" group="Empleado_id">
          <cfif valor eq 2>
            <cfquery name="qrySelVaciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT *
              FROM Vacacion
              WHERE autorizado_bit=1 and procesado_bit=0 and empleado_id=#Empleado_id# and Semana_ano_id=#semana#
            </cfquery>
            <cfif qrySelVaciones.recordcount neq 0>
              <cfif qrySelVaciones.sueldo neq 0 and qrySelVaciones.sueldo neq "">
                <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  INSERT Percepcion (Tipo_percepcion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                  Values (2,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#qrySelVaciones.sueldo#,'#qrySelVaciones.comentarios#', 1, #tiendasemamaid#)
                </cfquery>
              </cfif>
              <cfif qrySelVaciones.prima neq 0 and qrySelVaciones.prima neq "">
                <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  INSERT Percepcion (Tipo_percepcion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                  Values (10,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#qrySelVaciones.prima#,'#qrySelVaciones.comentarios#', 1, #tiendasemamaid#)
                </cfquery>
              </cfif>
              <cfquery name="qryDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                UPDATE Vacacion
                SET procesado_bit=1
                WHERE vacacion_id=#qrySelVaciones.vacacion_id#
              </cfquery>
              <cfquery name="qryDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                UPDATE Empleado
                SET dias_vacaciones=0
                WHERE empleado_id=#Empleado_id#
              </cfquery>
            </cfif>
            <cfquery name="qrySelPerstamos" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT *
              FROM prestamo
              WHERE autorizado_bit=1 and procesado_bit=0 and empleado_id=#Empleado_id# and Semana_ano_id=#semana#
            </cfquery>
            <cfif qrySelPerstamos.recordcount neq 0>
              <cfset siguientesemana=semana+1>
              <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="resprestamo">
                INSERT Deduccion_recurrente (tipo_deduccion_id, Empleado_id, Semana_ano_id, Fecha, Monto_inicial, resto, deduccion_semana, inicio_semana_ano_id, Comentarios, Automatica_bit, Semana_cambio_id)
                Values (4,#Empleado_id#,#semana#,#CreateODBCDate(now())#,#qrySelPerstamos.monto_autoriza#,#qrySelPerstamos.monto_autoriza#,#qrySelPerstamos.descuento_semanal#,#siguientesemana#,'#qrySelVaciones.comentarios#',1, #semana#)
              </cfquery>
              <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                INSERT Percepcion (Tipo_percepcion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                Values (1,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#qrySelPerstamos.monto_autoriza#,#CreateODBCDate(now())#,#resprestamo.Identitycol#,'#qrySelVaciones.comentarios#', 1, #tiendasemamaid#)
              </cfquery>
              <cfquery name="qryDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                UPDATE prestamo
                SET procesado_bit=1
                WHERE prestamo_id=#qrySelPerstamos.prestamo_id#
              </cfquery>
            </cfif>
            <cfquery name="qryAltaDeduccion" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              SELECT *
              FROM Deduccion_recurrente
              WHERE Empleado_id=#Empleado_id# and inicio_semana_ano_id<=#semana# and resto>0
              Order BY Deduccion_recurrente_id DESC
            </cfquery>
            <cfif qryAltaDeduccion.recordcount neq 0>
              <cfloop query="qryAltaDeduccion">
                <cfif deduccion_semana gt resto>
                  <cfset loquesequita=resto>
                  <cfelse>
                    <cfset loquesequita=deduccion_semana>
                </cfif>
                <cftry>
                  <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                    Values (#Tipo_deduccion_id#,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#loquesequita#,<cfif fecha neq "">'#fecha#'<cfelse>NULL</cfif>,#Deduccion_recurrente_id#,'#comentarios#', 1, #tiendasemamaid#)
                  </cfquery>
                  <cfcatch>
                    <script language="JavaScript">
                      alert(#Tipo_deduccion_id#,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#loquesequita#,<cfif fecha neq "">'#fecha#'<cfelse>NULL</cfif>,#Deduccion_recurrente_id#,'#comentarios#', 1, #tiendasemamaid#);
                    </script>
                  </cfcatch>
                </cftry>
                <cfset restos=resto-loquesequita>
                <cfquery name="qryDeduccionesrec" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  UPDATE Deduccion_recurrente
                  SET Resto=#restos#,
                  Semana_cambio_id=#semana#
                  WHERE Deduccion_recurrente_id=#Deduccion_recurrente_id#
                </cfquery>
              </cfloop>
            </cfif>
          </cfif>
          <cfset paresdesc= evaluate("pm#Empleado_id#")>
          <cfset dia1=1>
          <cfoutput>
            <cfset diaasistencia= evaluate("dia#Empleado_dia_semana_ano_id#")>
            <cfif dia1 eq 1>
              <cfset ventapares= evaluate("pd#Empleado_dia_semana_ano_id#")>
              <cfset dia1=2>
              <cfelse>
                <cfset ventapares=0>
            </cfif>
            <cfif ventapares eq "">
              <cfset ventapares=0>
            </cfif>
            <cfif diaasistencia neq "" and diaasistencia neq 0>
              <cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                UPDATE Empleado_dia_semana_ano
                SET Concepto_dia_trabajado_id=#diaasistencia#,
                Pares_descontados=#paresdesc#
                WHERE Empleado_dia_semana_ano_id=#Empleado_dia_semana_ano_id#
              </cfquery>
              <cfif valor eq 2>
                <cfif diaasistencia eq 12 or (diaasistencia eq 14 and capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1) or diaasistencia eq 18 or (diaasistencia eq 19 and capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1)>
                  <cfif (diaasistencia eq 14 or diaasistencia eq 19) and capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1>
                    <cfquery name="qrysueldotienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                      SELECT Monto_capacitacion
                      FROM Tabla_Comision
                      WHERE tienda_id=#tiendaid#
                    </cfquery>
                    <cfset sueldopropdes=Ceiling(qrysueldotienda.Monto_capacitacion/7)>
                    <cfset sueldopropfalta=Fix(qrysueldotienda.Monto_capacitacion/6)>
                    <cfelseif Sueldo_base neq "">
                      <cfset sueldopropdes=Ceiling(Sueldo_base/7)>
                      <cfset sueldopropfalta=Fix(Sueldo_base/6)>
                    <cfelse>
                      <cfset sueldopropdes=0>
                      <cfset sueldopropfalta=0>
                  </cfif>
                  <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    INSERT Percepcion (Tipo_percepcion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                    Values (7,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#sueldopropdes#,
                    <cfif isdefined("fecha") and fecha neq "">'#fecha#'<cfelse>NULL</cfif>,NULL,'Monto pagado por descanso trabajado', 1, #tiendasemamaid#)
                  </cfquery>
                </cfif>
                <cfif diaasistencia eq 10>
                  <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    INSERT Percepcion (Tipo_percepcion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                    Values (11,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#100#,
                    <cfif isdefined("fecha") and fecha neq "">'#fecha#'<cfelse>NULL</cfif>,NULL,'Monto pagado por Incapacidad Pagada', 1, #tiendasemamaid#)
                  </cfquery>
                </cfif>
                <cfif diaasistencia eq 4 or (diaasistencia eq 5 and capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1) or (diaasistencia eq 8 and Sueldo_fijo eq 1) or (diaasistencia eq 8 and capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1 and Sueldo_fijo eq 0)>
                  <cfif Sueldo_fijo eq 0>
                    <cfquery name="qrysueldotienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                      SELECT Monto_capacitacion
                      FROM Tabla_Comision
                      WHERE tienda_id=#tiendaid#
                    </cfquery>
                    <cfset sueldoempleado=qrysueldotienda.Monto_capacitacion>
                    <cfelseif Sueldo_base neq "">
                      <cfset sueldoempleado=Sueldo_base>
                    <cfelse>
                      <cfset sueldoempleado=0>
                  </cfif>
                  <cfset sueldopropdes=Ceiling(sueldoempleado/7)>
                  <cfset sueldopropfalta=Fix(sueldoempleado/6)>
                  <cfquery name="qryselDeduccionessum" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT ISNULL(SUM(Monto),0) as Monto FROM Deduccion
                    WHERE Tipo_deduccion_id in (8,14) and Empleado_id=#Empleado_id# and Tienda_semana_ano_id=#tiendasemamaid#
                  </cfquery>
                  <cfset restodeduccion=sueldoempleado-qryselDeduccionessum.Monto>
                  <cfif restodeduccion gte sueldopropfalta>
                    <cfset deduccionfinal=sueldopropfalta>
                    <cfelse>
                      <cfset deduccionfinal=restodeduccion>
                  </cfif>
                  <cfif deduccionfinal neq 0>
                    <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                      INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                      Values (8,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#deduccionfinal#,
                      <cfif isdefined("fecha") and fecha neq "">'#fecha#'<cfelse>NULL</cfif>,NULL,'Monto descontado por falta', 1, #tiendasemamaid#)
                    </cfquery>
                  </cfif>
                </cfif>
                <cfif diaasistencia eq 17  or (diaasistencia eq 16 and capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1)>
                  <cfif diaasistencia eq 16 and capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1>
                    <cfquery name="qrysueldotienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                      SELECT Monto_capacitacion
                      FROM Tabla_Comision
                      WHERE tienda_id=#tiendaid#
                    </cfquery>
                    <cfset sueldoempleado=qrysueldotienda.Monto_capacitacion>
                    <cfelseif Sueldo_base neq "">
                      <cfset sueldoempleado=Sueldo_base>
                    <cfelse>
                      <cfset sueldoempleado=0>
                  </cfif>
                  <cfset sueldopropdes=Ceiling(sueldoempleado/7)>
                  <cfset sueldopropfalta=Fix(sueldoempleado/6)>
                  <cfquery name="qryselDeduccionessum" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT ISNULL(SUM(Monto),0) as Monto FROM Deduccion
                    WHERE Tipo_deduccion_id in (8,14) and Empleado_id=#Empleado_id# and Tienda_semana_ano_id=#tiendasemamaid#
                  </cfquery>
                  <cfset restodeduccion=sueldoempleado-qryselDeduccionessum.Monto>
                  <cfif restodeduccion gte sueldopropfalta>
                    <cfset deduccionfinal=sueldopropfalta>
                    <cfelse>
                      <cfset deduccionfinal=restodeduccion>
                  </cfif>
                  <cfif deduccionfinal neq 0>
                    <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                      INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                      Values (14,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#deduccionfinal#,
                      <cfif isdefined("fecha") and fecha neq "">'#fecha#'<cfelse>NULL</cfif>,NULL,'Monto descontado porque no habia ingresado', 1, #tiendasemamaid#)
                    </cfquery>
                  </cfif>
                </cfif>
              </cfif>
            </cfif>
          </cfoutput>
          <cfquery name="qrytotalDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT * FROM Deduccion
            WHERE empleado_id=#Empleado_id# and Tipo_deduccion_id in (8,14) and Semana_ano_id=#semana#
          </cfquery>
          <cfif qrytotalDeducciones.recordcount eq 6>
            <cfquery name="qrytotalDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
              DELETE Deduccion
              WHERE empleado_id=#Empleado_id# and Tipo_deduccion_id in (8,14) and Automatica_bit=1 and Semana_ano_id=#semana#
            </cfquery>
            <cfif Sueldo_fijo eq 0>
              <cfquery name="qrysueldotienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                SELECT Monto_capacitacion
                FROM Tabla_Comision
                WHERE tienda_id=#tiendaid#
              </cfquery>
              <cfset sueldoempleado=qrysueldotienda.Monto_capacitacion>
              <cfelseif Sueldo_base neq "">
                <cfset sueldoempleado=Sueldo_base>
              <cfelse>
                <cfset sueldoempleado=0>
            </cfif>
            <cfif sueldoempleado neq 0>
              <cfquery name="qryaltaDeducciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                INSERT Deduccion (Tipo_deduccion_id,Empleado_id,Fecha,Semana_ano_id,Monto,Fecha_movimiento,Deduccion_recurrente_id,Comentaios, Automatica_bit, Tienda_semana_ano_id)
                Values (8,#Empleado_id#,#CreateODBCDate(now())#,#semana#,#sueldoempleado#,
                <cfif isdefined("fecha") and fecha neq "">'#fecha#'<cfelse>NULL</cfif>,NULL,'Monto descontado por falta', 1, #tiendasemamaid#)
              </cfquery>
            </cfif>
          </cfif>
        </cfoutput>
        <!--- primero inserta a los nuevos si es que existe--->
        <cfif isdefined("nuevoempleado")>
          <cfquery name="qrySemanaDias" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT * FROM Dia_Semana_ano
            WHERE Semana_ano_id=#semana#
          </cfquery>
          <cfloop list="#nuevoempleado#" index="Empleadoid">
            <cfset capturapares=0>
            <cfset Ventaparesfin= evaluate("pares#Empleadoid#")>
            <cfif Ventaparesfin eq "">
              <cfset Ventaparesfin=0>
            </cfif>
            <cfset Ventaotrosparesfin= evaluate("otrospares#Empleadoid#")>
            <cfif Ventaotrosparesfin eq "">
              <cfset Ventaotrosparesfin=0>
            </cfif>
            <cfloop query="qrySemanaDias">
              <cfset diaasistencia= evaluate("dia#Dia_Semana_ano_id#0#Empleadoid#")>
              <cfif diaasistencia neq "" and diaasistencia neq 0>
                <cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#" result="ressemanaemp">
                  INSERT INTO Empleado_dia_semana_ano (Empleado_id, Dia_Semana_ano_id, Concepto_dia_trabajado_id, origen_id, Tienda_semana_ano_id)
                  VALUES (#Empleadoid#, #Dia_Semana_ano_id#, #diaasistencia#, 2, #tiendasemamaid#)
                </cfquery>
                <cfquery name="altasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                  Update Empleado_dia_semana_ano
                  SET Control_id=#ressemanaemp.IDENTITYCOL#
                  <cfif capturapares eq 0>,
                    Venta_pares=#Ventaparesfin#,
                    Venta_pares_otra_tienda=#Ventaotrosparesfin#
                  </cfif>
                  WHERE Empleado_dia_semana_ano_id=#ressemanaemp.IDENTITYCOL#
                </cfquery>
                <cfif capturapares eq 0>
                  <cfset capturapares=1>
                </cfif>
              </cfif>
            </cfloop>	  
          </cfloop>			  
        </cfif>
      </cftransaction>
      <script language="JavaScript">
        alert("La semana se capturo con �xito");
      </script>
    </cfif>
    <cfif not isdefined("anosel")>
      <cfset anosel=year(now())>
    </cfif>
    <script language="JavaScript">
      function valida(val){
        if (document.forma.comentarioaut.value==""){
          alert("Favor de ingresar el comentario");
          return false;
        }
        document.forma.valor.value=val;
        document.forma.submit();
      }
      function valida2(val){
        answer = confirm("�Esta seguro que desea volver al paso 3?","si","no");
        if (!answer){
          alert("El paso no se borr�");
          return false;
        }
        document.forma.valor.value=val;
        document.forma.submit();
      }
      function submit2(){
        document.forma.semana.value=0;
        document.forma.submit();
      }
    </script>
  </head>

  <body class="g-sidenav-show  bg-gray-100">
    <!---sidenav.cfm es la barra del lateral izquierdo--->
    <cfinclude template="../sidenav.cfm">
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
      <!-- Navbar -->
      <cfinclude template="../navbar.cfm">
      <!-- End Navbar -->

    <div align="center">
      <table width="972" border="0" cellpadding="4" cellspacing="4">
        <tr>
          <td width="960" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div align="center" class="style7">

              <form method="post" name="forma" action="dias_laborados_aut.cfm">
                <input name="valor" type="hidden" value="0" />
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td>
                      <div align="center" class="style11">
                        <p>D�as  laborados por tienda <cfif session.tienda eq 0><b>(paso4)</b></cfif></p>
                      </div>
                    </td>
                  </tr>
                  <cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                    SELECT * FROM Tienda
                    WHERE Activo=1
                    <cfif Session.tienda neq 0>and Tienda_id=#Session.tienda#</cfif>
                    ORDER BY Tienda
                  </cfquery>
                  <cfif isdefined("tiendaid")>
                    <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                      SELECT Semana_ano.semana_ano_id, semana as semanaid, Terminada
                      FROM Semana_ano INNER JOIN Tienda_semana_ano ON Tienda_semana_ano.semana_ano_id=Semana_ano.Semana_ano_id
                      WHERE ano=#anosel# and Tienda_id=#tiendaid# and Tienda_semana_ano.Terminada=1
                      Order BY Semana_ano.semana_ano_id desc
                    </cfquery>
                    <cfelse>
                      <cfquery name="qrySemanas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                        SELECT semana_ano_id, semana as semanaid FROM Semana_ano 
                        WHERE ano=#anosel# 
                        Order BY semana_ano_id desc
                      </cfquery>
                  </cfif>
                  <tr>
                    <td>
                      <div align="right" class="style11">
                        <table border="0" width="100%">
                          <tr>
                            <td>Tienda</td>
                            <td>
                              <select name="tiendaid" onchange="submit2(this);">
                                <option value="0" >Seleccionar</option>
                                <CFOUTPUT query="Tiendas">
                                  <option value="#Tienda_id#" <cfif isdefined("tiendaid") and tienda_id eq tiendaid> selected="selected"</cfif>>#Tienda#</option>
                                </CFOUTPUT>
                              </select>
                            </td>
                            <td>A�o</td>
                            <td>
                              <select name="anosel" onchange="submit(this);">
                                <cfoutput>
                                  <cfset ano=lsdateformat(now(),"yyyy")>
                                  <cfset ano2=ano+3>
                                  <cfset ano=2011>
                                  <cfloop from="#ano#" to="#ano2#" index="i">
                                    <option value="#i#" <cfif (isdefined("anosel") and i eq anosel) or (not isdefined("anosel") and i eq ano)> selected="selected"</cfif>>#i#</option>
                                  </cfloop>
                                </cfoutput>
                              </select>
                            </td>
                            <td>Semana</td>
                            <td>
                              <select name="semana" onchange="submit(this);">
                                <cfoutput>
                                  <option value="0" >Seleccionar</option>
                                  <cfloop query="qrySemanas">
                                    <option value="#Semana_ano_id#" <cfif (isdefined("semana") and Semana_ano_id eq semana) > selected="selected"</cfif>>#semanaid#</option>
                                  </cfloop>
                                </cfoutput>
                              </select>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table width="956" border="0" cellpadding="3" cellspacing="1">
                        <cfif isdefined("tiendaid") and tiendaid neq 0 and qrySemanas.recordcount neq 0 and semana neq 0>
                          <cfquery name="qrytiendasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                              SELECT * FROM Tienda_semana_ano 
                              WHERE Tienda_id=#tiendaid# and Semana_ano_id=#semana#
                          </cfquery>
                          <cfquery name="qryempleados" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT Distinct Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo, capacitacion_fecha, Empleado.sueldo_base, Puesto 
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            INNER JOIN Empleado_dia_semana_ano ON Empleado_dia_semana_ano.Empleado_id=Empleado.Empleado_id
                            WHERE Tienda_semana_ano_id=#qrytiendasemana.Tienda_semana_ano_id#
                            ORDER BY Nombre
                          </cfquery>
                          <cfif qryempleados.recordcount neq 0>
                            <cfset empleadoslist=valuelist(qryempleados.Empleado_id)>
                            <cfelse>
                              <cfset empleadoslist=0>
                          </cfif>
                          <cfquery name="qryempleados2" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT Distinct Empleado.Empleado_id,  Apellido_pat+' '+Apellido_mat+' '+Nombre as NOmbre, Nombre +' '+ Apellido_Pat + ' '+ Apellido_Mat as busnombre, Sueldo_fijo 
                            FROM Empleado INNER JOIN Puesto ON Puesto.Puesto_id=Empleado.Puesto_id
                            WHERE not Empleado_id in (#empleadoslist#) and Empleado.tienda_id=#tiendaid# and empleado.activo=1
                            ORDER BY Nombre
                          </cfquery>
                          <cfquery name="qrySemanaDias" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT * FROM Dia_Semana_ano
                            WHERE Semana_ano_id=#semana#
                          </cfquery>
                          <cfquery name="qryultimodiasemana" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT top 1 dia FROM dia_semana_ano
                            WHERE Semana_ano_id=#semana#
                            Order by Dia desc
                          </cfquery>
                          <cfquery name="qrytablacomision" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                            SELECT * FROM Tabla_Comision
                            WHERE Tienda_id=#tiendaid#
                          </cfquery>
                          <tr>
                            <td width="950" valign="top" align="center">
                              <table width="900" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                                <tr>
                                  <td width="142" align="center" valign="middle"><p>Nombre</p></td>
                                  <cfoutput query="qrySemanaDias">
                                    <td width="73" align="center">#DayofWeekAsString(DayOfWeek(dia))#<br /> #LSDateFormat(dia,"dd-mmm-yyyy")#</td>
                                  </cfoutput>
                                  <td width="68" align="center" valign="middle"><p>Total pares vendidos  + otras tiendas+bolsas</p></td>
                                  <td width="74" align="center" valign="middle"><p>Pares descontados</p></td>
                                </tr>
                                <cfquery name="qryconceptosfijo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                  SELECT * FROM Concepto_dia_trabajado
                                  WHERE Sueldo_fijo=1 and Activo=1 <cfif Session.tienda neq 0>and bloqueado=0</cfif>
                                  Order BY Concepto_dia_trabajado
                                </cfquery>
                                <cfquery name="qryconceptosvariable" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                  SELECT * FROM Concepto_dia_trabajado
                                  WHERE sueldo_variable=1 and Activo=1 <cfif Session.tienda neq 0>and bloqueado=0</cfif>
                                  Order BY Concepto_dia_trabajado
                                </cfquery>
                                <cfset parestotaltienda=0>
                                <cfset bolsastotaltienda=0>
                                <cfset j=0>
                                <cfoutput query="qryempleados">
                                  <cfif sueldo_base neq "">
                                    <cfset sueldodias=sueldo_base>
                                    <cfelse>
                                      <cfset sueldodias=0>
                                  </cfif>
                                  <cfset marca="">
                                  <cfif capacitacion_fecha neq "" and datediff("d",qryultimodiasemana.dia,capacitacion_fecha) gt 1 and sueldodias eq 0>
                                    <cfset marca="*">
                                  </cfif>
                                  <cfset j=j+1>
                                  <tr>
                                    <td align="left" valign="middle" bgcolor="##FFFFFF">No. #j# #Nombre#<br />#marca# #puesto#</td>
                                    <cfset tiposueldo=Sueldo_fijo>
                                    <cfset empleadoid=Empleado_id>
                                    <cfset parestotal=0>
                                    <cfset otrosparestotal=0>
                                    <cfset bolsastotal=0>
                                    <cfset dia1=1>
                                    <cfloop query="qrySemanaDias">
                                      <cfquery name="qrytrabajo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                        SELECT Empleado_dia_semana_ano.Concepto_dia_trabajado_id, Empleado_dia_semana_ano_id, Concepto_dia_trabajado, Venta_pares, Venta_pares_otra_tienda, pares_descontados, Venta_bolsa
                                        FROM Empleado_dia_semana_ano INNER JOIN Concepto_dia_trabajado ON Concepto_dia_trabajado.Concepto_dia_trabajado_id=Empleado_dia_semana_ano.Concepto_dia_trabajado_id
                                        WHERE empleado_id=#Empleadoid# and Dia_Semana_ano_id=#Dia_Semana_ano_id# and Tienda_semana_ano_id=#qrytiendasemana.Tienda_semana_ano_id#
                                      </cfquery>
                                      <td>
                                        <cfif qrytiendasemana.autorizo_cont eq 1>
                                          <input type="hidden" name="dia#qrytrabajo.Empleado_dia_semana_ano_id#" value="#qrytrabajo.Concepto_dia_trabajado_id#" />
                                          #qrytrabajo.Concepto_dia_trabajado#
                                          <cfelse>
                                            <select name="dia#qrytrabajo.Empleado_dia_semana_ano_id#" style="width:50px">
                                              <option value="0" >Seleccionar</option>
                                              <cfif tiposueldo eq 1>
                                                <cfloop query="qryconceptosfijo">
                                                  <option value="#Concepto_dia_trabajado_id#" <cfif qrytrabajo.Concepto_dia_trabajado_id eq Concepto_dia_trabajado_id>selected="selected"</cfif>>#Concepto_dia_trabajado#</option>
                                                </cfloop>
                                                <cfelse>
                                                  <cfloop query="qryconceptosvariable">
                                                    <option value="#Concepto_dia_trabajado_id#" <cfif qrytrabajo.Concepto_dia_trabajado_id eq Concepto_dia_trabajado_id>selected="selected"</cfif>>#Concepto_dia_trabajado#</option>
                                                  </cfloop>
                                              </cfif>
                                            </select>
                                        </cfif>
                                        <br/>
                                        <cfif dia1 eq 1>
                                          <input type="hidden" name="pd#qrytrabajo.Empleado_dia_semana_ano_id#" size="5" value="#qrytrabajo.Venta_pares#" />
                                          <cfset dia1=2>
                                        </cfif>
                                        #qrytrabajo.Venta_pares#
                                        <cfif qrytrabajo.Venta_pares_otra_tienda neq 0>+#qrytrabajo.Venta_pares_otra_tienda#</cfif>
                                        <cfif qrytrabajo.venta_pares neq "">
                                          <cfset parestotal=parestotal+qrytrabajo.venta_pares>
                                        </cfif>
                                        <cfset otrosparestotal=otrosparestotal+qrytrabajo.Venta_pares_otra_tienda>
                                        <cfset bolsastotal=bolsastotal+qrytrabajo.Venta_bolsa>
                                      </td>
                                    </cfloop>
                                    <cfset grantotal=#parestotal#+#otrosparestotal#+#bolsastotal#>
                                    <td>#parestotal#<cfif grantotal neq parestotal>+#otrosparestotal#+#bolsastotal# =#grantotal#</cfif></td>
                                    <cfset parestotaltienda=parestotal+parestotaltienda>
                                    <cfset bolsastotaltienda=bolsastotal+bolsastotaltienda>
                                    <td>
                                      <cfif qrytiendasemana.autorizoencargada eq 1>
                                        #qrytrabajo.pares_descontados#
                                        -<input type="hidden" name="pm#Empleado_id#" size="5" value="<cfif tiendaid eq 31>0<cfelse>#qrytrabajo.pares_descontados#</cfif>" />
                                        <cfelse>
                                          -<input type="text" name="pm#Empleado_id#" size="5" value="<cfif tiendaid eq 31>0<cfelse>#qrytrabajo.pares_descontados#</cfif>" />
                                      </cfif>
                                    </td>
                                  </tr>
                                </cfoutput>
                                <!---Empleados no capturados por la tienda--->
                                <cfif qrytiendasemana.autorizo_cont neq 1>
                                  <cfoutput query="qryempleados2">
                                    <cfquery name="qrytrabajootratienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                                      SELECT Distinct Empleado_id, Tienda, Tienda_semana_ano.publicar, Tienda_semana_ano.Tienda_semana_ano_id
                                      FROM Empleado_dia_semana_ano INNER JOIN Dia_Semana_ano ON Dia_Semana_ano.Dia_Semana_ano_id=Empleado_dia_semana_ano.Dia_Semana_ano_id
                                      INNER JOIN Tienda_semana_ano ON (Tienda_semana_ano.Semana_ano_id=Dia_Semana_ano.Semana_ano_id and Empleado_dia_semana_ano.Tienda_semana_ano_id=Tienda_semana_ano.Tienda_semana_ano_id)
                                      INNER JOIN Tienda ON Tienda.Tienda_id=Tienda_semana_ano.Tienda_id
                                      WHERE Dia_semana_ano.Semana_ano_id=#semana# and Tienda_semana_ano.Tienda_id<>#tiendaid# and Empleado_id=#Empleado_id#
                                    </cfquery>
                                    <tr>
                                      <td align="left" valign="middle" bgcolor="##FFFFFF">
                                        <cfif qrytrabajootratienda.recordcount neq 0>
                                          <span class="Estilo1">Capturado en tienda #qrytrabajootratienda.tienda#</span><br />
                                          <cfif qrytiendasemana.autorizo_cont EQ 0>
                                            <cfif qrytrabajootratienda.publicar eq 1>
                                              <span class="Estilo2">La tienda #qrytrabajootratienda.tienda# ya esta publicada</span>
                                              <cfelse>
                                                <a href="dias_laborados_aut.cfm?semana=#semana#&tiendaid=#tiendaid#&Empleadoid=#Empleado_id#&valor=3&tiendasemanaanoid=#qrytrabajootratienda.Tienda_semana_ano_id#">Mover a esta tienda</a>
                                            </cfif>
                                          </cfif>
                                          <cfelse>
                                            <input type="checkbox" name="nuevoempleado" value="#empleado_id#" />
                                        </cfif>
                                        #Nombre#<br />Pares vendidos + otras tiendas + Bolsas
                                      </td>
                                      <cfset tiposueldo=Sueldo_fijo>
                                      <cfset empleadoid=Empleado_id>
                                      <cfset parestotal=0>
                                      <cfset otrosparestotal=0>
                                      <cfset bolsastotal=0>
                                      <cfloop query="qrySemanaDias">
                                        <td>
                                          <select name="dia#Dia_semana_ano_id#0#empleadoid#" style="width:50px">
                                            <option value="0" >Seleccionar</option>
                                            <cfif tiposueldo eq 1>
                                              <cfloop query="qryconceptosfijo">
                                                <option value="#Concepto_dia_trabajado_id#" <CFIF Concepto_dia_trabajado_id eq 1>selected="selected"</CFIF>>#Concepto_dia_trabajado#</option>
                                              </cfloop>
                                              <cfelse>
                                                <cfloop query="qryconceptosvariable">
                                                  <option value="#Concepto_dia_trabajado_id#" <CFIF Concepto_dia_trabajado_id eq 1>selected="selected"</CFIF>>#Concepto_dia_trabajado#</option>
                                                </cfloop>
                                            </cfif>
                                          </select>
                                        </td>
                                      </cfloop>
                                      <td>
                                        <input type="text" name="pares#Empleado_id#" size="5" />+<input type="text" name="otrospares#Empleado_id#" size="5" />+<input type="text" name="bolsas#Empleado_id#" size="5" />
                                      </td>
                                      <td>
                                        -<input type="text" name="pm#Empleado_id#" size="5" value="3" />                   
                                      </td>
                                    </tr>
                                  </cfoutput>
                                </cfif>
                                <tr>
                                  <td rowspan="2" align="left" valign="middle" bgcolor="#FFFFFF">Tienda</td>
                                  <cfoutput>
                                    <td colspan="5">&nbsp;</td><td colspan="3">Pares agregados (Otras vendedoras)</td>
                                    <td>+<input type="text" name="otratiendapares" size="5" <cfif qrytiendasemana.Pares_agregados neq "">value="#qrytiendasemana.Pares_agregados#"</cfif> /></td>
                                  </cfoutput>
                                </tr>
                                <tr>
                                  <cfoutput>
                                    <td colspan="7">&nbsp;</td><td>#parestotaltienda#+#bolsastotaltienda#</td>
                                    <td>-<input type="text" name="tiendapares" size="5" <cfif qrytiendasemana.Pares_descontados neq "">value="#qrytiendasemana.Pares_descontados#"</cfif> /></td>
                                  </cfoutput>                  
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <cfoutput>
                            <input type="hidden" name="tiendasemamaid" value="#qrytiendasemana.Tienda_semana_ano_id#" />
                            <tr><td>Comentarios</td></tr>
                            <tr>
                              <td>
                                <div align="center"><textarea name="comentario" cols="70" rows="3" disabled="disabled">#qrytiendasemana.Comentarios#</textarea></div>
                                <div align="center"><textarea name="comentarioaut" cols="70" rows="3">#qrytiendasemana.Comentario_autorizo#</textarea></div>
                              </td>
                            </tr>
                            <cfif qrytiendasemana.autorizo_cont EQ 0>
                              <tr>
                                <td>
                                  <div align="center"><input type="button" onclick="valida2(4);void(0);" value="Regresar a paso 3" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="valida(1);void(0);" value="Guardar" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="valida(2);void(0);" value="Autorizar" /></div>
                                </td>
                              </tr>
                              <cfelse>
                                <tr>
                                  <td>
                                    <div align="center"><input type="button" onclick="valida(1);void(0);" value="Guardar" /></div>
                                  </td>
                                </tr>
                            </cfif>
                          </cfoutput>
                        </cfif>
                      </table>
                    </td>
                  </tr>
                </table>
              </form>
            </div>
            <div align="right"></div>
          </td>
        </tr>
      </table>
    </div>
  </main>
        <!--   Core JS Files   -->
        <script src="../../../new-sistem/assets/js/core/bootstrap.min.js"></script>
        <script src="../../../new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
        <script src="../../../new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
        <script src="../../new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
        <!-- Kanban scripts -->
        <script src="../../../new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
        <script src="../../../new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
        <script src="../../../new-sistem/assets/js/plugins/chartjs.min.js"></script>
        <!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
        <script src="../../../new-sistem/assets/js/plugins/tilt.min.js"></script>
    
        <!-- Github buttons -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../../new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
        <script>
  </body>
</html>

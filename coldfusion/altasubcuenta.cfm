<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif isdefined("cuentaid")>
<cfquery name="qrySubcuenta" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Subcuenta_id FROM Subcuenta
        WHERE Cuenta=#Cuentaid# and Subcuenta='#Subcuenta#'			
</cfquery>
<cfif qrySubcuenta.recordcount eq 0>
	<script language="JavaScript" type="text/javascript">
		alert("Esta subcuenta ya exite");
		window.location="index.cfm";
	</script>
<CFABORT>
<cftransaction>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Subcuenta (Cuenta, Subcuenta, nombre, tipo_subcuenta_id, naturaleza_id, afectable_id) VALUES
		(#Cuentaid#,'#Subcuenta#', '#nombre#',#tiposubcuentaid#, #Naturalezaid#, #afectableid#)			
</cfquery>
</cftransaction>
<script language="JavaScript" type="text/javascript">
		alert("La subcuenta fue agregado con �xito");
		window.location="altasubcuenta.cfm";
	</script>
</cfif>
<script language="JavaScript">
function valida(){
	if (document.forma.cuentaid.selectedIndex==0){
	  	alert("La cuenta no ha sido seleccionado");
		return false;
	}
	if (document.forma.subcuenta.value==""){
		alert("Favor de ingresar el numero de la subcuenta");
		return false;
	}
	if (!(/^\d{4}$/.test(document.forma.subcuenta.value))){
		alert("Favor de ingresar 4 digitos para la subcuenta");
		return false;
	}
	if (document.forma.nombre.value==""){
		alert("Favor de ingresar el nombre de la subcuenta");
		return false;
	}
	if (document.forma.tiposubcuentaid.selectedIndex==0){
	  	alert("El tipo de subcuenta no ha sido seleccionado");
		return false;
	}
	if (document.forma.naturalezaid.selectedIndex==0){
	  	alert("La naturaleza no ha sido seleccionado");
		return false;
	}
	if (document.forma.afectableid.selectedIndex==0){
	  	alert("Afectable no ha sido seleccionado");
		return false;
	}
	return true;
}
</script>

</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      <form method="post" name="forma" action="altasubcuenta.cfm" onSubmit="return valida(this)">
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p>Alta de Subcuenta</p>
            </div></td>
        </tr>
        
        <tr>
          <td><table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td width="371" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                
				  <cfquery name="Cuentas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Cuenta_contable
						ORDER BY Cuenta
				  </cfquery>
                  <td width="109" align="left" valign="middle"><p>*Cuenta&#13;</p></td>
                  <td width="244" align="left" valign="middle" bgcolor="#FFFFFF"><label>
                     <cfoutput>
                  <select name="cuentaid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Cuentas">
                    <option value="#Cuenta#" >#cuenta#|#Descripcion#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput>
                  </label></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>*Subcuenta</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><label>
                  <input type="text" name="subcuenta" id="textfield2" />
                  </label></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>*Nombre</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><input type="text" name="nombre" id="textfield3" /></td>
                </tr>
              </table></td>
				  <cfquery name="Tiposubcuentas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Tipo_subcuenta
						ORDER BY Tipo_subcuenta
				  </cfquery>
                  <cfquery name="Naturalezas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Naturaleza
						ORDER BY Naturaleza
				  </cfquery>
                  <cfquery name="Afectables" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Afectable
						ORDER BY Afectable
				  </cfquery>
              <td width="385" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                <tr>
                  <td width="127" align="left" valign="middle"><p>*Tipo Subcuenta&#13;</p></td>
                  <td width="237" align="left" valign="middle" bgcolor="#FFFFFF">
                  <cfoutput>
                  <select name="tiposubcuentaid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Tiposubcuentas">
                    <option value="#Tipo_subcuenta_id#" >#Tipo_subcuenta#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput>
                        </td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>*Naturaleza</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput>
                  <select name="naturalezaid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="naturalezas">
                    <option value="#Naturaleza_id#" >#Naturaleza#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>*Afectable</p></td>
                  <td align="left" valign="middle" bgcolor="#FFFFFF"><cfoutput>
                  <select name="afectableid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="afectables">
                    <option value="#Afectable_id#" >#Afectable#</option>
                  </CFLOOP>
                        </select>
                        </cfoutput></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="3" cellspacing="1">
              <tr><td>
              <div align="center"><input type="submit" value="Agregar" /></div>
              </td></tr>
          </table></td>
        </tr>
      </table></form>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_catalogos.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

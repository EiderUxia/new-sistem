<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Carta Responsiva</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<CFIF session.tipousuario neq 1>
	<script language="JavaScript" type="text/javascript">
		alert("Lo sentimos, no tiene permiso para entrar a esta pantalla");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style>
.btn-contacto{
	background-color: #374859;
	padding:10px 15px;
	border: #fff 1px solid;
	color: #fff;
	font-size: 12px;
	font-weight: 600;
	text-decoration:none;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius:3px;
	transition:all .5s ease;
}
.btn-contacto:hover{
	background-color: #000;
	padding: 10px 20px;
}
</style>
<script language="JavaScript">

function show(event) {
		if (typeof event == "undefined")
			event = window.event;
		
		var val = event.keyCode;
		if(val == 13)
			document.forma.submit();
		
	}
</script>

<!--- Llenado de datos para las busquedas--->
<link type="text/css" href="styles/autocomplete.css" rel="stylesheet" media="screen" />
<!--- Fin del llenado de datos para busqueda --->
<link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
</head>
<cfsilent>
    <cfquery name="qryTienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Tienda_id, Tienda, '' as Comentarios FROM Tienda
        WHERE Activo=1 <cfif #Session.tienda# neq 0>and tienda_id=#Session.tienda#</cfif>
    </cfquery>
    <cfif isdefined('uid')>
        <cfquery name="qryDetalle" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
			Select UPPER(Nombre + ' ' + Apellido_pat + ' ' + Apellido_Mat) AS NombreCompleto, Usuario, Contrasena, TU.Tipo_Usuario, ISNULL(T.Tienda, 'Sin Asignar') AS TiendaAsignada
			FROM Usuario
			INNER JOIN Tipo_Usuario AS TU ON Usuario.Tipo_Usuario_id=TU.Tipo_Usuario_id
			LEFT JOIN Tienda AS T ON Usuario.Tienda_id=T.Tienda_id
			WHERE Usuario_id=#uid#
        </cfquery>
    </cfif>
</cfsilent>
<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
        <div class="contenido" align="center">
			<cfoutput query="qryDetalle">
				<div align="center">
				<cfsavecontent variable="savesummary">
				<cfif isdefined('uid')>
					<table width="800px">

						<tr align="center">
							<td>CARTA RESPONSIVA DE CUENTA DE ACCESO AL SISTEMA INFORM�TICO DE CAPTURA Y AUTORIZACI�N DE N�MINAS DE ZAPATER�AS EREZ</td>
						</tr>
						<tr align="center">
							<td><strong>CONFIDENCIAL</strong></td>
						</tr>
						<tr>
							<td>&nbsp</td>
						</tr>
						<tr align="right">
							<td>Monterrey, Nuevo Le�n, a #LSDateFormat(now(), 'dd')# de #LSDateFormat(now(), 'mmmm')# de #LSDateFormat(now(), 'yyyy')#</td>
						</tr>
						<tr>
							<td>&nbsp</td>
						</tr>
						<tr>
							<td><strong>#NombreCompleto#</strong></td>
						</tr>
						<tr>
							<td>Para atender sus funciones en lo que respecta al acceso, uso y captura de informaci�n pertinente para el c�lculo de percepciones semanales de los empleados de ZAPATER�AS EREZ, se le ha asignado una clave de usuario y contrase�a en el Sistema de N�minas, la cual es intransferible y usted se compromete a utilizar de manera discreta, responsable y �nicamente para los fines que ha sido elaborada.
								<br />
								<br />
								Sus claves son:
								<br>&nbsp
							</td>
						</tr>
						<tr align="center">
							<td>
							<div>
								<table border="1px">
									<tr>
										<td width="120px" >No. de Tienda</td>
										<td width="120px" >#TiendaAsignada#</td>
									</tr>
									<tr>
										<td>Usuario</td>
										<td background="images/Noise.jpg">#Usuario#</td>
									</tr>
									<tr>
										<td>Contrase�a</td>
										<td background="images/Noise.jpg">#Contrasena#</td>
									</tr>
									<tr>
										<td>Perfil</td>
										<td>#Tipo_Usuario#</td>
									</tr>
								</table>
							</div>
							</td>
						</tr>
						<tr>
							<td>&nbsp<td>
						</tr>
						<tr>
							<td>La direcci�n de acceso al sistema de N�minas EREZ es: <strong>ereztienda.control-web.com.mx</strong>
							<br />
							<br />
							Al momento de recibir la presente cuenta de acceso, usted est� aceptando la responsabilidad por el uso del sistema de acuerdo a los permisos que se le otorgan en su perfil.<br>&nbsp</td>
						</tr>
						<tr>
							<td align="center">
							<div>
								<table border="1px">
									<thead>
										<th width="180px" background-color="##CCCCCC"><strong>NOMBRE y FIRMA</strong></th>
										<th width="180px" background-color="##CCCCCC"><strong>FUNCI�N</strong></th>
									</thead>
									<tbody>
										<tr height="80px">
											<td>&nbsp;<br />&nbsp;<br />&nbsp;<br /></td>
											<td>&nbsp;<br />&nbsp;<br />&nbsp;<br /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</div>
							</td>
						</tr>
					</table>
				</cfif>
				</cfsavecontent>
				<cfif (not isdefined("pdf"))>
					<cfoutput>#savesummary#</cfoutput>
				<cfelseif isdefined("pdf") and isdefined('uid')>
					<cfdocument format="PDF" marginTop="1" marginLeft=".25" marginRight=".25" marginBottom=".25" pageType="custom" pageWidth="8.5" pageHeight="10.2" overwrite="yes" filename="C:\inetpub\ereztienda\temp\Carta_Responsiva_Usuario_#uid#.pdf" name="mypdf" >
						<cfdocumentitem type="header">
							<div align="center"><cfinclude template="Pdf_top.cfm"></div>
						 </cfdocumentitem>
						<cfoutput>#savesummary#</cfoutput>
					</cfdocument>
					<cfcontent type="application/pdf">
					<cfheader name="Content-Disposition" value="attachment;filename=Carta_Responsiva_Usuario_#uid#.pdf">
					<cfcontent type="application/octet-stream" file="C:\inetpub\ereztienda\temp\Carta_Responsiva_Usuario_#uid#.pdf" deletefile="Yes">
				</cfif>
				</div>
			</cfoutput>
        </div>
        <br />
        <br />
        <br />
        <div class="botones" align="center">
        <table>
            <td align="left" valign="middle" bgcolor="#FFFFFF">
                <div align="center"><a href="javascript:void(0);" onclick="getPDF();" class="btn-contacto">PDF</a></div>
            </td>
            <td align="left" valign="middle" bgcolor="#FFFFFF">
            </td>
        </table>
        </div>
        <br />
        <div class="footer">
		<!--- Footer --->
            <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
                <tr>
                  <td><div align="center">
                    <table border="0" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
                      <tr>
                        <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="186" height="26">
                              <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                              <tr>
                                <td><a href="Usuario_alta.cfm?Usuarioid=<cfoutput>#uid#</cfoutput>" class="style9">Regresar</a></td>
                              </tr>
                            </table></td>
                            <td width="186" height="26">
                              <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                              <tr>
                                <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                              </tr>
                            </table></td>
                          </tr>
                        </table>                      </td>
                        <td width="42%" bgcolor="#FFFFFF">Usuario: 
                          <cfoutput>
                          <div>#Session.nombre#</div></cfoutput></td>
                      </tr>
                    </table>
                  </div></td>
                </tr>
            </table>
        </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
 <!-- jQuery 2.0.2 -->  
    <script type="text/javascript">
 	function getPDF(){
		<cfif isdefined("uid")>
			<cfoutput>window.location="Carta_usuario.cfm?uid=#uid#&pdf=1";</cfoutput>
		</cfif>
	}
 </script> 

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Impresión de facturas</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no está dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style8 {
	font-size: 24px;
	color: #000000;
}
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
</head>
<cfset tiendaid=#Session.tienda#>
	<cfquery name="qryCorreo" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT Retail
        FROM Tienda
        WHERE Tienda_id=#Tiendaid#
    </cfquery>
<body>
<cftransaction>
	<cfif isdefined("archivo")>
	
        <cffile action="Upload"
                        filefield="archivo"
                        destination="C:\inetpub\ereztienda\facturas"
                        nameconflict="MAKEUNIQUE" 
                            mode="757">
        <cfset prodThumbFile=file.ServerFile>
        
        <cffile action = "read" file = "C:\inetpub\ereztienda\facturas\#prodThumbFile#" variable = "Message" charset="utf-8">
        
        <cfset MyXMLDoc= xmlParse(Message)>
            
        <cfquery name="qryFacturaExistente" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT *
            FROM Facturas_Contpaq_Ticket
            WHERE Tienda_id=#Tiendaid# and UU_id='#MyXMLDoc.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes.UUID#'
        </cfquery>
		
        
        <cfif qryFacturaExistente.recordcount eq 0>
        	<!---Inserta el registro--->
            <cfif StructKeyExists(MyXMLDoc.Comprobante.Receptor.XmlAttributes,"nombre")>
    			<cfset nombreReceptor=MyXMLDoc.Comprobante.Receptor.XmlAttributes.nombre>
            <cfelse>
                <cfset nombreReceptor="">
            </cfif>
            <cfset Fecha=left(Replace(MyXMLDoc.Comprobante.XmlAttributes.fecha,"T"," "),19)>
            <cfquery name="qryFacturaExistente" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                INSERT INTO Facturas_Contpaq_Ticket (UU_id, Fecha_Factura, Cliente, monto, RFC, Fecha_Captura, Tienda_id)
                VALUES ('#MyXMLDoc.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes.UUID#', '#Fecha#' , '#nombreReceptor#', #MyXMLDoc.Comprobante.XmlAttributes.total#, '#MyXMLDoc.Comprobante.Receptor.XmlAttributes.rfc#', #createodbcdatetime(now())#, #Tiendaid#)
            </cfquery>
            <cffile action = "copy"
                            destination = "C:\inetpub\ereztienda\facturas\#Tiendaid#\#MyXMLDoc.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes.UUID#.xml"
                            source = "C:\inetpub\ereztienda\facturas\#prodThumbFile#">
        <cfelse>
        	<!---Actualiza la fecha--->
            <cfquery name="qryFacturaExistente" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                UPDATE Facturas_Contpaq_Ticket
                SET Fecha_Captura=#createodbcdatetime(now())#
                WHERE Facturas_Contpaq_Ticket_id=#qryFacturaExistente.Facturas_Contpaq_Ticket_id#
            </cfquery>
            
        </cfif>
	</cfif>
<cfpop server="mail.control-web.com.mx" 
    username="ErezFact#qryCorreo.Retail#@control-web.com.mx" 
    password="Er3zF4ct2017"
    action="GetAll" 
    attachmentpath="C:\inetpub\ereztienda\facturas" 
    name="Sample"> 
   
 <cfset mensajes=valuelist(Sample.MessageNumber)>
    <cfset objSecurity = createObject("java", "java.security.Security") />
	<cfset storeProvider = objSecurity.getProvider("JsafeJCE") />
    <cfset dhKeyAgreement = storeProvider.getProperty("KeyAgreement.DiffieHellman")>
    <!--- dhKeyAgreement=com.rsa.jsafe.provider.JSA_DHKeyAgree --->
    <cfset storeProvider.remove("KeyAgreement.DiffieHellman")>
    Paso 1 Descargar XML
<cfloop query="Sample">
	<cfset atach="">
    <cfset gastobit=1>
    <cfset atach=REReplace(HTMLEditFormat(Sample.AttachmentFiles),"#CHR(9)#",",","ALL")>
    <cfset atach=Replace(atach,"C:\inetpub\ereztienda\facturas\","","ALL")>
    <cfset numero=FindNoCase("<",Sample.From)+1>
    <cfset total=len(Sample.From)-numero>
    <cfset correo=Mid(Sample.From,numero,total)>
    <cfset fecha=left(Sample.Date,25)>
    
    <cfoutput>
    <!---#Sample.Body#<br />
	<br /><br />
    #correo#<br /><br /><br />
    #Sample.Subject#<br />#Fecha#<br />
    #Sample.MESSAGEID#<br />--->
    </cfoutput>

  <cfset arrData = ListToArray(
    Sample.Body,
    "'"
    ) />
	<cfset posicionValorC= FindNoCase("https://www.cfdi.com.mx/MasFacturacion2/DownloadXML.aspx?c=", arrData[1])+59>
	<cfset ValorC=Mid(arrData[1],posicionValorC,36)>
  <cfif ArrayIsDefined(arrData, 10) or ValorC neq "">
		<cfif ArrayIsDefined(arrData, 10)>
			<cfset ligacompleta="#arrData[10]#">
		<cfelse>
			<cfset ligacompleta="https://www.cfdi.com.mx/MasFacturacion2/DownloadXML.aspx?c=#ValorC#">
		</cfif>
        <cfhttp url="#ligacompleta#"  method="get" getAsBinary="yes" path="C:\inetpub\ereztienda\facturas" file="testFile#Tiendaid#.xml" />

            <cffile action = "read" file = "C:\inetpub\ereztienda\facturas\testFile#Tiendaid#.xml" variable = "Message" charset="utf-8">
		<br />
        <cfoutput><a href="#ligacompleta#">descargar XML</a> </cfoutput><br />

        <cfset MyXMLDoc= xmlParse(Message)>
        
        <cfquery name="qryFacturaExistente" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            SELECT *
            FROM Facturas_Contpaq_Ticket
            WHERE Tienda_id=#Tiendaid# and UU_id='#MyXMLDoc.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes.UUID#'
        </cfquery>
		
        
        <cfif qryFacturaExistente.recordcount eq 0>
        	<!---Inserta el registro--->
            <cfif StructKeyExists(MyXMLDoc.Comprobante.Receptor.XmlAttributes,"nombre")>
    			<cfset nombreReceptor=MyXMLDoc.Comprobante.Receptor.XmlAttributes.nombre>
            <cfelse>
                <cfset nombreReceptor="">
            </cfif>
            <cfset Fecha=left(Replace(MyXMLDoc.Comprobante.XmlAttributes.fecha,"T"," "),19)>
            <cfquery name="qryFacturaExistente" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                INSERT INTO Facturas_Contpaq_Ticket (UU_id, Fecha_Factura, Cliente, monto, RFC, Fecha_Captura, Tienda_id)
                VALUES ('#MyXMLDoc.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes.UUID#', '#Fecha#' , '#nombreReceptor#', #MyXMLDoc.Comprobante.XmlAttributes.total#, '#MyXMLDoc.Comprobante.Receptor.XmlAttributes.rfc#', #createodbcdatetime(now())#, #Tiendaid#)
            </cfquery>
            <cffile action = "copy"
                            destination = "C:\inetpub\ereztienda\facturas\#Tiendaid#\#MyXMLDoc.Comprobante.Complemento.TimbreFiscalDigital.XmlAttributes.UUID#.xml"
                            source = "C:\inetpub\ereztienda\facturas\testFile#Tiendaid#.xml">
        <cfelse>
        	<!---Actualiza la fecha--->
            <cfquery name="qryFacturaExistente" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                UPDATE Facturas_Contpaq_Ticket
                SET Fecha_Captura=#createodbcdatetime(now())#
                WHERE Facturas_Contpaq_Ticket_id=#qryFacturaExistente.Facturas_Contpaq_Ticket_id#
            </cfquery>
            
        </cfif>
    </cfif>

</cfloop>
<cfset storeProvider.put("KeyAgreement.DiffieHellman", dhKeyAgreement)>
 <!---   	
<cfpop server="mail.control-web.com.mx" 
    username="ErezFact#qryCorreo.Retail#@control-web.com.mx" 
    password="Er3zF4ct2017"  
    action="Delete" 
    messagenumber="#mensajes#"> --->
</cftransaction>  
Paso 2 Cargar el archivo

<form class="form-horizontal" name="frmcarga" id="frmcarga" role="form" method="post" enctype="multipart/form-data" action="FactruasCorreo.cfm" >
                <div class="row">
                <div class="col-sm-6">
                    <label class="control-label" for="contacto">Subir archivo
                    </label>
                        <cfoutput><input type="file" id="archivo" name="archivo" required="required" class="form-control" ></cfoutput>
                </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-6">
                        <button type="submit" class="btn btn-success" id="guardarbtn">Guardar</button>
                    </div>
                </div>
                </form>  
    <cfquery name="qryFacturaExistente" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT top 20 *
        FROM Facturas_Contpaq_Ticket
        WHERE Tienda_id=#Tiendaid#
        order by Fecha_Captura desc
    </cfquery>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
    
    <font face="Arial, Helvetica, sans-serif" size="+2"><cfoutput>Correo:  ErezFact#qryCorreo.Retail#@control-web.com.mx</cfoutput></font>
    <table width="800" border="1" >
    	<tr style="font:bold">
            <td>Cliente</td>
        	<td>RFC</td>
            <td>Fecha</td>
            <td>Monto</td>
        </tr>
        <cfoutput query="qryFacturaExistente">
            <tr>
                <td>#Cliente#</td>
                <td>#RFC#</td>
                <td>#LSDAteFormat(Fecha_Factura,"dd-mmm-yyyy")#</td>
                <td><a href="FacturasDetalle.cfm?Factid=#Facturas_Contpaq_Ticket_id#" target="_blank">#LSCurrencyformat(monto)#</a></td>
            </tr>
        </cfoutput>
    </table>
    
    <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        
						<td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
                          <tr>
                            <td><a href="menu_nomina.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="img/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>


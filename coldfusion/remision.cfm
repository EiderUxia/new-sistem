<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Men&uacute;</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<cfif isdefined("borrarlst") and borrarlst neq "">
	<cfloop list="#borrarlst#" index="i">
    	<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
            UPDATE recepcion 
            SET oculto_bit=1
            WHERE recepcion_id=#i#
        </cfquery>
    </cfloop>
    <script language="JavaScript" type="text/javascript">
		alert("Las remisiones se borraron con exito");
	</script>
</cfif>
<cfif isdefined("listo")>
<cftransaction>
<cfif isdefined("remisionid") and remisionid neq "">
<cfquery name="remisionlast" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT remision_id FROM Remision
        WHERE remision_id=#remisionid#
	</cfquery>
<cfelse>
<cfquery name="remisionlast" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT remision_id FROM Remision
        WHERE remision='#remision#' and Proveedor_id=#proveedorid#
	</cfquery>
	<cfif remisionlast.recordcount neq 0>
	<script language="JavaScript" type="text/javascript">
		alert("La remision ya existe no puede duplicarse la remisi�n");
		window.location="remision.cfm";
	</script>
	<cfabort>
	</cfif>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Remision (Costo_total, Remision, tienda_id,Proveedor_id) VALUES
		(#montotot#,'#remision#', #tiendaid#, #proveedorid#)			
	</cfquery>
    <cfquery name="remisionlast" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT TOP 1 remision_id FROM Remision
        Order BY remision_id desc			
	</cfquery>
</cfif>
<cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		UPDATE recepcion 
        SET costo_par=#costopar#
        WHERE recepcion_id=#recepcionid#
	</cfquery>
    <cfquery name="Altavacuna" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Remision_recepcion (recepcion_id, Remision_id) VALUES
		(#recepcionid#,#remisionlast.remision_id#)			
	</cfquery>
</cftransaction>
    <script language="JavaScript" type="text/javascript">
		alert("La remision se dio de alta con �xito");
		window.location="remision.cfm";
	</script>
    <cfabort>
<cfelseif isdefined("recepcionid")>
<cfquery name="recepcion" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT nombre_corto, recepcion.*, proveedor.proveedor_id FROM Proveedor INNER JOIN Recepcion ON Recepcion.proveedor_id=Proveedor.proveedor_id
	WHERE recepcion_id=#recepcionid#
</cfquery>
<cfelse>
<cfquery name="recepciones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
	SELECT Proveedor.Proveedor_id, Proveedor.proveedor, nombre_corto, recepcion.* FROM Proveedor INNER JOIN Recepcion ON Recepcion.proveedor_id=Proveedor.proveedor_id
	WHERE recepcion_id>5 and recepcion_id not in (select recepcion_id FROM Remision_recepcion)
	and Recepcion_id >8612 and oculto_bit=0
    ORDER BY Nombre_corto
</cfquery>
<!---
(5644, 5609, 5521, 5516, 5650, 5651, 5643, 5610, 5126, 5652, 5633, 5656, 5657, 5658, 5611, 5683, 5630, 5631, 5215, 5632, 5500, 5612, 5613, 5614, 5327, 5615, 5616, 5617, 5642, 5254, 5255, 5256, 5257, 5618, 5619, 5665, 5666, 5667, 5499, 5606, 5607, 5684, 5435, 5436, 5437, 5438, 5439, 5440, 5441, 5442, 5443, 5539, 5553, 5620, 5670, 5444, 5445, 5621, 5672, 5622, 5623, 5624, 5446, 5447, 5448, 5449, 5309, 5674, 5675, 5450, 5451, 5452, 5625, 5453, 5454, 5174, 5455, 5456, 5626, 5457, 5458, 5459, 5460, 5461, 5462, 5463, 5464, 5627, 5628, 5629, 5680
)

--->
</cfif>
<script language="JavaScript">
function valida(){
	if (document.forma.remisionid.selectedIndex==0){
		if (document.forma.tiendaid.selectedIndex==0){
			alert("Favor de seleccionar la remision � una tienda");
			return false;
		}  	
		if (document.forma.montotot.value==""){
			alert("Favor de seleccionar la remision � ingresar el monto total");
			return false;
		}
		if (document.forma.remision.value==""){
			alert("Favor de seleccionar la remisi�n � ingresar el n�mero de remisi�n");
			return false;
		}
	}
	if (document.forma.costopar.value==""){
		alert("Favor de ingresar el costo por par");
		return false;
	}
	return true;
}

function valida2(){
	return true;	
}
</script>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
.style9 {color: #FFFFFF}
.style11 {font-size: 18px}
-->
</style>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
      
      <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
        <tr>
          <td><div align="center" class="style11">
            <p>Proveedores con remisiones pendientes</p>
            </div></td>
        </tr>
        
        <tr>
          <td>
            <cfif isdefined("recepcionid")><form method="post" name="forma" action="remision.cfm" onSubmit="return valida(this)">
            <cfoutput query="recepcion"><table width="756" border="0" cellpadding="3" cellspacing="1">
            <input type="hidden" name="listo" value="1" />
            <input type="hidden" name="recepcionid" value="#recepcion_id#" />
            <input type="hidden" name="proveedorid" value="#proveedor_id#" />
            <tr>
              <td width="371" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
                <tr>
                  <td width="94" align="left" valign="middle"><p>Proveedor&##13;</p></td>
                  <td width="263" align="left" valign="middle" bgcolor="##FFFFFF"><label>#nombre_corto#</label></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Monto</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><label>#LSCurrencyFormat(monto)#</label></td>
                </tr>
				  <cfquery name="Asociado" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Asociado
						ORDER BY Asociado
				  </cfquery>
                <tr>
                  <td align="left" valign="middle"><p>Recibido por</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF">
						<CFLOOP query="Asociado">
                    <cfif recepcion.asociado_id eq asociado_id>#Asociado#</cfif>
                  </CFLOOP>
                        </td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Cantidad pares</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF">#Pares#</td>
                </tr>
              </table></td>
              <td width="385" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
                <tr>
                  <td width="147" align="left" valign="middle"><p>Recepci�n num. </p></td>
                  <td width="206" align="left" valign="middle" bgcolor="##FFFFFF">#recepcion_num#</td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Fecha </p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF">#LSDateFormat(fecha_recepcion,"dd-mmm-yyyy")#</td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Remisi�n/Factura num.</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF">#remision_num#</td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Costo por par</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="costopar" /></td>
                </tr>
              </table></td>
            </tr>
            <tr><td colspan="2" align="left"><hr color="##0066FF" /></td></tr>
            <tr>
              <td width="371" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
                <cfquery name="remisiones" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT top 10 * FROM Remision
						WHERE proveedor_id=#proveedor_id#
                        Order BY Remision_id desc
				  </cfquery>
                <tr>
                  <td align="left" valign="middle"><p>Agregar a una remision</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><select name="remisionid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="remisiones">
                    <option value="#Remision_id#" >#remision#</option>
                  </CFLOOP>
                        </select></td>
                </tr>
              </table></td>
              <td width="385" valign="top">&nbsp;</td>
            </tr>
            <tr><td colspan="2" align="left"><hr color="##0066FF" /></td></tr>
            <tr>
              <td width="371" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
                <cfquery name="Tiendas" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
						SELECT * FROM Tienda
						ORDER BY Tienda
				  </cfquery>
                <tr>
                  <td align="left" valign="middle"><p>Entidad</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><select name="tiendaid">
						<option value="" >Seleccionar</option>
						<CFLOOP query="Tiendas">
                    <option value="#Tienda_id#"  <cfif Tienda_id eq 25> selected="selected"</cfif> >#Tienda#</option>
                  </CFLOOP>
                        </select></td>
                </tr>
              </table></td>
              <td width="385" valign="top"><table width="378" border="3" cellpadding="3" cellspacing="1" bordercolor="##FFFFFF">
                <tr>
                  <td align="left" valign="middle"><p>Monto total de remision</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="montotot" value="#recepcion.monto#" /></td>
                </tr>
                <tr>
                  <td align="left" valign="middle"><p>Remision</p></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><input type="text" name="remision" value="#recepcion.remision_num#" /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
          <td colspan="2"><table width="100%" border="0" cellpadding="3" cellspacing="1">
              <tr><td>
              <div align="center"><input type="submit" value="Agregar" /></div>
              </td></tr>
          </table></td>
        </tr>
        </table>
            </cfoutput></form>
            <cfelse>
            <form method="post" name="forma" action="remision.cfm" onSubmit="return valida2(this)">
            <table width="756" border="0" cellpadding="3" cellspacing="1">
            <tr>
              <td colspan="2"><table width="478" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" align="center">
                <tr>
                  <td align="center" valign="middle" colspan="4"><p>Recepciones&#13;</p></td></tr>
                  <cfoutput query="recepciones">
                  <tr>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><input type="checkbox" name="borrarlst" value="#recepcion_id#" /><label>#nombre_corto#</label></td>
                  <td align="left" valign="middle" bgcolor="##FFFFFF"><label><a href="remision.cfm?recepcionid=#recepcion_id#">#Recepcion_num#</a></label></td><td>#Remision_num#</td><td>#LSCurrencyFormat(monto)#</td>
                </tr>
                </cfoutput>
              </table></td>
            </tr>
          </table>
          <div align="center"><input type="submit" value="Eliminar" /></div>
          </form>
            </cfif>
          
          </td>
        </tr>
      </table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_recepcion.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
      <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

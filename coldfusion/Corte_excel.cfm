<cfparam name="form.InputExcelFile" default="">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Archivo Cortes</title>
<CFIF not isdefined("session.usuarioid")>
	<script language="JavaScript" type="text/javascript">
		alert("Usted no est� dentro del sistema");
		window.location="index.cfm";
	</script>
<CFABORT>
</CFIF>
<style type="text/css">
<!--
.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; }
.style8 {font-family: Arial, Helvetica, sans-serif;
	font-size: 24px;
	color: #000000;
}
.style9 {font-family: Arial, Helvetica, sans-serif; color: #FFFFFF}
.style11 {font-family: Arial, Helvetica, sans-serif; font-size: 18px}
-->
</style>
<script language="JavaScript">
<!--
function valida(){
	if (document.forma1.InputGeneral.value==""){
		alert("Favor de seleccionar el corte general");
		return false;	
	}
	if (document.forma1.InputEmpresarial.value==""){
		alert("Favor de seleccionar el corte empresarial");
		return false;	
	}
	document.forma1.submit();
}
-->
</script>
</head>

<body>
<div align="center">
  <table width="772" border="0" cellpadding="4" cellspacing="4">
    
    <tr>
      <td width="760" align="center" height="91" valign="middle"><cfinclude template="top.cfm"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="center" class="style7">
  <table width="772" border="3" cellpadding="1" cellspacing="1" bordercolor="#FFFFFF">
    
    <tbody>
	<tr>
	  <td align="center" height="37" valign="middle"><div align="center"><span class="style11">Subir Archivo de Cortes</span></div></td>
	  </tr>
	<tr>
      <td align="center" height="37" valign="middle" width="756">
      <cfif not isdefined("InputGeneral")>
      <table width="500" cellpadding="2" cellspacing="2" border="0" class="tablestandard">
		<cfform action="corte_excel.cfm" name="forma1" method="POST" enctype="multipart/form-data" onSubmit="return valida(this)">
            <tr>
				<td nowrap valign="top" class="style7">Corte General</td>
				<td width="100%" valign="top">
					<input type="File" name="InputGeneral" size="40" class="textfield">
				</td>
			</tr>
            <tr>
				<td nowrap valign="top" class="style7">Corte Empresarial</td>
				<td width="100%" valign="top">
					<input type="File" name="InputEmpresarial" size="40" class="textfield">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="Submit" value="Procesar" class="button"><br>
					<br>			
				</td>
			</tr>
		</cfform>
	</table>
    <cfelse>
		<!--- read operation --->
		<hr size="1">
		
		<!--- define temp excel --->
		<cfset strDir=GetDirectoryFromPath(ExpandPath("*.*")) & "/temp">
		<cfset strInExcel=strDir>
		
		<!--- upload excel1 --->
		<cffile action="Upload"
  					filefield="InputEmpresarial"
  					destination="#strInExcel#"
  					nameconflict="MAKEUNIQUE" 
						mode="757">
		<cfset prodThumbDir=file.ServerDirectory>
		<cfset prodThumbFile=file.ServerFile>
		<cfset prodThumbExt=file.serverfileext>
        <cffile action="Upload"
  					filefield="InputGeneral"
  					destination="#strInExcel#"
  					nameconflict="MAKEUNIQUE" 
						mode="757">
		<cfset prodThumbDir2=file.ServerDirectory>
		<cfset prodThumbFile2=file.ServerFile>
		<cfset prodThumbExt2=file.serverfileext>
		<cfif (prodThumbExt neq "xls") or (prodThumbExt2 neq "xls")>
			Favor de utilizar archivos de Excel solamente
		<cfelse>  
<!--- Create a new instance of the POI utility. --->
<cfset objPOIUtility = CreateObject(
"component",
"POIUtility"
).Init()
/>
 

<!--- Get the path to our Excel document. --->
<cfset strFilePath = "#prodThumbDir#/#prodThumbFile#" />
<cfset strFilePath2 = "#prodThumbDir2#/#prodThumbFile2#" />

<!---
Read the Excel document into an array of Sheet objects.
Each sheet object will contain the data in the Excel
sheet as well as some other property-type information.
--->
<CFSET repetido=0>
<cfset arrSheets = objPOIUtility.ReadExcel(
FilePath = strFilePath,
HasHeaderRow = false
) />
<cfset arrSheets2 = objPOIUtility.ReadExcel(
FilePath = strFilePath2,
HasHeaderRow = false
) />
 <cfoutput>
<cftransaction>
<cfloop
index="intSheet"
from="1"
to="#ArrayLen( arrSheets )#"
step="1">
 

<!---
Let's get a pointer to the current sheet object. We
could continue to refer to the sheet as an index of
the sheets array, but this is more convenient and
breaks it up into easier to read code.
--->
<cfset objSheet = arrSheets[ intSheet ] />
 

<!---
The data from the excel object is stored in a query
within this "sheet" object and can be accessed at the
key "query." For ease of use and short hand, let's
get a pointer to that query.
--->
<cfset qSheetData = objSheet.Query />
 

<!---
We can treat this query just like any old ColdFusion
query because it is just a plain old ColdFusion query.
Let's loop over it to get at each row.
 

NOTE: Since we told the POI Utility to use the first
row as a header row, the first row has already been
stripped off and returned as part of the sheet object.
We will not encounter it in THIS query.
--->
<cfloop query="qSheetData">
 
<cfset fecha = qSheetData.column1 />
<cfset tienda =  qSheetData.column2  />
<cfset pares = qSheetData.column3 />
<cfset venta = qSheetData.column4 />

 <cfif FindNoCase("@",fecha)>
 	El formato del documento no es correcto
 <cfabort>
 </cfif>
 <CFIF fecha NEQ ""> 
    <cfquery name="qrytienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Tienda_id FROM Tienda
		WHERE Retail='#tienda#'
	</cfquery>
    <cfif qrytienda.recordcount eq 0>
    	La tienda #tienda# no existe en el sistema
    <cfabort>
    </cfif>
  <cfquery name="qrycorte" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Corte_Retail_id, Corte_empresarial FROM Corte_Retail
		WHERE Fecha='#LSDateFormat(fecha,"mm/dd/yyyy")#' and tienda_id=#qrytienda.tienda_id#
	</cfquery>
    <CFIF qrycorte.recordcount EQ 0>
    <cfquery name="blah" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Corte_Retail (Fecha, Corte_empresarial, Tienda_id, pares_empresarial)
			VALUES ('#LSDateFormat(fecha,"mm/dd/yyyy")#',#venta#,#qrytienda.tienda_id#,#pares#)
	</cfquery>
    <cfelse>
    <cfquery name="blah" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		UPDATE Corte_Retail
        SET Corte_empresarial=#venta#,
        	pares_empresarial=#pares#
		WHERE Corte_Retail_id=#qrycorte.Corte_Retail_id#
	</cfquery>
    </CFIF>
	</CFIF>
</cfloop>
</cfloop>

<cfloop index="intSheet" from="1" to="#ArrayLen( arrSheets2 )#" step="1">
<cfset objSheet = arrSheets2[ intSheet ] />
<cfset qSheetData = objSheet.Query />
<cfloop query="qSheetData">
<cfset fecha = qSheetData.column1 />
<cfset tienda =  qSheetData.column2  />
<cfset pares = qSheetData.column3 />
<cfset venta = qSheetData.column4 />

 <cfif FindNoCase("@",fecha)>
 	El formato del documento no es correcto
 <cfabort>
 </cfif>
 <CFIF fecha NEQ ""> 
    <cfquery name="qrytienda" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Tienda_id FROM Tienda
		WHERE Retail='#tienda#'
	</cfquery>
    <cfif qrytienda.recordcount eq 0>
    	La tienda #tienda# no existe en el sistema
    <cfabort>
    </cfif>
  <cfquery name="qrycorte" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		SELECT Corte_Retail_id FROM Corte_Retail
		WHERE Fecha='#LSDateFormat(fecha,"mm/dd/yyyy")#' and tienda_id=#qrytienda.tienda_id#
	</cfquery>
    <CFIF qrycorte.recordcount EQ 0>
    <cfquery name="blah" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		INSERT INTO Corte_Retail (Fecha, Corte_General, Tienda_id, pares_general)
			VALUES ('#LSDateFormat(fecha,"mm/dd/yyyy")#',#venta#,#qrytienda.tienda_id#,#pares#)
	</cfquery>
    <cfelse>
    <cfquery name="blah" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
		UPDATE Corte_Retail
        SET Corte_General=#venta#,
        	pares_general=#pares#
		WHERE Corte_Retail_id=#qrycorte.Corte_Retail_id#
	</cfquery>
    </CFIF>
	</CFIF>
</cfloop>
</cfloop>
</cftransaction>
</cfoutput>
Los cortes se capturaron con exito
<a href="corte_excel.cfm">Agregar mas</a>
    </cfif>
    
    </CFIF>
      </td>
    </tr>
  </tbody></table>
          <table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
              <td><div align="left">
                <table width="100%" border="3" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF">
                  <tr>
                    <td width="58%"><table border="0" cellpadding="0" cellspacing="0">
					  <tr>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu_archivos.cfm" class="style9">Regresar</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="menu.cfm" class="style9">Menu</a></td>
                          </tr>
                        </table></td>
                        <td width="186" height="26">
						  <table width="186" height="25" border="0" cellpadding="5" cellspacing="0" background="images/fndobtn.gif">
                          <tr>
                            <td><a href="logout.cfm" class="style9">Salir del sistema</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>                      </td>
                    <td width="42%" bgcolor="#FFFFFF">Usuario: 
                      <cfoutput>
                      <div>#Session.nombre#</div></cfoutput></td>
                  </tr>
                </table>
              </div></td>
            </tr>
          </table>
      </div>
          <div align="right"></div></td>
    </tr>
  </table>
</div>
</body>
</html>

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../../../new-sistem/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../../../new-sistem/assets/img/favicon.png">
    <title>
      EREZ
    </title>
    <CFIF not isdefined("session.usuarioid")>
      <script language="JavaScript" type="text/javascript">
        alert("Usted no est� dentro del sistema");
        window.location="index.cfm";
      </script>
    <CFABORT>
    </CFIF>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
    <!-- Nepcha Analytics (nepcha.com) -->
    <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
    <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
  </head>

  <body class="g-sidenav-show  bg-gray-100">
    <!---sidenav.cfm es la barra del lateral izquierdo--->
    <cfinclude template="sidenav.cfm">
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
      <!-- Navbar -->
      <cfinclude template="navbar.cfm">
      <!-- End Navbar -->
        <!---
        <div class="card card-body blur shadow-blur mx-4 overflow-hidden">
          <div class="row gx-4">
            <div class="col-auto">
              <div class="avatar avatar-xl position-relative">
                <img src="../../../new-sistem/assets/img/bruce-mars.jpg" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
              </div>
            </div>
            <div class="col-auto my-auto">
              <div class="h-100">
                <h5 class="mb-1">
                  Bienvenido
                </h5>
                <p class="mb-0 font-weight-bold text-sm">
                  <cfoutput>#Session.nombre#</cfoutput>
                </p>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
              <div class="nav-wrapper position-relative end-0">
                <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
                  <li class="nav-item">
                    <div>
                      Hoy es: </br> <cfoutput>#dateFormat(now(), 'dd/mm/yyyy')#</cfoutput>
                    </div>
                  </li>
                  <li class="nav-item">
                    <cfquery name="qrysemanaActual" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
                      SELECT Semana
                      FROM Semana_curso INNER JOIN Semana_ano ON Semana_ano.Semana_ano_id = Semana_curso.Semana_ano_id

                      </cfquery>
                    <div>
                      Semana activa: </br>
                      <cfoutput>#qrysemanaActual.semana#</cfoutput>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        --->

      <div class="container-fluid py-2">
        <div class="row mt-1">

          <div class="mb-2 ">
            <div class="card card-auto overflow-scroll max-height-vh-40">
              <div class="card-header pb-0">
                <h6>Anuncios</h6>
              </div>
              <div class="card-body p-3">
                <div class="timeline timeline-one-side" data-timeline-axis-style="dotted">
                  <div class="timeline-block mb-3">
                    <span class="timeline-step">
                      <i class="ni ni-bell-55 text-success text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">$2400, Design changes</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">22 DEC 7:20 PM</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                      <span class="badge badge-sm bg-gradient-success">Design</span>
                    </div>
                  </div>
                  <div class="timeline-block mb-3">
                    <span class="timeline-step">
                      <i class="ni ni-html5 text-danger text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">New order #1832412</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">21 DEC 11 PM</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                      <span class="badge badge-sm bg-gradient-danger">Order</span>
                      <span class="badge badge-sm bg-gradient-danger">#1832412</span>
                    </div>
                  </div>
                  <div class="timeline-block mb-3">
                    <span class="timeline-step">
                      <i class="ni ni-cart text-info text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">Server payments for April</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">21 DEC 9:34 PM</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                      <span class="badge badge-sm bg-gradient-info">Server</span>
                      <span class="badge badge-sm bg-gradient-info">Payments</span>
                    </div>
                  </div>
                  <div class="timeline-block mb-3">
                    <span class="timeline-step">
                      <i class="ni ni-credit-card text-warning text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">New card added for order #4395133</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">20 DEC 2:20 AM</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                      <span class="badge badge-sm bg-gradient-warning">Card</span>
                      <span class="badge badge-sm bg-gradient-warning">#4395133</span>
                      <span class="badge badge-sm bg-gradient-warning">Priority</span>
                    </div>
                  </div>
                  <div class="timeline-block mb-3">
                    <span class="timeline-step">
                      <i class="ni ni-key-25 text-primary text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">Unlock packages for development</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">18 DEC 4:54 AM</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                      <span class="badge badge-sm bg-gradient-primary">Development</span>
                    </div>
                  </div>
                  <div class="timeline-block">
                    <span class="timeline-step">
                      <i class="ni ni-archive-2 text-success text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">New message unread</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">16 DEC</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                      <span class="badge badge-sm bg-gradient-success">Message</span>
                    </div>
                  </div>
                  <div class="timeline-block">
                    <span class="timeline-step">
                      <i class="ni ni-check-bold text-info text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">Notifications unread</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">15 DEC</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                    </div>
                  </div>
                  <div class="timeline-block">
                    <span class="timeline-step">
                      <i class="ni ni-box-2 text-warning text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">New request</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">14 DEC</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                      <span class="badge badge-sm bg-gradient-warning">Request</span>
                      <span class="badge badge-sm bg-gradient-warning">Priority</span>
                    </div>
                  </div>
                  <div class="timeline-block">
                    <span class="timeline-step">
                      <i class="ni ni-controller text-dark text-gradient"></i>
                    </span>
                    <div class="timeline-content">
                      <h6 class="text-dark text-sm font-weight-bold mb-0">Controller issues</h6>
                      <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">13 DEC</p>
                      <p class="text-sm mt-3 mb-2">
                        People care about how you see the world, how you think, what motivates you, what you�??re struggling with or afraid of.
                      </p>
                      <span class="badge badge-sm bg-gradient-dark">Controller</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-12">
            <div class="card">
              <div class="card-header p-3">
                <div class="row">
                  <div class="col-md-6">
                    <h6 class="mb-0">To do list</h6>
                  </div>
                </div>
              </div>
              <div class="card-body p-3 pt-0">
                <ul class="list-group list-group-flush" data-toggle="checklist">
                  <li class="list-group-item border-0 flex-column align-items-start ps-0 py-0 mb-3">
                    <div class="checklist-item checklist-item-primary ps-2 ms-3">
                      <div class="d-flex align-items-center">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                        </div>
                        <h6 class="mb-0 text-dark font-weight-bold text-sm">Check status</h6>
                        <div class="dropstart float-lg-end ms-auto pe-0">
                          <a href="javascript:;" class="cursor-pointer" id="dropdownTable2" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis-h text-secondary" aria-hidden="true"></i>
                          </a>
                          <ul class="dropdown-menu px-2 py-3 ms-sm-n4 ms-n5" aria-labelledby="dropdownTable2" style="">
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Action</a></li>
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Another action</a></li>
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Something else here</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="d-flex align-items-center ms-4 mt-3 ps-1">
                        <div>
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Date</p>
                          <span class="text-xs font-weight-bolder">24 March 2019</span>
                        </div><!---
                        <div class="ms-auto">
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Project</p>
                          <span class="text-xs font-weight-bolder">2414_VR4sf3#</span>
                        </div>
                        <div class="mx-auto">
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Company</p>
                          <span class="text-xs font-weight-bolder">Creative Tim</span>
                        </div>--->
                      </div>
                    </div>
                    <hr class="horizontal dark mt-4 mb-0">
                  </li>
                  <li class="list-group-item border-0 flex-column align-items-start ps-0 py-0 mb-3">
                    <div class="checklist-item checklist-item-dark ps-2 ms-3">
                      <div class="d-flex align-items-center">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault1" checked>
                        </div>
                        <h6 class="mb-0 text-dark font-weight-bold text-sm">Management discussion</h6>
                        <div class="dropstart float-lg-end ms-auto pe-0">
                          <a href="javascript:;" class="cursor-pointer" id="dropdownTable3" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis-h text-secondary" aria-hidden="true"></i>
                          </a>
                          <ul class="dropdown-menu px-2 py-3 ms-sm-n4 ms-n5" aria-labelledby="dropdownTable3" style="">
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Action</a></li>
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Another action</a></li>
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Something else here</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="d-flex align-items-center ms-4 mt-3 ps-1">
                        <div>
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Date</p>
                          <span class="text-xs font-weight-bolder">24 March 2019</span>
                        </div><!---
                        <div class="ms-auto">
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Project</p>
                          <span class="text-xs font-weight-bolder">2414_VR4sf3#</span>
                        </div>
                        <div class="mx-auto">
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Company</p>
                          <span class="text-xs font-weight-bolder">Creative Tim</span>
                        </div>--->
                      </div>
                    </div>
                    <hr class="horizontal dark mt-4 mb-0">
                  </li>
                  <li class="list-group-item border-0 flex-column align-items-start ps-0 py-0 mb-3">
                    <div class="checklist-item checklist-item-warning ps-2 ms-3">
                      <div class="d-flex align-items-center">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault2" checked>
                        </div>
                        <h6 class="mb-0 text-dark font-weight-bold text-sm">New channel distribution</h6>
                        <div class="dropstart float-lg-end ms-auto pe-0">
                          <a href="javascript:;" class="cursor-pointer" id="dropdownTable" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis-h text-secondary" aria-hidden="true"></i>
                          </a>
                          <ul class="dropdown-menu px-2 py-3 ms-sm-n4 ms-n5" aria-labelledby="dropdownTable" style="">
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Action</a></li>
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Another action</a></li>
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Something else here</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="d-flex align-items-center ms-4 mt-3 ps-1">
                        <div>
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Date</p>
                          <span class="text-xs font-weight-bolder">24 March 2019</span>
                        </div><!---
                        <div class="ms-auto">
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Project</p>
                          <span class="text-xs font-weight-bolder">2414_VR4sf3#</span>
                        </div>
                        <div class="mx-auto">
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Company</p>
                          <span class="text-xs font-weight-bolder">Creative Tim</span>
                        </div>--->
                      </div>
                    </div>
                    <hr class="horizontal dark mt-4 mb-0">
                  </li>
                  <li class="list-group-item border-0 flex-column align-items-start ps-0 py-0 mb-3">
                    <div class="checklist-item checklist-item-success ps-2 ms-3">
                      <div class="d-flex align-items-center">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault3">
                        </div>
                        <h6 class="mb-0 text-dark font-weight-bold text-sm">IOS App development</h6>
                        <div class="dropstart float-lg-end ms-auto pe-0">
                          <a href="javascript:;" class="cursor-pointer" id="dropdownTable1" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis-h text-secondary" aria-hidden="true"></i>
                          </a>
                          <ul class="dropdown-menu px-2 py-3 ms-sm-n4 ms-n5" aria-labelledby="dropdownTable1" style="">
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Action</a></li>
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Another action</a></li>
                            <li><a class="dropdown-item border-radius-md" href="javascript:;">Something else here</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="d-flex align-items-center ms-4 mt-3 ps-1">
                        <div>
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Date</p>
                          <span class="text-xs font-weight-bolder">24 March 2019</span>
                        </div><!---
                        <div class="ms-auto">
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Project</p>
                          <span class="text-xs font-weight-bolder">2414_VR4sf3#</span>
                        </div>
                        <div class="mx-auto">
                          <p class="text-xs mb-0 text-secondary font-weight-bold">Company</p>
                          <span class="text-xs font-weight-bolder">Creative Tim</span>
                        </div>--->
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-12">
            <div class="card card-calendar">
              <div class="card-body p-3">
                <div class="calendar" data-bs-toggle="calendar" id="calendar"></div>
              </div>
            </div>


           

          </div>




        </div>
      </div>
    </main>

    <!--   Core JS Files   -->
    <script src="../../../new-sistem/assets/js/core/bootstrap.min.js"></script>
    <script src="../../../new-sistem/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="../../../new-sistem/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script src="../../new-sistem/assets/js/plugins/fullcalendar.min.js"></script>
    <!-- Kanban scripts -->
    <script src="../../../new-sistem/assets/js/plugins/dragula/dragula.min.js"></script>
    <script src="../../../new-sistem/assets/js/plugins/jkanban/jkanban.js"></script>
    <script src="../../../new-sistem/assets/js/plugins/chartjs.min.js"></script>
    <!--  Plugin for TiltJS, full documentation here: https://gijsroge.github.io/tilt.js/ -->
    <script src="../../../new-sistem/assets/js/plugins/tilt.min.js"></script>

    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="../../../new-sistem/assets/js/soft-ui-dashboard.min.js?v=1.1.1"></script>
    <script>
      var calendar = new FullCalendar.Calendar(document.getElementById("calendar"), {
        contentHeight: 'auto',
        initialView: "dayGridMonth",
        headerToolbar: {
          start: 'title', // will normally be on the left. if RTL, will be on the right
          center: '',
          end: 'today prev,next' // will normally be on the right. if RTL, will be on the left
        },
        selectable: true,
        editable: true,
        initialDate: '2020-12-01',
        events: [{
            title: 'Call with Dave',
            start: '2020-11-18',
            end: '2020-11-18',
            className: 'bg-gradient-danger'
          },

          {
            title: 'Lunch meeting',
            start: '2020-11-21',
            end: '2020-11-22',
            className: 'bg-gradient-warning'
          },

          {
            title: 'All day conference',
            start: '2020-11-29',
            end: '2020-11-29',
            className: 'bg-gradient-success'
          },

          {
            title: 'Meeting with Mary',
            start: '2020-12-01',
            end: '2020-12-01',
            className: 'bg-gradient-info'
          },

          {
            title: 'Winter Hackaton',
            start: '2020-12-03',
            end: '2020-12-03',
            className: 'bg-gradient-danger'
          },

          {
            title: 'Digital event',
            start: '2020-12-07',
            end: '2020-12-09',
            className: 'bg-gradient-warning'
          },

          {
            title: 'Marketing event',
            start: '2020-12-10',
            end: '2020-12-10',
            className: 'bg-gradient-primary'
          },

          {
            title: 'Dinner with Family',
            start: '2020-12-19',
            end: '2020-12-19',
            className: 'bg-gradient-danger'
          },

          {
            title: 'Black Friday',
            start: '2020-12-23',
            end: '2020-12-23',
            className: 'bg-gradient-info'
          },

          {
            title: 'Cyber Week',
            start: '2020-12-02',
            end: '2020-12-02',
            className: 'bg-gradient-warning'
          },

        ],
        views: {
          month: {
            titleFormat: {
              month: "long",
              year: "numeric"
            }
          },
          agendaWeek: {
            titleFormat: {
              month: "long",
              year: "numeric",
              day: "numeric"
            }
          },
          agendaDay: {
            titleFormat: {
              month: "short",
              year: "numeric",
              day: "numeric"
            }
          }
        },
      });

      calendar.render();
    </script>
  </body>

</html>
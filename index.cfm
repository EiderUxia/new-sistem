<!DOCTYPE html> 
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../../../new-sistem/assets/img/favicon.png">
    <link rel="icon" type="image/png" href="../../../new-sistem/assets/img/favicon.png">
    <title>
      EREZ
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="../../../new-sistem/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="../../../new-sistem/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="../../../new-sistem/assets/css/soft-ui-dashboard.css?v=1.1.1" rel="stylesheet" />
  
    <!--- Verificar si tiene datos --->
    <CFIF isdefined("form.usuario")>
      <cfquery name="login" datasource="#varDb#" dbtype="odbc" username="#varDbU#" password="#varDbP#">
        SELECT * FROM Usuario WHERE usuario='#form.usuario#' and contrasena='#form.contrasena#' and activo_bit=1
      </cfquery>
      <!--- Si encontr� un registro--->
      <CFIF login.recordcount EQ 1>
        <!--- Guarda los datos en session --->
        <CFSET Session.usuarioid=#login.usuario_id#>
        <CFSET Session.nombre=#login.nombre#>
        <CFSET Session.tipousuario=#login.Tipo_Usuario_id#>
        <CFSET Session.tienda=#login.Tienda_id#>
        <script>
        <CFOUTPUT>alert("Bienvenido(a) #session.nombre#.");</CFOUTPUT>
              window.location="menu.cfm";
        </script>>
        <CFELSE>
        <script language="JavaScript">
          alert("Datos incorrectos o usuario in-activo");
        </script>
      </CFIF>
    </CFIF>

    <!--- Verifica que no falten datos--->
    <script language="JavaScript">

        function valida(){
          var l1 = document.form1.usuario.value;
          var l2 = document.form1.contrasena.value;

          if (l1.length == 0){
            alert(" Ingrese usuario");
            return false;
          }
          if (l2.length == 0){
              alert("Ingrese contrase�a.");
            return false;
          }
          return true;
        }

    </script>
  
  </head>
  
  <body class="">
    <main class="main-content  mt-0">
      <section>
        <div class="page-header min-vh-100">
          <div class="container">
            <div class="row">
              <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
                <div class="card card-plain">
                  <div class="card-header pb-0 text-start">
                    <h4 class="font-weight-bolder">Iniciar Sesi�n</h4>
                    <p class="mb-0">Bienvenido</p>
                  </div>
                  <div class="card-body">
                    <form id="formulario" name="form1" method="post" action="index.cfm" onSubmit="return valida(this)">
                      <div class="mb-3">
                        <input name="usuario" type="text" class="form-control form-control-lg" placeholder="Nombre de usuario" aria-label="Nombre de usuario">
                      </div>
                      <div class="mb-3">
                        <input name="contrasena" type="password" class="form-control form-control-lg" placeholder="Contrase�a" aria-label="Contrase�a">
                      </div>
                      <div class="text-center">
                        <input class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0" type="submit" name="Submit" value="Iniciar sesi�n" />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column">
                <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center">
                  <img src="../../../new-sistem/assets/img/shapes/pattern-lines.svg" alt="pattern-lines" class="position-absolute opacity-4 start-0">
                  <div class="position-relative">
                    <img class="max-width-500 w-100 position-relative z-index-2" src="../../../new-sistem/assets/img/logos/logo-erez-bn.png" alt="chat-img">
                  </div>
                  <h4 class="mt-5 text-white font-weight-bolder">"Hay que exigir a cada uno lo que cada uno puede hacer"</h4>
                  <p class="text-white">El Principito.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>

    <script src="../../../new-sistem/assets/js/core/bootstrap.min.js"></script>
  </body>
  
</html>
